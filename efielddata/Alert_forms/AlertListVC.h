//
//  AlertListVC.h
//  Efield
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
//#import "SWTableViewCell.h"

@interface AlertListVC : ViewController {
    __weak IBOutlet UITableView *alertTable;
  //  __weak IBOutlet UISearchBar *searchbar;
    NSArray *tempArr;
    NSMutableArray *alertList;
    NSMutableArray *checkBoxArr;
    NSInteger index;
    NSDictionary *dummyDic;
    NSMutableDictionary *alertListDic;
    UIRefreshControl *refreshControl;
    __weak IBOutlet UIView *tableFooterView;
    
    __weak IBOutlet UIButton *markreadbutton;
    __weak IBOutlet UIButton *markAllButton;
}
@property (weak, nonatomic) IBOutlet UIButton *editbtn;

@property (weak, nonatomic) IBOutlet UILabel *companynametxt;
 @property (weak, nonatomic) IBOutlet UILabel *date;
//@property (weak, nonatomic) IBOutlet UIButton *editIcon;
//@property(nonatomic, retain)NSString *alertPatientID;
//@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
- (IBAction)logoutAction:(id)sender;
- (IBAction)changepasswordAction:(id)sender;
- (IBAction)showMoreAction:(id)sender;
- (IBAction)editAction:(id)sender;
- (IBAction)markAllAction:(id)sender;
- (IBAction)Specialization:(id)sender;
//- (IBAction)add_patient:(id)sender;
//@property (weak, nonatomic) IBOutlet UIButton *addpatient_button;


- (void)updateData;
@end
