
//  Efield
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertListcell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *workorder_id;
@property (weak, nonatomic) IBOutlet UILabel *AlertMessage;
@property (weak, nonatomic) IBOutlet UILabel *date_lbl;

@property (weak, nonatomic) IBOutlet UILabel *alertfrom;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxBtn;

@end
