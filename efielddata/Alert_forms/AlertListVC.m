                             //
//  AlertListVC.m
//  Efield
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import "AlertListVC.h"
#import "Constants.h"
#import "NoRecordsTableViewCell.h"
#import "AlertListcell.h"
@interface AlertListVC ()
{
    BOOL isOnEditing;
    BOOL isMarkAll;
    __weak IBOutlet UILabel *titleLbl;
    //__weak IBOutlet UITableView *alertSearchTable;
    NSString *loadingmsg;
  //  NSMutableArray *searchResultArray;
  //  NSMutableArray *searchFullResultArray;
    NSInteger selectedRowIndex;
    int selectedUtilityIndex;
 }
@end

@implementation AlertListVC

- (void)viewDidLoad {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
         loadingmsg=@"Loading...";
    markreadbutton.hidden=YES;
    markAllButton.hidden=YES;
   // [self.view addGestureRecognizer:tap];
    [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"logout"];
    self.navigationItem.leftItemsSupplementBackButton = YES;
    alertTable.rowHeight = UITableViewAutomaticDimension;
    alertTable.estimatedRowHeight = 100;
    _companynametxt.text=[[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
  //  markAllButton.hidden = YES;
  //  markreadbutton.hidden = YES;
    NSString *eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@", eventDateStr);
 //   self.editbtn.hidden=YES;

    _date.text=eventDateStr;
    //alertSearchTable.hidden = YES;
   // isOnEditing = NO;
   // isMarkAll = NO;
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    alertList = [[NSMutableArray alloc]init];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor whiteColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self
                       action:@selector(updateALERTData)
             forControlEvents:UIControlEventValueChanged];
    [alertTable addSubview:refreshControl];
   
    
}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    //   [aTextField resignFirstResponder];
}
- (IBAction)cancelaction:(id)sender {
        [self.navigationController popViewControllerAnimated:YES];
}

//- (IBAction)editAction:(id)sender
//{
//    isOnEditing = !isOnEditing;
//  //  self.toolBar.hidden = !isOnEditing;
//   // [self.editIcon setImage:(isOnEditing) ? [UIImage imageNamed:@"edit_selected"] : [UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
//    markAllButton.hidden = !isOnEditing;
//    markreadbutton.hidden = !isOnEditing;
//    [markAllButton setTitle: @"Mark All" forState: UIControlStateNormal];
//
//
//     isMarkAll = NO;
//    [checkBoxArr removeAllObjects];
//    [alertTable reloadData];
//
//}

- (IBAction)markAllAction:(id)sender {
    isMarkAll = !isMarkAll;
    if (isMarkAll) {
         [markAllButton setTitle: @"Unselect All" forState: UIControlStateNormal]; //[markAllButton setTitle: @"Select All" forState: UIControlStateNormal];
        for (int i = 0; i < [alertTable numberOfSections]; i++) {
            for (int j = 0; j < [alertTable numberOfRowsInSection:i]; j++) {
                NSUInteger ints[2] = {i,j};
                NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:ints length:2];
                AlertListcell *cell = [alertTable cellForRowAtIndexPath:indexPath];
                [checkBoxArr addObject:[self alertValuesAtIndex:j]];
                [cell.checkBoxBtn setImage:[UIImage imageNamed:@"checkedCheckbox"] forState:UIControlStateNormal];
            }
        }
    }
    else
    {
        [checkBoxArr removeAllObjects];
        [markAllButton setTitle: @"Select All" forState: UIControlStateNormal];
        
         [alertTable reloadData];
    }
    
    NSLog(@"checkBoxArr --> %@",checkBoxArr);
}

-(NSMutableDictionary *)alertValuesAtIndex:(int)aindex
{
    alertListDic = [[NSMutableDictionary alloc] init];
    
    
    [alertListDic setValue:[[alertList objectAtIndex:aindex] objectForKey:@"AlertMessage"] forKey:@"AlertMessage"];
    
    [alertListDic setValue:[[alertList objectAtIndex:aindex] objectForKey:@"AlertId"] forKey:@"alertId"];
   [alertListDic setValue:[[alertList objectAtIndex:aindex] objectForKey:@"JobNumber"] forKey:@"JobNumber"];
//    [alertListDic setValue:[[alertList objectAtIndex:aindex] objectForKey:@"EfieldPatientId"] forKey:@"patientId"];
//    [alertListDic setValue:[[alertList objectAtIndex:aindex] objectForKey:@"AlertId"] forKey:@"alertId"];
//    [alertListDic setValue:[[alertList objectAtIndex:aindex] objectForKey:@"UserName"] forKey:@"UserName"];
//
    return alertListDic;
}

- (IBAction)readAction:(id)sender {
    NSString *alertid=@"";
    
    if (0 == [checkBoxArr count]) {
        [self showAlertWithTitle:@"Efield Message" message:@"Please select any item from the list"];
        return;
    }
    NSDictionary * dict = [[NSDictionary alloc] initWithObjectsAndKeys:checkBoxArr,@"RequestList", nil];
      [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   
    for(int i=0;i<[checkBoxArr count];i++)
    {
        if([alertid isEqualToString:@""])
        {
            alertid= [[[checkBoxArr valueForKey:@"alertId"] objectAtIndex:i]description];
        }else{
            
            alertid=[NSString stringWithFormat:@"%@,%@",alertid,[[[checkBoxArr valueForKey:@"alertId"] objectAtIndex:i]description]];
        }
        
    }
//       NSDictionary *json ;
//    json = @{
//
//             @"AlertsId" : alertid
//
//
//             };
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@ClearAlertsForApp?AlertsId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],alertid] andParameter:nil andMethod:@"POST" andDelegate:self andKey:@"ClearAlertsForApp"];
    NSLog(@"url%@",[NSString stringWithFormat:@"%@ClearAlertsForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]]);
}

-(void)progress
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    
}

- (IBAction)showMoreAction:(id)sender
{
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efield Message" message:@"No network, please check your internet connection"];
        return;
    }
//    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [self updateALERTData];
}

- (void)updateDeviceTokentoServer
{
     
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efield Message" message:@"No network, please check your internet connection"];
        return;
    }else{
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kdeviceToken]);
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
        NSDictionary *json ;
        NSString *deviceoken=[[NSUserDefaults standardUserDefaults] objectForKey:kdeviceToken];
        if ([deviceoken isKindOfClass:[NSNull class]]||[deviceoken isEqualToString:@"null"]||deviceoken==nil)
        {
            json = @{
                     
                     @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
                     @"AppId" :@""
                     
                     };
        }else
        {
            json = @{
                     @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
                     @"AppId" : deviceoken
                     };
               }
        
        NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
        [dictEntry setObject:json forKey:[NSString stringWithFormat:@"UserModel"]];
        
   
     [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@IOSUpdateUserAppId",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"UpdateUserAppId"];
    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
//    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"gotopatientlist"] isEqualToString:@"true"]){
//        
//        [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"gotopatientlist"];
//        UINavigationController *nav      =[[self.tabBarController childViewControllers] objectAtIndex:3];
//        
//        PatientListVC *myController = (PatientListVC *)nav.topViewController;
//        self.tabBarController.selectedIndex = 3;
//    }
//    else{
    //_alertPatientID = nil;
    //}
}
- (void)viewWillAppear:(BOOL)animated
{
    
        [super viewWillAppear:animated];
        
    
    isMarkAll = NO;
    [checkBoxArr removeAllObjects];
    [markAllButton setTitle: @"Select All" forState: UIControlStateNormal];
    
    [alertTable reloadData];
    if (isOnEditing == YES)
    {
        [self editAction:nil];
    }
        if (![self isNetworkAvailable]) {
            [self showAlertno_network:@"Efield Message" message:@"No network, please check your internet connection"];
            return;
        }else{
            
            
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                });
                 DownloadManager *downloadObj = [[DownloadManager alloc]init];
              //  [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                
            
                 [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@GetWorkorderAlertsForApp?UserName=%@&RoleName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[NSUserDefaults standardUserDefaults] objectForKey:userRole]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetWorkorderAlertsForApp"];
            }
  
    
}
- (void)loadAlertData
{
    NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:details];
    NSDictionary* json = [NSKeyedUnarchiver unarchiveObjectWithData:data];
   
        alertList = [[NSMutableArray alloc]initWithArray:[json valueForKeyPath:@"Data"]];
        tempArr = [[NSArray alloc]initWithArray:[json valueForKeyPath:@"Data"]];
   
    
    if([alertList count]>0)
    {
        self.editbtn.hidden=NO;
        markreadbutton.hidden=NO;
            markAllButton.hidden=NO;
    }
    else{
        self.editbtn.hidden=YES;
          markreadbutton.hidden=YES;
           markAllButton.hidden=YES;
    }
    checkBoxArr = [[NSMutableArray alloc]init];
    [alertTable reloadData];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//
//#pragma mark - TableView Delegates
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    if (tableView == alertTable)
//    {
//        return nil;
//    }
//
//
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
 
       return 0.0;
 
}
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    if (tableView == alertTable)
//    {
//        return 1;
//    }
//
//}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([alertList count] == 0) {
        return 1;
    } else {
        return [alertList count];
    }
}


-(CGFloat)calculateStringHeigth:(CGSize)size text:(NSString *)text andFont:(UIFont *)font {
    NSLog(@"text : %@",text);
    
    
    NSLog(@"Font : %@", font);
    NSLog(@"size : %@",NSStringFromCGSize(size));
    
    if([text isEqual:[NSNull null]]) {
        text = @"";
    }
    
    if(![text length])
        return 0;
    
    CGSize labelSize = [text sizeWithFont:font
                        constrainedToSize:size
                            lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat labelHeight = labelSize.height;
    NSLog(@"labelHeight : %f",labelHeight);
    return labelHeight;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//
//
//        CGSize size = alertTable.frame.size;
//        size.width = size.width - 16;
//   CGFloat     height = [self calculateStringHeigth:size text:[[alertList valueForKey:@"AlertMessage"] objectAtIndex:indexPath.row] andFont:[UIFont systemFontOfSize:13]];
//
//            return height +90;
//
// }
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([alertList count] == 0) {
        return 40;
        
    } else {
    CGFloat height;
    if([[[alertList valueForKey:@"AlertMessage"] objectAtIndex:indexPath.row] length]==0)
    {
        height = 110   ;
    }else{
        NSString *text = [[alertList valueForKey:@"AlertMessage"] objectAtIndex:indexPath.row];
        if([text length]>100){
            height=125;
        }else{
            
            height = 80  ;
            
            
        }
        
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:13];
        NSAttributedString *attributedText =
        [[NSAttributedString alloc] initWithString:text
                                        attributes:@{NSFontAttributeName: font}];
        CGRect rect = [attributedText boundingRectWithSize:(CGSize){self.view.bounds.size.width, CGFLOAT_MAX+10}
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                   context:nil];
        CGSize size = rect.size;
        height =height+ ceilf(size.height);
    }
    return height;
    }
}
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//
////
//// CGSize size = alertTable.frame.size;
////            size.width = size.width - 16;
////       CGFloat     height = [self calculateStringHeigth:size text:[[alertList valueForKey:@"AlertMessage"] objectAtIndex:indexPath.row] andFont:[UIFont systemFontOfSize:13]];
//
//                return  200;
//
//}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
  
    
         if ([alertList count] > 0) {
        NSString *cellIdentifier = @"cell1";
        AlertListcell *cell = (AlertListcell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
    
        if ([[[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
            cell.workorder_id.text = [[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row];
        }
        else
        {
            cell.workorder_id.text = @"";
        }
    
    if ([[[alertList valueForKey:@"DateViewedText"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
        cell.date_lbl.text = [[alertList valueForKey:@"DateViewedText"] objectAtIndex:indexPath.row];
        
    }  else
    {
        cell.date_lbl.text = @"";
    }
//        if ([[[alertList valueForKey:@"AlertMessage"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
//            cell.AlertMessage.text = [[alertList valueForKey:@"AlertMessage"] objectAtIndex:indexPath.row];
//
//        }  else
//        {
//            cell.AlertMessage.text = @"";
//        }
    cell.AlertMessage.numberOfLines = 0;
    [  cell.AlertMessage sizeToFit];
    [  cell.AlertMessage setNeedsDisplay];
    
    if ([[[alertList valueForKey:@"AlertMessage"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
                    cell.AlertMessage.text = [[alertList valueForKey:@"AlertMessage"] objectAtIndex:indexPath.row];
        
                }  else
                {
                    cell.AlertMessage.text = @"";
                }
    
        if ([[[alertList valueForKey:@"AlertFrom"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
            
            cell.alertfrom.text =[NSString stringWithFormat:@"From: %@",[[[alertList valueForKey:@"AlertFrom"] objectAtIndex:indexPath.row]description] ];
            
         }
        else
        {
            cell.alertfrom.text = @"";
        }
      
//        if (isOnEditing) {
//            cell.checkBoxBtn.hidden = NO;
//        } else {
//            cell.checkBoxBtn.hidden = YES;
//        }
    cell.checkBoxBtn.hidden = NO;
         NSArray *filtered = [checkBoxArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(alertId == %@) AND (JobNumber == %@) AND (AlertMessage == %@)", [[alertList objectAtIndex:indexPath.row] objectForKey:@"AlertId"],[[alertList objectAtIndex:indexPath.row] objectForKey:@"JobNumber"], [[alertList objectAtIndex:indexPath.row] objectForKey:@"AlertMessage"]]];
//
//    NSArray *filtered = [checkBoxArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(alertId == %@) AND (JobNumber == %@) AND (AlertMessage == %@)", [[alertList objectAtIndex:btn.tag] objectForKey:@"AlertId"],[[alertList objectAtIndex:btn.tag] objectForKey:@"JobNumber"], [[alertList objectAtIndex:btn.tag] objectForKey:@"AlertMessage"]]];
    
    
        if ([filtered count])
        {
            [cell.checkBoxBtn setImage:[UIImage imageNamed:@"checkedCheckbox"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.checkBoxBtn setImage:[UIImage imageNamed:@"uncheckedCheckbox"] forState:UIControlStateNormal];
        }
        
        cell.checkBoxBtn.tag = indexPath.row;
        [cell.checkBoxBtn addTarget:self action:@selector(checkBox:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
      
        return cell;

  
    }
         else{
             
             NoRecordsTableViewCell *cell = (NoRecordsTableViewCell *)[tableView
                                                                       dequeueReusableCellWithIdentifier:noRecordsResuableIdentifier];
             cell.noRecordsLabel.text = loadingmsg;
             return cell;
             
         }
}



- (void)checkBox:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    index = btn.tag;
    dummyDic = [alertList objectAtIndex:btn.tag];
    
    alertListDic = [[NSMutableDictionary alloc] init];
    [alertListDic setValue:[[alertList objectAtIndex:btn.tag] objectForKey:@"AlertMessage"] forKey:@"AlertMessage"];
    
    [alertListDic setValue:[[alertList objectAtIndex:btn.tag] objectForKey:@"AlertId"] forKey:@"alertId"];
    [alertListDic setValue:[[alertList objectAtIndex:btn.tag] objectForKey:@"JobNumber"] forKey:@"JobNumber"];

    NSArray *filtered = [checkBoxArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(alertId == %@) AND (JobNumber == %@) AND (AlertMessage == %@)", [[alertList objectAtIndex:btn.tag] objectForKey:@"AlertId"],[[alertList objectAtIndex:btn.tag] objectForKey:@"JobNumber"], [[alertList objectAtIndex:btn.tag] objectForKey:@"AlertMessage"]]];
    
    if ([filtered count])
    {
        [checkBoxArr removeObjectAtIndex:[checkBoxArr indexOfObject:[filtered objectAtIndex:0]]];
        [btn setImage:[UIImage imageNamed:@"uncheckedCheckbox"] forState:UIControlStateNormal];
    }
    else
    {
        [checkBoxArr addObject:alertListDic];
        
        [btn setImage:[UIImage imageNamed:@"checkedCheckbox"] forState:UIControlStateNormal];
    }

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 57)
    {
        self.tabBarController.selectedIndex = 0;
    }
    if (alertView.tag == 58)
    {
     
    }
}


#pragma mark - API Delegate

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
 //   [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [checkBoxArr removeAllObjects];
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:response delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    callAlert.tag = 57;
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    NSLog(@"response%@",response);
    if([key isEqualToString:@"UpdateDashboardAlert"])
    {
        
        //searchbar.text = @"";
        if ([[response objectForKey:@"Response"]isEqualToString:@"Success"])
        {
//            [alertList removeObject:alertListDic];
            [alertTable reloadData];
            [self updateALERTData];
            UITabBarItem *itemToBadge = self.tabBarController.tabBar.items[1];
            int count = (int)[checkBoxArr count];
            int currentTabValue = [itemToBadge.badgeValue intValue];
            int newTabValue = currentTabValue - count; // Or whatever you want to calculate
            itemToBadge.badgeValue = [NSString stringWithFormat:@"%d", newTabValue];
            [UIApplication sharedApplication].applicationIconBadgeNumber = newTabValue;
            if (isOnEditing == YES)
            {
                [self editAction:nil];
            }
            [checkBoxArr removeAllObjects];
        }
        else
        {
            
        }
    }
    else if([key isEqualToString:@"ClearAlertsForApp"])
    {
         
            if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
            {
                [markAllButton setTitle: @"Select All" forState: UIControlStateNormal];

        DownloadManager *downloadObj = [[DownloadManager alloc]init];
       
        
    
        [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@GetWorkorderAlertsForApp?UserName=%@&RoleName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[NSUserDefaults standardUserDefaults] objectForKey:userRole]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetWorkorderAlertsForApp"];
        }
        
    }
    else if ([key isEqualToString:@"GetWorkorderAlertsForApp"])
    {
       // _alertPatientID = nil;
      //  [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        alertTable.tableFooterView.hidden = YES;
        detailsDic = [[NSMutableDictionary alloc]initWithDictionary:response];
        NSData* data=[NSKeyedArchiver archivedDataWithRootObject:response];
        [[NSUserDefaults standardUserDefaults]setObject:data forKey:details];
        [[NSUserDefaults standardUserDefaults]synchronize];
        NSLog(@"data%@",response);
        
          loadingmsg=@"No Data Found!";
        [self loadAlertData];
          [alertTable reloadData];
    }
    else if ([key isEqualToString:@"UpdateUserAppId"])
    {
        
        NSLog(@"app id uploaded response %@",response);
    }
    else if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efield Message" message:@"Please try again later"];
        }
    }
  
}



-(NSMutableArray *)removeDuplicates:(NSArray *)searchResult withTitle:(NSString *)title
{
    NSMutableArray *filteredArray = [NSMutableArray array];
    for (int i = (int)searchResult.count - 1; i>= 0; i--)
    {
        NSMutableDictionary* E1 = [searchResult objectAtIndex:i];
        BOOL hasDuplicate = [[filteredArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == %@",title, [E1 objectForKey:title]]] count] > 0;
        
        if (!hasDuplicate)
        {
            [filteredArray addObject:E1];
        }
    }
    
    return filteredArray;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)add_patient:(id)sender {
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue1"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue0"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue2"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue3"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue4"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue5"];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue6"];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue7"];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue8"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedid9"];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue9"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue10"];
    
    
    [self performSegueWithIdentifier:@"add_patient_alert" sender:self];
}



- (void)updateALERTData
{
    
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efield Message" message:@"No network, please check your internet connection"];
        return;
    }else{
  
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        
        DownloadManager *downloadObj = [[DownloadManager alloc]init];
       // [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
 
      [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@GetWorkorderAlertsForApp?UserName=%@&RoleName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[NSUserDefaults standardUserDefaults] objectForKey:userRole]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetWorkorderAlertsForApp"];
    }
    [refreshControl performSelector:@selector(endRefreshing) withObject:nil afterDelay:1.0f];
 
}
-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {
}
- (IBAction)logoutAction:(id)sender
{
      [self logoutUser];
    
}

- (IBAction)changepasswordAction:(id)sender {
    [self changePassword];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {}
@end
