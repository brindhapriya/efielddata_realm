//
//  SelectionViewController.m
//  DynamicForm
//
//  Created by Mahesh Kumar on 1/28/17.
//  Copyright © 2017 mahesh. All rights reserved.
//

#import "SelectionViewController.h"

@interface SelectionViewController ()


//@property(nonatomic,retain)NSArray *listAry;
@property(nonatomic,retain)NSMutableArray *selectedItem;

@end

@implementation SelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
    isInitialFlag = YES;
    _Header_lbl.text=_Workorder_type;
    self.question_name.text = _listInfo[@"QuestionDisplayName"];
    [self.question_name sizeToFit];
    NSLog(@"Height :%@", NSStringFromCGRect(self.question_name.frame));
    
    self.taskname_lbl.text= _ProjectName   ;
    self.jobnumber.text=[NSString stringWithFormat:@"%@ - %@", _JobNumber,_TaskName];
    _heightConstrains.constant = 200;//self.question_name.frame.size.height + 50;
    
//    CGRect frame = _questionView.frame;
//    frame.size.height = self.question_name.frame.size.height;
//    
//    
//    [_questionView setFrame:frame];
    
    lblTitle.text = self.noteType;
    _selectedItem = [[NSMutableArray alloc] init];
    self.title = _listInfo[@"CategoryName"];
  
  if([[NSString stringWithFormat:@"%@",_listInfo[@"IsSearchProvider"] ] isEqualToString:@"1"])
  {
  self.searchBar.delegate = (id)self;
        CGRect searchFrame = self.searchBar.frame;
      searchFrame.size.height = 44;
      self.searchBar.frame = searchFrame;
      self.searchBar.hidden = NO;
      notelListTemp=_listInfo;
      
  }else{
      
      CGRect searchFrame = self.searchBar.frame;
    
      searchFrame.size.height = 0;
      
      self.searchBar.frame = searchFrame;
      self.searchBar.hidden = YES;
  }

    if([_listInfo[@"DataType"] isEqualToString:@"Dropdown(Multiple)"])
    {
        for(NSString *str in _listInfo[@"MultiAnswers"])
        {
            [_selectedItem addObject:[NSNumber numberWithInteger:[str intValue]]];
        }
    }
       // [_selectedItem addObjectsFromArray:_listInfo[@"MultiAnswers"]];
    else if([_listInfo[@"DataType"] isEqualToString:@"Checkbox(Multiple)"])
    {
        for(NSString *str in _listInfo[@"MultiCheckBoxAnswers"])
        {
            
            [_selectedItem addObject:[NSNumber numberWithInteger:[str intValue]]];
        }
    }
        //[_selectedItem addObjectsFromArray:_listInfo[@"MultiCheckBoxAnswers"]];
    else if(([_listInfo[@"DataType"] isEqualToString:@"Dropdown"]) ||([_listInfo[@"DataType"] isEqualToString:@"Dropdown(Special)"]) || ([_listInfo[@"DataType"] isEqualToString:@"Radio"])) {
        if([_listInfo[@"AnswerId"] intValue] != 0) {
            [_selectedItem addObject:_listInfo[@"AnswerId"]];
        }
    }
        
//        else if([_listInfo[@"DataType"] isEqualToString:@"Dropdown"])
//            [_selectedItem addObject:_listInfo[@"AnswerId"]];
//        else if([_listInfo[@"DataType"] isEqualToString:@"Radio"])
//            [_selectedItem addObject:_listInfo[@"AnswerId"]];

        
    [self.tblForm reloadData];
      self.done_btn.enabled = NO;
    
    // Do any additional setup after loading the view.
}




- (void)searchBar:(UISearchBar *)searchBr textDidChange:(NSString *)searchText
{
    if(searchText.length)
    {
         [_selectedItem removeAllObjects];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"AnswerValue CONTAINS[c] %@",searchText];
        
        NSArray *filterArray = [notelListTemp[@"AnswersList"]filteredArrayUsingPredicate:predicate];
        NSMutableDictionary *temp = [[NSMutableDictionary alloc] initWithDictionary:notelListTemp];
        [temp setObject:filterArray forKey:@"AnswersList"];
        _listInfo = temp;
        [_tblForm reloadData];
        
    }
    else
    {
        _selectedItem = [[NSMutableArray alloc] init];
        
        [_selectedItem removeAllObjects];
        _listInfo = notelListTemp;
        [self.tblForm reloadData];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBr
{
    [searchBr setShowsCancelButton:YES animated:YES];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBr
{
    _selectedItem = [[NSMutableArray alloc] init];
    
    [_selectedItem removeAllObjects];
    [searchBr setShowsCancelButton:NO animated:YES];
    searchBr.text = @"";
    _listInfo = notelListTemp;
    [self.tblForm reloadData];
    [searchBr resignFirstResponder];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [[self view]endEditing:YES];
}


- (IBAction)cancelAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnDoneClick:(id)sender {
    
    if([[NSString stringWithFormat:@"%@",_listInfo[@"IsSearchProvider"] ] isEqualToString:@"1"])
    {
         _listInfo = notelListTemp;
    }
    NSString *value = @"";
    
    for(NSString *str in _selectedItem) {
        NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"AnswerId = %@",str]];
        NSArray* filteredData = [_listInfo[@"AnswersList"] filteredArrayUsingPredicate:predicate];

        NSLog(@"Filter : %@",filteredData);
        
        if(filteredData.count) {
            if([value isEqualToString:@""])
                value = filteredData[0][@"AnswerValue"];
            else
                value = [NSString stringWithFormat:@"%@,%@",value,filteredData[0][@"AnswerValue"]];
        }
    }
    
    //Checkbox(Multiple) - MultiSelectCheckBoxAnswers
    //Dropdown(multiple) -  MultiAnswers
    
    
    if([_listInfo[@"DataType"] isEqualToString:@"Dropdown(Multiple)"])
    {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:_listInfo];
        [dic setObject:_selectedItem forKey:@"MultiAnswers"];
        [dic setObject:value forKey:@"QuestionValue"];
        NSLog(@"dic : %@",dic);
        [_delegate updateSelectedItems:dic questionID:_listInfo[@"QuestionId"]];
    }
    else if([_listInfo[@"DataType"] isEqualToString:@"Checkbox(Multiple)"])
    {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:_listInfo];
        [dic setObject:_selectedItem forKey:@"MultiCheckBoxAnswers"];
       NSMutableArray   *selectedItem1 = [[NSMutableArray alloc] init];
          [dic setObject:selectedItem1 forKey:@"MultiSelectCheckBoxAnswers"];
        [dic setObject:value forKey:@"QuestionValue"];
        NSLog(@"dic : %@",dic);
        [_delegate updateSelectedItems:dic questionID:_listInfo[@"QuestionId"]];
    }
    else if(([_listInfo[@"DataType"] isEqualToString:@"Dropdown"])||([_listInfo[@"DataType"] isEqualToString:@"Dropdown(Special)"]) || ([_listInfo[@"DataType"] isEqualToString:@"Radio"])) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:_listInfo];
        if(_selectedItem.count)
            [dic setObject:_selectedItem[0] forKey:@"AnswerId"];
        else
            [dic setObject:@"0" forKey:@"AnswerId"];
        
        [dic setObject:value forKey:@"QuestionValue"];
        NSLog(@"dic : %@",dic);
        [_delegate updateSelectedItems:dic questionID:_listInfo[@"QuestionId"]];
        
        
        if([_listInfo[@"DataType"] isEqualToString:@"Dropdown(Special)"])
        {  if(_selectedItem.count)
        {
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@", _selectedItem[0]] forKey:@"Dropdown_special_id"];
        }
        else{
                [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@""] forKey:@"Dropdown_special_id"];
            
        }
            
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
   
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_listInfo[@"AnswersList"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.textLabel.text = _listInfo[@"AnswersList"][indexPath.row][@"AnswerValue"];
   // cell.textLabel.font = [UIFont systemFontOfSize:13];
    
    cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    [cell.textLabel sizeToFit];
    cell.textLabel.numberOfLines = 0;
    
    // Configure the cell...
    if ([_selectedItem containsObject:_listInfo[@"AnswersList"][indexPath.row][@"AnswerId"]])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}


-(CGFloat)calculateStringHeigth1:(CGSize)size text:(NSString *)text andFont:(UIFont *)font {
    NSLog(@"text : %@",text);
    
    
    NSLog(@"Font : %@", font);
    NSLog(@"size : %@",NSStringFromCGSize(size));
    
    if([text isEqual:[NSNull null]]) {
        text = @"";
    }
    
    if(![text length])
        return 0;
    
    CGSize labelSize = [text sizeWithFont:font
                        constrainedToSize:size
                            lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat labelHeight = labelSize.height;
    NSLog(@"labelHeight : %f",labelHeight);
    return labelHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height;
    CGSize size = self.tblForm.frame.size;
    size.width = size.width - 5;
     height = [self calculateStringHeigth1:size text: _listInfo[@"AnswersList"][indexPath.row][@"AnswerValue"] andFont:[UIFont systemFontOfSize:14]];
    return height+20;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.done_btn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.done_btn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.done_btn.enabled = YES;
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if((([_listInfo[@"DataType"] isEqualToString:@"Dropdown"]) ||([_listInfo[@"DataType"] isEqualToString:@"Dropdown(Special)"])|| ([_listInfo[@"DataType"] isEqualToString:@"Radio"])) && isInitialFlag){
        NSLog(@"_sele : %@",_selectedItem);
        isInitialFlag = NO;
        if(_selectedItem.count) {
            NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"AnswerId = %@",_selectedItem[0]]];
            NSArray* filteredData = [_listInfo[@"AnswersList"] filteredArrayUsingPredicate:predicate];
            if(filteredData.count) {
                NSInteger index = [_listInfo[@"AnswersList"] indexOfObject:filteredData[0]];
                NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
                
                if(currentIndexPath != indexPath)
                {
                    UITableViewCell *cell = [tableView cellForRowAtIndexPath:currentIndexPath];
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    [_selectedItem removeObject:_listInfo[@"AnswersList"][index][@"AnswerId"]];
                }
                NSLog(@"_sele 1: %@",_selectedItem);

            }
        }
    }
    
    if ([_selectedItem containsObject:_listInfo[@"AnswersList"][indexPath.row][@"AnswerId"]])
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [_selectedItem removeObject:_listInfo[@"AnswersList"][indexPath.row][@"AnswerId"]];
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [_selectedItem addObject:_listInfo[@"AnswersList"][indexPath.row][@"AnswerId"]];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(([_listInfo[@"DataType"] isEqualToString:@"Dropdown"])||([_listInfo[@"DataType"] isEqualToString:@"Dropdown(Special)"])  || ([_listInfo[@"DataType"] isEqualToString:@"Radio"])) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryNone;
        [_selectedItem removeObject:_listInfo[@"AnswersList"][indexPath.row][@"AnswerId"]];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)logoutAction:(id)sender {
     [self logoutUser];
//    if (![self isNetworkAvailable]) {
//        [self showAlertWithTitle:@"Efield Message" message:@"No network, please check your internet connection"];
//        return;
//    }
//    
//    [MBProgressHUD showHUDAddedTo:self.view  animated:YES];
//    NSDictionary *json = @{
//                           
//                           @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username]
//                           
//                           };
//    NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
//    [dictEntry setObject:json forKey:[NSString stringWithFormat:@"UserModel"]];
//    
//    DownloadManager *downloadObj = [[DownloadManager alloc]init];
//    
//    [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@IOSResetUserAppId",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"logout"];
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    DownloadManager *downloadObj = [[DownloadManager alloc]init];
//    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@ResetUserAppId?UserName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username]] andParameter:nil andMethod:@"POST" andDelegate:self andKey:@"logout"];
}

- (IBAction)changepasswordAction:(id)sender {
    [self changePassword];
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"response : %@",response);
    if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efield Message" message:@"Please try again later"];
        }
    }
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
