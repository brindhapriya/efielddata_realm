//
//  MultySelectionTableViewCell.m
//  DynamicForm
//
//  Created by Mahesh Kumar on 1/28/17.
//  Copyright © 2017 mahesh. All rights reserved.
//

#import "Ready_Checkbox_Cell.h"

@implementation Ready_Checkbox_Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
