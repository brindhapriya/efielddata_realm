//
//  SelectionViewController.h
//  DynamicForm
//
//  Created by Mahesh Kumar on 1/28/17.
//  Copyright © 2017 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ViewController.h"


@protocol UpdateSelectedItemsDelegate <NSObject>

@required
-(void)updateSelectedItems:(NSDictionary *)updatedItem questionID:(NSString *)questionID;
@end


@interface SelectionViewController :  ViewController
{
    __weak IBOutlet UILabel *lblTitle;
    
    BOOL isInitialFlag;
        
        NSDictionary *notelListTemp;
        
 

}
@property (weak, nonatomic) IBOutlet UILabel *alertcnt;
@property (weak, nonatomic) IBOutlet UILabel *Header_lbl;
@property(nonatomic,strong) id<UpdateSelectedItemsDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *done_btn;
@property(nonatomic,retain)NSString *selectedTypeStr;
@property (nonatomic, retain) IBOutlet UITableView *tblForm;
@property(nonatomic,retain)NSDictionary *listInfo;
@property(nonatomic,retain)NSArray *selectedList;
@property(nonatomic,retain)NSString *noteType;

@property (weak, nonatomic) IBOutlet UILabel *taskname_lbl;
@property (weak, nonatomic) IBOutlet UILabel *jobnumber;
@property (weak, nonatomic) IBOutlet UILabel *question_name;
@property (weak, nonatomic) IBOutlet UIView *questionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstrains;
@property (nonatomic, retain) NSString *TaskName,*JobNumber,*ProjectName,*Workorder_type;

- (IBAction)logoutAction:(id)sender;
- (IBAction)changepasswordAction:(id)sender;
@end
