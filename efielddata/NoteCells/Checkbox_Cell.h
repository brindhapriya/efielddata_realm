//
//  MultySelectionTableViewCell.h
//  DynamicForm
//
//  Created by Mahesh Kumar on 1/28/17.
//  Copyright © 2017 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Checkbox_Cell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *non_billable;
@property (weak, nonatomic) IBOutlet UIButton *special_Type;


@end
