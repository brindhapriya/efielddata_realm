//
//  DescriptionTableViewCell.h
//  DynamicForm
//
//  Created by Mahesh Kumar on 2/5/17.
//  Copyright © 2017 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscribtion;

@end
