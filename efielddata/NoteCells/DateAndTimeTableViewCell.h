//
//  DateAndTimeTableViewCell.h
//  DynamicForm
//
//  Created by Mahesh Kumar on 2/5/17.
//  Copyright © 2017 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateAndTimeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (weak, nonatomic) IBOutlet UIButton *date_img;

@end
