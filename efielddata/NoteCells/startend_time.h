//
//  MultySelectionTableViewCell.h
//  DynamicForm
//
//  Created by Mahesh Kumar on 1/28/17.
//  Copyright © 2017 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface startend_time : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *start_timeview;
@property (weak, nonatomic) IBOutlet UILabel *start_timelbl;
@property (weak, nonatomic) IBOutlet UILabel *start_timevalue;
@property (weak, nonatomic) IBOutlet UIView *end_timeview;
@property (weak, nonatomic) IBOutlet UILabel *end_timelbl;
@property (weak, nonatomic) IBOutlet UILabel *end_timevalue;
@property (weak, nonatomic) IBOutlet UILabel *startend_timespent;

@end
