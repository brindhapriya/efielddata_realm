//
//  SelectionViewController.m
//  DynamicForm
//
//  Created by Mahesh Kumar on 1/28/17.
//  Copyright © 2017 mahesh. All rights reserved.
//

#import "sampledSelectionViewController.h"

@interface sampledSelectionViewController ()


//@property(nonatomic,retain)NSArray *listAry;

@property(nonatomic,retain)NSMutableArray *selectedItem,*selectedItem_name;

@end

@implementation sampledSelectionViewController
{
    
    NSMutableArray *arSelectedRows;
    NSString *SampleSourceText;
   // [[NSUserDefaults standardUserDefaults]setObject:[jsonDict valueForKeyPath:@"Data.SampleSourceText"] forKey:@"SampleSourceText"]
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
    isInitialFlag = YES;
    _Header_lbl.text=_Work_type;

    NSLog(@"Height :%@", NSStringFromCGRect(self.question_name.frame));
    
    self.taskname_lbl.text= _ProjectName   ;
    self.jobnumber.text=[NSString stringWithFormat:@"%@ - %@", _JobNumber,_TaskName];
    _heightConstrains.constant = 200;//self.question_name.frame.size.height + 50;
    
    CGRect frame = _questionView.frame;
    frame.size.height = self.question_name.frame.size.height;
    SampleSourceText=[[NSUserDefaults standardUserDefaults] valueForKey:@"SampleSourceText"];
    self.tblForm.allowsMultipleSelection = true;

    [_questionView setFrame:frame];
    self.question_name= @"Sample Type";
    lblTitle.text = @"Sample Type";
     self.title =@"Sample Type"; //_listInfo[@"CategoryName"];
  
 _listInfo= @[
    @{ @"SampleId": @"1068", @"SampleName": @"Standard Proctor" },
    @{ @"SampleId": @"1071", @"SampleName": @"Sieve" },
    @{ @"SampleId": @"1072", @"SampleName": @"Organic" },
    @{ @"SampleId": @"1069", @"SampleName": @"LBR" },
    @{ @"SampleId": @"1070", @"SampleName": @"CBR" },
    @{ @"SampleId": @"1073", @"SampleName": @"Carbonate Content" },
    @{ @"SampleId": @"1074", @"SampleName": @"Florida Bearing Value" },
    @{ @"SampleId": @"1075", @"SampleName": @"Permeability of Granular Soils" },
    @{ @"SampleId": @"1084", @"SampleName": @"Modified Proctor" },
      @{ @"SampleId": @"2117", @"SampleName": @"Gradation" }
     
     ];
  self.searchBar.delegate = (id)self;
        CGRect searchFrame = self.searchBar.frame;
      searchFrame.size.height = 44;
      self.searchBar.frame = searchFrame;
      self.searchBar.hidden = NO;
      notelListTemp=_listInfo;
    _selectedItem=[[NSMutableArray alloc]init];
    _selectedItem_name=[[NSMutableArray alloc]init];

    if([_selectedList count]>0)
      {
          for(int i=0;i<[_listInfo count];i++)
          {
              for(int j=0;j<[_selectedList count];j++)
              {
                  NSLog(@"list: %@, sample:%@",[ [_listInfo valueForKey:@"SampleId"] objectAtIndex:i],[ _selectedList objectAtIndex:j]);
              if([[[ [_listInfo valueForKey:@"SampleId"] objectAtIndex:i]description] isEqualToString:[ _selectedList objectAtIndex:j]])
              {
                     NSLog(@"list: %@ ",[_listInfo objectAtIndex:i]);
              [_selectedItem addObject:[[ [_listInfo valueForKey:@"SampleId"] objectAtIndex:i]description]];
                  [_selectedItem_name addObject:[[ [_listInfo valueForKey:@"SampleName"] objectAtIndex:i]description]];

              }
                               
              }
          }
      }
    NSLog(@"list: %@ ",_selectedItem);

    [self.tblForm reloadData];
    
    
    // Do any additional setup after loading the view.
}




- (void)searchBar:(UISearchBar *)searchBr textDidChange:(NSString *)searchText
{
    if(searchText.length)
    {
//         [_selectedItem removeAllObjects];
//        [_selectedItem_name removeAllObjects];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SampleName CONTAINS[c] %@",searchText];
        
        NSArray *filterArray = [notelListTemp filteredArrayUsingPredicate:predicate];
     
        _listInfo = filterArray;
        [_tblForm reloadData];
        
    }
    else
    {
//        _selectedItem = [[NSMutableArray alloc] init];
//                [_selectedItem removeAllObjects];
//        _selectedItem_name = [[NSMutableArray alloc] init];
//
//        [_selectedItem_name removeAllObjects];

        _listInfo = notelListTemp;
        [self.tblForm reloadData];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBr
{
    [searchBr setShowsCancelButton:YES animated:YES];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBr
{
//    _selectedItem = [[NSMutableArray alloc] init];
//
//    [_selectedItem removeAllObjects];
//
//    _selectedItem_name = [[NSMutableArray alloc] init];
//
//    [_selectedItem_name removeAllObjects];
    [searchBr setShowsCancelButton:NO animated:YES];
    searchBr.text = @"";
    _listInfo = notelListTemp;
    [self.tblForm reloadData];
    [searchBr resignFirstResponder];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [[self view]endEditing:YES];
}


- (IBAction)cancelAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnDoneClick:(id)sender {
    
 
      [[NSUserDefaults standardUserDefaults]setObject:  _selectedItem   forKey:@"sampled_array"];
     [[NSUserDefaults standardUserDefaults]setObject:  _selectedItem_name   forKey:@"sampled_array_name"];
 [[NSUserDefaults standardUserDefaults]setObject:  @"none"   forKey:@"SampleSourceText"];

     [self.navigationController popViewControllerAnimated:YES];
   
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_listInfo  count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.textLabel.text =   [[_listInfo valueForKey:@"SampleName"] objectAtIndex:indexPath.row] ;
   // cell.textLabel.font = [UIFont systemFontOfSize:13];
    
    cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    [cell.textLabel sizeToFit];
    cell.textLabel.numberOfLines = 0;
    cell.accessoryType = UITableViewCellAccessoryNone;

    // Configure the cell...
    if ([_selectedList containsObject: [ [_listInfo valueForKey:@"SampleId"] objectAtIndex:indexPath.row]])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
      
       // [_selectedItem removeObject:[ [_listInfo valueForKey:@"SampleId"] objectAtIndex:indexPath.row]];

    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}


-(CGFloat)calculateStringHeigth1:(CGSize)size text:(NSString *)text andFont:(UIFont *)font {
    NSLog(@"text : %@",text);
    
    
    NSLog(@"Font : %@", font);
    NSLog(@"size : %@",NSStringFromCGSize(size));
    
    if([text isEqual:[NSNull null]]) {
        text = @"";
    }
    
    if(![text length])
        return 0;
    
    CGSize labelSize = [text sizeWithFont:font
                        constrainedToSize:size
                            lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat labelHeight = labelSize.height;
    NSLog(@"labelHeight : %f",labelHeight);
    return labelHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height;
    CGSize size = self.tblForm.frame.size;
    size.width = size.width - 5;
    return 35;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
 
 
    
 // [_selectedItem addObject:[ [_listInfo valueForKey:@"SampleId"] objectAtIndex:indexPath.row]];
    
  //  for(int i=0;i<[_selectedItem count];i++){
  //  if ([[ _selectedItem  objectAtIndex:i] isEqualToString: [ [_listInfo valueForKey:@"SampleId"] objectAtIndex:indexPath.row]])
        if(cell.accessoryType == UITableViewCellAccessoryCheckmark) {

            cell.accessoryType = UITableViewCellAccessoryNone;

     
      
        [_selectedItem removeObject:[ [_listInfo valueForKey:@"SampleId"] objectAtIndex:indexPath.row]];
            [_selectedItem_name removeObject:[ [_listInfo valueForKey:@"SampleName"] objectAtIndex:indexPath.row]];

    } else {
           cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [_selectedItem addObject:[ [_listInfo valueForKey:@"SampleId"] objectAtIndex:indexPath.row]];
        [_selectedItem_name addObject:[ [_listInfo valueForKey:@"SampleName"] objectAtIndex:indexPath.row]];

//        if(SampleSourceText.length>0)
//         {
//          SampleSourceText=[NSString stringWithFormat:@"%@,%@",SampleSourceText,[ [_listInfo valueForKey:@"SampleName"] objectAtIndex:indexPath.row]];
//          }
//         else{
//         SampleSourceText=[ [_listInfo valueForKey:@"SampleName"] objectAtIndex:indexPath.row];
//         }
    
    }
   
    for (NSIndexPath *indexPath in tableView.indexPathsForSelectedRows) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
   // for(int i=0;i<[_selectedItem count];i++){
        //if ([[ _selectedItem  objectAtIndex:i] isEqualToString: [ [_listInfo valueForKey:@"SampleId"] objectAtIndex:indexPath.row]])
        if(cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            
        
            cell.accessoryType = UITableViewCellAccessoryNone;
     [_selectedItem removeObject:[ [_listInfo valueForKey:@"SampleId"] objectAtIndex:indexPath.row]];
            [_selectedItem_name removeObject:[ [_listInfo valueForKey:@"SampleName"] objectAtIndex:indexPath.row]];

            //            if(SampleSourceText.length>0)
//            {
//                if([SampleSourceText containsString:[NSString stringWithFormat:@"%@,",[ [_listInfo valueForKey:@"SampleName"] objectAtIndex:indexPath.row]]])
//                {
//                    [SampleSourceText stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@,",[ [_listInfo valueForKey:@"SampleName"] objectAtIndex:indexPath.row]] withString:@""];
//
//                }
//                if([SampleSourceText containsString:[NSString stringWithFormat:@"%@",[ [_listInfo valueForKey:@"SampleName"] objectAtIndex:indexPath.row]]])
//                {
//                    [SampleSourceText stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@",[ [_listInfo valueForKey:@"SampleName"] objectAtIndex:indexPath.row]] withString:@""];
//
//                }
//            }


        }
//        else {
//
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//
//            [_selectedItem addObject:[ [_listInfo valueForKey:@"SampleId"] objectAtIndex:indexPath.row]];
//
//        }
   // }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)logoutAction:(id)sender {
     [self logoutUser];
//    if (![self isNetworkAvailable]) {
//        [self showAlertWithTitle:@"Efield Message" message:@"No network, please check your internet connection"];
//        return;
//    }
//    
//    [MBProgressHUD showHUDAddedTo:self.view  animated:YES];
//    NSDictionary *json = @{
//                           
//                           @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username]
//                           
//                           };
//    NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
//    [dictEntry setObject:json forKey:[NSString stringWithFormat:@"UserModel"]];
//    
//    DownloadManager *downloadObj = [[DownloadManager alloc]init];
//    
//    [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@IOSResetUserAppId",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"logout"];
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    DownloadManager *downloadObj = [[DownloadManager alloc]init];
//    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@ResetUserAppId?UserName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username]] andParameter:nil andMethod:@"POST" andDelegate:self andKey:@"logout"];
}

- (IBAction)changepasswordAction:(id)sender {
    [self changePassword];
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"response : %@",response);
    if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efield Message" message:@"Please try again later"];
        }
    }
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
