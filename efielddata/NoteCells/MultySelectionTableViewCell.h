//
//  MultySelectionTableViewCell.h
//  DynamicForm
//
//  Created by Mahesh Kumar on 1/28/17.
//  Copyright © 2017 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultySelectionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;

@end
