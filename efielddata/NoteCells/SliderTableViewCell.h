//
//  SliderTableViewCell.h
//  DynamicForm
//
//  Created by Mahesh Kumar on 2/5/17.
//  Copyright © 2017 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SliderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UISlider *sliderValue;
@property (weak, nonatomic) IBOutlet UITextField *txtValue;
@property (weak, nonatomic) IBOutlet UILabel *lblMinValue;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxValue;

@end
