//
//  SelectionViewController.h
//  DynamicForm
//
//  Created by Mahesh Kumar on 1/28/17.
//  Copyright © 2017 mahesh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ViewController.h"
 


@interface Image_SelectionViewController :  ViewController <UIScrollViewDelegate>
{
    __weak IBOutlet UILabel *lblTitle;
    
    BOOL isInitialFlag;
        
        NSMutableArray *notelListTemp;
        
   
        
        NSMutableArray  *imgListTemp;
    NSMutableArray  *imgList ;

    

}
- (IBAction)saveaction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UIButton *save_image;

//@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(nonatomic,retain)NSString *selectedTypeStr;
@property (nonatomic, retain) IBOutlet UITableView *tblForm;
@property(nonatomic,retain)NSDictionary *listInfo;
@property(nonatomic,retain)NSArray *selectedList;
@property(nonatomic,retain)NSString *noteType;

@property (weak, nonatomic) IBOutlet UILabel *taskname_lbl;
@property (weak, nonatomic) IBOutlet UILabel *jobnumber;
@property (weak, nonatomic) IBOutlet UILabel *question_name;
@property (weak, nonatomic) IBOutlet UIView *questionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstrains;
@property (weak, nonatomic) IBOutlet UIButton *addimage;
@property (nonatomic, retain) NSString *TaskName,*JobNumber,*ProjectName;

@property (nonatomic, retain)NSString *WorkorderId;
- (IBAction)camera_action:(id)sender;

- (IBAction)logoutAction:(id)sender;
- (IBAction)changepasswordAction:(id)sender;
@end
