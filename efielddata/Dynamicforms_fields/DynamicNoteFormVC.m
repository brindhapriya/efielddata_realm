
//
//  Created by brindha on 12/02/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "DLRadioButton.h"
#import "DynamicNoteFormVC.h"
#import "Image_SelectionViewController.h"
#import "LocationTableViewCell.h"
#import "RadioTableViewCell.h"
#import "Suborder_listVC.h"
#import "childform_list.h"
#import "dynamiccameraVC.h"
#import "imageviewTableviewcell.h"

#import "LocationList.h"
#import "PlacementTableViewCell.h"
#import "sampledSelectionViewController.h"
#import "startend_time.h"
//#import "PlacementFormVC.h"

@interface DynamicNoteFormVC () <UpdateSelectedItemsDelegate,
UITextViewDelegate, UITextFieldDelegate> {
    NSString *iscomplete, *iscomplete_frompending, *iscompleteoriginal;
    NSString *timespent;
    NSInteger rowOfTheCell;
    //    UITextField *txtfield;
    NSIndexPath *editingIndexPath;
    NSString *ChildTestType,*ChildTestType_name;
    NSString *sampled_vale;
    NSString *start_timestr, *end_timestr, *timespent_str, *DisplayJobDateText,
    *timespent_save;
    int timespent_hours;   // integer division to get the hours part
    int timespent_minutes; // interval
    
    NSData *imgListdata;
    
    NSMutableArray *imgListList;
    BOOL IsChild3TestTypeRequired,IsChild2TestTypeRequired,IsChildTestTypeRequired;
    
    // NSInteger indexpath;
}
//@property (strong, nonatomic) IBOutlet UISlider *timespentslider;
@property(nonatomic, retain) IBOutlet UITableView *tblForm;

@end

@implementation DynamicNoteFormVC
NSDictionary *jsonDict;
- (void)viewDidLoad {
    [super viewDidLoad];
    timespent_minutes = -1;
    // self.title = @"Efield";
    start_timestr = @"";
    end_timestr = @"";
    timespent_str = @"";
    timespent_save = @"";
    _Header_lbl.text=_Work_type;
    [[NSUserDefaults standardUserDefaults] setObject:@"false"
                                              forKey:@"placement_flag"];
    [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:@"loc_flag"];
    
    // Do any additional setup after loading the view, typically from a nib.
    // _sampledataview.hidden=YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    //   [self.view addGestureRecognizer:tap];
    _companynametxt.text =
    [[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
    
    NSString *eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@", eventDateStr);
    [[NSUserDefaults standardUserDefaults] setObject:@""
                                              forKey:@"SampleSourceText"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"sampled_array"];
    _date.text = eventDateStr;
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"imgList"];
    [_tblForm setDataSource:self];
    [_tblForm setDelegate:self];
    // [self updateALERTcount];
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
    _alertcnt.text = [self updateALERTcount];
    
    _timespentslider.minimumValue = 0.0;
    _timespentslider.maximumValue = 20.0;
    
    self.taskname_lbl.text = _ProjectName;
    self.jobnumber.text =
    [NSString stringWithFormat:@"%@ - %@", _JobNumber, _TaskName];
    if (_isFromEditNote && !_IsEditNote) {
        _saveBtn.hidden = YES;
    } else {
        _saveBtn.hidden = NO;
    }
    
    _saveBtn.enabled = NO;
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification
     object:nil];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self performSelector:@selector(getworkorder_details)
               withObject:nil
               afterDelay:0.1];
    
    //  UITapGestureRecognizer *starttime_tapRecognizer = [[UITapGestureRecognizer
    //  alloc] initWithTarget:self action:@selector(startime_gesture:)];
    
    //   [starttime_tapRecognizer setNumberOfTapsRequired:1];
    //   [_start_timeview setUserInteractionEnabled:YES];
    
    // [_start_timeview addGestureRecognizer:starttime_tapRecognizer];
    
    //  UITapGestureRecognizer *endtime_tapRecognizer = [[UITapGestureRecognizer
    //  alloc] initWithTarget:self action:@selector(endime_gesture:)];
    
    //   [endtime_tapRecognizer setNumberOfTapsRequired:1];
    //   [_end_timeview setUserInteractionEnabled:YES];
    
    //   [_end_timeview addGestureRecognizer:endtime_tapRecognizer];
}
- (void)dismissKeyboard {
    [self.view endEditing:YES];
    //   [aTextField resignFirstResponder];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //  NSLog(@"SampleSourceText%@",[[NSUserDefaults standardUserDefaults]
    //  valueForKey:@"SampleSourceText"]);
    
    //_timespentslider.value = [[jsonDict
    // valueForKeyPath:@"Data.TimeSpent"]floatValue];
    //_timespentfield.text=[[jsonDict
    // valueForKeyPath:@"Data.TimeSpent"]description];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification
     object:nil];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"placement_flag"]
         isEqualToString:@"true"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"false"
                                                  forKey:@"placement_flag"];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self performSelector:@selector(getworkorder_details)
                   withObject:nil
                   afterDelay:0.1];
        
        
    }
    // loc_flag
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"loc_flag"]
              isEqualToString:@"true"]) {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self performSelector:@selector(getsubworkorder_count)
                   withObject:nil
                   afterDelay:0.1];
    }
    [self.tblForm reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // Disable iOS 7 back gesture
    if ([self.navigationController
         respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize =
    [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey]
     CGRectValue]
    .size;
    
    UIEdgeInsets contentInsets;
    //   if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication]
    //   statusBarOrientation])) {
    contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height+30), 0.0);
    //    } else {
    //        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width),
    //        0.0);
    //    }
    
    self.tblForm.contentInset = contentInsets;
    self.tblForm.scrollIndicatorInsets = contentInsets;
    [self.tblForm scrollToRowAtIndexPath:editingIndexPath
                        atScrollPosition:UITableViewScrollPositionTop
                                animated:YES];
}
- (void)keyboardWillHide:(NSNotification *)notification {
    // self.tblForm.contentInset = UIEdgeInsetsZero;
    [self.tblForm setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    self.tblForm.scrollIndicatorInsets = UIEdgeInsetsZero;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[[NSNotificationCenter defaultCenter] removeObserver:self
    // name:UIKeyboardWillShowNotification object:nil];
    //  [[NSNotificationCenter defaultCenter] removeObserver:self
    //  name:UIKeyboardWillHideNotification object:nil];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController
         respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return NO;
}

- (void)startime_gesture:(UITapGestureRecognizer *)sender

{
    
    [self show_starttimePicker:UIDatePickerModeTime currentLbl:sender];
}
- (void)endime_gesture:(UITapGestureRecognizer *)sender

{
    
    [self show_endtimePicker:UIDatePickerModeTime currentLbl:sender];
}
- (void)placement_gesture:(UITapGestureRecognizer *)sender

{
    ChildTestType_name = dataList[sender.view.tag][@"QuestionValue"];

    if ([iscomplete isEqualToString:@"false"]) {
        if (![self isNetworkAvailable]) {
            [self
             showAlertno_network:@"Efielddata Message"
             message:
             @"No network, please check your internet connection"];
            return;
        } else {
           
            imgListdata =
            [[NSUserDefaults standardUserDefaults] objectForKey:@"imgList"];
            
            imgListList = [NSKeyedUnarchiver unarchiveObjectWithData:imgListdata];
            NSMutableArray *tempAryimgListList =
            [NSKeyedUnarchiver unarchiveObjectWithData:imgListdata];
            int index = 0;
            
            for (NSMutableDictionary *dic in imgListList) {
                
                NSMutableDictionary *tempDic =
                [[NSMutableDictionary alloc] initWithDictionary:dic];
                [tempDic setObject:@"" forKey:@"NSData_value"];
                [tempAryimgListList replaceObjectAtIndex:index withObject:tempDic];
                // dic.removeObject(forKey: "NSData_value")
                
                index++;
            }
            imgListList = [tempAryimgListList mutableCopy];
            NSLog(@"imgListList%@", imgListList);
            
            if (timespent_minutes != -1) {
                int send_minutes = 0;
                if (timespent_minutes == 15) {
                    send_minutes = 25;
                } else if (timespent_minutes == 30) {
                    send_minutes = 5;
                } else if (timespent_minutes == 45) {
                    send_minutes = 75;
                }
                
                timespent_save =
                [NSString stringWithFormat:@"%d.%d", timespent_hours, send_minutes];
            }
            if ([timespent_save length] == 0) {
                timespent_save = 0;
            }
            Boolean locationbool;
            NSMutableArray *alertList;
            
            if ([IsAddMoreDisplay isEqualToString:@"1"])
                
            {
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                NSString *str =
                [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",
                 [[NSUserDefaults standardUserDefaults]
                  objectForKey:SERVERURL],
                 _WorkorderId];
                NSLog(
                      @"url%@",
                      [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",
                       [[NSUserDefaults standardUserDefaults]
                        objectForKey:SERVERURL],
                       _WorkorderId]);
                [request setURL:[NSURL URLWithString:str]];
                [request setHTTPMethod:@"GET"];
                [request setValue:@"application/x-www-form-urlencoded"
               forHTTPHeaderField:@"Content-Type"];
                NSError *error;
                NSURLResponse *response;
                NSData *urlData = [NSURLConnection sendSynchronousRequest:request
                                                        returningResponse:&response
                                                                    error:&error];
                NSDictionary *jsonDict =
                [NSJSONSerialization JSONObjectWithData:urlData
                                                options:kNilOptions
                                                  error:&error];
                NSLog(@"jsonDict%@", jsonDict);
                
                alertList = [[NSMutableArray alloc]
                             initWithArray:[jsonDict valueForKeyPath:@"Data.SubWorkOrderList"]];
                
                if ([alertList count] > 0) {
                    locationbool = true;
                } else {
                    locationbool = false;
                }
                
            } else {
                locationbool = true;
            }
            BOOL starttime_bool, endtime_bool, timepentbool;
            if ([IsStartEndTimeDisplay isEqualToString:@"1"])
                
            {
                
                if ([start_timestr length] > 0 &&
                    [IsStartEndTimeDisplay isEqualToString:@"1"]) {
                    starttime_bool = true;
                    
                } else if ([IsStartEndTimeDisplay isEqualToString:@"0"]) {
                    starttime_bool = true;
                } else {
                    starttime_bool = false;
                }
                
                endtime_bool = false;
                if ([iscomplete isEqualToString:@"true"]) {
                    if ([end_timestr length] > 0 &&
                        [IsStartEndTimeDisplay isEqualToString:@"1"]) {
                        endtime_bool = true;
                        
                    } else if ([IsStartEndTimeDisplay isEqualToString:@"0"]) {
                        endtime_bool = true;
                    } else {
                        endtime_bool = false;
                    }
                    
                } else {
                    endtime_bool = true;
                }
                timepentbool = true;
            } else {
                starttime_bool = true;
                endtime_bool = true;
                if ([timespent_save length] == 0) {
                    
                    timepentbool = false;
                } else {
                    timepentbool = true;
                }
            }
            
            //  start time/ end time validation
            if (starttime_bool && endtime_bool) {
                
                if (timepentbool) {
                    if ([self isValidate:dataList]) {
                        if ([self isValidatenumeric:dataList]) {
                            
                            if (![self isNetworkAvailable]) {
                                [self showAlertno_network:@"Efielddata Message"
                                                  message:@"No network, please check your "
                                 @"internet connection"];
                                return;
                            } else {
                                [[self view] endEditing:YES];
                                //            NSString *iscompletestr;
                                //            if(iscomplete){
                                //            iscompletestr=@"true";
                                //            }else{
                                //            iscompletestr=@"false";
                                //            }
                                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                
                                if ([[[NSUserDefaults standardUserDefaults]
                                      objectForKey:@"FileName"] length] > 0) {
                                    int index = 0;
                                    NSMutableArray *tempAry =
                                    [[NSMutableArray alloc] initWithArray:dataList];
                                    
                                    for (NSDictionary *dic in dataList) {
                                        
                                        if ([dic[@"DataType"] isEqualToString:@"Image"]) {
                                            NSMutableDictionary *tempDic =
                                            [[NSMutableDictionary alloc] initWithDictionary:dic];
                                            NSString *aString1 = dataList[index][@"TextAnswer"];
                                            NSString *aString;
                                            
                                            aString = [aString1
                                                       stringByAppendingFormat:
                                                       @",%@", [[NSUserDefaults standardUserDefaults]
                                                                objectForKey:@"FileName"]];
                                            
                                            [tempDic setObject:aString forKey:@"TextAnswer"];
                                            [tempAry replaceObjectAtIndex:index withObject:tempDic];
                                        }
                                        index++;
                                    }
                                    
                                    dataList = [[NSMutableArray alloc] initWithArray:tempAry];
                                }
                                [self.saveBtn setTitleColor:[UIColor colorWithRed:85 / 255.0f
                                                                            green:85 / 255.0f
                                                                             blue:85 / 255.0f
                                                                            alpha:1.0]
                                                   forState:UIControlStateNormal];
                                ///  NSLog(@"json%@",dataList);
                                NSDictionary *json;
                                
                                json = @{
                                         
                                         @"RoleName" : [[NSUserDefaults standardUserDefaults]
                                                        objectForKey:userRole],
                                         @"UserName" : [[NSUserDefaults standardUserDefaults]
                                                        objectForKey:username],
                                         @"IsComplete" : iscomplete,
                                         @"WorkOrderId" : _WorkorderId,
                                         @"TimeSpent" : timespent_save,
                                         @"List" : dataList,
                                         @"SampleIdArrays" : [[NSUserDefaults standardUserDefaults]
                                                              valueForKey:@"sampled_array"],
                                         @"IsfromIOS" : @true,
                                         @"StartTime" : start_timestr,
                                         @"DepartureTime" : end_timestr,
                                         @"ImageDiscriptionList" : imgListList
                                         
                                         };
                                // }
                                NSMutableDictionary *dictEntry =
                                [[NSMutableDictionary alloc] init];
                                [dictEntry
                                 setObject:json
                                 forKey:[NSString stringWithFormat:@"WorkOrderModel"]];
                                NSLog(@"dictEntry %@", dictEntry);
                                
                                DownloadManager *downloadObj = [[DownloadManager alloc] init];
                                [downloadObj
                                 callServerWithURL:
                                 [NSString stringWithFormat:@"%@UpdateWorkOrderDetails",
                                  [[NSUserDefaults
                                    standardUserDefaults]
                                   objectForKey:SERVERURL]]
                                 andParameter:dictEntry
                                 andMethod:@"POST"
                                 andDelegate:self
                                 andKey:@"UpdateWorkOrderDetails_placement"];
                            }
                            
                        } else {
                            UIAlertView *callAlert = [[UIAlertView alloc]
                                                      initWithTitle:@"Efielddata Message"
                                                      message:@"Please fill values within range"
                                                      delegate:self
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
                            [callAlert show];
                        }
                    } else {
                        UIAlertView *callAlert = [[UIAlertView alloc]
                                                  initWithTitle:@"Efielddata Message"
                                                  message:@"Please fill required fields"
                                                  delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
                        [callAlert show];
                    }
                } else {
                    UIAlertView *callAlert =
                    [[UIAlertView alloc] initWithTitle:@"Efielddata Message"
                                               message:@"Time Spent is required field"
                                              delegate:self
                                     cancelButtonTitle:@"Ok"
                                     otherButtonTitles:nil];
                    [callAlert show];
                }
            } else {
                UIAlertView *callAlert = [[UIAlertView alloc]
                                          initWithTitle:@"Efielddata Message"
                                          message:@"Start/End time are required fields"
                                          delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
                [callAlert show];
            }
        }
    } else {
         [[NSUserDefaults standardUserDefaults] setObject:@"true"
                                                  forKey:@"placement_flag"];
 
        [self performSegueWithIdentifier:@"placementlist_segue" sender:nil];
    }
    //        else {
    //            UIAlertView *callAlert = [[UIAlertView
    //            alloc]initWithTitle:@"Efielddata Message" message:@"No density
    //            test results are added. Minimum one result is needed to complete
    //            the report. Please check." delegate:self cancelButtonTitle:@"Ok"
    //            otherButtonTitles:nil]; [callAlert show];
    //        }
    
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
}

- (void)gestureHandlerMethod1:(UITapGestureRecognizer *)sender {
    
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    [self performSegueWithIdentifier:@"samplesegue" sender:nil];
}
- (void)gestureHandlerMethod:(UITapGestureRecognizer *)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"false"
                                              forKey:@"placement_flag"];
    [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"loc_flag"];
    
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    ChildTestType = dataList[sender.view.tag][@"ChildTestType"];
    ChildTestType_name = dataList[sender.view.tag][@"QuestionValue"];
    if ([ChildTestType isEqualToString:@"ChildTestTypeName"]) {
        // do something here
        [self performSegueWithIdentifier:@"suborderlist_segue" sender:self];
        
    } else if ([ChildTestType isEqualToString:@"Child2TestTypeName"]) {
        // do something here
        [self performSegueWithIdentifier:@"childform_segue" sender:self];
    }
    else if ([ChildTestType isEqualToString:@"Child3TestTypeName"]) {
        // do something here
        [self performSegueWithIdentifier:@"childform_segue" sender:self];
    }
}
- (void)getworkorder_details {
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message"
                          message:
         @"No network, please check your internet connection"];
        return;
    } else {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        NSString *str =
        [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",
         [[NSUserDefaults standardUserDefaults]
          objectForKey:SERVERURL],
         _WorkorderId];
        NSLog(@"url%@",
              [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",
               [[NSUserDefaults standardUserDefaults]
                objectForKey:SERVERURL],
               _WorkorderId]);
        [request setURL:[NSURL URLWithString:str]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/x-www-form-urlencoded"
       forHTTPHeaderField:@"Content-Type"];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData = [NSURLConnection sendSynchronousRequest:request
                                                returningResponse:&response
                                                            error:&error];
        jsonDict = [NSJSONSerialization JSONObjectWithData:urlData
                                                   options:kNilOptions
                                                     error:&error];
        
        dataList = [[NSMutableArray alloc]
                    initWithArray:[jsonDict valueForKeyPath:@"Data.List"]];
        IsAddMoreDisplay =
        [[jsonDict valueForKeyPath:@"Data.IsAddMoreDisplay"] description];
        Is2AddMoreDisplay =
        [[jsonDict valueForKeyPath:@"Data.Is2AddMoreDisplay"] description];
        Is3AddMoreDisplay =
        [[jsonDict valueForKeyPath:@"Data.Is3AddMoreDisplay"] description];
        
        IsStartEndTimeDisplay =
        [[jsonDict valueForKeyPath:@"Data.IsStartEndTimeDisplay"] description];
        iscompleteoriginal =
        [[jsonDict valueForKeyPath:@"Data.IsComplete"] description];
        iscomplete = [[jsonDict valueForKeyPath:@"Data.IsComplete"] description];
        //      float  timespent =  [[jsonDict valueForKeyPath:@"Data.TimeSpent"]f];
        _timespentslider.value =
        [[jsonDict valueForKeyPath:@"Data.TimeSpent"] floatValue];
        DisplayJobDateText =
        [[jsonDict valueForKeyPath:@"Data.JobNumber"] description];
        if ([iscomplete isEqualToString:@"1"]) {
            iscomplete = @"true";
            [_completeswitch setOn:true];
            
            _completeswitch.enabled = false;
            
        } else {
            iscomplete = @"false";
            [_completeswitch setOn:false];
            
            _completeswitch.enabled = true;
        }
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                                 initWithTarget:self
                                                 action:@selector(gestureHandlerMethod:)];
        _locationlink.userInteractionEnabled = YES;
        
        [_locationlink addGestureRecognizer:tapRecognizer];
        _locationlink.tag = 2;
        
        ChildTestTypeId =
        [[jsonDict valueForKeyPath:@"Data.ChildTestTypeId"] description];
        Child2TestTypeId =
        [[jsonDict valueForKeyPath:@"Data.Child2TestTypeId"] description];
        Child3TestTypeId =
        [[jsonDict valueForKeyPath:@"Data.Child3TestTypeId"] description];
        
        TestTypeId = [[jsonDict valueForKeyPath:@"Data.TestTypeId"] description];
        _dividerlbl.hidden = YES;
        _locationlbl.hidden = YES;
        NSMutableArray *tempAry = [[NSMutableArray alloc]
                                   initWithArray:[jsonDict valueForKeyPath:@"Data.List"]];
        NSLog(@"dataList :%@", dataList);
        
        NSMutableArray *imagelist = [[NSMutableArray alloc]
                                     initWithArray:[jsonDict valueForKeyPath:@"Data.ImageDiscriptionList"]];
        NSMutableArray *tempimagelist = [[NSMutableArray alloc]
                                         initWithArray:[jsonDict valueForKeyPath:@"Data.ImageDiscriptionList"]];
        int imagelistindex = 0;
        
        for (NSMutableDictionary *dic in imagelist) {
            NSMutableDictionary *tempDic =
            [[NSMutableDictionary alloc] initWithDictionary:dic];
            [tempDic setObject:@"" forKey:@"NSData_value"];
            [tempimagelist replaceObjectAtIndex:imagelistindex withObject:tempDic];
            
            imagelistindex++;
        }
        // NSData_value
        imagelist = [tempimagelist mutableCopy];
        
        NSData *imgListtdata =
        [NSKeyedArchiver archivedDataWithRootObject:imagelist];
        [[NSUserDefaults standardUserDefaults] setObject:imgListtdata
                                                  forKey:@"imgList"];
        int index = 0;
        
        for (NSDictionary *dic in dataList) {
            
            if ([dic[@"DataType"] isEqualToString:@"Image"]) {
                // ImageDiscriptionList
                
                [[NSUserDefaults standardUserDefaults] setObject:dic[@"TextAnswer"]
                                                          forKey:@"TextAnswer"];
                
            } else if ([dic[@"DataType"] isEqualToString:@"NumericTextBox"]) {
                
                if ([[dic[@"NumericAnswer"] description] isEqualToString:@"-1"]) {
                    
                    NSMutableDictionary *tempDic =
                    [[NSMutableDictionary alloc] initWithDictionary:dic];
                    [tempDic setObject:@"" forKey:@"NumericAnswer"];
                    [tempAry replaceObjectAtIndex:index withObject:tempDic];
                }
            } else if (([dic[@"DataType"] isEqualToString:@"Dropdown"]) ||
                       ([dic[@"DataType"] isEqualToString:@"Radio"])) {
                if ([dic[@"AnswerId"] intValue] != 0) {
                    [tempAry replaceObjectAtIndex:index
                                       withObject:[self singleCheckEditValues:dic]];
                }
            } else if ([dic[@"DataType"] isEqualToString:@"Dropdown(Multiple)"]) {
                NSArray *selectAnswer =
                [[NSArray alloc] initWithArray:dic[@"MultiAnswers"]];
                NSString *questionValue = @"";
                
                for (NSString *answerStr in selectAnswer) {
                    if ([answerStr intValue] != 0) {
                        NSPredicate *predicate = [NSPredicate
                                                  predicateWithFormat:[NSString stringWithFormat:@"AnswerId = %@",
                                                                       answerStr]];
                        NSArray *filteredData =
                        [dic[@"AnswersList"] filteredArrayUsingPredicate:predicate];
                        NSLog(@"Filter : %@", filteredData);
                        if (filteredData.count) {
                            if (questionValue.length)
                                questionValue =
                                [NSString stringWithFormat:@"%@, %@", questionValue,
                                 filteredData[0][@"AnswerValue"]];
                            else
                                questionValue = filteredData[0][@"AnswerValue"];
                        }
                    }
                }
                NSMutableDictionary *tempDic =
                [[NSMutableDictionary alloc] initWithDictionary:dic];
                [tempDic setObject:questionValue forKey:@"QuestionValue"];
                [tempAry replaceObjectAtIndex:index withObject:tempDic];
            } else if ([dic[@"DataType"] isEqualToString:@"Checkbox(Multiple)"]) {
                NSArray *selectAnswer =
                [[NSArray alloc] initWithArray:dic[@"MultiCheckBoxAnswers"]];
                NSString *questionValue = @"";
                
                for (NSString *answerStr in selectAnswer) {
                    if ([answerStr intValue] != 0) {
                        NSPredicate *predicate = [NSPredicate
                                                  predicateWithFormat:[NSString stringWithFormat:@"AnswerId = %@",
                                                                       answerStr]];
                        NSArray *filteredData =
                        [dic[@"AnswersList"] filteredArrayUsingPredicate:predicate];
                        NSLog(@"Filter : %@", filteredData);
                        if (filteredData.count) {
                            if (questionValue.length)
                                questionValue =
                                [NSString stringWithFormat:@"%@, %@", questionValue,
                                 filteredData[0][@"AnswerValue"]];
                            else
                                questionValue = filteredData[0][@"AnswerValue"];
                        }
                    }
                }
                NSMutableDictionary *tempDic =
                [[NSMutableDictionary alloc] initWithDictionary:dic];
                [tempDic setObject:questionValue forKey:@"QuestionValue"];
                [tempAry replaceObjectAtIndex:index withObject:tempDic];
            }
            
            index++;
        }
        if ([IsAddMoreDisplay isEqualToString:@"1"])
            
        {
            // ChildTestTypeName
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
            [tempDic setObject:@"locationlist" forKey:@"DataType"];
            [tempDic setObject:@"ChildTestTypeName" forKey:@"ChildTestType"];
            
            [tempDic
             setObject:@"Please click the link below to add multiple lines of data"
             forKey:@"QuestionDisplayName"];
            [tempDic setObject:[[jsonDict valueForKeyPath:@"Data.ChildTestTypeName"]
                                description]
                        forKey:@"QuestionValue"];
            
           IsChildTestTypeRequired=   [[jsonDict valueForKeyPath:@"Data.IsChildTestTypeRequired"]
                                           boolValue];
            [tempDic setObject:[NSNumber numberWithBool:IsChildTestTypeRequired] forKey:@"IsRequired"];
 
            [tempDic
             setObject:[[jsonDict valueForKeyPath:@"Data.ChildTestTypeNameCount"]
                        description]
             forKey:@"locationCount"];
            
            [tempAry addObject:tempDic];
        }
        if ([Is2AddMoreDisplay isEqualToString:@"1"])
            
        {
            
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
            [tempDic setObject:@"locationlist" forKey:@"DataType"];
            [tempDic setObject:@"Child2TestTypeName" forKey:@"ChildTestType"];
            
            [tempDic
             setObject:@"Please click the link below to add multiple lines of data"
             forKey:@"QuestionDisplayName"];
            [tempDic setObject:[[jsonDict valueForKeyPath:@"Data.Child2TestTypeName"]
                                description]
                        forKey:@"QuestionValue"];
       //   Bo  IsChild2TestTypeRequired =IsChild2TestTypeRequired;
            
       
            IsChild2TestTypeRequired=   [[jsonDict valueForKeyPath:@"Data.IsChild2TestTypeRequired"]
                                             boolValue];
            [tempDic setObject:[NSNumber numberWithBool:IsChild2TestTypeRequired] forKey:@"IsRequired"];
            
            
          
            [tempDic
             setObject:[[jsonDict valueForKeyPath:@"Data.ChildTestTypeNameCount2"]
                        description]
             forKey:@"locationCount"];
            
            [tempAry addObject:tempDic];
        }
        if ([Is3AddMoreDisplay isEqualToString:@"1"])
            
        {
            
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
            [tempDic setObject:@"locationlist" forKey:@"DataType"];
            [tempDic setObject:@"Child3TestTypeName" forKey:@"ChildTestType"];
            
            [tempDic
             setObject:@"Please click the link below to add multiple lines of data"
             forKey:@"QuestionDisplayName"];
            [tempDic setObject:[[jsonDict valueForKeyPath:@"Data.Child3TestTypeName"]
                                description]
                        forKey:@"QuestionValue"];
            
            IsChild3TestTypeRequired=   [[jsonDict valueForKeyPath:@"Data.IsChild3TestTypeRequired"]
                                             boolValue];
            [tempDic setObject:[NSNumber numberWithBool:IsChild3TestTypeRequired] forKey:@"IsRequired"];
            
            
            [tempDic
             setObject:[[jsonDict valueForKeyPath:@"Data.ChildTestTypeNameCount3"]
                        description]
             forKey:@"locationCount"];
            
            [tempAry addObject:tempDic];
        }
        
        if ([TestTypeId isEqualToString:@"1011"]) {
            [[NSUserDefaults standardUserDefaults]
             setObject:[jsonDict valueForKeyPath:@"Data.SampleIdArray"]
             forKey:@"sampled_array"];
            [[NSUserDefaults standardUserDefaults]
             setObject:[jsonDict valueForKeyPath:@"Data.SampleSourceText"]
             forKey:@"SampleSourceText"];
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
            [tempDic setObject:@"sampledata" forKey:@"DataType"];
            
            [tempDic setObject:@"Sample Type" forKey:@"QuestionDisplayName"];
            [tempDic setObject:[[jsonDict valueForKeyPath:@"Data.SampleSourceText"]
                                description]
                        forKey:@"QuestionValue"];
            
            [tempAry addObject:tempDic];
        }
        
        IsDisplayPlacement =
        [[jsonDict valueForKeyPath:@"Data.IsDisplayPlacement"] description];
        if ([IsDisplayPlacement isEqualToString:@"1"]) {
            
            // ChildTestTypeName
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
            [tempDic setObject:@"placementlist" forKey:@"DataType"];
            // [tempDic setObject: @"ChildTestTypeName"  forKey:@"ChildTestType"];
            
            [tempDic setObject:@"Please click the link add placement data"
                        forKey:@"QuestionDisplayName"];
            NSArray *ConcreteWorkOrderList = [[NSArray alloc] init];
            ConcreteWorkOrderList =
            [jsonDict valueForKeyPath:@"Data.ConcreteWorkOrderList"];
            [tempDic
             setObject:[NSString stringWithFormat:@"(%d item(s) added)",
                        [ConcreteWorkOrderList count]]
             forKey:@"QuestionValue"];
            
            [tempAry addObject:tempDic];
        }
        // IsStartEndTimeDisplay=@"0";
        if ([IsStartEndTimeDisplay isEqualToString:@"1"])
            
        {
            
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
            [tempDic setObject:@"start_endtime" forKey:@"DataType"];
            [tempDic setObject:[[jsonDict valueForKeyPath:@"Data.StartTimeIosText"]
                                description]
                        forKey:@"StartTime"];
            [tempDic
             setObject:[[jsonDict valueForKeyPath:@"Data.DepartureTimeIosText"]
                        description]
             forKey:@"DepartureTime"];
            [tempDic
             setObject:[[jsonDict valueForKeyPath:@"Data.TimeSpent"] description]
             forKey:@"TimeSpent"];
            //    [tempDic setObject:1 forKey:@"IsRequired"];
            start_timestr =
            [[jsonDict valueForKeyPath:@"Data.StartTimeIosText"] description];
            end_timestr =
            [[jsonDict valueForKeyPath:@"Data.DepartureTimeIosText"] description];
            timespent_str =
            [[jsonDict valueForKeyPath:@"Data.TimeSpent"] description];
            timespent_save =
            [[jsonDict valueForKeyPath:@"Data.TimeSpent"] description];
            if ([timespent_str rangeOfString:@"."].location == NSNotFound) {
                
                timespent_str =
                [NSString stringWithFormat:@"%@ hrs 00 mins", timespent_str];
            }
            timespent_str =
            [timespent_str stringByReplacingOccurrencesOfString:@".5"
                                                     withString:@" hrs 30 mins"];
            
            timespent_str =
            [timespent_str stringByReplacingOccurrencesOfString:@".25"
                                                     withString:@" hrs 15 mins"];
            timespent_str =
            [timespent_str stringByReplacingOccurrencesOfString:@".75"
                                                     withString:@" hrs 45 mins"];
            
            if ([timespent_str length] == 1) {
                timespent_str =
                [NSString stringWithFormat:@"%@ hrs 00 mins", timespent_str];
            }
            [tempAry addObject:tempDic];
            
        } else {
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
            [tempDic setObject:@"TimeSpent" forKey:@"DataType"];
            
            [tempDic
             setObject:[[jsonDict valueForKeyPath:@"Data.TimeSpent"] description]
             forKey:@"TimeSpent"];
            timespent_str =
            [[jsonDict valueForKeyPath:@"Data.TimeSpent"] description];
            timespent_save =
            [[jsonDict valueForKeyPath:@"Data.TimeSpent"] description];
            
            // if (![timespent_str i) {
            
            if ([timespent_str rangeOfString:@"."].location == NSNotFound) {
                
                timespent_str =
                [NSString stringWithFormat:@"%@ hrs 00 mins", timespent_str];
            }
            timespent_str =
            [timespent_str stringByReplacingOccurrencesOfString:@".5"
                                                     withString:@" hrs 30 mins"];
            
            timespent_str =
            [timespent_str stringByReplacingOccurrencesOfString:@".25"
                                                     withString:@" hrs 15 mins"];
            timespent_str =
            [timespent_str stringByReplacingOccurrencesOfString:@".75"
                                                     withString:@" hrs 45 mins"];
            
            [tempAry addObject:tempDic];
        }
        
        dataList = [tempAry mutableCopy];
        [_tblForm reloadData];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }
}

- (void)getsubworkorder_count {
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message"
                          message:
         @"No network, please check your internet connection"];
        return;
    } else {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        NSString *str =
        
        //[NSString
        // stringWithFormat:@"%@GetIOSEditAssessment?AssessmentMasterid=%@&NoteId=%@",[[NSUserDefaults
        // standardUserDefaults]
        // objectForKey:SERVERURL],_assessmentMasterId,_WorkorderId];
        [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",
         [[NSUserDefaults standardUserDefaults]
          objectForKey:SERVERURL],
         _WorkorderId];
        NSLog(@"url%@",
              [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",
               [[NSUserDefaults standardUserDefaults]
                objectForKey:SERVERURL],
               _WorkorderId]);
        [request setURL:[NSURL URLWithString:str]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/x-www-form-urlencoded"
       forHTTPHeaderField:@"Content-Type"];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData = [NSURLConnection sendSynchronousRequest:request
                                                returningResponse:&response
                                                            error:&error];
        jsonDict = [NSJSONSerialization JSONObjectWithData:urlData
                                                   options:kNilOptions
                                                     error:&error];
        IsAddMoreDisplay =
        [[jsonDict valueForKeyPath:@"Data.IsAddMoreDisplay"] description];
        Is2AddMoreDisplay =
        [[jsonDict valueForKeyPath:@"Data.Is2AddMoreDisplay"] description];
        
        NSMutableArray *tempAry = [[NSMutableArray alloc] initWithArray:dataList];
        
        int index = 0;
        
        for (NSDictionary *dic in dataList) {
            
            if ([dic[@"DataType"] isEqualToString:@"locationlist"] &&
                [dic[@"ChildTestType"] isEqualToString:@"ChildTestTypeName"]) {
                
                // ChildTestTypeName
                NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
                [tempDic setObject:@"locationlist" forKey:@"DataType"];
                [tempDic setObject:@"ChildTestTypeName" forKey:@"ChildTestType"];
                
                [tempDic
                 setObject:
                 @"Please click the link below to add multiple lines of data"
                 forKey:@"QuestionDisplayName"];
                [tempDic setObject:[[jsonDict valueForKeyPath:@"Data.ChildTestTypeName"]
                                    description]
                            forKey:@"QuestionValue"];
                
                
                IsChildTestTypeRequired=   [[jsonDict valueForKeyPath:@"Data.IsChildTestTypeRequired"]
                                            boolValue];
                [tempDic setObject:[NSNumber numberWithBool:IsChildTestTypeRequired] forKey:@"IsRequired"];
                
                [tempDic
                 setObject:[[jsonDict valueForKeyPath:@"Data.ChildTestTypeNameCount"]
                            description]
                 forKey:@"locationCount"];
                
                [tempAry replaceObjectAtIndex:index withObject:tempDic];
            }
            if ([dic[@"DataType"] isEqualToString:@"locationlist"] &&
                [dic[@"ChildTestType"] isEqualToString:@"Child2TestTypeName"]) {
                
                NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
                [tempDic setObject:@"locationlist" forKey:@"DataType"];
                [tempDic setObject:@"Child2TestTypeName" forKey:@"ChildTestType"];
                
                [tempDic
                 setObject:
                 @"Please click the link below to add multiple lines of data"
                 forKey:@"QuestionDisplayName"];
                [tempDic
                 setObject:[[jsonDict valueForKeyPath:@"Data.Child2TestTypeName"]
                            description]
                 forKey:@"QuestionValue"];
                
                
                IsChild2TestTypeRequired=   [[jsonDict valueForKeyPath:@"Data.IsChild2TestTypeRequired"]
                                             boolValue];
                [tempDic setObject:[NSNumber numberWithBool:IsChild2TestTypeRequired] forKey:@"IsRequired"];
                
                [tempDic setObject:[[jsonDict
                                     valueForKeyPath:@"Data.ChildTestTypeNameCount2"]
                                    description]
                            forKey:@"locationCount"];
                
                [tempAry replaceObjectAtIndex:index withObject:tempDic];
            }
            
       
        
        if ([dic[@"DataType"] isEqualToString:@"locationlist"] &&
            [dic[@"ChildTestType"] isEqualToString:@"Child3TestTypeName"]) {
            
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
            [tempDic setObject:@"locationlist" forKey:@"DataType"];
            [tempDic setObject:@"Child3TestTypeName" forKey:@"ChildTestType"];
            
            [tempDic
             setObject:
             @"Please click the link below to add multiple lines of data"
             forKey:@"QuestionDisplayName"];
            [tempDic
             setObject:[[jsonDict valueForKeyPath:@"Data.Child3TestTypeName"]
                        description]
             forKey:@"QuestionValue"];
            
            IsChild3TestTypeRequired=   [[jsonDict valueForKeyPath:@"Data.IsChild3TestTypeRequired"]
                                         boolValue];
            [tempDic setObject:[NSNumber numberWithBool:IsChild3TestTypeRequired] forKey:@"IsRequired"];
            
            [tempDic setObject:[[jsonDict
                                 valueForKeyPath:@"Data.ChildTestTypeNameCount3"]
                                description]
                        forKey:@"locationCount"];
            
            [tempAry replaceObjectAtIndex:index withObject:tempDic];
        }
        
        index++;
    }
        
        dataList = [tempAry mutableCopy];
        [_tblForm reloadData];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }
}
- (NSDictionary *)singleCheckEditValues:(NSDictionary *)dic {
    NSPredicate *predicate = [NSPredicate
                              predicateWithFormat:[NSString
                                                   stringWithFormat:@"AnswerId = %@",
                                                   [dic[@"AnswerId"] description]]];
    NSArray *filteredData =
    [dic[@"AnswersList"] filteredArrayUsingPredicate:predicate];
    NSLog(@"Filter : %@", filteredData);
    if (filteredData.count) {
        NSMutableDictionary *tempDic =
        [[NSMutableDictionary alloc] initWithDictionary:dic];
        [tempDic setObject:filteredData[0][@"AnswerValue"] forKey:@"QuestionValue"];
        return tempDic;
    }
    return dic;
}

- (void)updateSelectedItems:(NSDictionary *)updatedItem
                 questionID:(NSString *)questionID {
    NSPredicate *predicate = [NSPredicate
                              predicateWithFormat:[NSString
                                                   stringWithFormat:@"QuestionId = %@", questionID]];
    NSArray *filteredData = [dataList filteredArrayUsingPredicate:predicate];
    NSLog(@"Filter : %@", filteredData);
    if (filteredData.count) {
        
        NSInteger index = [dataList indexOfObject:filteredData[0]];
        NSLog(@"Index : %ld", (long)index);
        [dataList replaceObjectAtIndex:index withObject:updatedItem];
        [_tblForm reloadData];
    }
    NSLog(@"Came");
    NSLog(@"dataList : %@", dataList);
}

- (CGFloat)calculateStringHeigth:(CGSize)size
                            text:(NSString *)text
                         andFont:(UIFont *)font {
    NSLog(@"text : %@", text);
    
    NSLog(@"Font : %@", font);
    NSLog(@"size : %@", NSStringFromCGSize(size));
    
    if ([text isEqual:[NSNull null]]) {
        text = @"";
    }
    
    if (![text length])
        return 0;
    
    CGSize labelSize = [text sizeWithFont:font
                        constrainedToSize:size
                            lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat labelHeight = labelSize.height;
    NSLog(@"labelHeight : %f", labelHeight);
    return labelHeight;
}

- (NSMutableAttributedString *)AddRequiredField:(NSString *)title
                                  andIsRequired:(BOOL)required {
    
    if (required) {
        NSString *string = [NSString stringWithFormat:@"%@ *", title];
        NSMutableAttributedString *attString =
        [[NSMutableAttributedString alloc] initWithString:string];
        NSRange range = [string rangeOfString:@"*"];
        [attString addAttribute:NSForegroundColorAttributeName
                          value:[UIColor redColor]
                          range:range];
        return attString;
    } else {
        NSMutableAttributedString *attString =
        [[NSMutableAttributedString alloc] initWithString:title];
        return attString;
    }
}

#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    
    return dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // if(dataList.count<indexPath.row){'
    
    if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"start_endtime"]) {
        
        startend_time *cell =
        [tableView dequeueReusableCellWithIdentifier:@"startendtime_cell"];
        cell.start_timelbl.attributedText = [self AddRequiredField:@"Start Time"
                                                     andIsRequired:true];
        
        if ([iscompleteoriginal isEqualToString:@"1"]) {
            cell.end_timelbl.attributedText = [self AddRequiredField:@"End Time"
                                                       andIsRequired:true];
        } else {
            
            cell.end_timelbl.text = @"End Time";
        }
        cell.start_timevalue.text = start_timestr;
        
        cell.end_timevalue.text = end_timestr;
        cell.startend_timespent.text = timespent_str;
        UITapGestureRecognizer *startRecognizer = [[UITapGestureRecognizer alloc]
                                                   initWithTarget:self
                                                   action:@selector(startime_gesture:)];
        cell.start_timeview.userInteractionEnabled = YES;
        startRecognizer.numberOfTapsRequired = 1;
        
        cell.start_timeview.tag = indexPath.row;
        
        [cell.start_timeview addGestureRecognizer:startRecognizer];
        
        cell.start_timevalue.tag = indexPath.row;
        UITapGestureRecognizer *endRecognizer = [[UITapGestureRecognizer alloc]
                                                 initWithTarget:self
                                                 action:@selector(endime_gesture:)];
        cell.end_timeview.userInteractionEnabled = YES;
        endRecognizer.numberOfTapsRequired = 1;
        cell.end_timeview.tag = indexPath.row;
        [cell.end_timeview addGestureRecognizer:endRecognizer];
        
        cell.end_timevalue.tag = indexPath.row;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"TimeSpent"]) {
        
        DateAndTimeTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"
                                        forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[DateAndTimeTableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:@"DateAndTimeCell"];
        }
        
        cell.lblValue.tag = indexPath.row;
        //                if([NSString
        //                stringWithFormat:@"%.f",[dataList[indexPath.row][@"TimeSpent"]
        //                floatValue]] == [NSNull null]||[[NSString
        //                stringWithFormat:@"%.f",[dataList[indexPath.row][@"TimeSpent"]
        //                floatValue]] isEqualToString:@""]){ }else{
        cell.lblValue.text = timespent_str;
        //  }
        cell.lblTitle.attributedText = [self AddRequiredField:@"Time Spent"
                                                andIsRequired:true];
        
        // cell.txtValue.text = [NSString
        // stringWithFormat:@"%.f",[dataList[indexPath.row][@"TimeSpent"]
        // floatValue]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"sampledata"]) {
        MultySelectionTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
        if (_isFromEditNote && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
        //     [cell.lblTitle sizeToFit];
        
        cell.lblValue.lineBreakMode = UILineBreakModeWordWrap;
        
        cell.lblValue.numberOfLines = 0;
        [cell.lblValue sizeToFit];
        cell.lblTitle.text = dataList[indexPath.row][@"QuestionDisplayName"];
        
        //        UILabel *textLabel = [cell lblValue];
        //        CGSize size = [ dataList[indexPath.row][@"QuestionValue"]
        //        sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:14.0]
        //                       constrainedToSize:CGSizeMake(240.0, 480.0)
        //                           lineBreakMode:UILineBreakModeWordWrap];
        //
        //        cell.textLabel.frame = CGRectMake(0, 0, size.width + 20,
        //        size.height + 20);
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"SampleSourceText"]
             isEqualToString:@"none"]) {
            
            sampled_vale = @"";
            NSArray *sampled_array = [[NSArray alloc] init];
            sampled_array = [[NSUserDefaults standardUserDefaults]
                             valueForKey:@"sampled_array_name"];
            if ([sampled_array count] > 0) {
                for (int i = 0; i < [sampled_array count]; i++) {
                    if ([sampled_vale length] > 0) {
                        sampled_vale =
                        [NSString stringWithFormat:@"%@, %@", sampled_vale,
                         [sampled_array objectAtIndex:i]];
                        
                    } else {
                        sampled_vale = [sampled_array objectAtIndex:i];
                    }
                }
                cell.lblValue.text = sampled_vale;
            } else {
                cell.lblValue.text = @"";
            }
        } else {
            cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
            
            // _sampled_date.text=[[NSUserDefaults standardUserDefaults]
            // valueForKey:@"SampleSourceText"];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"placementlist"]) {
        
        //@"Field Image Attachments";
        
        static NSString *simpleTableIdentifier = @"placementcell";
        
        PlacementTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        [cell.contentView clearsContextBeforeDrawing];
        
        if (cell == nil) {
            cell = [[PlacementTableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:simpleTableIdentifier];
        }
        [cell.lblTitle sizeToFit];
        cell.lblTitle.numberOfLines = 0;
        cell.lblTitle.text = dataList[indexPath.row][@"QuestionDisplayName"];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                                 initWithTarget:self
                                                 action:@selector(placement_gesture:)];
        
        cell.lblValue.tag = indexPath.row;
        cell.lbl_count.text = dataList[indexPath.row][@"QuestionValue"];
        [tapRecognizer setNumberOfTapsRequired:1];
        [cell.lblValue setUserInteractionEnabled:YES];
        
        [cell.lblValue addGestureRecognizer:tapRecognizer];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        return cell;
        
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"locationlist"]) {
        
        //@"Field Image Attachments";
        
        static NSString *simpleTableIdentifier = @"location2cell";
        
        LocationTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        [cell.contentView clearsContextBeforeDrawing];
        
        if (cell == nil) {
            cell = [[LocationTableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:simpleTableIdentifier];
        }
        [cell.lblTitle sizeToFit];
        cell.lblTitle.numberOfLines = 0;
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"]
                                        andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
        //dataList[indexPath.row][@"QuestionDisplayName"];
        cell.lbl_count.text = dataList[indexPath.row][@"locationCount"];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                                 initWithTarget:self
                                                 action:@selector(gestureHandlerMethod:)];
        
        cell.lblValue.tag = indexPath.row;
        cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
        [tapRecognizer setNumberOfTapsRequired:1];
        [cell.lblValue setUserInteractionEnabled:YES];
        
        [cell.lblValue addGestureRecognizer:tapRecognizer];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        return cell;
        
    } else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Image"]) {
        
        //@"Field Image Attachments";
        
        static NSString *simpleTableIdentifier = @"imageviewcell";
        
        imageviewTableviewcell *cell =
        [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        [cell.contentView clearsContextBeforeDrawing];
        
        if (cell == nil) {
            cell = [[imageviewTableviewcell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:simpleTableIdentifier];
        }
        [cell.ttbimage_lbl sizeToFit];
        cell.ttbimage_lbl.numberOfLines = 0;
        cell.ttbimage_lbl.text = dataList[indexPath.row][@"QuestionDisplayName"];
        imgListdata =
        [[NSUserDefaults standardUserDefaults] objectForKey:@"imgList"];
        
        imgListList = [NSKeyedUnarchiver unarchiveObjectWithData:imgListdata];
        
        cell.ttb_count.text =
        [NSString stringWithFormat:@"(%lu item(s) added)",
         (unsigned long)[imgListList count]];
        
        UITapGestureRecognizer *viewimag_tap = [[UITapGestureRecognizer alloc]
                                                initWithTarget:self
                                                action:@selector(viewimag_taping:)];
        cell.ttbimage_txt.tag = indexPath.row;
        [viewimag_tap setNumberOfTapsRequired:1];
        [cell.ttbimage_txt setUserInteractionEnabled:YES];
        
        [cell.ttbimage_txt addGestureRecognizer:viewimag_tap];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        return cell;
        
    }
    
    else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Category"]) {
        DescriptionTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
        
        //   cell.lblTitle.text = dataList[indexPath.row][@"CategoryName"];
        
        //  if(dataList[indexPath.row][@"CategoryDescription"] != [NSNull null])
        [cell.lblTitle sizeToFit];
        cell.lblTitle.numberOfLines = 1;
        [cell.lblDiscribtion sizeToFit];
        cell.lblDiscribtion.numberOfLines = 0;
        
        if ([dataList[indexPath.row][@"CategoryName"]
             isKindOfClass:[NSNull class]]) {
            cell.lblTitle.hidden = YES;
            
        } else {
            cell.lblTitle.hidden = NO;
            cell.lblTitle.text = dataList[indexPath.row][@"CategoryName"];
        }
        if ([dataList[indexPath.row][@"CategoryDescription"]
             isKindOfClass:[NSNull class]]) {
            cell.lblDiscribtion.hidden = YES;
            
        } else {
            cell.lblDiscribtion.hidden = NO;
            cell.lblDiscribtion.text =
            dataList[indexPath.row][@"CategoryDescription"];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"NumericTextBox"])
        //{
        
    {
        TextFieldTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
        // if(_isFromEditNote && !_IsEditNote)
        //  cell.userInteractionEnabled = NO;
        if (cell == nil) {
            cell = [[TextFieldTableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:@"TextFieldCell"];
        }
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"]
                                        andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
        [cell.lblTitle sizeToFit];
        cell.lblTitle.numberOfLines = 0;
        [cell.lblTitle setNeedsDisplay];
        cell.txtValue.delegate = self;
        cell.txtValue.tag = indexPath.row;
        [cell.txtValue setKeyboardType:UIKeyboardTypeDecimalPad];
        
        if ([dataList[indexPath.row][@"NumericAnswer"] floatValue] <= 0) { // }
            cell.txtValue.placeholder = @"Type here ...";
            
            cell.txtValue.text = @"";
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]
                                        initWithDictionary:dataList[indexPath.row]];
            [dic setObject:@"" forKey:@"NumericAnswer"];
            NSLog(@"dic : %@", dic);
            [dataList replaceObjectAtIndex:indexPath.row withObject:dic];
        } else {
            
            cell.txtValue.text = [NSString
                                  stringWithFormat:@"%.f", [dataList[indexPath.row][@"NumericAnswer"]
                                                            floatValue]];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.txtValue addTarget:self
                          action:@selector(textFieldDidEndEditing:)
                forControlEvents:UIControlEventEditingChanged];
        
        return cell;
    }
    
    else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown"]) {
        MultySelectionTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
        
        if (cell == nil) {
            cell = [[MultySelectionTableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:@"MultySelectionCell"];
        }
        if (_isFromEditNote && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"]
                                        andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
        
        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
        //         [cell.lblValue sizeToFit];
        //        cell.lblValue.numberOfLines = 0;
        //        [  cell.lblValue setNeedsDisplay];
        //
        if ([dataList[indexPath.row][@"QuestionValue"] isEqualToString:@""]) {
            cell.lblValue.text = @"";
        } else {
            cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
        }
        [cell.lblValue sizeToFit];
        cell.lblValue.numberOfLines = 0;
        [cell.lblValue setNeedsDisplay];
        //
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"Checkbox"]) {
        SwitchTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
        if (_isFromEditNote && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"]
                                        andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
        [cell.switchValue addTarget:self
                             action:@selector(switchValueChanges:)
                   forControlEvents:UIControlEventValueChanged];
        cell.switchValue.tag = indexPath.row;
        [cell.switchValue
         setOn:[dataList[indexPath.row][@"BooleanAnswer"] boolValue]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"TextBoxMultiline"]) {
        TextViewTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"TextViewCell"];
        if (cell == nil) {
            cell = [[TextViewTableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:@"TextViewCell"];
        }
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"]
                                        andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
        [cell.lblTitle sizeToFit];
        cell.lblTitle.numberOfLines = 0;
        [cell.lblTitle setNeedsDisplay];
        [cell.txtViewValue setKeyboardType:UIKeyboardTypeAlphabet];
        
        cell.txtViewValue.delegate = self;
        [cell.txtViewValue sizeToFit];
        cell.txtViewValue.tag = indexPath.row;
        if (dataList[indexPath.row][@"TextAnswer"] == [NSNull null] ||
            [dataList[indexPath.row][@"TextAnswer"] isEqualToString:@""]) {
            cell.txtViewValue.text = @"Type here ...";
            cell.txtViewValue.textColor = [UIColor lightGrayColor];
        } else if (dataList[indexPath.row][@"TextAnswer"] != [NSNull null]) {
            cell.txtViewValue.text = dataList[indexPath.row][@"TextAnswer"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"TextBox"]) {
        TextFieldTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
        if (cell == nil) {
            cell = [[TextFieldTableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:@"TextFieldCell"];
        }
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"]
                                        andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
        cell.txtValue.delegate = self;
        cell.txtValue.tag = indexPath.row;
        [cell.txtValue setKeyboardType:UIKeyboardTypeAlphabet];
        //
        [cell.txtValue addTarget:self
                          action:@selector(textFieldDidEndEditing:)
                forControlEvents:UIControlEventEditingChanged];
        if (dataList[indexPath.row][@"TextAnswer"] == [NSNull null] ||
            [dataList[indexPath.row][@"TextAnswer"] isEqualToString:@""]) {
            cell.txtValue.placeholder = @"Type here ...";
            cell.txtValue.text = @"";
            
        } else {
            cell.txtValue.text = dataList[indexPath.row][@"TextAnswer"];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"Dropdown(Multiple)"]) {
        MultySelectionTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
        if (_isFromEditNote && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"]
                                        andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
        //     [cell.lblTitle sizeToFit];
        
        cell.lblValue.lineBreakMode = UILineBreakModeTailTruncation;
        
        cell.lblValue.numberOfLines = 0;
        [cell.lblValue sizeToFit];
        cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"Checkbox(Multiple)"]) {
        
        NSMutableDictionary *dic1 = [[NSMutableDictionary alloc]
                                     initWithDictionary:dataList[indexPath.row]];
        
        NSMutableArray *selectedItem1 = [[NSMutableArray alloc] init];
        [dic1 setObject:selectedItem1 forKey:@"MultiSelectCheckBoxAnswers"];
        [dataList replaceObjectAtIndex:indexPath.row withObject:dic1];
        
        MultySelectionTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
        if (_isFromEditNote && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        //  cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"]
                                        andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
        cell.lblValue.lineBreakMode = UILineBreakModeTailTruncation;
        
        cell.lblValue.numberOfLines = 0;
        [cell.lblValue sizeToFit];
        
        cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    } else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Date"]) {
        DateAndTimeTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"
                                        forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[DateAndTimeTableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:@"DateAndTimeCell"];
        }
        if (_isFromEditNote && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        cell.lblValue.tag = indexPath.row;
        if (dataList[indexPath.row][@"DateAnswerText"] == [NSNull null] ||
            [dataList[indexPath.row][@"DateAnswerText"] isEqualToString:@""]) {
            cell.lblValue.text = @"";
        } else {
            cell.lblValue.text = dataList[indexPath.row][@"DateAnswerText"];
        }
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"]
                                        andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Radio"]) {
        RadioTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"radiocell"];
        
        if (cell == nil) {
            cell =
            [[RadioTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:@"radiocell"];
        }
        if (_isFromEditNote && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        //  cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"]
                                        andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
        NSString *selectedbtn = [dataList[indexPath.row][@"AnswerId"] description];
        NSArray *radiolist = dataList[indexPath.row][@"AnswersList"];
        NSLog(@"count%d", [radiolist count]);
        [cell.button3 setSelected:YES];
        
        [cell.button3 setSelected:NO];
        if ([radiolist count] != 3) {
            
            cell.button3.hidden = YES;
        } else {
            cell.button3.hidden = NO;
        }
        if ([radiolist count] <= 2) {
            
            for (int i = 0; i < [radiolist count]; i++) {
                if (i == 0) {
                    if ([selectedbtn
                         isEqualToString:[radiolist[0][@"AnswerId"] description]]) {
                        
                        [cell.button1 setSelected:YES];
                        
                    } else {
                        [cell.button1 setSelected:NO];
                    }
                    [cell.button1 setTitle:radiolist[0][@"AnswerValue"]
                                  forState:UIControlStateNormal];
                    cell.button1.tag = indexPath.row;
                    [cell.button1 setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                                green:110 / 255.0f
                                                                 blue:255 / 255.0f
                                                                alpha:1.0]
                                       forState:UIControlStateNormal];
                } else if (i == 1) {
                    if ([selectedbtn
                         isEqualToString:[radiolist[1][@"AnswerId"] description]]) {
                        
                        [cell.button2 setSelected:YES];
                        
                    } else {
                        [cell.button2 setSelected:NO];
                    }
                    [cell.button2 setTitle:radiolist[1][@"AnswerValue"]
                                  forState:UIControlStateNormal];
                    // [    cell.button2 setTitle:
                    [cell.button2 setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                                green:110 / 255.0f
                                                                 blue:255 / 255.0f
                                                                alpha:1.0]
                                       forState:UIControlStateNormal];
                    cell.button2.tag = indexPath.row;
                }
            }
        } else if ([radiolist count] > 2) {
            // cell.button3.hidden=NO;
            
            for (int i = 0; i < [radiolist count]; i++) {
                if (i == 0) {
                    if ([selectedbtn
                         isEqualToString:[radiolist[0][@"AnswerId"] description]]) {
                        
                        [cell.button1 setSelected:YES];
                        
                    } else {
                        [cell.button1 setSelected:NO];
                    }
                    [cell.button1 setTitle:radiolist[0][@"AnswerValue"]
                                  forState:UIControlStateNormal];
                    cell.button1.tag = indexPath.row;
                    // cell.button1.text=radiolist[i][@"AnswerValue"];
                } else if (i == 1) {
                    if ([selectedbtn
                         isEqualToString:[radiolist[1][@"AnswerId"] description]]) {
                        
                        [cell.button2 setSelected:YES];
                        
                    } else {
                        [cell.button2 setSelected:NO];
                    }
                    [cell.button2 setTitle:radiolist[1][@"AnswerValue"]
                                  forState:UIControlStateNormal];
                    //  cell.button1.titleLabel.text=radiolist[i][@"AnswerValue"];
                    cell.button2.tag = indexPath.row;
                }
                
                else if (i == 2) {
                    if ([selectedbtn
                         isEqualToString:[radiolist[2][@"AnswerId"] description]]) {
                        
                        [cell.button3 setSelected:YES];
                        
                    } else {
                        [cell.button3 setSelected:NO];
                    }
                    [cell.button3 setTitle:radiolist[2][@"AnswerValue"]
                                  forState:UIControlStateNormal];
                    // cell.button1.titleLabel.text=radiolist[i][@"AnswerValue"];
                    [cell.button3 setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                                green:110 / 255.0f
                                                                 blue:255 / 255.0f
                                                                alpha:1.0]
                                       forState:UIControlStateNormal];
                    cell.button3.tag = indexPath.row;
                }
            }
        }
        [cell.button1 setTitleColor:[UIColor blackColor]
                           forState:UIControlStateNormal];
        [cell.button2 setTitleColor:[UIColor blackColor]
                           forState:UIControlStateNormal];
        
        [cell.button3 setTitleColor:[UIColor blackColor]
                           forState:UIControlStateNormal];
        [cell.button1 addTarget:self
                         action:@selector(logSelectedButton:)
               forControlEvents:UIControlEventTouchUpInside];
        [cell.button2 addTarget:self
                         action:@selector(logSelectedButton:)
               forControlEvents:UIControlEventTouchUpInside];
        [cell.button3 addTarget:self
                         action:@selector(logSelectedButton:)
               forControlEvents:UIControlEventTouchUpInside];
        
        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        return cell;
    } else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Time"]) {
        
        DateAndTimeTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"
                                        forIndexPath:indexPath];
        
        if (cell == nil) {
            cell = [[DateAndTimeTableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:@"DateAndTimeCell"];
        }
        if (_isFromEditNote && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        cell.lblValue.tag = indexPath.row;
        if (dataList[indexPath.row][@"TimeAnswerText"] == [NSNull null] ||
            [dataList[indexPath.row][@"TimeAnswerText"] isEqualToString:@""]) {
            cell.lblValue.text = @"";
        } else {
            cell.lblValue.text = dataList[indexPath.row][@"TimeAnswerText"];
        }
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"]
                                        andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    return nil;
}
- (IBAction)logSelectedButton:(DLRadioButton *)radioButton {
    if (radioButton.isMultipleSelectionEnabled) {
        for (DLRadioButton *button in radioButton.selectedButtons) {
            NSLog(@"%@ is selected.\n", button.titleLabel.text);
        }
    } else {
        [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                    green:110 / 255.0f
                                                     blue:255 / 255.0f
                                                    alpha:1.0]
                           forState:UIControlStateNormal];
        self.saveBtn.enabled = YES;
        NSLog(@"%@ is selected.\n", radioButton.selectedButton.titleLabel.text);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]
                                    initWithDictionary:dataList[radioButton.tag]];
        [dic setObject:radioButton.selectedButton.titleLabel.text
                forKey:@"QuestionValue"];
        
        NSArray *radiolist = dataList[radioButton.tag][@"AnswersList"];
        
        for (int i = 0; i < [radiolist count]; i++) {
            if ([[radiolist[i][@"AnswerValue"] description]
                 isEqualToString:radioButton.selectedButton.titleLabel.text]) {
                [dic setObject:radiolist[i][@"AnswerId"] forKey:@"AnswerId"];
            }
        }
        NSLog(@"dic : %@", dic);
        NSPredicate *predicate = [NSPredicate
                                  predicateWithFormat:[NSString
                                                       stringWithFormat:@"QuestionId = %@",
                                                       dataList[radioButton.tag]
                                                       [@"QuestionId"]]];
        NSArray *filteredData = [dataList filteredArrayUsingPredicate:predicate];
        NSLog(@"Filter : %@", filteredData);
        if (filteredData.count) {
            
            NSInteger index = [dataList indexOfObject:filteredData[0]];
            NSLog(@"Index : %ld", (long)index);
            [dataList replaceObjectAtIndex:index withObject:dic];
        }
        NSLog(@"Came");
        NSLog(@"dataList : %@", dataList);
        
        NSLog(@"dic : %@", dataList);
    }
}

- (void)sliderValueChanges:(id)sender {
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    UISlider *slider = (UISlider *)sender;
    NSMutableDictionary *dic =
    [[NSMutableDictionary alloc] initWithDictionary:dataList[slider.tag]];
    [dic setObject:[NSString stringWithFormat:@"%.f", slider.value]
            forKey:@"NumericAnswer"];
    NSLog(@"dic : %@", dic);
    [dataList replaceObjectAtIndex:slider.tag withObject:dic];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:slider.tag inSection:0];
    
    SliderTableViewCell *cell = [_tblForm cellForRowAtIndexPath:indexPath];
    cell.txtValue.text = [NSString stringWithFormat:@"%.f", slider.value];
}
- (void)switchValueChanges:(id)sender {
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    UIButton *btn = (UIButton *)sender;
    
    NSMutableDictionary *dic =
    [[NSMutableDictionary alloc] initWithDictionary:dataList[btn.tag]];
    [dic setObject:[sender isOn] ? [NSNumber numberWithBool:true]
                  : [NSNumber numberWithBool:false]
            forKey:@"BooleanAnswer"];
    NSLog(@"dic : %@", dic);
    [dataList replaceObjectAtIndex:btn.tag withObject:dic];
}

- (IBAction)textFieldDidBeginEditing:(UITextField *)textField {
    editingIndexPath = [NSIndexPath indexPathForRow:textField.tag inSection:0];
}

- (void)textViewDidChange:(UITextView *)textView {
    
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    
    if (textView.text.length == 0) {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = @"Type here ...";
        NSLog(@"textView : %ld", (long)textView.tag);
        NSMutableDictionary *dic =
        [[NSMutableDictionary alloc] initWithDictionary:dataList[textView.tag]];
        [dic setObject:@"" forKey:@"TextAnswer"];
        NSLog(@"dic : %@", dic);
        [dataList replaceObjectAtIndex:textView.tag withObject:dic];
        
        [textView resignFirstResponder];
    }
    
    else {
        NSLog(@"textView : %ld", (long)textView.tag);
        NSMutableDictionary *dic =
        [[NSMutableDictionary alloc] initWithDictionary:dataList[textView.tag]];
        [dic setObject:textView.text forKey:@"TextAnswer"];
        NSLog(@"dic : %@", dic);
        [dataList replaceObjectAtIndex:textView.tag withObject:dic];
    }
    self.saveBtn.enabled = YES;
}

//-(void) textFieldDidChange:(UITextField *)textField'

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([dataList[textField.tag][@"DataType"]
         isEqualToString:@"NumericTextBox"]) {
        
        [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                    green:110 / 255.0f
                                                     blue:255 / 255.0f
                                                    alpha:1.0]
                           forState:UIControlStateNormal];
        self.saveBtn.enabled = YES;
        //  [dataList[indexPath.row][@"MaxValue"] floatValue];
        float inte = [textField.text floatValue];
        if (inte <= [dataList[textField.tag][@"MaxValue"] floatValue])
            //   && inte >=[dataList[textField.tag][@"MinValue"] floatValue])
        {
            [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                        green:110 / 255.0f
                                                         blue:255 / 255.0f
                                                        alpha:1.0]
                               forState:UIControlStateNormal];
            self.saveBtn.enabled = YES;
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]
                                        initWithDictionary:dataList[textField.tag]];
            [dic setObject:textField.text forKey:@"NumericAnswer"];
            NSLog(@"dic : %@", dic);
            [dataList replaceObjectAtIndex:textField.tag withObject:dic];
        }
        
        else {
            textField.text = @"";
        }
        
        // [_tblForm reloadData];
    } else {
        
        [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                    green:110 / 255.0f
                                                     blue:255 / 255.0f
                                                    alpha:1.0]
                           forState:UIControlStateNormal];
        self.saveBtn.enabled = YES;
        
        NSLog(@"textView : %ld", (long)textField.tag);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]
                                    initWithDictionary:dataList[textField.tag]];
        [dic setObject:textField.text forKey:@"TextAnswer"];
        NSLog(@"dic : %@", dic);
        [dataList replaceObjectAtIndex:textField.tag withObject:dic];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    if ([dataList[textField.tag][@"DataType"]
         isEqualToString:@"NumericTextBox"]) {
        
        [textField resignFirstResponder];
        
        return NO;
    } else {
        
        NSLog(@"textView : %ld", (long)textField.tag);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]
                                    initWithDictionary:dataList[textField.tag]];
        [dic setObject:textField.text forKey:@"TextAnswer"];
        NSLog(@"dic : %@", dic);
        [dataList replaceObjectAtIndex:textField.tag withObject:dic];
        
        return YES;
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    editingIndexPath = [NSIndexPath indexPathForRow:textView.tag inSection:0];
    
    // if([self substring:@"null" existsInString:_admitingdiagnosis]) {
    if ([textView.text isEqualToString:@"Type here ..."]) {
        textView.text = @"";
        textView.textColor = [UIColor lightGrayColor];
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (![textView.text isEqualToString:@""]) {
        textView.text = dataList[textView.tag][@"TextAnswer"];
        ;
    } else {
        textView.text = @"Type here ...";
    }
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    NSLog(@"textView : %ld", (long)textView.tag);
    NSMutableDictionary *dic =
    [[NSMutableDictionary alloc] initWithDictionary:dataList[textView.tag]];
    [dic setObject:textView.text forKey:@"TextAnswer"];
    NSLog(@"dic : %@", dic);
    [dataList replaceObjectAtIndex:textView.tag withObject:dic];
    //  [_tblForm reloadData];
}
- (BOOL)textView:(UITextView *)textView
shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
    
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = 80;
    if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"start_endtime"]) {
        height = 60;
    }
    
    else if ([dataList[indexPath.row][@"DataType"]
              isEqualToString:@"TimeSpent"]) {
        height = 60;
    } else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Image"]) {
        height = 65;
    }
    
    else if ([dataList[indexPath.row][@"DataType"]
              isEqualToString:@"sampledata"]) {
        CGSize size = self.tblForm.frame.size;
        size.width = size.width - 78;
        height = [self calculateStringHeigth:size
                                        text:sampled_vale
                                     andFont:[UIFont systemFontOfSize:15]];
        
        NSString *string1 = dataList[indexPath.row][@"QuestionValue"];
        height = height + [self calculateStringHeigth:size
                                                 text:string1
                                              andFont:[UIFont systemFontOfSize:14]];
        if (height > 39.0f) {
            // we know exactly what will happen
            height = height + 25;
        } else {
            height = 60;
        }
        
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"placementlist"]) {
        height = 80;
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"Category"]) {
        CGSize size = self.tblForm.frame.size;
        size.width = size.width - 16;
       CGFloat CategoryDescriptionheight = [self
                  calculateStringHeigth:size
                  text:dataList[indexPath.row][@"CategoryDescription"]
                  andFont:[UIFont systemFontOfSize:13]];
        CGFloat CategoryNameheight1;
        CategoryNameheight1 =
        [self calculateStringHeigth:size
                               text:dataList[indexPath.row][@"CategoryName"]
                            andFont:[UIFont systemFontOfSize:13]];
      
         if ([dataList[indexPath.row][@"CategoryName"]
                  isKindOfClass:[NSNull class]] ||
                 [dataList[indexPath.row][@"CategoryName"] isEqualToString:@""]) {
            
            height = CategoryDescriptionheight + 23;
        }
        
        else if ([dataList[indexPath.row][@"CategoryDescription"]
                  isKindOfClass:[NSNull class]] ||
                 [dataList[indexPath.row][@"CategoryDescription"] isEqualToString:@""]) {
            
            height = CategoryNameheight1 + 23;
        }
        else   if ([dataList[indexPath.row][@"CategoryName"] length]>0 &&
                   [dataList[indexPath.row][@"CategoryDescription"] length]>0) {
            NSLog(@"CategoryDescriptionheight%f",CategoryDescriptionheight);
            if(CategoryDescriptionheight>15)
            {
            height = CategoryDescriptionheight +CategoryNameheight1+ 27;
            }
            else{
                height = CategoryDescriptionheight +CategoryNameheight1+ 15;

            }
        }
        
        
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"NumericTextBox"])
        height = 65;
    else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown"]) {
        // height = 51;
        CGSize size = self.tblForm.frame.size;
        size.width = size.width - 78;
        NSString *string =
        [dataList[indexPath.row][@"IsRequired"] boolValue]
        ? [NSString stringWithFormat:@"%@ *",
           dataList[indexPath.row]
           [@"QuestionDisplayName"]]
        : dataList[indexPath.row][@"QuestionDisplayName"];
        height = [self calculateStringHeigth:size
                                        text:string
                                     andFont:[UIFont systemFontOfSize:15]];
        
        NSString *string1 = dataList[indexPath.row][@"QuestionValue"];
        height = height + [self calculateStringHeigth:size
                                                 text:string1
                                              andFont:[UIFont systemFontOfSize:15]];
        height = 60;
        NSLog(@"height%f", height);
    } else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Checkbox"])
        height = 54;
    else if ([dataList[indexPath.row][@"DataType"]
              isEqualToString:@"TextBoxMultiline"])
        height = 150;
    else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"TextBox"])
        height = 60;
    else if ([dataList[indexPath.row][@"DataType"]
              isEqualToString:@"Dropdown(Multiple)"]) {
        CGSize size = self.tblForm.frame.size;
        size.width = size.width - 78;
        NSString *string =
        [dataList[indexPath.row][@"IsRequired"] boolValue]
        ? [NSString stringWithFormat:@"%@ *",
           dataList[indexPath.row]
           [@"QuestionDisplayName"]]
        : dataList[indexPath.row][@"QuestionDisplayName"];
        height = [self calculateStringHeigth:size
                                        text:string
                                     andFont:[UIFont systemFontOfSize:15]];
        
        NSString *string1 = dataList[indexPath.row][@"QuestionValue"];
        height = height + [self calculateStringHeigth:size
                                                 text:string1
                                              andFont:[UIFont systemFontOfSize:14]];
        
        height = 60;
        
        NSLog(@"Dropdown(Multiple)height%d", height);
    }
    // height = 51;
    else if ([dataList[indexPath.row][@"DataType"]
              isEqualToString:@"Checkbox(Multiple)"]) {
        CGSize size = self.tblForm.frame.size;
        size.width = size.width - 78;
        NSString *string =
        [dataList[indexPath.row][@"IsRequired"] boolValue]
        ? [NSString stringWithFormat:@"%@ *",
           dataList[indexPath.row]
           [@"QuestionDisplayName"]]
        : dataList[indexPath.row][@"QuestionDisplayName"];
        height = [self calculateStringHeigth:size
                                        text:string
                                     andFont:[UIFont systemFontOfSize:14]];
        
        NSString *string1 = dataList[indexPath.row][@"QuestionValue"];
        height = height + [self calculateStringHeigth:size
                                                 text:string1
                                              andFont:[UIFont systemFontOfSize:14]];
        
        if (height > 39.0f) {
            // we know exactly what will happen
            height = height + 5;
        } else {
            height = 60;
        }
        
    }
    // height = 51;
    else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Date"])
        height = 60;
    else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Time"])
        height = 60;
    else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Radio"]) {
        CGSize size = self.tblForm.frame.size;
        size.width = size.width - 78;
        NSString *string = dataList[indexPath.row][@"QuestionDisplayName"];
        height = [self calculateStringHeigth:size
                                        text:string
                                     andFont:[UIFont systemFontOfSize:14]];
        
        NSString *string1 = dataList[indexPath.row][@"QuestionValue"];
        height = height + [self calculateStringHeigth:size
                                                 text:string1
                                              andFont:[UIFont systemFontOfSize:14]];
        
        height = 62;
    }
    // height = 51;
    return height;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    [self.view endEditing:YES];
    self.saveBtn.enabled = YES;
    editingIndexPath = indexPath;
    rowOfTheCell = indexPath.row;
    if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Image"]) {
    }
    if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"sampledata"]) {
        [self performSegueWithIdentifier:@"samplesegue" sender:indexPath];
    }
    if (([dataList[indexPath.row][@"DataType"]
          isEqualToString:@"Dropdown(Multiple)"]) ||
        ([dataList[indexPath.row][@"DataType"]
          isEqualToString:@"Checkbox(Multiple)"]) ||
        ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown"])) {
        
        [self performSegueWithIdentifier:@"selectionsegue" sender:indexPath];
    } else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Date"]) {
        DateAndTimeTableViewCell *cell =
        [tableView cellForRowAtIndexPath:indexPath];
        cell.lblValue.tag = indexPath.row;
        [self showDatePicker:UIDatePickerModeDate currentLbl:cell.lblValue];
    }
    
    else if ([dataList[indexPath.row][@"DataType"]
              isEqualToString:@"TimeSpent"]) {
        DateAndTimeTableViewCell *cell =
        [tableView cellForRowAtIndexPath:indexPath];
        cell.lblValue.tag = indexPath.row;
        [self showTimePicker:UIDatePickerModeTime
                  currentLbl:cell.lblValue
               sendindexpath:indexPath];
        
    } else if ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Time"]) {
        DateAndTimeTableViewCell *cell =
        [tableView cellForRowAtIndexPath:indexPath];
        cell.lblValue.tag = indexPath.row;
        [self showDatePicker:UIDatePickerModeTime currentLbl:cell.lblValue];
        
    } else if ([dataList[indexPath.row][@"DataType"]
                isEqualToString:@"Checkbox"]) {
    }
}

- (void)location_taping:(UITapGestureRecognizer *)recognizer {
    
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    rowOfTheCell = recognizer.view.tag;
    [self performSegueWithIdentifier:@"ImageselectionSegue" sender:self];
    
    // [self performSegueWithIdentifier:@"dynamiccamera" sender:indexPath];
}

- (void)viewimag_taping:(UITapGestureRecognizer *)recognizer {
    
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    rowOfTheCell = recognizer.view.tag;
    [self performSegueWithIdentifier:@"ImageselectionSegue" sender:self];
}

- (void)singleTapping:(UIGestureRecognizer *)recognizer {
    
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    [self performSegueWithIdentifier:@"dynamiccamera" sender:self];
}

- (NSDate *)roundToNearestQuarterHour:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |
    NSCalendarUnitDay | NSCalendarUnitHour |
    NSCalendarUnitMinute | NSCalendarUnitWeekday |
    NSCalendarUnitWeekdayOrdinal | NSCalendarUnitWeekOfYear;
    NSDateComponents *components = [calendar components:unitFlags fromDate:date];
    NSInteger roundedToQuarterHour = round((components.minute / 15.0)) * 15;
    components.minute = roundedToQuarterHour;
    return [calendar dateFromComponents:components];
}

- (void)show_starttimePicker:(UIDatePickerMode)modeDatePicker
                  currentLbl:(UITapGestureRecognizer *)sender {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:UIDatePickerModeTime];
    [alertController.view addSubview:picker];
    picker.minuteInterval = 15;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    NSLog(@"starttime%@", _start_timelbl.text);
    if ([start_timestr length] > 0) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm a"];
        picker.date = [formatter dateFromString:start_timestr];
        
    } else {
        picker.date = [self roundToNearestQuarterHour:[NSDate date]];
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender view].tag
                                                inSection:0];
    startend_time *cell = [_tblForm cellForRowAtIndexPath:indexPath];
    
    UIAlertAction *doneAction = [UIAlertAction
                                 actionWithTitle:@"Done"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action) {
                                     [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                                                 green:110 / 255.0f
                                                                                  blue:255 / 255.0f
                                                                                 alpha:1.0]
                                                        forState:UIControlStateNormal];
                                     self.saveBtn.enabled = YES;
                                     
                                     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                     [formatter setDateFormat:@"hh:mm a"];
                                     picker.minuteInterval = 15;
                                     NSString *datestring = [formatter stringFromDate:picker.date];
                                     
                                     cell.start_timevalue.text = datestring;
                                     
                                     NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
                                     [formatter1 setDateFormat:@"hh:mm a"];
                                     NSLog(@"date2 is equal date1%@ %@", cell.start_timevalue,
                                           datestring);
                                     
                                     NSDate *startdate = [formatter1 dateFromString:datestring];
                                     NSDate *enddate =
                                     [formatter1 dateFromString:cell.end_timevalue.text];
                                     
                                     NSComparisonResult result = [startdate compare:enddate];
                                     if (result == NSOrderedDescending) {
                                         NSLog(@"date1 is later than date2");
                                         cell.end_timevalue.text = @"";
                                         cell.startend_timespent.text = @"";
                                         
                                         start_timestr = @"";
                                         end_timestr = @"";
                                         timespent_str = @"";
                                     } else if (result == NSOrderedAscending) {
                                         NSLog(@"date2 is later than date1");
                                         
                                         cell.start_timevalue.text = datestring;
                                         start_timestr = datestring;
                                         
                                         NSTimeInterval interval =
                                         [enddate timeIntervalSinceDate:startdate];
                                         timespent_hours =
                                         (int)interval /
                                         3600; // integer division to get the hours part
                                         timespent_minutes =
                                         (interval - (timespent_hours * 3600)) /
                                         60; // interval minus hours part (in seconds) divided by
                                         // 60 yields minutes
                                         
                                         NSString *timeDiff;
                                         if (timespent_minutes == 0) {
                                             timeDiff = [NSString stringWithFormat:@"%d hrs %d0 mins",
                                                         timespent_hours,
                                                         timespent_minutes];
                                             
                                         } else {
                                             timeDiff = [NSString stringWithFormat:@"%d hrs %d mins",
                                                         timespent_hours,
                                                         timespent_minutes];
                                         }
                                         
                                         timespent_str = timeDiff;
                                         
                                         cell.startend_timespent.text = timeDiff;
                                     } else {
                                         
                                         cell.start_timevalue.text = datestring;
                                         
                                         start_timestr = datestring;
                                         
                                         NSTimeInterval interval =
                                         [enddate timeIntervalSinceDate:startdate];
                                         timespent_hours =
                                         (int)interval /
                                         3600; // integer division to get the hours part
                                         timespent_minutes = (interval - (timespent_hours * 3600)) /
                                         60; // interval minus
                                         
                                         NSString *timeDiff;
                                         if (timespent_minutes == 0) {
                                             timeDiff = [NSString stringWithFormat:@"%d hrs %d0 mins",
                                                         timespent_hours,
                                                         timespent_minutes];
                                             
                                         } else {
                                             timeDiff = [NSString stringWithFormat:@"%d hrs %d mins",
                                                         timespent_hours,
                                                         timespent_minutes];
                                         }
                                         
                                         timespent_str = timeDiff;
                                         
                                         cell.startend_timespent.text = timeDiff;
                                     }
                                 }];
    [alertController addAction:doneAction];
    
    UIAlertAction *clearAction =
    [UIAlertAction actionWithTitle:@"Clear"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action) {
                               cell.start_timevalue.text = @"";
                               start_timestr = @"";
                               ;
                               cell.startend_timespent.text = @"";
                               timespent_str = @"";
                               timespent_hours = -1;
                               timespent_minutes = -1;
                               timespent_save = @"";
                           }];
    
    UIAlertAction *cancelAction =
    [UIAlertAction actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction *action) {
                               NSLog(@"Cancel action");
                           }];
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController =
    alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect =
    CGRectMake(self.view.bounds.size.width / 2.0,
               self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)show_endtimePicker:(UIDatePickerMode)modeDatePicker
                currentLbl:(UITapGestureRecognizer *)sender {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:UIDatePickerModeTime];
    picker.minuteInterval = 15;
    [alertController.view addSubview:picker];
    if ([end_timestr length] > 0) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm a"];
        picker.date = [formatter dateFromString:end_timestr];
        
    } else {
        picker.date = [self roundToNearestQuarterHour:[NSDate date]];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender view].tag
                                                inSection:0];
    startend_time *cell = [_tblForm cellForRowAtIndexPath:indexPath];
    // NSIndexPath *indexPath = [NSIndexPath indexPathForRow:slider.tag
    // inSection:0];
    
    //  NSDate *start_date = [formatter dateFromString:_start_timelbl.text];
    //
    //
    //    NSCalendar *calendar = [NSCalendar currentCalendar];
    //    NSDateComponents *components = [calendar components:(NSCalendarUnitHour
    //    | NSCalendarUnitMinute) fromDate:start_date]; NSInteger hour =
    //    [components hour]; NSInteger minute = [components minute];
    //
    //    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:
    //    NSGregorianCalendar]; NSDateComponents *components1 = [gregorian
    //    components: NSUIntegerMax fromDate: start_date]; [components1 setHour:
    //    hour]; [components1 setMinute: minute]; [components1 setSecond: 0];
    //    NSLog(@"startdate%@,%ld,%ld",_start_timelbl.text,(long)hour,(long)minute);
    //
    //    NSDate *startDate = [gregorian dateFromComponents: components1];
    //
    //
    //
    //
    //    [picker setMinimumDate:startDate];
    
    UIAlertAction *doneAction = [UIAlertAction
                                 actionWithTitle:@"Done"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action) {
                                     [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                                                 green:110 / 255.0f
                                                                                  blue:255 / 255.0f
                                                                                 alpha:1.0]
                                                        forState:UIControlStateNormal];
                                     self.saveBtn.enabled = YES;
                                     if ([start_timestr length] > 0) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"hh:mm a"];
                                         NSString *datestring = [formatter stringFromDate:picker.date];
                                         
                                         NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
                                         [formatter1 setDateFormat:@"hh:mm a"];
                                         NSLog(@"date2 is equal date1%@ %@", cell.start_timevalue.text,
                                               datestring);
                                         
                                         NSDate *startdate =
                                         [formatter1 dateFromString:cell.start_timevalue.text];
                                         NSDate *enddate = [formatter1 dateFromString:datestring];
                                         
                                         NSComparisonResult result = [startdate compare:enddate];
                                         if (result == NSOrderedDescending) {
                                             NSLog(@"date1 is later than date2");
                                             cell.end_timevalue.text = @"";
                                             cell.startend_timespent.text = @"";
                                             
                                             end_timestr = @"";
                                             timespent_str = @"";
                                         } else if (result == NSOrderedAscending) {
                                             NSLog(@"date2 is later than date1");
                                             
                                             cell.end_timevalue.text = datestring;
                                             
                                             end_timestr = datestring;
                                             
                                             NSTimeInterval interval =
                                             [enddate timeIntervalSinceDate:startdate];
                                             
                                             timespent_hours =
                                             (int)interval /
                                             3600; // integer division to get the hours part
                                             timespent_minutes =
                                             (interval - (timespent_hours * 3600)) / 60;
                                             NSString *timeDiff;
                                             if (timespent_minutes == 0) {
                                                 timeDiff = [NSString stringWithFormat:@"%d hrs %d0 mins",
                                                             timespent_hours,
                                                             timespent_minutes];
                                                 
                                             } else {
                                                 timeDiff = [NSString stringWithFormat:@"%d hrs %d mins",
                                                             timespent_hours,
                                                             timespent_minutes];
                                             }
                                             
                                             timespent_str = timeDiff;
                                             
                                             cell.startend_timespent.text = timeDiff;
                                             
                                         } else {
                                             
                                             cell.end_timevalue.text = datestring;
                                             
                                             NSTimeInterval interval =
                                             [enddate timeIntervalSinceDate:startdate];
                                             
                                             timespent_hours =
                                             (int)interval /
                                             3600; // integer division to get the hours part
                                             timespent_minutes = (interval - (timespent_hours * 3600)) /
                                             60; // interval minus
                                             
                                             NSString *timeDiff;
                                             if (timespent_minutes == 0) {
                                                 timeDiff = [NSString stringWithFormat:@"%d hrs %d0 mins",
                                                             timespent_hours,
                                                             timespent_minutes];
                                                 
                                             } else {
                                                 timeDiff = [NSString stringWithFormat:@"%d hrs %d mins",
                                                             timespent_hours,
                                                             timespent_minutes];
                                             }
                                             
                                             timespent_str = timeDiff;
                                             
                                             cell.startend_timespent.text = timeDiff;
                                             end_timestr = datestring;
                                         }
                                     } else {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"hh:mm a"];
                                         NSString *datestring = [formatter stringFromDate:picker.date];
                                         
                                         cell.end_timevalue.text = datestring;
                                         end_timestr = datestring;
                                         // UIAlertView *callAlert = [[UIAlertView
                                         // alloc]initWithTitle:@"Efielddata Message" message:@"Please
                                         // fill Start Time" delegate:self cancelButtonTitle:@"Ok"
                                         // otherButtonTitles:nil];
                                         //[callAlert show];
                                     }
                                 }];
    [alertController addAction:doneAction];
    
    UIAlertAction *clearAction =
    [UIAlertAction actionWithTitle:@"Clear"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action) {
                               cell.end_timevalue.text = @"";
                               cell.startend_timespent.text = @"";
                               timespent_str = @"";
                               end_timestr = @"";
                               timespent_hours = -1;
                               timespent_minutes = -1;
                               timespent_save = @"";
                           }];
    
    UIAlertAction *cancelAction =
    [UIAlertAction actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction *action) {
                               NSLog(@"Cancel action");
                           }];
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController =
    alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect =
    CGRectMake(self.view.bounds.size.width / 2.0,
               self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showTimePicker:(UIDatePickerMode)modeDatePicker
            currentLbl:(UILabel *)lbl
         sendindexpath:(NSIndexPath *)indexpathrow {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
    [picker setLocale:locale];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    
    if ([timespent_str isEqualToString:@""] ||[timespent_str isEqualToString:@"0 hrs 00 mins"]||
        [timespent_str isKindOfClass:[NSNull class]]) {
        picker.date = [self roundToNearestQuarterHour:[NSDate date]];

    } else {
        
        NSString *times = [timespent_str stringByReplacingOccurrencesOfString:@" hrs"
                                                 withString:@":"];
        times = [times stringByReplacingOccurrencesOfString:@" mins"
                                                 withString:@""];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"HH:mm";
        NSDate *date = [dateFormatter dateFromString:times];
        NSString *pmamDateString = [dateFormatter stringFromDate:date];
        NSData *date_hrs = [formatter dateFromString:pmamDateString];
        [picker setDate:date];
    }
    picker.minuteInterval = 15;
    UIAlertAction *doneAction = [UIAlertAction
                                 actionWithTitle:@"Done"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action) {
                                     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                     [formatter setDateFormat:@"HH:mm"];
                                     NSString *datestring = [formatter stringFromDate:picker.date];
                                     
                                     NSCalendar *calendar = [NSCalendar currentCalendar];
                                     NSDateComponents *components = [calendar
                                                                     components:(NSCalendarUnitHour | NSCalendarUnitMinute)
                                                                     fromDate:picker.date];
                                     
                                     timespent_hours = [components hour];
                                     timespent_minutes = [components minute];
                                     
                                     NSString *timeDiff;
                                     if (timespent_minutes == 0) {
                                         timeDiff = [NSString
                                                     stringWithFormat:@"%d hrs %ld0 mins", timespent_hours,
                                                     (long)timespent_minutes];
                                         
                                     } else {
                                         timeDiff = [NSString
                                                     stringWithFormat:@"%d hrs %ld mins", timespent_hours,
                                                     (long)timespent_minutes];
                                     }
                                     timespent_str = timeDiff;
                                     timespent_save = timeDiff;
                                     if (lbl.tag == indexpathrow.row) {
                                         lbl.text = timeDiff;
                                     }
                                 }];
    [alertController addAction:doneAction];
    
    UIAlertAction *clearAction =
    [UIAlertAction actionWithTitle:@"Clear"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action) {
                               lbl.text = @"";
                               timespent_str = @"";
                               timespent_save = @"";
                           }];
    
    UIAlertAction *cancelAction =
    [UIAlertAction actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction *action) {
                               NSLog(@"Cancel action");
                           }];
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController =
    alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect =
    CGRectMake(self.view.bounds.size.width / 2.0,
               self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showDatePicker:(UIDatePickerMode)modeDatePicker
            currentLbl:(UILabel *)lbl {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    if (modeDatePicker == UIDatePickerModeDate) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd/yyyy"];
        NSString *dateanswerstr = dataList[rowOfTheCell][@"DateAnswerText"];
        if ([dateanswerstr isEqualToString:@""] ||
            [dateanswerstr isKindOfClass:[NSNull class]]) {
        } else {
            NSData *date = [formatter dateFromString:dateanswerstr];
            [picker setDate:date];
        }
    } else {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm a"];
        NSString *dateanswerstr = dataList[rowOfTheCell][@"TimeAnswerText"];
        ;
        if ([dateanswerstr isEqualToString:@""] ||
            [dateanswerstr isKindOfClass:[NSNull class]]) {
        } else {
            NSData *date = [formatter dateFromString:dateanswerstr];
            [picker setDate:date];
        }
    }
    
    UIAlertAction *doneAction = [UIAlertAction
                                 actionWithTitle:@"Done"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action) {
                                     if (modeDatePicker == UIDatePickerModeDate) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                                         NSString *datestring = [formatter stringFromDate:picker.date];
                                         
                                         lbl.text = datestring;
                                         
                                         NSMutableDictionary *dic = [[NSMutableDictionary alloc]
                                                                     initWithDictionary:dataList[lbl.tag]];
                                         [dic setObject:[formatter stringFromDate:picker.date]
                                                 forKey:@"DateAnswerText"];
                                         [dic setObject:[formatter stringFromDate:picker.date]
                                                 forKey:@"DateAnswer"];
                                         [dic setObject:[formatter stringFromDate:picker.date]
                                                 forKey:@"DisplayDateAnswerText"];
                                         
                                         NSLog(@"dic : %@", dic);
                                         [dataList replaceObjectAtIndex:lbl.tag withObject:dic];
                                     } else {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"hh:mm a"];
                                         NSString *datestring = [formatter stringFromDate:picker.date];
                                         
                                         lbl.text = datestring;
                                         
                                         NSMutableDictionary *dic = [[NSMutableDictionary alloc]
                                                                     initWithDictionary:dataList[lbl.tag]];
                                         
                                         [dic setObject:[formatter stringFromDate:picker.date]
                                                 forKey:@"TimeAnswer"];
                                         [dic setObject:lbl.text forKey:@"TimeAnswerText"];
                                         
                                         [dic setObject:lbl.text forKey:@"DisplayTimeAnswerText"];
                                         NSLog(@"dic : %@", dic);
                                         [dataList replaceObjectAtIndex:lbl.tag withObject:dic];
                                     }
                                 }];
    [alertController addAction:doneAction];
    
    UIAlertAction *clearAction = [UIAlertAction
                                  actionWithTitle:@"Clear"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *action) {
                                      if (modeDatePicker == UIDatePickerModeDate) {
                                          
                                          NSMutableDictionary *dic = [[NSMutableDictionary alloc]
                                                                      initWithDictionary:dataList[lbl.tag]];
                                          [dic setObject:@"" forKey:@"DateAnswer"];
                                           [dic setObject:@"" forKey:@"DateAnswerText"];
                                          [dic setObject:@"" forKey:@"DisplayDateAnswerText"];
                                          [dataList replaceObjectAtIndex:lbl.tag withObject:dic];
                                          
                                      } else {
                                          
                                          NSMutableDictionary *dic = [[NSMutableDictionary alloc]
                                                                      initWithDictionary:dataList[lbl.tag]];
                                          [dic setObject:@"" forKey:@"TimeAnswer"];
                                          
                                          [dic setObject:@"" forKey:@"TimeAnswerText"];
                                          
                                          [dic setObject:@"" forKey:@"DisplayTimeAnswerText"];
                                          [dataList replaceObjectAtIndex:lbl.tag withObject:dic];
                                      }
                                      NSLog(@"Clear action");
                                      lbl.text = @"";
                                  }];
    
    UIAlertAction *cancelAction =
    [UIAlertAction actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction *action) {
                               NSLog(@"Cancel action");
                           }];
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController =
    alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect =
    CGRectMake(self.view.bounds.size.width / 2.0,
               self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)setSelectedDateInField {
    NSLog(@"date :: %@", datePicker.date.description);
    // set Date formatter
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"MMMM dd, yyyy"];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // backtoorders
    if ([[segue identifier] isEqualToString:@"backtoorders"]) {
    }
    if ([[segue identifier] isEqualToString:@"placementlist_segue"]) {
        NSIndexPath *indexpath = (NSIndexPath *)sender;
        LocationList *vc = [segue destinationViewController];
        vc.TaskName = _TaskName;
        vc.ProjectName = _ProjectName;
        vc.JobNumber = _JobNumber;
        vc.childname=@"Placement";
        //     vc.= placeTestTypeId;
        vc.WorkorderId = _WorkorderId;
    } else if ([[segue identifier] isEqualToString:@"childform_segue"]) {
        
        NSIndexPath *indexpath = (NSIndexPath *)sender;
        childform_list *vc = [segue destinationViewController];
        vc.TaskName = _TaskName;
        vc.ProjectName = _ProjectName;
        vc.JobNumber = _JobNumber;
        vc.childname=ChildTestType_name;
        if ([ChildTestType isEqualToString:@"Child3TestTypeName"]) {
            vc.Testtype_ID = Child3TestTypeId;
            vc.Testtype_number=@"3";

        }else
        {
        vc.Testtype_ID = Child2TestTypeId;
            vc.Testtype_number=@"2";

    }
        vc.WorkorderId = _WorkorderId;
    } else if ([[segue identifier] isEqualToString:@"suborderlist_segue"]) {
        
        NSIndexPath *indexpath = (NSIndexPath *)sender;
        Suborder_listVC *vc = [segue destinationViewController];
        vc.TaskName = _TaskName;
        vc.ProjectName = _ProjectName;
        vc.JobNumber = _JobNumber;
        vc.Testtype_ID = ChildTestTypeId;
        vc.WorkorderId = _WorkorderId;
             vc.childname=ChildTestType_name;
    } else if ([[segue identifier] isEqualToString:@"ImageselectionSegue"]) {
        
        NSIndexPath *indexpath = (NSIndexPath *)sender;
        Image_SelectionViewController *vc = [segue destinationViewController];
        vc.listInfo = dataList[rowOfTheCell];
        vc.WorkorderId = _WorkorderId;
        vc.TaskName = _TaskName;
        vc.ProjectName = _ProjectName;
        vc.JobNumber = _JobNumber;
    }
    
    else if ([[segue identifier] isEqualToString:@"samplesegue"]) {
        
        NSIndexPath *indexpath = (NSIndexPath *)sender;
        sampledSelectionViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.TaskName = _TaskName;
        vc.ProjectName = _ProjectName;
        vc.JobNumber = _JobNumber;
        vc.Work_type=_Work_type;
        vc.selectedList =
        [[NSUserDefaults standardUserDefaults] valueForKey:@"sampled_array"];
        //  [jsonDict valueForKeyPath:@"Data.SampleIdArray"];
    } else if ([[segue identifier] isEqualToString:@"selectionsegue"]) {
        
        NSIndexPath *indexpath = (NSIndexPath *)sender;
        SelectionViewController *vc = [segue destinationViewController];
        vc.listInfo = dataList[indexpath.row];
        vc.delegate = self;
        vc.TaskName = _TaskName;
        vc.Workorder_type=_Work_type;
        vc.ProjectName = _ProjectName;
        vc.JobNumber = _JobNumber;
    } else if ([[segue identifier] isEqualToString:@"dynamiccamera"]) {
        dynamiccameraVC *vc = [segue destinationViewController];
        vc.TaskName = _TaskName;
        vc.ProjectName = _ProjectName;
        vc.JobNumber = _JobNumber;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelAction:(id)sender {
    
    //   [self.navigationController popViewControllerAnimated:YES];
    
    [self performSegueWithIdentifier:@"backtoorders" sender:self];
}

- (IBAction)alertaction:(id)sender {
    
    [self performSegueWithIdentifier:@"alertsegue" sender:self];
}

- (BOOL)isValidatenumeric:(NSArray *)list {
    
    for (NSDictionary *dic in list) {
        NSLog(@"boolValue%@", dic);
        
        if ([dic[@"DataType"] isEqualToString:@"NumericTextBox"]) {
            if ([[NSString stringWithFormat:@"%@", dic[@"NumericAnswer"]] length] >
                0) {
                NSLog(@"Numerictext%@", dic);
                
                if ([dic[@"NumericAnswer"] floatValue] <
                    [dic[@"MinValue"] floatValue]) {
                    return NO;
                } else if ([dic[@"NumericAnswer"] floatValue] >
                           [dic[@"MaxValue"] floatValue]) {
                    return NO;
                } else if ([dic[@"NumericAnswer"] floatValue] == 0) {
                    return YES;
                }
            }
            
            //
            //            else{
            //                return NO;
            //            }
        }
    }
    
    return YES;
}
- (BOOL)isValidate:(NSArray *)list {
    
    for (NSDictionary *dic in list) {
        NSLog(@"boolValue%@", dic);
        if ([dic[@"IsRequired"] boolValue]) {
            if ([dic[@"DataType"] isEqualToString:@"NumericTextBox"]) {
                if ([[NSString stringWithFormat:@"%@", dic[@"NumericAnswer"]] length] >
                    0) {
                    NSLog(@"Numerictext%@", dic);
                    if ([dic[@"NumericAnswer"] floatValue] <
                        [dic[@"MinValue"] floatValue]) {
                        return YES;
                    } else if ([dic[@"NumericAnswer"] floatValue] >
                               [dic[@"MaxValue"] floatValue]) {
                        return YES;
                    }
                    
                }
                
                else {
                    return NO;
                }
                
            } else if ([dic[@"DataType"] isEqualToString:@"Dropdown"]) {
                if (![dic[@"QuestionValue"] length]) {
                    return NO;
                }
            } else if ([dic[@"DataType"] isEqualToString:@"TextBoxMultiline"]) {
                if (![dic[@"TextAnswer"] length]) {
                    return NO;
                }
            } else if ([dic[@"DataType"] isEqualToString:@"TextBox"]) {
                if (![dic[@"TextAnswer"] length]) {
                    return NO;
                }
            } else if ([dic[@"DataType"] isEqualToString:@"Checkbox"]) {
                // if(!dic[@"BooleanAnswer"] ){
                if (![dic[@"BooleanAnswer"] boolValue]) {
                    return NO;
                }
            } else if ([dic[@"DataType"] isEqualToString:@"Dropdown(Multiple)"]) {
                if (![dic[@"QuestionValue"] length]) {
                    return NO;
                }
            } else if ([dic[@"DataType"] isEqualToString:@"Checkbox(Multiple)"]) {
                if (![dic[@"QuestionValue"] length]) {
                    return NO;
                }
            } else if ([dic[@"DataType"] isEqualToString:@"Radio"]) {
                if (![dic[@"QuestionValue"] length]) {
                    return NO;
                }
            } else if ([dic[@"DataType"] isEqualToString:@"Date"]) {
                NSLog(@"DateAnswer%@", dic[@"DateAnswer"]);
                if (![dic[@"DateAnswer"] isKindOfClass:[NSString class]] ||
                    [dic[@"DateAnswer"] isKindOfClass:[NSNull class]] ||
                    dic[@"DateAnswer"] == nil) {
                    return NO;
                } else {
                    if (![dic[@"DateAnswer"] length]) {
                        return NO;
                    }
                }
                
            } else if ([dic[@"DataType"] isEqualToString:@"Time"]) {
                
                if (![dic[@"TimeAnswer"] isKindOfClass:[NSString class]] ||
                    [dic[@"TimeAnswer"] isKindOfClass:[NSNull class]] ||
                    dic[@"TimeAnswer"] == nil) {
                    return NO;
                } else {
                    if (![dic[@"TimeAnswer"] length]) {
                        return NO;
                    }
                }
            }
        }
    }
    
    return YES;
}

- (IBAction)saveAction:(id)sender {
    [[self view] endEditing:YES];

    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message"
                          message:
         @"No network, please check your internet connection"];
        return;
    } else {
        imgListdata =
        [[NSUserDefaults standardUserDefaults] objectForKey:@"imgList"];
        
        imgListList = [NSKeyedUnarchiver unarchiveObjectWithData:imgListdata];
        NSMutableArray *tempAryimgListList =
        [NSKeyedUnarchiver unarchiveObjectWithData:imgListdata];
        int index = 0;
        
        for (NSMutableDictionary *dic in imgListList) {
            
            NSMutableDictionary *tempDic =
            [[NSMutableDictionary alloc] initWithDictionary:dic];
            [tempDic setObject:@"" forKey:@"NSData_value"];
            [tempAryimgListList replaceObjectAtIndex:index withObject:tempDic];
            // dic.removeObject(forKey: "NSData_value")
            
            index++;
        }
        imgListList = [tempAryimgListList mutableCopy];
        NSLog(@"imgListList%@", imgListList);
        if (timespent_minutes != -1) {
            int send_minutes = 0;
            if (timespent_minutes == 15) {
                send_minutes = 25;
            } else if (timespent_minutes == 30) {
                send_minutes = 5;
            } else if (timespent_minutes == 45) {
                send_minutes = 75;
            }
            
            timespent_save =
            [NSString stringWithFormat:@"%d.%d", timespent_hours, send_minutes];
        }
        //        if([timespent_save length]==0)
        //        {
        //            timespent_save=@"0";
        //        }
        Boolean locationbool, locationbool1,locationbool2;
        NSMutableArray *alertList;
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        NSString *str =
        [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",
         [[NSUserDefaults standardUserDefaults]
          objectForKey:SERVERURL],
         _WorkorderId];
        NSLog(@"url%@",
              [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",
               [[NSUserDefaults standardUserDefaults]
                objectForKey:SERVERURL],
               _WorkorderId]);
        [request setURL:[NSURL URLWithString:str]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/x-www-form-urlencoded"
       forHTTPHeaderField:@"Content-Type"];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData = [NSURLConnection sendSynchronousRequest:request
                                                returningResponse:&response
                                                            error:&error];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:urlData
                                                                 options:kNilOptions
                                                                   error:&error];
        NSLog(@"jsonDict%@", jsonDict);
        
        alertList = [[NSMutableArray alloc]
                     initWithArray:[jsonDict valueForKeyPath:@"Data.SubWorkOrderList"]];
        NSString *NameofChild = [jsonDict valueForKeyPath:@"Data.NameofChild"];
        NSString *NameofChild2 = [jsonDict valueForKeyPath:@"Data.NameofChild2"];
        NSString *NameofChild3;
        if ([IsAddMoreDisplay isEqualToString:@"1"] &&IsChildTestTypeRequired)
            
        {
            
            if ([alertList count] > 0) {
                locationbool = true;
            } else {
                locationbool = false;
            }
            
        } else {
            locationbool = true;
        }
        
        if ([Is2AddMoreDisplay isEqualToString:@"1"]&&IsChild2TestTypeRequired)
            
        {
            NSMutableArray *alertList1 = [[NSMutableArray alloc]
                                          initWithArray:[jsonDict valueForKeyPath:@"Data.SubWorkOrder2List"]];
            
            if ([alertList1 count] > 0) {
                locationbool1 = true;
            } else {
                locationbool1 = false;
            }
            
        } else {
            locationbool1 = true;
        }
        
        
        if ([Is3AddMoreDisplay isEqualToString:@"1"]&&IsChild3TestTypeRequired)
            
        {
            NSMutableArray *alertList1 = [[NSMutableArray alloc]
                                          initWithArray:[jsonDict valueForKeyPath:@"Data.SubWorkOrder3List"]];
            NameofChild3 = [jsonDict valueForKeyPath:@"Data.NameofChild3"];
            if ([alertList1 count] > 0) {
                locationbool2 = true;
            } else {
                locationbool2 = false;
            }
            
        } else {
            locationbool2 = true;
        }
        BOOL starttime_bool, endtime_bool, timepentbool;
        if ([IsStartEndTimeDisplay isEqualToString:@"1"])
            
        {
            
            if ([start_timestr length] > 0 &&
                [IsStartEndTimeDisplay isEqualToString:@"1"]) {
                starttime_bool = true;
                
            } else if ([IsStartEndTimeDisplay isEqualToString:@"0"]) {
                starttime_bool = true;
            } else {
                starttime_bool = false;
            }
            
            endtime_bool = false;
            if ([iscomplete isEqualToString:@"true"]) {
                if ([end_timestr length] > 0 &&
                    [IsStartEndTimeDisplay isEqualToString:@"1"]) {
                    endtime_bool = true;
                    
                } else if ([IsStartEndTimeDisplay isEqualToString:@"0"]) {
                    endtime_bool = true;
                } else {
                    endtime_bool = false;
                }
                
            } else {
                endtime_bool = true;
            }
            timepentbool = true;
        } else {
            starttime_bool = true;
            endtime_bool = true;
            if ([timespent_save length] == 0) {
                
                timepentbool = false;
            } else {
                timepentbool = true;
            }
        }
        if (locationbool) {
            if (locationbool1) {
                 if (locationbool2) {
                if (starttime_bool && endtime_bool) {
                    if (timepentbool) {
                        if ([self isValidate:dataList]) {
                            if ([self isValidatenumeric:dataList]) {
                                if (![self isNetworkAvailable]) {
                                    [self showAlertno_network:@"Efielddata Message"
                                                      message:@"No network, please check your "
                                     @"internet connection"];
                                    return;
                                } else {
                                     //            NSString *iscompletestr;
                                    //            if(iscomplete){
                                    //            iscompletestr=@"true";
                                    //            }else{
                                    //            iscompletestr=@"false";
                                    //            }
                                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                    
                                    if ([[[NSUserDefaults standardUserDefaults]
                                          objectForKey:@"FileName"] length] > 0) {
                                        int index = 0;
                                        NSMutableArray *tempAry =
                                        [[NSMutableArray alloc] initWithArray:dataList];
                                        
                                        for (NSDictionary *dic in dataList) {
                                            
                                            if ([dic[@"DataType"] isEqualToString:@"Image"]) {
                                                NSMutableDictionary *tempDic =
                                                [[NSMutableDictionary alloc]
                                                 initWithDictionary:dic];
                                                NSString *aString1 = dataList[index][@"TextAnswer"];
                                                NSString *aString;
                                                
                                                aString = [aString1
                                                           stringByAppendingFormat:
                                                           @",%@", [[NSUserDefaults standardUserDefaults]
                                                                    objectForKey:@"FileName"]];
                                                
                                                [tempDic setObject:aString forKey:@"TextAnswer"];
                                                [tempAry replaceObjectAtIndex:index withObject:tempDic];
                                            }
                                            index++;
                                        }
                                        
                                        dataList = [[NSMutableArray alloc] initWithArray:tempAry];
                                    }
                                    [self.saveBtn setTitleColor:[UIColor colorWithRed:85 / 255.0f
                                                                                green:85 / 255.0f
                                                                                 blue:85 / 255.0f
                                                                                alpha:1.0]
                                                       forState:UIControlStateNormal];
                                    ///  NSLog(@"json%@",dataList);
                                    NSDictionary *json;
                                    
                                    json = @{
                                             @"ImageDiscriptionList" : imgListList,
                                             @"List" : dataList,
                                             @"RoleName" : [[NSUserDefaults standardUserDefaults]
                                                            objectForKey:userRole],
                                             @"UserName" : [[NSUserDefaults standardUserDefaults]
                                                            objectForKey:username],
                                             @"IsComplete" : iscomplete,
                                             @"WorkOrderId" : _WorkorderId,
                                             @"TimeSpent" : timespent_save,
                                             @"IsfromIOS" : @true,
                                             @"SampleIdArrays" : [[NSUserDefaults standardUserDefaults]
                                                                  valueForKey:@"sampled_array"],
                                             
                                             @"StartTime" : start_timestr,
                                             @"DepartureTime" : end_timestr,
                                             };
                                    // }
                                    NSMutableDictionary *dictEntry =
                                    [[NSMutableDictionary alloc] init];
                                    [dictEntry
                                     setObject:json
                                     forKey:[NSString stringWithFormat:@"WorkOrderModel"]];
                                    NSLog(@"%@", dictEntry);
                                    
                                    DownloadManager *downloadObj = [[DownloadManager alloc] init];
                                    [downloadObj
                                     callServerWithURL:
                                     [NSString
                                      stringWithFormat:@"%@UpdateWorkOrderDetails",
                                      [[NSUserDefaults
                                        standardUserDefaults]
                                       objectForKey:SERVERURL]]
                                     andParameter:dictEntry
                                     andMethod:@"POST"
                                     andDelegate:self
                                     andKey:@"UpdateWorkOrderDetails"];
                                }
                                
                            } else {
                                UIAlertView *callAlert = [[UIAlertView alloc]
                                                          initWithTitle:@"Efielddata Message"
                                                          message:@"Please fill values within range"
                                                          delegate:self
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                                [callAlert show];
                            }
                        } else {
                            UIAlertView *callAlert = [[UIAlertView alloc]
                                                      initWithTitle:@"Efielddata Message"
                                                      message:@"Please fill required fields"
                                                      delegate:self
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
                            [callAlert show];
                        }
                    } else {
                        UIAlertView *callAlert = [[UIAlertView alloc]
                                                  initWithTitle:@"Efielddata Message"
                                                  message:@"Time Spent is required field"
                                                  delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
                        [callAlert show];
                    }
                } else {
                    UIAlertView *callAlert = [[UIAlertView alloc]
                                              initWithTitle:@"Efielddata Message"
                                              message:@"Start/End time are required fields"
                                              delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
                    [callAlert show];
                }
                     
                 } else {
                     UIAlertView *callAlert = [[UIAlertView alloc]
                                               initWithTitle:@"Efielddata Message"
                                               message:[NSString stringWithFormat:
                                                        @"Add minimum one %@ to continue.",
                                                        NameofChild3]
                                               
                                               delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
                     [callAlert show];
                 }
            } else {
                UIAlertView *callAlert = [[UIAlertView alloc]
                                          initWithTitle:@"Efielddata Message"
                                          message:[NSString stringWithFormat:
                                                   @"Add minimum one %@ to continue.",
                                                   NameofChild2]
                                          
                                          delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
                [callAlert show];
            }
        } else {
            UIAlertView *callAlert = [[UIAlertView alloc]
                                      initWithTitle:@"Efielddata Message"
                                      message:[NSString stringWithFormat:
                                               @"Add minimum one %@ to continue.",
                                               NameofChild]
                                      
                                      delegate:self
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil];
            [callAlert show];
        }
    }
}
#pragma mark - API Delegate

- (void)callBackWithFailureResponse:(NSDictionary *)response
                             andKey:(NSString *)key {
    NSLog(@"CallBackFailure");
    
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    UIAlertView *callAlert = [[UIAlertView alloc]
                              initWithTitle:@"Efielddata Message"
                              message:@"Server may be busy. Please try again later."
                              delegate:self
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil];
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (IBAction)logoutAction:(id)sender {
    [self logoutUser];
}

//- (IBAction)changepasswordAction:(id)sender {
//    [self changePassword];
//}
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (0 == buttonIndex && alertView.tag == 55) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"imgList"];
        if (![iscomplete isEqualToString:@"true"])
            
        {
            [self performSelector:@selector(popViewController)
                       withObject:nil
                       afterDelay:0.5];
            
        } else {
            [self performSelector:@selector(completed_ViewController)
                       withObject:nil
                       afterDelay:0.5];
        }
    }
}

- (void)popViewController {
   // [self performSegueWithIdentifier:@"backtoorders" sender:self];

    [self.navigationController popViewControllerAnimated:YES];
    //[[self presentingViewController] dismissViewControllerAnimated:YES];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    UITabBarController *rootViewController =
    [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];

    rootViewController.selectedIndex = 0;
    [[UIApplication sharedApplication].keyWindow
     setRootViewController:rootViewController];
}

- (void)completed_ViewController {
    if ([iscomplete_frompending isEqualToString:@"true"]) {
        [[NSUserDefaults standardUserDefaults] setObject:DisplayJobDateText
                                                  forKey:@"DisplayJobDateText"];
    }
  //  [self performSegueWithIdentifier:@"backtoorders" sender:self];

    [self.navigationController popViewControllerAnimated:YES];
 //   [[self presentingViewController] dismissViewControllerAnimated:NO
              //                                          completion:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    UITabBarController *rootViewController =
    [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];

    rootViewController.selectedIndex = 1;

    // }
    [[UIApplication sharedApplication].keyWindow
     setRootViewController:rootViewController];
    // DisplayJobDateText
}

- (void)callBackWithSuccessResponse:(NSDictionary *)response
                             andKey:(NSString *)key {
    NSLog(@"response : %@", response);
    
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    if ([key isEqualToString:@"UpdateWorkOrderDetails_placement"]) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if ([[response valueForKeyPath:@"Data.Response"]
             isEqualToString:@"Success"]) {
            [[NSUserDefaults standardUserDefaults] setObject:@"true"
                                                      forKey:@"placement_flag"];
            
            [self performSegueWithIdentifier:@"placementlist_segue" sender:nil];
        } else {
            
            UIAlertView *callAlert = [[UIAlertView alloc]
                                      initWithTitle:nil
                                      message:[[response valueForKeyPath:@"Data.Message"]
                                               description]
                                      delegate:self
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil];
            //  callAlert.tag = 55;
            [callAlert show];
        }
    }
    
    else
        
        if ([key isEqualToString:@"UpdateWorkOrderDetails"]) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            if ([[response valueForKeyPath:@"Data.Response"]
                 isEqualToString:@"Success"]) {
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FileName"];
                UIAlertView *callAlert = [[UIAlertView alloc]
                                          initWithTitle:nil
                                          message:[[response valueForKeyPath:@"Data.Message"]
                                                   description]
                                          delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
                callAlert.tag = 55;
                [callAlert show];
                
            } else {
                
                UIAlertView *callAlert = [[UIAlertView alloc]
                                          initWithTitle:nil
                                          message:[[response valueForKeyPath:@"Data.Message"]
                                                   description]
                                          delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
                //  callAlert.tag = 55;
                [callAlert show];
            }
        }
    
        else if ([key isEqualToString:@"logout"]) {
            if ([[response valueForKeyPath:@"Data.Response"]
                 isEqualToString:@"Success"]) {
                [self logoutUser];
            } else {
                [self showAlertWithTitle:@"Efielddata Message"
                                 message:@"Please try again later"];
            }
        }
}
- (IBAction)iscomplete:(id)sender {
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    UIButton *btn = (UIButton *)sender;
    if ([sender isOn]) {
        iscomplete = @"true";
        iscomplete_frompending = @"true";
    } else {
        iscomplete = @"false";
        iscomplete_frompending = @"true";
    }
}
//
- (IBAction)time_spent:(id)sender {
    
    [self.saveBtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    UISlider *slider = (UISlider *)sender;
    float roundedValue;
    float increment = 0.25;
    if ([sender isEqual:slider]) {
        
        roundedValue = roundf((slider.value + 0.25) / 0.25f) * 0.25f;
        slider.value = roundedValue;
    }
    NSLog(@"Current value of slider is %.2f", slider.value);
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:slider.tag inSection:0];
    
    SliderTableViewCell *cell = [_tblForm cellForRowAtIndexPath:indexPath];
    cell.txtValue.text = [NSString stringWithFormat:@"%.f", slider.value];
    timespent_save = [NSString stringWithFormat:@"%.f", slider.value];
    //  timespent_minutes=-1;
}
@end
