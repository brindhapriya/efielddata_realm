//
//  SelectionViewController.m
//  DynamicForm
//
//  Created by Mahesh Kumar on 1/28/17.
//  Copyright © 2017 mahesh. All rights reserved.
//
#define ZOOM_STEP 2.0


#import "Image_SelectionViewController.h"
#import "dynamiccameraVC.h"

@interface Image_SelectionViewController ()


@property(nonatomic,retain)NSMutableArray *selectedItem;
@end

@implementation Image_SelectionViewController
UIScrollView  *scrollVie;
UIWebView *webview;
UIImage *image;
NSString *base64str;
CGFloat  lastScale1;
//UIButton *webviewbutton ;
//bool showimag,showpdf;
NSData* selectedimageData;
UIImageView *img ;
    NSIndexPath *editingIndexPath;
NSString *ext ;
NSInteger img_countload ;
NSData *ttbresponse;
NSInteger deleteindex;
NSString *selected_img;
NSData *imageData;
Boolean showimag;
NSArray *temp_imgList;


- (void)viewDidLoad {
    [super viewDidLoad];
    
temp_imgList=[[NSArray alloc]init];
    NSData *imgListdata = [[NSUserDefaults standardUserDefaults] objectForKey:@"imgList"];
    if([imgListdata length]==0)
    {}else{
        temp_imgList = [NSKeyedUnarchiver unarchiveObjectWithData:imgListdata];
    }
    imgList=[[NSMutableArray alloc]init];
    [self.save_image setTitleColor:[UIColor  colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.save_image.enabled=false;
   // self.searchBar.delegate = (id)self;
    img_countload = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
    [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"fromcamera"];

    self.taskname_lbl.text= _ProjectName   ;
    self.jobnumber.text=[NSString stringWithFormat:@"%@ - %@", _JobNumber,_TaskName];
    isInitialFlag = YES;
 showimag=false;
    //showpdf=false;
//    self.patient_name.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"patient_name"];
//    self.room_no.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"roomno"];
    self.question_name.text = @"Field Image Attachments";
    [self.question_name sizeToFit];
  //  self.searchBar.delegate = (id)self;
    NSLog(@"Height :%@", NSStringFromCGRect(self.question_name.frame));
    
    _heightConstrains.constant = 200;//self.question_name.frame.size.height + 50;
    
    lblTitle.text = self.noteType;
    _selectedItem = [[NSMutableArray alloc] init];
    self.title = _listInfo[@"CategoryName"];
    
    self.tblForm.allowsMultipleSelectionDuringEditing = NO;

    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification
     object:nil];

 }
- (IBAction)textFieldDidBeginEditing:(UITextField *)textField {
    [scrollVie removeFromSuperview];
    editingIndexPath = [NSIndexPath indexPathForRow:textField.tag inSection:0];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize =
    [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey]
     CGRectValue]
    .size;
    
    UIEdgeInsets contentInsets;
    contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height+30), 0.0);
    
    self.tblForm.contentInset = contentInsets;
    self.tblForm.scrollIndicatorInsets = contentInsets;
    [self.tblForm scrollToRowAtIndexPath:editingIndexPath
                        atScrollPosition:UITableViewScrollPositionTop
                                animated:YES];
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  
        [textField resignFirstResponder];
        
   
        return YES;
   
}

- (void)keyboardWillHide:(NSNotification *)notification {
    // self.tblForm.contentInset = UIEdgeInsetsZero;
    [self.tblForm setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    self.tblForm.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification
     object:nil];
  if([[[NSUserDefaults standardUserDefaults] objectForKey:@"fromcamera"] isEqualToString:@"true"])
   {
    NSArray *imgListList=[[NSArray alloc]init];
    NSData *imgListdata = [[NSUserDefaults standardUserDefaults] objectForKey:@"imgList"];
    if([imgListdata length]==0)
    {}else{
    imgListList = [NSKeyedUnarchiver unarchiveObjectWithData:imgListdata];
    }
        imgList=[[NSMutableArray alloc]init];
    for(int i=0;i<[imgListList count]; i++) {
        
          NSDictionary  *temp = [imgListList objectAtIndex:i] ;
        [imgList addObject:temp];
        
        
    }
    

    
    [super viewWillAppear:animated];
    [self.tblForm reloadData];
 }

 
}

- (IBAction)cancelAction:(id)sender {
    //  [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"imgList"];
//    NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
//
    Boolean showalert;
    showalert=false;
    for (int i=0; i<[imgList count];i++ )
    {
        if([imgList[i][@"QuestionValue"]isEqualToString:@"true"]) {
    showalert=true;
}
    }
    if(showalert){
        
        UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:@"Not saved the newly added image(s). Do you want to save and continue?"  delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil];
        callAlert.tag = 106;
        [callAlert show];
    }
    else{
        NSData *imgListtdata = [NSKeyedArchiver archivedDataWithRootObject:temp_imgList];
        [[NSUserDefaults standardUserDefaults] setObject:imgListtdata forKey:@"imgList"];
        
        [self.navigationController popViewControllerAnimated:YES];

    }
    
 }

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
     
        [self.view endEditing:YES];

        editingIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:0];

        deleteindex=indexPath.row;
        UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:@"Are you sure to delete this item?"  delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil];
        callAlert.tag = 56;
        [callAlert show];
     
        
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return imgList.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        TextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
        if (cell == nil) {
            cell= [[TextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TextFieldCell"];
        }
    NSString *imgname;
    imgname= imgList[indexPath.row][@"ImageName"] ;
    imgname=[imgname stringByReplacingOccurrencesOfString:@".jpeg"
withString:@""];
    cell.lblTitle.text =imgname;
     cell.lblTitle.font = [UIFont systemFontOfSize:14];
     cell.lblTitle.textColor=[UIColor blueColor];
    
    cell.lblTitle.lineBreakMode = UILineBreakModeWordWrap;
    
    cell.lblTitle.numberOfLines = 0;
    [cell.lblTitle sizeToFit];
        cell.txtValue.delegate = self;
        cell.txtValue.tag = indexPath.row;
        [cell.txtValue setKeyboardType:UIKeyboardTypeAlphabet];
        cell.txtValue.placeholder = @"Type image description here ...";
        cell.lblTitle.tag = indexPath.row;
    
    UITapGestureRecognizer *imagename_geture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageview_geture:)];
    cell.lblTitle.userInteractionEnabled = YES;
    imagename_geture.numberOfTapsRequired = 1;
    [ cell.lblTitle addGestureRecognizer:imagename_geture];
    
        [cell.txtValue addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
         if(imgList[indexPath.row][@"ImageDiscription"]  == [NSNull null]||[imgList[indexPath.row][@"ImageDiscription"] isEqualToString:@""]){
            cell.txtValue.placeholder = @"Type image description here ...";
            cell.txtValue.text = @"";
            
        }else
            //  if(dataList[indexPath.row][@"TextAnswer"] != [NSNull null])
        {
            cell.txtValue.text =imgList[indexPath.row][@"ImageDiscription"] ;
            
        }  cell.txtValue.font = [UIFont systemFontOfSize:14];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
  if([_listInfo[@"DataType"] isEqualToString:@"Image"])
     {
         cell.selectionStyle = UITableViewCellSelectionStyleNone;

     }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

-(void)imageview_geture:(UITapGestureRecognizer*)sender

{
  
[self.view endEditing:YES];
  
    if([imgList[sender.view.tag][@"QuestionValue"] isKindOfClass:[NSNull class]]||imgList[sender.view.tag][@"QuestionValue"] ==nil||[imgList[sender.view.tag][@"QuestionValue"]isEqualToString:@""])
    {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        
        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetImageBaseList?PathName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],imgList[sender.view.tag][@"ImageName"]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetImageBaseList"];
    }
     else if([imgList[sender.view.tag][@"QuestionValue"] isEqualToString:@"true"])  {
         base64str = imgList[sender.view.tag][@"NSData_value"];
        
        [self showimage];
        
    }

    
}

-(CGFloat)calculateStringHeigth1:(CGSize)size text:(NSString *)text andFont:(UIFont *)font {
    NSLog(@"text : %@",text);
    
    
    NSLog(@"Font : %@", font);
    NSLog(@"size : %@",NSStringFromCGSize(size));
    
    if([text isEqual:[NSNull null]]) {
        text = @"";
    }
    
    if(![text length])
        return 0;
    
    CGSize labelSize = [text sizeWithFont:font
                        constrainedToSize:size
                            lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat labelHeight = labelSize.height;
    NSLog(@"labelHeight : %f",labelHeight);
    return labelHeight;
}

-(void)textFieldDidChange:(UITextField *)textField
{
    
         [scrollVie removeFromSuperview];
        [self.save_image setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
        self.save_image.enabled = YES;
        
        NSLog(@"textView : %ld",(long)textField.tag);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:imgList[textField.tag]];
        [dic setObject:textField.text forKey:@"ImageDiscription"];
        NSLog(@"dic : %@",dic);
        [imgList replaceObjectAtIndex:textField.tag withObject:dic];
    
    NSData *imgListtdata = [NSKeyedArchiver archivedDataWithRootObject:imgList];
    [[NSUserDefaults standardUserDefaults] setObject:imgListtdata forKey:@"imgList"];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height;
    CGSize size = self.tblForm.frame.size;
    size.width = size.width - 5;
     height = [self calculateStringHeigth1:size text: imgList[indexPath.row][@"ImageDiscription"] andFont:[UIFont systemFontOfSize:14]];
    return 65;
}

-(void)showimagefromcamera
{
    [self.cancel setTitleColor:[UIColor  colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.cancel.enabled=false;
    [self.addimage setTitleColor:[UIColor  colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.addimage.enabled = false;
    [self.save_image setTitleColor:[UIColor  colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.save_image.enabled=false;
    [scrollVie removeFromSuperview];
   
     UIImage* image = [UIImage imageWithData:selectedimageData];
  
     scrollVie=[[UIScrollView alloc]init];
    scrollVie.delegate = self;
    
    scrollVie.scrollEnabled = YES;
    scrollVie.frame=CGRectMake( 0, 0, self.view.frame.size.width-50, self.view.frame.size.height-150);
    
    //CGSizeMake(self.view.frame.size.width-50, self.view.frame.size.height-150);
    
    scrollVie.center = CGPointMake( self.view.frame.size.width/2, self.view.frame.size.height/2+30);
    scrollVie.backgroundColor=[UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1.0f];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setFrame: CGRectMake(0,0,50, 30)];
    [button setTitle:@"Close" forState:UIControlStateNormal];
    button.userInteractionEnabled = YES;
    button.titleLabel.lineBreakMode   = UILineBreakModeTailTruncation;
    button.titleLabel.textColor =[UIColor blueColor];
    button.showsTouchWhenHighlighted = YES;
    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin|
    UIViewAutoresizingFlexibleTopMargin|
    UIViewAutoresizingFlexibleHeight|
    UIViewAutoresizingFlexibleBottomMargin;
    
    [button addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    
    
    int xOffset = 0;
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake( 0, 0, self.view.frame.size.width-50, self.view.frame.size.height-150)];
    img.userInteractionEnabled = YES;
    img.autoresizingMask = ( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
    
    img.contentMode = UIViewContentModeScaleAspectFit;
    
    [img setImage:image];
    [scrollVie addSubview:img];
    [scrollVie addSubview:button];
    scrollVie.decelerationRate = UIScrollViewDecelerationRateFast;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    UITapGestureRecognizer *twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoFingerTap:)];
    
    [doubleTap setNumberOfTapsRequired:2];
    [twoFingerTap setNumberOfTouchesRequired:2];
    
    //Adding gesture recognizer
    [img addGestureRecognizer:doubleTap];
    [img addGestureRecognizer:twoFingerTap];
    float minimumScale = 1.0;//This is the minimum scale, set it to whatever you want. 1.0 = default
    scrollVie.maximumZoomScale = 4.0;
    scrollVie.minimumZoomScale = minimumScale;
    scrollVie.zoomScale = minimumScale;
    
    UIPinchGestureRecognizer *pgr = [[UIPinchGestureRecognizer alloc]
                                     initWithTarget:self action:@selector(handlePinchGesture:)];
    pgr.delegate = self;
    [ img addGestureRecognizer:pgr];
    [self.view addSubview:scrollVie];
}


-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    if(showimag)
    {
        [self showimage];
        
    }
//    else if(showpdf)
//    {
//
//        [self showpdfview];
//    }
}
-(void)showimage
{
    showimag=true;
         [self.cancel setTitleColor:[UIColor  colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.cancel.enabled=false;
    [self.addimage setTitleColor:[UIColor  colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.addimage.enabled = false;
    
    [self.save_image setTitleColor:[UIColor  colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.save_image.enabled=false;
        [scrollVie removeFromSuperview];
    NSData *data = [[NSData alloc]initWithBase64EncodedString:base64str options:NSDataBase64DecodingIgnoreUnknownCharacters];
    [UIImage imageWithData:data];
    image=[UIImage imageWithData:data];
    scrollVie=[[UIScrollView alloc]init];
    scrollVie.delegate = self;
    
    scrollVie.scrollEnabled = YES;
    scrollVie.frame=CGRectMake( 0, 0, self.view.frame.size.width-50, self.view.frame.size.height-150);
    scrollVie.center = CGPointMake( self.view.frame.size.width/2, self.view.frame.size.height/2+30);
    scrollVie.backgroundColor=[UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1.0f];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setFrame: CGRectMake(0,0,50, 30)];
    [button setTitle:@"Close" forState:UIControlStateNormal];
    button.userInteractionEnabled = YES;
    button.titleLabel.lineBreakMode   = UILineBreakModeTailTruncation;
    button.titleLabel.textColor =[UIColor blueColor];
    button.showsTouchWhenHighlighted = YES;
    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin|
    UIViewAutoresizingFlexibleTopMargin|
    UIViewAutoresizingFlexibleHeight|
    UIViewAutoresizingFlexibleBottomMargin;
    
    [button addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    
    
    int xOffset = 0;
    
    UIImage *image=[UIImage imageWithData:data];
    
    
    
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake( 0, 0, self.view.frame.size.width-50, self.view.frame.size.height-150)];
    img.userInteractionEnabled = YES;
    img.autoresizingMask = ( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
    
    img.contentMode = UIViewContentModeScaleAspectFit;
    
    [img setImage:[UIImage imageWithData:data]];
    [scrollVie addSubview:img];
   [scrollVie addSubview:button];
    scrollVie.decelerationRate = UIScrollViewDecelerationRateFast;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    UITapGestureRecognizer *twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoFingerTap:)];
    
    [doubleTap setNumberOfTapsRequired:2];
    [twoFingerTap setNumberOfTouchesRequired:2];
    
    //Adding gesture recognizer
    [img addGestureRecognizer:doubleTap];
    [img addGestureRecognizer:twoFingerTap];
    float minimumScale = 1.0;//This is the minimum scale, set it to whatever you want. 1.0 = default
    scrollVie.maximumZoomScale = 4.0;
    scrollVie.minimumZoomScale = minimumScale;
    scrollVie.zoomScale = minimumScale;
    [scrollVie setContentSize: img.bounds.size];

    UIPinchGestureRecognizer *pgr = [[UIPinchGestureRecognizer alloc]
                                     initWithTarget:self action:@selector(handlePinchGesture:)];
    pgr.delegate = self;
    [ img addGestureRecognizer:pgr];
   // [self.view addSubview:button];
    [self.view addSubview:scrollVie];
}
- (void)scrollViewDidScroll:(UIScrollView*)scrollView{
    
    // this is just a demo method on how to compute the scale factor based on the current contentOffset
    float scale = 1.0f + fabsf(scrollVie.contentOffset.y)  / scrollVie.frame.size.height;
    
    //Cap the scaling between zero and 1
    scale = MAX(0.0f, scale);
    
    // Set the scale to the imageView
    img.transform = CGAffineTransformMakeScale(scale, scale);
}
- (void)handlePinchGesture:(UIPinchGestureRecognizer *)gestureRecognizer {
    
    if([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
        // Reset the last scale, necessary if there are multiple objects with different scales.
       lastScale1 = [gestureRecognizer scale];
    }
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan ||
        [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        
        CGFloat currentScale = [[[gestureRecognizer view].layer valueForKeyPath:@"transform.scale"] floatValue];
        
        // Constants to adjust the max/min values of zoom.
        const CGFloat kMaxScale = 2.0;
        const CGFloat kMinScale = 1.0;
        
        CGFloat newScale = 1 -  (lastScale1 - [gestureRecognizer scale]); // new scale is in the range (0-1)
        newScale = MIN(newScale, kMaxScale / currentScale);
        newScale = MAX(newScale, kMinScale / currentScale);
        CGAffineTransform transform = CGAffineTransformScale([[gestureRecognizer view] transform], newScale, newScale);
        [gestureRecognizer view].transform = transform;
        
       lastScale1 = [gestureRecognizer scale];  // Store the previous. scale factor for the next pinch gesture call
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return img;
}

#pragma mark TapDetectingImageViewDelegate methods

- (void)scrollViewDidZoom:(UIScrollView *)aScrollView {
    CGFloat offsetX = (scrollVie.bounds.size.width > scrollVie.contentSize.width)?
    (scrollVie.bounds.size.width - scrollVie.contentSize.width) * 0.5 : 0.0;
    CGFloat offsetY = (scrollVie.bounds.size.height > scrollVie.contentSize.height)?
    (scrollVie.bounds.size.height - scrollVie.contentSize.height) * 0.5 : 0.0;
    img.center = CGPointMake(scrollVie.contentSize.width * 0.5 + offsetX,
                                   scrollVie.contentSize.height * 0.5 + offsetY);
}
- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    // zoom in
    float newScale = [scrollVie zoomScale] * ZOOM_STEP;
    
    if (newScale >  scrollVie.maximumZoomScale){
        newScale = scrollVie.minimumZoomScale;
        CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
        
        [scrollVie zoomToRect:zoomRect animated:YES];
        
    }
    else{
        
        newScale = scrollVie.maximumZoomScale;
        CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
        
        [scrollVie zoomToRect:zoomRect animated:YES];
    }
}


- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer {
    // two-finger tap zooms out
    float newScale = [scrollVie zoomScale] / ZOOM_STEP;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [scrollVie zoomToRect:zoomRect animated:YES];
}

#pragma mark Utility methods

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    CGRect zoomRect;
    
    // the zoom rect is in the content view's coordinates.
    //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = [scrollVie frame].size.height / scale;
    zoomRect.size.width  = [scrollVie frame].size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}




- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    if([_listInfo[@"DataType"] isEqualToString:@"Image"]){
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (IBAction)closepdf:(id)sender {
//    showpdf=false;
//    [webview removeFromSuperview];
//     [webviewbutton removeFromSuperview];
//    [self.cancel setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
//    self.cancel.enabled = YES;
//}
- (IBAction)close:(id)sender {
       showimag=false;
    [scrollVie removeFromSuperview];
    [self.cancel setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.cancel.enabled = YES;
    
    [self.addimage setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
   self.addimage.enabled = YES;
    [self.save_image setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.save_image.enabled=YES;
}

- (IBAction)camera_action:(id)sender {
    // [self.addimage setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
   // self.addimage.enabled = YES;
    
    [self.save_image setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.save_image.enabled = YES;


    NSData *imgListtdata = [NSKeyedArchiver archivedDataWithRootObject:imgList];
    [[NSUserDefaults standardUserDefaults] setObject:imgListtdata forKey:@"imgList"];
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Efield Message"
                          message:@"Select an option"
                          delegate:self
                          cancelButtonTitle:@"Cancel"
                          otherButtonTitles:@"Camera", @"Photo Gallery", nil];
    alert.tag=90;
    [alert show];
    
    
}

- (void)imagePickerController:(UIImagePickerController *)imagePicker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
   // [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"imgList"];

    [self dismissViewControllerAnimated:YES completion:^{
        
        //    You can retrieve the actual UIImage

        UIImage *image1 = [info valueForKey:UIImagePickerControllerOriginalImage];
        //    Or you can get the image url from AssetsLibrary
        image=[info valueForKey:UIImagePickerControllerOriginalImage];
        NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
        
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
        NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
       
//        NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
//        if([[NSUserDefaults standardUserDefaults] objectForKey:@"img_count"] ==nil||img_count==0){
//            img_count=1;
//        }else{
//            img_count=img_count+1;
//        }
//        [[NSUserDefaults standardUserDefaults] setInteger:img_count forKey:@"img_count"];
        
        //[[NSUserDefaults standardUserDefaults]setObject:UIImageJPEGRepresentation(image, 0.5)  forKey:[NSString stringWithFormat:@"image%@", [dateFormatter stringFromDate:[NSDate date]]]];
        [[NSUserDefaults standardUserDefaults] synchronize];

      //  NSLog(@"img_arraycount%d",img_count);
        [imagePicker dismissViewControllerAnimated:NO completion:nil];
      //  img_countload=img_count;
        
        
      NSString  *datestring =[dateFormatter stringFromDate:[NSDate date]];
        datestring=[datestring stringByReplacingOccurrencesOfString:@":" withString:@""];
        datestring=[datestring stringByReplacingOccurrencesOfString:@"-" withString:@""];
        datestring=[datestring stringByReplacingOccurrencesOfString:@" " withString:@""];

                    NSDictionary *json = @{
                                           @"DetailsImageListId": @"",
                                           @"WorkorderAttachmentsId": @"",
                                           @"WorkOrderId":_WorkorderId,
                                           @"ImageName": [NSString stringWithFormat:@"image%@.jpeg",datestring],
                                           @"FilePath": @"",
                                           @"Comments": @"",
                                           @"QuestionValue": @"true",
                                           @"ImageDiscription": @"",
                                            @"NSData_value": [self imageToNSString:image1]
    };
            [imgList addObject:json];
         NSData *imgListtdata = [NSKeyedArchiver archivedDataWithRootObject:imgList];
        [[NSUserDefaults standardUserDefaults] setObject:imgListtdata forKey:@"imgList"];
        [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"fromcamera"];

              [self.tblForm reloadData];
     }];
}


- (NSString *)imageToNSString:(UIImage *)image
{
 imageData =UIImageJPEGRepresentation([self compressForUpload:image scale:1.0], 0.5);
    
    return   [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}


-(UIImage *)compressForUpload:(UIImage *)original scale:(CGFloat)scale
{
    CGSize originalSize = original.size;
    CGSize newSize = CGSizeMake(originalSize.width  *scale, originalSize.height  *scale);
    
    UIGraphicsBeginImageContext(newSize);
    [original drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* compressedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return compressedImage;
    
}



- (NSData *)stringToUIImage:(NSString *)string
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:string
                                                      options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    return data;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)imagePicker
{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:@"fromcamera"];

    [imagePicker dismissViewControllerAnimated:NO completion:nil];//Or call YES if you want the nice dismissal animation
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 90 && buttonIndex==1)
    {
       
        [self performSegueWithIdentifier:@"camera_imagesegue" sender:self];

}
    
    else  if (1 == buttonIndex && alertView.tag == 106)
    {
 
        [self save_Operation];
    }
    else  if (0 == buttonIndex && alertView.tag == 106)
    {
//      for (NSInteger i = [imgList count] - 1; i >= 0; --i) {
//
//
//            if([imgList[i][@"QuestionValue"]isEqualToString:@"true"]) {
//                [imgList removeObjectAtIndex:i];
//
//            }
//        }
         NSData *imgListtdata = [NSKeyedArchiver archivedDataWithRootObject:temp_imgList];
        [[NSUserDefaults standardUserDefaults] setObject:imgListtdata forKey:@"imgList"];
        [self.navigationController popViewControllerAnimated:YES];

    }
    else  if (1 == buttonIndex && alertView.tag == 56)
    {
       
        
            if([imgList[deleteindex][@"QuestionValue"] isKindOfClass:[NSNull class]]||imgList[deleteindex][@"QuestionValue"] ==nil||[imgList[deleteindex][@"QuestionValue"] isEqualToString:@""])
            {
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        DownloadManager *downloadObj = [[DownloadManager alloc]init];
       
           [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@DeleteMobileDashboardImageDiscription?DetailsImageListId%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],imgList[deleteindex][@"DetailsImageListId"]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"ImageDeletePath"];
 
        }
            
     else   if([imgList[deleteindex][@"QuestionValue"] isEqualToString:@"true"])
            
        {
            [imgList removeObjectAtIndex:deleteindex];
      
        }
    NSLog(@"imgList%@",imgList);
        [self.tblForm reloadData];
 
    }
    else if (alertView.tag == 90 && buttonIndex==2)
    {
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:nil];
        
    }  else if (alertView.tag == 55)
    {
        
         [self.navigationController popViewControllerAnimated:YES];
    }
    
}
- (IBAction)logoutAction:(id)sender {  [self logoutUser];
}

- (IBAction)changepasswordAction:(id)sender {
  [self performSegueWithIdentifier:@"alertsegue" sender:self];
    
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"response : %@",response);
    if ([key isEqualToString:@"GetImageBaseList"])
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
       base64str= [response valueForKeyPath:@"Data"];
        if([base64str length]>0)
        {
        [self showimage];
        }
    }
    
    
    if ([key isEqualToString:@"ImageDeletePath"])
    {
        [imgList removeObjectAtIndex:deleteindex];
        NSData *imgListtdata = [NSKeyedArchiver archivedDataWithRootObject:imgList];
        [[NSUserDefaults standardUserDefaults] setObject:imgListtdata forKey:@"imgList"];
        
        [self.tblForm reloadData];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    }else
    if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"]) 
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efield Message" message:@"Please try again later"];
        }
    }
    
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"camera_imagesegue"])
    {
        dynamiccameraVC *vc=[segue destinationViewController];
        vc.WorkorderId=_WorkorderId;
    }
}
- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}



-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

}

-(void)successmessage{
  
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSError *error1;
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:ttbresponse
                                                              options:kNilOptions
                                                                error:&error1];
    NSMutableArray  *tempimgList=[imgList mutableCopy];
    NSArray *imagefile_name=       [[[response1 valueForKeyPath:@"FileName"] description] componentsSeparatedByString:@","];
    NSLog(@"imagefile_name%@",[[response1 valueForKeyPath:@"FileName"] description]);
    int count = 0;//[tempimgList count]-[imagefile_name count];
    
    if([imagefile_name count]>0)
    {
 
            for(int j=0;j<[imgList count];j++)
            {
                
                NSMutableDictionary *dic=[imgList objectAtIndex:j];
        if ([dic[@"QuestionValue"] isEqualToString:@"true"]) {
               
                NSMutableDictionary *tempDic =
                [[NSMutableDictionary alloc] initWithDictionary:dic];
                [tempDic setObject:[imagefile_name objectAtIndex:count] forKey:@"ImageName"];
                [tempDic setObject:@"" forKey:@"QuestionValue"];

                [tempimgList replaceObjectAtIndex:j withObject:tempDic];
                count++;
        }
     }
    }
imgList=[tempimgList mutableCopy];
    NSLog(@"imgList%@",imgList);
    [[NSUserDefaults standardUserDefaults] setObject:[[response1 valueForKeyPath:@"FileName"] description] forKey:@"FileName"];
    NSData *imgListtdata = [NSKeyedArchiver archivedDataWithRootObject:imgList];
    [[NSUserDefaults standardUserDefaults] setObject:imgListtdata forKey:@"imgList"];
    
    [self.navigationController popViewControllerAnimated:YES];
     }



-(void)save_Operation
{
    NSData *imgListtdata = [NSKeyedArchiver archivedDataWithRootObject:imgList];
    [[NSUserDefaults standardUserDefaults] setObject:imgListtdata forKey:@"imgList"];
    NSInteger *upload_newimgcount;
    upload_newimgcount=0;
    for (int i=0; i<[imgList count];i++ ){
        if([imgList[i][@"QuestionValue"] isKindOfClass:[NSNull class]]||imgList[i][@"QuestionValue"] ==nil)
        {
        }else  if([imgList[i][@"QuestionValue"] isEqualToString:@"true"])
        {
            upload_newimgcount=upload_newimgcount+1;
        }
    }
     if(upload_newimgcount>0)
    {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSString *boundary = [self generateBoundaryString];
        
        // configure the request
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@UploadImage",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]]]];
        [request setHTTPMethod:@"POST"];
        
        // set content type
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // create body
        NSMutableData *httpBody = [NSMutableData data];
        
        // add params (all params are strings)
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        //[parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:username] forKey:@"UserName"];
        
        
        [parameters setValue:_WorkorderId forKey:@"WorkOrderId"];
        [parameters setValue:[NSString stringWithFormat:@"%d",upload_newimgcount]forKey:@"EntryCount" ];
        
        [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
            [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
        }];
        
        for (int i=0; i<[imgList count];i++ ){
            if([imgList[i][@"QuestionValue"] isKindOfClass:[NSNull class]]||imgList[i][@"QuestionValue"] ==nil)
            {
            }else  if([imgList[i][@"QuestionValue"] isEqualToString:@"true"])
            {
                
                NSString *imagename=[imgList[i][@"ImageName"] stringByReplacingOccurrencesOfString:@".jpg"
                                                                                        withString:@""];
                
                imagename=[imagename stringByReplacingOccurrencesOfString:@".jpeg"
                                                               withString:@""];
                
                NSLog(@"request = %@", [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@%@\"\r\n", @"FilePath1", imagename,@".jpeg"]  );
                
                
                
                [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@%@\"\r\n", @"FilePath1", imagename,@".jpeg"] dataUsingEncoding:NSUTF8StringEncoding]];
                
                [httpBody appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                
                
                base64str=imgList[i][@"NSData_value"];
                
                NSLog(@"base64str = %@", base64str);
                
                NSData *data =[[NSData alloc]initWithBase64EncodedString:base64str options:NSDataBase64DecodingIgnoreUnknownCharacters];
                
                [httpBody appendData:data];
                
                
                [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            }
        }
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        NSURLSession *session = [NSURLSession sharedSession];  // use sharedSession or create your own
        
        NSURLSessionTask *task = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (error) {
                UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Server Error - Please try again" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                
             //   callAlert.tag = 57;
                [callAlert show];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                NSLog(@"error = %@", error);
                return;
            }
            
            NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"result = %@", result);
            ttbresponse=data;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self successmessage];
            });
        }];
        [task resume];
        
        
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}
- (IBAction)saveaction:(id)sender

{
    [self save_Operation];
}


@end
