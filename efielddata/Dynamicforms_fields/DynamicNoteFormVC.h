//
//  DynamicNoteFormVC.h
//  Efield
//
//  Created by Praveen Kumar on 12/02/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextViewTableViewCell.h"
#import "TextFieldTableViewCell.h"
#import "SwitchTableViewCell.h"
#import "SingleSelectionTableViewCell.h"
#import "MultySelectionTableViewCell.h"
#import "SelectionViewController.h"
#import "DateAndTimeTableViewCell.h"
#import "DescriptionTableViewCell.h"
#import "SliderTableViewCell.h"
#import "ViewController.h"


@interface DynamicNoteFormVC : ViewController<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *dataList,*SubWorkOrderList;
    NSString *IsAddMoreDisplay,*Is2AddMoreDisplay,*IsDisplayPlacement,*IsStartEndTimeDisplay,*Is3AddMoreDisplay;
    NSString *ChildTestTypeId,*Child2TestTypeId,*Child3TestTypeId,*TestTypeId;
    __weak IBOutlet UILabel *lblTitle;
    UIDatePicker *datePicker;

}


@property (weak, nonatomic) IBOutlet UILabel *Header_lbl;

-(void)updateSelectedItems:(NSDictionary *)updatedItem questionID:(NSString *)questionID;
 
//@property (weak, nonatomic) IBOutlet UIView *sampledataview;
//@property (weak, nonatomic) IBOutlet UILabel *sampled_date;
@property (weak, nonatomic) IBOutlet UILabel *date;

@property (weak, nonatomic) IBOutlet UILabel *companynametxt;
@property (weak, nonatomic) IBOutlet UILabel *taskname_lbl;
@property (weak, nonatomic) IBOutlet UILabel *jobnumber;
- (IBAction)iscomplete:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *timespentfield;
- (IBAction)time_spent:(id)sender;
//@property (strong, nonatomic) IBOutlet UITextField *timespentfield;
@property(nonatomic)BOOL *isFromEditNote;
@property (nonatomic) BOOL IsEditNote;

@property (nonatomic, retain)NSString *assessmentMasterId;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UILabel *alertcnt;
@property (nonatomic, retain) NSString *TaskName,*JobNumber,*ProjectName,*Work_type;
 @property (weak, nonatomic) IBOutlet UISlider *timespentslider;
@property (weak, nonatomic) IBOutlet UISwitch *completeswitch;
@property (weak, nonatomic) IBOutlet UILabel *company_name;
@property (weak, nonatomic) IBOutlet UILabel *locationlink;
@property (weak, nonatomic) IBOutlet UILabel *locationlbl;
@property (weak, nonatomic) IBOutlet UILabel *dividerlbl;
@property (weak, nonatomic) IBOutlet UILabel *start_timelbl;
@property (weak, nonatomic) IBOutlet UIView *start_timeview;
@property (weak, nonatomic) IBOutlet UIView *end_timeview;
@property (weak, nonatomic) IBOutlet UILabel *end_timelbl;
@property (weak, nonatomic) IBOutlet UILabel *time_spentlbl;

@property (nonatomic, retain)NSString *WorkorderId;
@property (nonatomic, retain)NSString *noteTitle;
@property (weak, nonatomic) IBOutlet UIView *location;

- (IBAction)saveAction:(id)sender;
- (IBAction)cancelAction:(id)sender;
- (IBAction)alertaction:(id)sender;

- (IBAction)logoutAction:(id)sender;
 
@end
