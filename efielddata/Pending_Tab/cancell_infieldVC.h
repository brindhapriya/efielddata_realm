//
//  Appointmentselectvc.h
//  Efield
//
//  Created by MyMac1 on 2/28/17.
//  Copyright © 2017 iPhone. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "TextViewTableViewCell.h"
#import "TextFieldTableViewCell.h"
#import "SwitchTableViewCell.h"
#import "SingleSelectionTableViewCell.h"
#import "MultySelectionTableViewCell.h"
#import "SelectionViewController.h"
#import "DateAndTimeTableViewCell.h"
#import "DescriptionTableViewCell.h"
#import "SliderTableViewCell.h"
#import "ViewController.h"
@interface cancell_infieldVC : ViewController<UITableViewDelegate, UITableViewDataSource ,UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>

 
@property (nonatomic, retain) NSString *TaskName,*JobNumber,*ProjectName;

@property (weak, nonatomic) IBOutlet UILabel *date;


@property (weak, nonatomic) IBOutlet UILabel *taskname_lbl;
@property (weak, nonatomic) IBOutlet UILabel *jobnumber;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, retain)NSString *WorkorderId,*TesttypeId;
- (IBAction)save:(id)sender;
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *savebtn;



@end
