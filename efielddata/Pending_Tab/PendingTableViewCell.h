//
//  AlertListTableViewCell.h
//  Efield
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PendingTableViewCell :  UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *copworkorder;
@property (weak, nonatomic) IBOutlet UILabel *dividerlabel;
@property (strong, nonatomic) IBOutlet UILabel *task_name;
@property (strong, nonatomic) IBOutlet UILabel *job_scheduleddate;
@property (strong, nonatomic) IBOutlet UILabel *status;
@property (strong, nonatomic) IBOutlet UILabel *workorder;
@property (strong, nonatomic) IBOutlet UILabel *project;
@property (strong, nonatomic) IBOutlet UILabel *taskdetails;
@property (strong, nonatomic) IBOutlet UILabel *assignedto;
@property (strong, nonatomic) IBOutlet UILabel *contactname;
@property (strong, nonatomic) IBOutlet UILabel *contact_no;
@property (strong, nonatomic) IBOutlet UILabel *joblocation;
@property (strong, nonatomic) IBOutlet UILabel *direction;
@property (strong, nonatomic) IBOutlet UIView *orderview;
@property (weak, nonatomic) IBOutlet UIButton *cancel_infield;

@end
