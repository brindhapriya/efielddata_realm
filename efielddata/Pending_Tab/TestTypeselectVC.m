//
//  multiselectViewController.m
//  Efield
//
//  Created by MyMac1 on 2/7/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "TestTypeselectVC.h"
#import "DynamicNoteFormVC.h"
#import "multiselectTableViewCell.h"

@interface TestTypeselectVC ()
{
    NSString *
    selectedvalue,*selectedid,*Specialist,*locationname
    ;
}


@end

@implementation TestTypeselectVC{
    
    NSMutableArray *arSelectedRows;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _companynametxt.text=[[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];
    
    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
    [currentDTFormatter1 setDateFormat:@"dd/MM/YYYY"];
    
    self.navigationController.navigationBar.hidden = YES;
    
    NSString *eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@", eventDateStr);
    self.date.text=eventDateStr;
    self.taskname_lbl.text= _ProjectName   ;
  //  self.jobnumber.text=[NSString stringWithFormat:@"%@ - %@", _JobNumber,_TaskName];
     selectedvalue=@"";
     selectedid=@"";
    arSelectedRows = [[NSMutableArray alloc] init];
 
        [self  getTesttypeList];
        
        
   
        //[self.savebtn setEnabled:NO];
 
    
    
}



- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];

    
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
    self.alertcnt.text=[self updateALERTcount];
    for(int j=0;j<[testtypelist count];j++)
    {
        
        
        if([[[ [testtypelist valueForKey:@"TestTypeId"] objectAtIndex:j]description] isEqualToString:_TesttypeId])
        {
            
            
            NSIndexPath* selectedCellIndexPath= [NSIndexPath indexPathForRow:j inSection:0];
            [arSelectedRows addObject:selectedCellIndexPath];
            
       [self tableView:self.tableView didSelectRowAtIndexPath:selectedCellIndexPath];
  [self.tableView selectRowAtIndexPath:selectedCellIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        }
        
        
    }
}

- (void)getTesttypeList   {
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetTestTypeList?WorkOrderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetTestTypeList"];
    
    
    
}

 

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Server may be busy. Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
   // callAlert.tag = 56;
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


- (void)popViewController {
    
    
    [self performSegueWithIdentifier:@"dynamicformsegue" sender:self];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

{
    if (0 == buttonIndex && alertView.tag == 55)
    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//
//        [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
// });
    }
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"jsonString:\n%@", response);
    if ([key isEqualToString:@"SubmitTestTypeForApp"])
    {
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        //Open alert here
   
        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
        {
            [self performSegueWithIdentifier:@"dynamicformsegue" sender:self];
            
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            callAlert.tag = 55;
//            [callAlert show];
//                 });
        }
        else{
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //  callAlert.tag = 55;
            [callAlert show];
        }
    }else
    if ([key isEqualToString:@"GetTestTypeList"])
    {
        NSLog(@"GetTestTypeList%@",response);
        [arSelectedRows removeAllObjects];

        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        // showMoreFlag = true;
        [[NSUserDefaults standardUserDefaults]synchronize];
        testtypelist = [[NSMutableArray alloc]initWithArray:[response valueForKeyPath:@"Data"]];
        
        
       
        for(int j=0;j<[testtypelist count];j++)
        {
          
            NSString *TestTypeId=[[ [testtypelist valueForKey:@"ProjectTestTypeId"] objectAtIndex:j]description];
                if([TestTypeId isEqualToString:_TesttypeId])
                {
                    
                    
                    NSIndexPath* selectedCellIndexPath= [NSIndexPath indexPathForRow:j inSection:0];
         [arSelectedRows addObject:selectedCellIndexPath];
                    
            [self tableView:self.tableView didSelectRowAtIndexPath:selectedCellIndexPath];
                [self.tableView selectRowAtIndexPath:selectedCellIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
                }
                
            
        }
        [self.tableView reloadData];

        
    }
    else if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efield Message" message:@"Please try again later"];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [testtypelist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"multislectcell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"multislectcell"] ;
    }
   
//    if([testtypelist count]==0)
//    {
//        cell.textLabel.text =@"No Record Found" ;
//        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
//
//    }else{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   // if([self.tableView.indexPathsForSelectedRows containsObject:indexPath]){
        
        NSString *TestTypeId=[[ [testtypelist valueForKey:@"ProjectTestTypeId"] objectAtIndex:indexPath.row]description];
        if([TestTypeId isEqualToString:_TesttypeId])
        {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
      [arSelectedRows addObject:indexPath];
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
        
        cell.textLabel.text =[ [testtypelist valueForKey:@"TaskName"] objectAtIndex:indexPath.row] ;
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
   
        
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines=0;
        [  cell.textLabel sizeToFit];

   
    
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    
//    self.savebtn.enabled = YES;
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    // if([_selected_index_multi isEqualToString:@"0"]||[_selected_index_multi isEqualToString:@"1"]){
 //   if([_selected_index_multi isEqualToString:@"0"]||[_selected_index_multi isEqualToString:@"1"])
 //   {
        if(arSelectedRows.count) {
            
            //  for (int i=0; i< [arSelectedRows count]; i++) {
            
            
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:arSelectedRows[0]];
            cell.accessoryType = UITableViewCellAccessoryNone;
            NSIndexPath *indexpath=arSelectedRows[0];
            [arSelectedRows removeObject:arSelectedRows[0]];
            // [arSelectedRows removeAllObjects];
            cell.accessoryType = UITableViewCellAccessoryNone;

            // }
        }
    //}
//    if([_selected_index_multi isEqualToString:@"0"]||[_selected_index_multi isEqualToString:@"1"])
//    {
//        [_tableView deselectRowAtIndexPath:indexPath animated:YES];
//    }
    if(cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        //  if(arSelectedRows.count) {
        
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        [arSelectedRows removeObject:indexPath];
      
        
        
    }else   {
        [arSelectedRows addObject:indexPath];
     
        
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
       
        _TaskName=[[ [testtypelist valueForKey:@"TaskName"] objectAtIndex:indexPath.row]description];
        
    }
    NSArray *selectedRows = [tableView indexPathsForSelectedRows];
    for(NSIndexPath *i in selectedRows)
    {
        if(![i isEqual:indexPath])
        {
            [tableView deselectRowAtIndexPath:i animated:NO];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//    if([_selected_index_multi isEqualToString:@"0"]||[_selected_index_multi isEqualToString:@"1"])
  //  {
        if([arSelectedRows count]>0)
        {
        NSInteger    theRow       = arSelectedRows[0];
        NSIndexPath *theIndexPath = [NSIndexPath indexPathForRow:theRow inSection:0];
        [self.tableView selectRowAtIndexPath:theIndexPath
                                    animated:NO
                              scrollPosition:UITableViewScrollPositionNone];
        }
   // }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
//    self.savebtn.enabled = YES;
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if(cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        
        
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        // [arSelectedRows removeAllObjects];
 [arSelectedRows removeObject:indexPath];
        
    
        
    }
}

- (IBAction)cancel:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    location_dischargeViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"loc_discharge"];
    //
    //    viewController.patientId = self.patientId;
    //    viewController.providesPresentationContextTransitionStyle = YES;
    //    viewController.definesPresentationContext = YES;
    //
    //    [viewController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    //    [self presentViewController:viewController animated:YES completion:nil];
    
}

- (IBAction)save:(id)sender {
    
    
    
    for(NSIndexPath *indexPath in arSelectedRows) {
       
 selectedid=  [[testtypelist valueForKey:@"ProjectTestTypeId"] objectAtIndex:indexPath.row];
        
        
        
        
        }
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
 
    
    NSDictionary *json = @{
                           
                           @"WorkOrderId" : _WorkorderId,
                           
                           @"ProjectTestTypeId" :selectedid,
                           
                       
                           
                           
                           };
 
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTestTypeForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTestTypeForApp"];
    
 
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
   
        if ([segue.identifier isEqualToString:@"dynamicformsegue"]) {
            //dynamicform
            DynamicNoteFormVC *vc = [segue destinationViewController];
            vc.WorkorderId =_WorkorderId;
            vc.Work_type=_Work_type;
            vc.TaskName=_TaskName;
            vc.ProjectName=_ProjectName;
            vc.JobNumber=_JobNumber;
        }
}
- (IBAction)logoutAction:(id)sender {
    [self logoutUser];
    
}



- (IBAction)alertaction:(id)sender {
    [self performSegueWithIdentifier:@"alertsegue" sender:self];
}

@end
