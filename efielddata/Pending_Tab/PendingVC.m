                             //
//  AlertListVC.m
//  Efielddata
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import "PendingVC.h"
#import "Constants.h"  
#import "DynamicNoteFormVC.h"
#import "NoRecordsTableViewCell.h"
#import "TestTypeselectVC.h"
#import "AlertListVC.h"
#import "cancell_infieldVC.h"
//#import "UITabBarItem+CustomBadge.h"
@interface PendingVC ()<UITextFieldDelegate>
{
    BOOL isOnEditing;
    NSString *cancellworkorderIDstr;
    BOOL isMarkAll;
    NSIndexPath *COPYindexPath ;
    __weak IBOutlet UILabel *titleLbl;
    __weak IBOutlet UITableView *alertSearchTable;
    NSMutableArray *searchResultArray;
    NSMutableArray *searchFullResultArray;
    NSInteger selectedRowIndex;
    int selectedUtilityIndex;
    NSString *places;
    NSString *IsChange,*TestTypeId;
    NSString *TaskName,*JobNumber,*ProjectName;
    NSString *loadingmsg,*wono;
    NSString *datestring;
   }
@end

@implementation PendingVC

- (void)viewDidLoad {
      
    [super viewDidLoad];
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:isLoggedin];

    [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"pendingnext"];
    wono=@"";
    datestring=@"";
     loadingmsg=@"Loading...";
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
    self.cloasebtn.hidden=true;
   // [self updateALERTcount];
//    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"searchText"];
//    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"searchText_alert"];
//    
//    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"patientlistflag"];
    _companynametxt.text=[[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
    
     NSString *eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@", eventDateStr);
    _date.text=eventDateStr;
    self.navigationController.navigationBar.hidden = YES;
    alertList = [[NSMutableArray alloc]init];
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor whiteColor];
    refreshControl.tintColor = [UIColor blackColor];
  
    [refreshControl addTarget:self
                       action:@selector(updateDatarefresh)
             forControlEvents:UIControlEventValueChanged];
    [alertTable addSubview:refreshControl];
  _alertcnt.text=[self updateALERTcount];
   // [self performSelectorInBackground:@selector(updateDeviceTokentoServer) withObject:nil];
    self->jobdate.tag=101;
    
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 20)];
     self->jobdate.leftView = paddingView;
     self->jobdate.leftViewMode = UITextFieldViewModeAlways;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateData)
                                                 name:@"updateDataAlert"
                                               object:nil];
    [self->jobdate  setDelegate:self];
    
    [self->jobdate addTarget:self action:@selector(updateData1) forControlEvents:UIControlEventEditingDidEnd];
    
    [self->jobdate addTarget:self action:@selector(enable_closebtn) forControlEvents:UIControlEventEditingChanged];
 //   [self->jobdate addTarget:self action:@selector(textFieldShouldClear)  forControlEvents:UIControlEventEditingDidEnd];
      //[self->workorder addTarget:self action:@selector(updateData1) forControlEvents:UIControlEventEditingChanged];

//    [[NSUserDefaults standardUserDefaults]setObject:  @"" forKey:@"predicateString"];
//    if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"IsShowAddPatient"] isEqualToString:@"1"]){
//        _addpatient_button.hidden=NO;
//        _addpatient_button.userInteractionEnabled=YES;
//    }else{
//        _addpatient_button.hidden=YES;
//        _addpatient_button.userInteractionEnabled=NO;
//    }
    // Do any additional setup after loading the view.
//}
//       if([[[NSUserDefaults standardUserDefaults] objectForKey:userRole] isEqualToString:@"Admin"]){
//        
//        [self->searchbar setImage:[UIImage imageNamed:@"patientlist"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateNormal];
//        [self->searchbar setImage:[UIImage imageNamed:@"patientlist"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateSelected];
//        self->searchbar.showsBookmarkButton=YES;
//    }
//    else{
//    self->searchbar.showsBookmarkButton=NO;}
//    
}
//
//- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar
//{
//    
//    [self performSegueWithIdentifier:@"showlogfile" sender:nil];
//}


-(void)progress
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    
}
- (BOOL)textFieldShouldClear:(UITextField *)textField {
    //if we only try and resignFirstResponder on textField or searchBar,
    //the keyboard will not dissapear (at least not on iPad)!
    
        
        if (![self isNetworkAvailable]) {
            [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
         
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            });
            
            NSDictionary *json = @{
                                   
                                   @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
                                   
                                   @"RoleName" :[[NSUserDefaults standardUserDefaults] objectForKey:userRole],
                                   @"ReportStatus" : @"NW",
                                   @"Status" : @"P",
                                   @"Wono" : @"",
                                   @"FromJobDateTime" :@""
                                   
                                   
                                   };
            NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
            [dictEntry setObject:json forKey:[NSString stringWithFormat:@"FilterWorkOrder"]];
            
            DownloadManager *downloadObj = [[DownloadManager alloc]init];
            //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            
            [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetDashboard",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"GetDashboard"];
            
            
        }
   
     return YES;
}
//- (void)updateDeviceTokentoServer
//{
//     
//    if (![self isNetworkAvailable]) {
//        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
//        return;
//    }else{
//        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kdeviceToken]);
//    DownloadManager *downloadObj = [[DownloadManager alloc]init];
//        NSDictionary *json ;
//        NSString *deviceoken=[[NSUserDefaults standardUserDefaults] objectForKey:kdeviceToken];
//        if ([deviceoken isKindOfClass:[NSNull class]]||[deviceoken isEqualToString:@"null"]||deviceoken==nil)
//        {
//            json = @{
//                     
//                     @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
//                     @"AppId" :@""
//                     
//                     };
//        }else
//        {
//            json = @{
//                     @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
//                     @"AppId" : deviceoken
//                     };
//               }
//        
//        NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
//        [dictEntry setObject:json forKey:[NSString stringWithFormat:@"UserModel"]];
//        
//       // [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@UpdateUserAppId?UserName=%@&AppId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[NSUserDefaults standardUserDefaults] objectForKey:kdeviceToken]] andParameter:nil andMethod:@"POST" andDelegate:self andKey:@"UpdateUserAppId"];
//     [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@IOSUpdateUserAppId",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"UpdateUserAppId"];
//    }
//}
- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
//    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"gotopatientlist"] isEqualToString:@"true"]){
//        
//        [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"gotopatientlist"];
//        UINavigationController *nav      =[[self.tabBarController childViewControllers] objectAtIndex:3];
//        
//        PatientListVC *myController = (PatientListVC *)nav.topViewController;
//        self.tabBarController.selectedIndex = 3;
//    }
//    else{
  //  _alertPatientID = nil;
    //}
}
- (void)viewWillAppear:(BOOL)animated
{
    
    
        [super viewWillAppear:animated];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Display_canceledJobDateText"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"DisplayJobDateText"];

    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"datereplace"];
    
    _alertcnt.text=[self updateALERTcount];

    [[NSUserDefaults standardUserDefaults]setBool:true forKey:isLoggedin];
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:isPatientUpdatedNeed];
    [[NSUserDefaults standardUserDefaults] synchronize];
   if([[[NSUserDefaults standardUserDefaults] objectForKey:@"pendingnext"] isEqualToString:@"false"])
   {
  datestring=@"";
       wono=@"";

 self->jobdate.text=@"";
    
 
   }
    else if([[[NSUserDefaults standardUserDefaults] objectForKey:@"pendingnext"] isEqualToString:@"true"])
    {
          [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"pendingnext"];
   }
     [self updateData];
}
- (void)loadAlertData
{
    NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:details];
    NSDictionary* json = [NSKeyedUnarchiver unarchiveObjectWithData:data];
   
        alertList = [[NSMutableArray alloc]initWithArray:[json valueForKeyPath:@"Data"]];
        tempArr = [[NSArray alloc]initWithArray:[json valueForKeyPath:@"Data"]];
   
    
         [alertTable reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([alertList count]==0)
            {
                return 40;
        
            }else{
        //     if(![[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isEqualToString:@"New"]){
        //               return 205;
        //     }else{
        
                return 270;
             //}
            }
}
// - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if([alertList count]==0)
//    {
//        return 40;
//
//    }else{
////     if(![[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isEqualToString:@"New"]){
////               return 205;
////     }else{
//
//        return 270;
//     //}
//    }
//}


- (void) handlelocationtapFrom: (UITapGestureRecognizer *)recognizer
{
    
    UIView *parentCell = recognizer.view.superview;
    
    while (![parentCell isKindOfClass:[UITableViewCell class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentCell = parentCell.superview;
    }
    
    UIView *parentView = parentCell.superview;
    
    while (![parentView isKindOfClass:[UITableView class]]) {
        // iOS 7 onwards the table cell hierachy has changed.
        parentView = parentView.superview;
    }
    
    
    UITableView *tableView = (UITableView *)parentView;
    NSIndexPath *indexPath = [tableView indexPathForCell:(UITableViewCell *)parentCell];
    
    NSString *ProjectAddress = [[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row] description];
//
    
     ProjectAddress = [ProjectAddress stringByReplacingOccurrencesOfString:@" " withString:@"+"];
      NSString *url = [NSString stringWithFormat: @"https://www.google.com/maps/dir/Tierra+South+Florida,+Inc,+2765+Vista+Pkwy+%2310,+West+Palm+Beach,+FL+33411,+United+States/%@",ProjectAddress];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: url]];

    
}


- (void) handleNameLblTapFrom: (UITapGestureRecognizer *)recognizer
{
    
    UIView *parentCell = recognizer.view.superview;
    
    while (![parentCell isKindOfClass:[UITableViewCell class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentCell = parentCell.superview;
    }
    
    UIView *parentView = parentCell.superview;
    
    while (![parentView isKindOfClass:[UITableView class]]) {
        // iOS 7 onwards the table cell hierachy has changed.
        parentView = parentView.superview;
    }
    
    
    UITableView *tableView = (UITableView *)parentView;
    NSIndexPath *indexPath = [tableView indexPathForCell:(UITableViewCell *)parentCell];
    
    NSLog(@"indexPath = %@", indexPath);
    
    NSString *phoneStr = [[[alertList valueForKey:@"Phone"] objectAtIndex:indexPath.row] description];
    phoneStr = [phoneStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:phoneStr]];
    NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneStr]];
    
    if (![phoneStr isEqualToString:@""]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Call from Efielddata"
                                      message:@"Please click OK to call"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl])
                                 {
                                     [UIApplication.sharedApplication openURL:phoneFallbackUrl];
                                 }
                                 else if ([UIApplication.sharedApplication canOpenURL:phoneUrl])
                                 {
                                     [UIApplication.sharedApplication openURL:phoneUrl];
                                 }
                                 else
                                 {
                                     [self showAlertWithTitle:@"Efielddata Message" message:@"Your device not supported for phone calls"];
                                 }
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([alertList count]==0)
       {
           NoRecordsTableViewCell *cell = (NoRecordsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:noRecordsResuableIdentifier];
           cell.noRecordsLabel.text = loadingmsg;
           return cell;
           
       }else{
           NSLog(@"IsPlacement - %@,StatusText-%@",[[[alertList valueForKey:@"IsPlacement"] objectAtIndex:indexPath.row]description],
                 [[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row]description]);
           if(![[[alertList valueForKey:@"IsPlacement"] objectAtIndex:indexPath.row]boolValue]
              &&![[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isEqualToString:@"New"])
           {
               NSString *cellIdentifier = @"cellcancel";
               PendingTableViewCell_cancelled *cell = (PendingTableViewCell_cancelled *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
               cell.orderview.backgroundColor = [UIColor whiteColor];
               for (UIGestureRecognizer *gesture in cell.contact_no.gestureRecognizers) {
                   [cell.contact_no removeGestureRecognizer:gesture];
               }

               UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleNameLblTapFrom:)];
               cell.contact_no.userInteractionEnabled = YES;
               [cell.contact_no addGestureRecognizer:tapGestureRecognizer];
             //  cell.cancel_infield.tag = indexPath.row;
               cell.contact_no.tag = indexPath.row;
               //  int value =  [[[alertList valueForKey:@"TimeSpent"] objectAtIndex:indexPath.row] intValue];
               if([[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isEqualToString:@"New"]){

                   cell.status.textColor= [UIColor  colorWithRed:236/255.0f green:114/255.0f blue:103/255.0f alpha:1.0];
                   //[cell.cancel_infield addTarget:self action:@selector(cancellfield_clicked:) forControlEvents:UIControlEventTouchUpInside];
                  // cell.cancel_infield.hidden=false;
                   // cell.dividerlabel.hidden=false;
               }
               else{
                   cell.status.textColor=[UIColor grayColor];
                 //  cell.cancel_infield.hidden=true;
                   //   cell.dividerlabel.hidden=true;

               }

//               if([[[alertList valueForKey:@"IsPlacement"] objectAtIndex:indexPath.row]boolValue])
//               {
//                   cell.copworkorder.hidden=false;
//               }else{
//                   cell.copworkorder.hidden=true;
//               }
//               if( cell.copworkorder.hidden &&  cell.cancel_infield.hidden)
//               {
//                   cell.dividerlabel.hidden=true;
//               }
//               else{ cell.dividerlabel.hidden=false;
//
//               }
//

               if ([[[alertList valueForKey:@"TaskDisplayName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
                   cell.task_name.text = [[alertList valueForKey:@"TaskDisplayName"] objectAtIndex:indexPath.row];
               }
               else
               {
                   cell.task_name.text = @"";
               }

               cell.selectionStyle = UITableViewCellSelectionStyleNone;
               if ([[[alertList valueForKey:@"JobDateTimeDisplayUTCText"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
               {
                   cell.job_scheduleddate.text =[[alertList valueForKey:@"JobDateTimeDisplayUTCText"] objectAtIndex:indexPath.row];
               }
               else
               {
               }
               if ([[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
               {
                   cell.status.text =[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row];

               }
               else
               {

               }
               if ([[[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
               {
                   cell.workorder.text =[[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row];
               }
               else
               {
               }
               if ([[[alertList valueForKey:@"ProjectName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
               {
                   cell.project.text =[[alertList valueForKey:@"ProjectName"] objectAtIndex:indexPath.row];
               }
               else
               {
               }
               if ([[[alertList valueForKey:@"ProjectInfo"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
               {
                   cell.taskdetails.text =[[alertList valueForKey:@"ProjectInfo"] objectAtIndex:indexPath.row];
               }
               else
               {
               }
               if ([[[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
               {
                   cell.assignedto.text =[[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row];
               }
               else
               {
               }
               if ([[[alertList valueForKey:@"Contact"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
               {
                   cell.contactname.text =[[alertList valueForKey:@"Contact"] objectAtIndex:indexPath.row];
               }
               else
               {
               }
               for (UIGestureRecognizer *gesture in cell.joblocation.gestureRecognizers) {
                   [cell.joblocation removeGestureRecognizer:gesture];
               }
               if ([[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]&&[[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row]length]>0)
               {
                   cell.joblocation.text =[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row];


                   UITapGestureRecognizer *tapGestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlelocationtapFrom:)];
                   cell.joblocation.userInteractionEnabled = YES;
                   [cell.joblocation addGestureRecognizer:tapGestureRecognizer1];

               }
               else
               {
               }
               if ([[[alertList valueForKey:@"Instructions"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
               {
                   cell.direction.text =[[alertList valueForKey:@"Instructions"] objectAtIndex:indexPath.row];
               }
               else
               {
               }
               if ([[[alertList valueForKey:@"Phone"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
               {
                   cell.contact_no.text =[[alertList valueForKey:@"Phone"] objectAtIndex:indexPath.row];   }
               else
               {
               }
//
//               cell.copworkorder.tag = indexPath.row;
//
//               UITapGestureRecognizer *copytapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(copytapgestureaction:)];
//
//               cell.copworkorder.userInteractionEnabled = YES;
//               [cell.copworkorder addGestureRecognizer:copytapGestureRecognizer];
//
               return cell;

           }else{
           NSString *cellIdentifier = @"cell";
           PendingTableViewCell *cell = (PendingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
           cell.orderview.backgroundColor = [UIColor whiteColor];
        for (UIGestureRecognizer *gesture in cell.contact_no.gestureRecognizers) {
            [cell.contact_no removeGestureRecognizer:gesture];
        }
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleNameLblTapFrom:)];
        cell.contact_no.userInteractionEnabled = YES;
        [cell.contact_no addGestureRecognizer:tapGestureRecognizer];
    cell.cancel_infield.tag = indexPath.row;
      cell.contact_no.tag = indexPath.row;
//  int value =  [[[alertList valueForKey:@"TimeSpent"] objectAtIndex:indexPath.row] intValue];
    if([[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isEqualToString:@"New"]){
     
                cell.status.textColor= [UIColor  colorWithRed:236/255.0f green:114/255.0f blue:103/255.0f alpha:1.0];
    [cell.cancel_infield addTarget:self action:@selector(cancellfield_clicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.cancel_infield.hidden=false;
       // cell.dividerlabel.hidden=false;
    }
    else{
        cell.status.textColor=[UIColor grayColor];
        cell.cancel_infield.hidden=true;
     //   cell.dividerlabel.hidden=true;

    }
           
           if([[[alertList valueForKey:@"IsPlacement"] objectAtIndex:indexPath.row]boolValue])
           {
                  cell.copworkorder.hidden=false;
           }else{
               cell.copworkorder.hidden=true;
           }
    if( cell.copworkorder.hidden &&  cell.cancel_infield.hidden)
    {
           cell.dividerlabel.hidden=true;
    }
    else{ cell.dividerlabel.hidden=false;
        
    }
  
    
   
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if ([[[alertList valueForKey:@"JobDateTimeDisplayUTCText"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.job_scheduleddate.text =[[alertList valueForKey:@"JobDateTimeDisplayUTCText"] objectAtIndex:indexPath.row];
      }
    else
    {
  }
    if ([[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.status.text =[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row];
      
    }
    else
    {
 
    }
    if ([[[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.workorder.text =[[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row];
     }
    else
    {
   }
    if ([[[alertList valueForKey:@"ProjectName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.project.text =[[alertList valueForKey:@"ProjectName"] objectAtIndex:indexPath.row];
  }
    else
    {
   }
    if ([[[alertList valueForKey:@"ProjectInfo"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
    cell.taskdetails.text =[[alertList valueForKey:@"ProjectInfo"] objectAtIndex:indexPath.row];
   }
    else
    {
  }
           if ([[[alertList valueForKey:@"TaskDisplayName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
               cell.task_name.text = [[alertList valueForKey:@"TaskDisplayName"] objectAtIndex:indexPath.row];
           }
           else
           {
               //  cell.task_name.text = @"";
           }
           
    if ([[[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.assignedto.text =[[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row];
   }
    else
    {
  }
    if ([[[alertList valueForKey:@"Contact"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.contactname.text =[[alertList valueForKey:@"Contact"] objectAtIndex:indexPath.row];
    }
    else
    {
  }
           for (UIGestureRecognizer *gesture in cell.joblocation.gestureRecognizers) {
               [cell.joblocation removeGestureRecognizer:gesture];
           }
    if ([[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]&&[[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row]length]>0)
    {
        cell.joblocation.text =[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row];
      
        
        UITapGestureRecognizer *tapGestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlelocationtapFrom:)];
        cell.joblocation.userInteractionEnabled = YES;
        [cell.joblocation addGestureRecognizer:tapGestureRecognizer1];
        
    }
    else
    {
  }
    if ([[[alertList valueForKey:@"Instructions"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.direction.text =[[alertList valueForKey:@"Instructions"] objectAtIndex:indexPath.row];
   }
    else
    {
   }
    if ([[[alertList valueForKey:@"Phone"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.contact_no.text =[[alertList valueForKey:@"Phone"] objectAtIndex:indexPath.row];   }
        else
        {
    }
           
           cell.copworkorder.tag = indexPath.row;

           UITapGestureRecognizer *copytapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(copytapgestureaction:)];

           cell.copworkorder.userInteractionEnabled = YES;
           [cell.copworkorder addGestureRecognizer:copytapGestureRecognizer];
        
        return cell;

   }
}

}
 




- (void)copypopViewController {
    
    
    
    
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
    
            
            DownloadManager *downloadObj = [[DownloadManager alloc]init];
            //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            
            [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@CopyWorkOrderForApp?WorkOrderId=%@&UserName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],cancellworkorderIDstr,[[NSUserDefaults standardUserDefaults] objectForKey:username]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"CopyWorkOrderForApp"];
 
}
-(void)copytapgestureaction:(UITapGestureRecognizer*)recognizer
{
    
    
    UIView *parentCell = recognizer.view.superview;
    
    while (![parentCell isKindOfClass:[UITableViewCell class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentCell = parentCell.superview;
    }
    
    UIView *parentView = parentCell.superview;
    
    while (![parentView isKindOfClass:[UITableView class]]) {
        // iOS 7 onwards the table cell hierachy has changed.
        parentView = parentView.superview;
    }
    
    
    UITableView *tableView = (UITableView *)parentView;
 COPYindexPath = [tableView indexPathForCell:(UITableViewCell *)parentCell];
    
    cancellworkorderIDstr = [[[alertList valueForKey:@"WorkOrderId"] objectAtIndex:COPYindexPath.row]description];
    
    [self performSelector:@selector(copypopViewController) withObject:nil afterDelay:0.1];
    
    
}


-(void)cancellfield_clicked:(UIButton*)cancellworkorderID
{
    _workorderID=[[[alertList valueForKey:@"WorkOrderId"] objectAtIndex:cancellworkorderID.tag]description];
    TaskName=  [[[alertList valueForKey:@"TaskDisplayName"] objectAtIndex:cancellworkorderID.tag]description];
    JobNumber=    [[[alertList valueForKey:@"JobNumber"] objectAtIndex:cancellworkorderID.tag]description];
    ProjectName= [[[alertList valueForKey:@"ProjectName"] objectAtIndex:cancellworkorderID.tag]description];
 
    [self performSegueWithIdentifier:@"cancelinfield_segue" sender:nil];


    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == alertTable)
    {
        return 1;
    }
    else
    {
        
        
        return [searchResultArray count];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == alertTable)
    {
        NSLog(@"[alertList count] %lu",(unsigned long)[alertList count]);
        if([alertList count]==0)
        {
             return  1;
        }else{
        return [alertList count];
        }
    }
    else
    {
        if([[[searchResultArray objectAtIndex:section] objectForKey:@"childObject"] count]==0)
        {
            return  1;
        }else{
        return [[[searchResultArray objectAtIndex:section] objectForKey:@"childObject"] count];
        }
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([alertList count]==0)
    {}else{
    _workorderID=[[[alertList valueForKey:@"WorkOrderId"] objectAtIndex:indexPath.row]description];
   TaskName=  [[[alertList valueForKey:@"TaskDisplayName"] objectAtIndex:indexPath.row]description];
   JobNumber=    [[[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row]description];
    ProjectName= [[[alertList valueForKey:@"ProjectName"] objectAtIndex:indexPath.row]description];
    TestTypeId=[[[alertList valueForKey:@"ProjectTestTypeId"] objectAtIndex:indexPath.row]description];
    IsChange=[[[alertList valueForKey:@"IsChange"] objectAtIndex:indexPath.row]description];
if([IsChange isEqualToString:@"1"])
{
   [self performSegueWithIdentifier:@"testtypesegue" sender:nil];

}
else
{
    [self performSegueWithIdentifier:@"dynamicform" sender:nil];
}
    }
}

 //
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 57)
    {
        self.tabBarController.selectedIndex = 0;
    }
    if (alertView.tag == 58)
    {
     
    }
}


#pragma mark - API Delegate

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
    //[[UIApplication sharedApplication] endIgnoringInteractionEvents];
     UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:response delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    callAlert.tag = 57;
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    if ([key isEqualToString:@"GetDashboard"])
    {
           loadingmsg=@"No Data Found!";
       
      //  [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        alertTable.tableFooterView.hidden = YES;
        detailsDic = [[NSMutableDictionary alloc]initWithDictionary:response];
        NSData* data=[NSKeyedArchiver archivedDataWithRootObject:response];
        [[NSUserDefaults standardUserDefaults]setObject:data forKey:details];
        [[NSUserDefaults standardUserDefaults]synchronize];
        NSLog(@"data%@",response);
        
        [self loadAlertData];
    }
    
    //CopyWorkOrderForApp
    else if ([key isEqualToString:@"CopyWorkOrderForApp"])
    {   [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSLog(@"data%@",response);
       
            if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
            {
                _workorderID=[response valueForKeyPath:@"Data.WorkOrderId"];
                TaskName=  [response valueForKeyPath:@"Data.TaskName"];
                JobNumber=    [response valueForKeyPath:@"Data.JobNumber"];
                ProjectName= [response valueForKeyPath:@"Data.ProjectName"];
                TestTypeId=[[response valueForKeyPath:@"Data.ProjectTestTypeId"]description];
                IsChange=[[response valueForKeyPath:@"Data.IsChange"]description];
                if([IsChange isEqualToString:@"1"])
                {
                    [self performSegueWithIdentifier:@"testtypesegue" sender:nil];
                    
                }
                else
                {
                    [self performSegueWithIdentifier:@"dynamicform" sender:nil];
                }
            }
 
       // [self updateData];
        //[self viewDidLoad];
        //  [self viewWillAppear:YES];
    }
    else if ([key isEqualToString:@"SubmitCancelWoForApp"])
    {   [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        [self updateData];
        //[self viewDidLoad];
      //  [self viewWillAppear:YES];
    }
    else if ([key isEqualToString:@"UpdateUserAppId"])
    {
        
        NSLog(@"app id uploaded response %@",response);
    }
    else if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efielddata Message" message:@"Please try again later"];
        }
    }
   
}

-(void) showDatePicker: (UIDatePickerMode) modeDatePicker currentLbl:(UILabel *)lbl
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                     if(modeDatePicker == UIDatePickerModeDate) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                                       datestring= [formatter stringFromDate:picker.date];
                                         _cloasebtn.hidden=false;
                                         self->jobdate.text=datestring;
                                         wono=@"";
                                         
                                             [self updateData];
//                                         NSArray *temp;
//                                         temp = [tempArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"JobDateTimeText CONTAINS[c] %@",datestring]];
//                                     alertList = (NSMutableArray*)temp;
//                                     [alertTable reloadData];
                                     }
                                     
                                     
                                 }];
    [alertController addAction:doneAction];
    
    
    UIAlertAction *clearAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                    
                                      self->jobdate.text=@"";
                                      datestring=@"";
                                      wono=@"";
                                       [self updateData];
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    [alertController addAction:cancelAction];
    
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController  animated:YES completion:nil]; }


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.cloasebtn.hidden=true;
    [textField resignFirstResponder];

   // wono=@"";
   // datestring=@"";
    [self updateData];
    return YES;
}

- (void)updateData1
{
    datestring=@"";
    if([self->jobdate.text length]==0){
        self.cloasebtn.hidden=true;
        wono=@"";
        datestring=@"";
        [self updateData];
    }else{
        self.cloasebtn.hidden=false;
        wono=self->jobdate.text;
        datestring=self->jobdate.text;
        
        if([wono isEqualToString:@"00"]||[wono isEqualToString:@"0"]){
            
        }else{
   [self updateData];
        }
        
    }
  
}

- (IBAction)add_WOaction:(id)sender {
  //  [self performSegueWithIdentifier:@"addWO_segue" sender:self];
    [self performSegueWithIdentifier:@"WO_listsegue" sender:self];

//WO_listsegue
    //addWO_segue
}

- (IBAction)alertaction:(id)sender {
    [self performSegueWithIdentifier:@"alertsegue" sender:self];

}


- (void)enable_closebtn
{
    if([jobdate.text length]==0){
        self.cloasebtn.hidden=true;
    }else{
        self.cloasebtn.hidden=false;
        
    }
}
- (void)updateDatarefresh
{
    _cloasebtn.hidden=false;

    
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
        return;
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        wono = @"";
   
        self->jobdate.text=@"";
        
        NSDictionary *json = @{
                               
                               @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
                               
                               @"RoleName" :[[NSUserDefaults standardUserDefaults] objectForKey:userRole],
                                      @"ReportStatus" : @"NW",
                               @"Status" : @"P",
                               @"Wono" : @"",
                               @"FromJobDateTime" : @""
                               
                               
                               };
        NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
        [dictEntry setObject:json forKey:[NSString stringWithFormat:@"FilterWorkOrder"]];
        
        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetDashboard",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"GetDashboard"];
        
        
    }
    [refreshControl performSelector:@selector(endRefreshing) withObject:nil afterDelay:1.0f];
    [refreshControl endRefreshing];


}
- (void)updateData
{
    
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
        return;
    }else{
 
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        
        NSDictionary *json = @{
                               
                               @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
                               
                               @"RoleName" :[[NSUserDefaults standardUserDefaults] objectForKey:userRole],
                                     @"ReportStatus" : @"NW",
                               @"Status" : @"P",
                               @"Wono" : wono,
                               @"FromJobDateTime" :datestring
                               
                               
                               };
        NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
        [dictEntry setObject:json forKey:[NSString stringWithFormat:@"FilterWorkOrder"]];

        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetDashboard",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"GetDashboard"];
     
    
    }
}
-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {
}
- (IBAction)logoutAction:(id)sender
{
//    if (![self isNetworkAvailable]) {
//        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
//        return;
//    }else{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    });
//    DownloadManager *downloadObj = [[DownloadManager alloc]init];
//    //[downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@IOSResetUserAppId?UserName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username]] andParameter:nil andMethod:@"POST" andDelegate:self andKey:@"logout"];
//        NSDictionary *json = @{
//                               
//                               @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username]
//                               
//                               };
//        NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
//        [dictEntry setObject:json forKey:[NSString stringWithFormat:@"UserModel"]];
//        
//        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@IOSResetUserAppId",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"logout"];
    
         [self logoutUser];
        
   // }
}

- (IBAction)changepasswordAction:(id)sender {
    [self changePassword];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSString *name;
    NSString *room ;
       [[NSUserDefaults standardUserDefaults]setObject:@"true" forKey:@"pendingnext"];
    
    if ([segue.identifier isEqualToString:@"alertsegue"]) {
        
        AlertListVC *vc = [segue destinationViewController];
        
    }
    else if ([segue.identifier isEqualToString:@"cancelinfield_segue"]) {
        
        cancell_infieldVC *vc = [segue destinationViewController];
        vc.WorkorderId =_workorderID;
        
        vc.TaskName=TaskName;
        vc.ProjectName=ProjectName;
        vc.JobNumber=JobNumber;
    }
    //
    else
     
        
    if ([segue.identifier isEqualToString:@"dynamicform"]) {
        //dynamicform
        DynamicNoteFormVC *vc = [segue destinationViewController];
        vc.WorkorderId =_workorderID;
        vc.Work_type=@"Pending Work Order";

        vc.TaskName=TaskName;
        vc.ProjectName=ProjectName;
         vc.JobNumber=JobNumber;
    }
   else if ([segue.identifier isEqualToString:@"testtypesegue"]) {
        //dynamicform
        TestTypeselectVC *vc = [segue destinationViewController];
        vc.WorkorderId =_workorderID;
        vc.TaskName=TaskName;
        vc.ProjectName=ProjectName;
       vc.Work_type=@"Pending Work Order";

        vc.JobNumber=JobNumber;
       vc.TesttypeId=TestTypeId;
    }
    
}

- (IBAction)jobdateaction:(id)sender {
    
    [self showDatePicker:UIDatePickerModeDate currentLbl:self->jobdate];
}


- (IBAction)close:(id)sender {
    self->jobdate.text=@"";wono=@"";
    datestring=@"";
    self.cloasebtn.hidden=true;
    [self updateData];
}
@end
