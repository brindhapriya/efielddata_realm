//
//  Appointmentselectvc.h
//  Efield
//
//  Created by MyMac1 on 2/28/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "ViewController.h"

@interface TestTypeselectVC : ViewController<UITableViewDelegate, UITableViewDataSource ,UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>

{
    
    NSMutableArray *testtypelist;

}
@property (nonatomic, retain) NSString *TaskName,*JobNumber,*ProjectName;

@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *alertcnt;

@property (weak, nonatomic) IBOutlet UILabel *companynametxt;
@property (weak, nonatomic) IBOutlet UILabel *taskname_lbl;
@property (weak, nonatomic) IBOutlet UILabel *jobnumber;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, retain)NSString *WorkorderId,*TesttypeId,*Work_type;
- (IBAction)save:(id)sender;
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *savebtn;
- (IBAction)alertaction:(id)sender;
- (IBAction)logoutAction:(id)sender;



@end
