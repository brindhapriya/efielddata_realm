//
//  NoRecordsTableViewCell.h


//
//  Created by brindha on 25/03/16.
//  Copyright © 2016 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoRecordsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *noRecordsLabel;

@end
