//
//  DownloadManager.m
//  EatHalal
//
//  Created by Tech Basics on 14/09/15.
//  Copyright (c) 2015 Tech Basics. All rights reserved.
//

#import "DownloadManager.h"
#import "Constants.h"

@implementation DownloadManager

-(void)callServerWithURL:(NSString *)serverUrl andParameter:(NSDictionary *)parameter andMethod:(NSString *)method andDelegate:(id)callDelegate andKey:(NSString *)key
{
    
    _delegate = callDelegate;
   // serverUrl = [serverUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"serverUrl : %@",serverUrl);
    if([method isEqualToString:@"GET"])
    {
        
        
        NSURLSession *session = [NSURLSession sharedSession];
        [[session dataTaskWithURL:[NSURL URLWithString:serverUrl]
                completionHandler:^(NSData *data,
                                    NSURLResponse *response,
                                    NSError *error) {
                    // handle response
                    if(data != nil)
                    {
                        NSError *error = nil;
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if([_delegate respondsToSelector:@selector(callBackWithSuccessResponse:andKey:)])
                            [_delegate callBackWithSuccessResponse:json andKey:key];
                        });
                        
                        
                    }
                    else
                    {
                        if([_delegate respondsToSelector:@selector(callBackWithFailureResponse:andKey:)])
                        [_delegate callBackWithFailureResponse:nil andKey:key];
                    }
                    
                }] resume];
    }
    else if ([method isEqualToString:@"POST"])
    {
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        NSURL *url = [NSURL URLWithString:serverUrl];
       // NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                           timeoutInterval:60.0];
        if (nil != parameter) {
            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:parameter options:NSJSONWritingPrettyPrinted error:nil];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
            NSLog(@"jsonString:\n%@", jsonString);
            
            request.HTTPBody = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        }
        request.HTTPMethod = @"POST";
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
         NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // The server answers with an error because it doesn't receive the params
            if (error) {
//                NSError* error;
//                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//                NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"LogFile.txt"];
//                NSString *content = [NSString stringWithFormat:@"URL :  %@Request  :  %@\r\n Parameter : %@ \r\n error : %@ \r\n  ",serverUrl,request,parameter,error];
//                NSFileHandle *file = [NSFileHandle fileHandleForUpdatingAtPath:filePath];
//                
//                NSString *contents = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];     NSString *newstring= [NSString stringWithFormat:@"%@\n%@",contents,content];
//                [file writeData:[newstring dataUsingEncoding:NSUTF8StringEncoding]];
//                
                NSLog(@"%@", error);
                dispatch_async(dispatch_get_main_queue(), ^{
                    if([_delegate respondsToSelector:@selector(callBackWithFailureResponse:andKey:)])
                     // [_delegate callBackWithFailureResponse:[NSString stringWithFormat: @"%@%@",error,@"- Please try again"] andKey:key];
                        [_delegate callBackWithFailureResponse:@"Server Error - Please try again" andKey:key];
                });
            } else {
            if(data != nil)
            {
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                //NSLog(@"json : %@", json);
                //NSLog(@"json Str : %@",[[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy]);
                dispatch_async(dispatch_get_main_queue(), ^{
                    if([_delegate respondsToSelector:@selector(callBackWithSuccessResponse:andKey:)])
                    [_delegate callBackWithSuccessResponse:json andKey:key];
                });
            }
            else
            {
//                NSError* error;
//                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//                NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"LogFile.txt"];
//                NSString *content = [NSString stringWithFormat:@"URL :  %@Request  :  %@\r\n Parameter : %@ \r\n  ",serverUrl,request,parameter];
//                NSFileHandle *file = [NSFileHandle fileHandleForUpdatingAtPath:filePath];
//                
//                NSString *contents = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];     NSString *newstring= [NSString stringWithFormat:@"%@\n%@",contents,content];
//                [file writeData:[newstring dataUsingEncoding:NSUTF8StringEncoding]];
//                
                if([_delegate respondsToSelector:@selector(callBackWithFailureResponse:andKey:)])
                [_delegate callBackWithFailureResponse:@"Server may be busy. Please try again later." andKey:key];
            }
        }
            
        }];
        [postDataTask resume];
    }
    
}


-(void)callServerWithURLAuthentication:(NSString *)serverUrl andParameter:(NSDictionary *)parameter andMethod:(NSString *)method andDelegate:(id)callDelegate andKey:(NSString *)key
{
    
    _delegate = callDelegate;
    NSLog(@"serverUrl : %@",serverUrl);
  //  serverUrl = [serverUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    if([method isEqualToString:@"GET"])
    {
        
                NSURLSession *session = [NSURLSession sharedSession];
        [[session dataTaskWithURL:[NSURL URLWithString:serverUrl]
                completionHandler:^(NSData *data,
                                    NSURLResponse *response,
                                    NSError *error) {
                    // handle response
                    if(data != nil)
                    {
                        
                        
                        NSError *error = nil;
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        
                        //NSLog(@"Eoror:%@",error.localizedDescription);
                        //NSLog(@"json : %@", json);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if([_delegate respondsToSelector:@selector(callBackWithSuccessResponse:andKey:)])
                                [_delegate callBackWithSuccessResponse:json andKey:key];
                        });
                        
                    }
                    else
                    {
                        //NSLog(@"jsonStr::%@",[[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy]);
                        if([_delegate respondsToSelector:@selector(callBackWithFailureResponse:andKey:)])
                            [_delegate callBackWithFailureResponse:nil andKey:key];
                    }
                    
                }] resume];
    }
    else if ([method isEqualToString:@"POST"])
    {
    
        NSDictionary *headers = @{ @"authorization": @"Basic RWZpZWxkQXBwVXNlcjpFVCVeKSlVU2VAOXIh"
                                  // @"cache-control": @"no-cache",
                                   //@"postman-token": @"58ae4691-989b-d38d-c85c-2a2346692668"
                                   };
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serverUrl]
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                           timeoutInterval:60.0];
        
        
        if (nil != parameter) {
            NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:parameter options:NSJSONWritingPrettyPrinted error:nil];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
            NSLog(@"jsonString:\n%@", jsonString);
            
            request.HTTPBody = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        }
 
      
         request.HTTPMethod = @"POST";
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [request setAllHTTPHeaderFields:headers];
       
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        
                                                        
                                                        if (error) {
                                                            
//                                                           
//                                                            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//                                                            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"LogFile.txt"];
//                                                            NSString *content = [NSString stringWithFormat:@"URL :  %@Request  :  %@\r\n Parameter : %@ \r\n Header : %@  \r\n error code %@",serverUrl,request,parameter,headers,[error localizedDescription]];
//                                                            NSFileHandle *file = [NSFileHandle fileHandleForUpdatingAtPath:filePath];
//                                                            
//                                                            NSString *contents = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];     NSString *newstring= [NSString stringWithFormat:@"%@\n%@",contents,content];
//                                                            [file writeData:[newstring dataUsingEncoding:NSUTF8StringEncoding]];
//                                                            
                                                          
                                                            NSLog(@"%@", error);
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                if([_delegate respondsToSelector:@selector(callBackWithFailureResponse:andKey:)])
                                                                     //[_delegate callBackWithFailureResponse:[NSString stringWithFormat: @"authenticated%@%@",error,@"- Please try again"] andKey:key];
                                                                    [_delegate callBackWithFailureResponse:[NSString stringWithFormat: @"Server Error - Please try again"] andKey:key];
                                                            });
                                                        } else {
                                                            //NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                                                                        if(data != nil)
                                                            {
                                                                
                                                                
                                                                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                //NSLog(@"json : %@", json);
                                                                //NSLog(@"json Str : %@",[[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy]);
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    if([_delegate respondsToSelector:@selector(callBackWithSuccessResponse:andKey:)])
                                                                        [_delegate callBackWithSuccessResponse:json andKey:key];
                                                                });
                                                            }
                                                            else
                                                            {
//                                                                NSError* error;
//                                                                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//                                                                NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"LogFile.txt"];
//                                                                NSString *content = [NSString stringWithFormat:@"URL :  %@Request  :  %@\r\n Parameter : %@ \r\n Header : %@",serverUrl,request,parameter,headers];
//                                                                NSFileHandle *file = [NSFileHandle fileHandleForUpdatingAtPath:filePath];
//                                                                
//                                                                NSString *contents = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];     NSString *newstring= [NSString stringWithFormat:@"%@\n%@",contents,content];
//                                                                [file writeData:[newstring dataUsingEncoding:NSUTF8StringEncoding]];
                                                                
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                if([_delegate respondsToSelector:@selector(callBackWithFailureResponse:andKey:)])
                                                                    [_delegate callBackWithFailureResponse:@"Server may be busy. Please try again later." andKey:key];
                                                                     });
                                                            }
                                                        }
                                                    }];
        [dataTask resume];
        
    }
    
}
//
////Updating Dashboard counts
//+(void)updateDashboardCount:(NSDictionary *)dict
//{
//    if (nil != dict) {
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        //[defaults setValue:[dict valueForKey:NS_AlertCount] forKey:NS_AlertCount];
//       // [defaults setValue:[dict valueForKey:NS_HomeHeath] forKey:NS_HomeHeath];
//        [defaults setValue:[dict valueForKey:NS_HomeWithSelfCare] forKey:NS_HomeWithSelfCare];
//        [defaults setValue:[dict valueForKey:NS_Hospital] forKey:NS_Hospital];
//        [defaults setValue:[dict valueForKey:NS_InpatientSNF] forKey:NS_InpatientSNF];
//        [defaults setValue:[dict valueForKey:NS_OutpatientRehap] forKey:NS_OutpatientRehap];
//        [defaults setValue:[dict valueForKey:NS_ShowMoreFlag] forKey:NS_ShowMoreFlag];
//        [defaults synchronize];
//        
//    }
//}



@end
