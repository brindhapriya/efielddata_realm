//
//  Constants.h
//  Efield
//
//  Created by vijay kumar on 17/03/16.
//  Copyright © 2016 VJ Solutions. All rights reserved.
//
#import <UIKit/UIKit.h>

#ifndef Constants_h
#define Constants_h

#define NOTES_COLOR [UIColor colorWithRed:75.0/255.0 green:135.0/255.0 blue:203.0/255.0 alpha:1.0]
#define MDLIST_COLOR [UIColor colorWithRed:77.0/255.0 green:188.0/255.0 blue:249.0/255.0 alpha:1.0]
#define MEDS_COLOR  [UIColor colorWithRed:254.0/255.0 green:130.0/255.0 blue:8.0/255.0 alpha:1.0]
#define LOCATIONS_COLOR [UIColor colorWithRed:67.0/255 green:214.0/255.0 blue:82.0/255.0 alpha:1.0]
#define HOSPITAL_COLOR [UIColor redColor]

#define HIGH_RISK_COLOR RED
#define MEDIUM_RISK_COLOR   YELLOW
#define LOW_RISK_COLOR  DARK_GREEN

#define DARK_GREEN [UIColor colorWithRed:18.0/255 green:153.0/255.0 blue:72.0/255.0 alpha:1.0]
#define Light_GREEN [UIColor colorWithRed:138.0/255 green:196.0/255.0 blue:64.0/255.0 alpha:1.0]
#define ORANGE [UIColor colorWithRed:255.0/255 green:165.0/255.0 blue:0.0/255.0 alpha:1.0]
#define RED [UIColor colorWithRed:255.0/255 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]
#define YELLOW [UIColor colorWithRed:255.0/255 green:255.0/255.0 blue:72.0/255.0 alpha:1.0]
#define DEFAULT_BUTTON_COLOR [UIColor colorWithRed:0.0/255 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]

#define HEADER_FONT [UIFont boldSystemFontOfSize:17.0]

static NSString * const NS_HomeWithSelfCare = @"NewwoTotal";
static NSString * const NS_Hospital = @"DraftwoTotal";
static NSString * const NS_InpatientSNF = @"ReportwoTotal";
static NSString * const NS_OutpatientRehap = @"ReviewedwoTotal"; 
//TableView Cell
static NSString * const noRecordsResuableIdentifier = @"NoRecordsCell";

/****Alert List****/
static NSString * const READ = @"Mark Read";


/* LoginViewController */
static NSString * const TouchIDEnabled = @"touchID";
static NSString * const TouchIDEnabled_Key = @"yes";
static NSString * const TouchID_AUTHENTICATION_MSG = @"Place your finger for authentication!";

static NSString * const FaceID_AUTHENTICATION_MSG = @"Show your face for authentication!";
static NSString * const TouchID_AUTHENTICATION_PROBLEM = @"There was a problem verifying your identity.";
static NSString * const TouchID_AUTHENTICATION_FAILED = @"You are not the device owner.";
static NSString * const TouchID_AUTHENTICATION_OKBTN = @"OK";
static NSString * const TouchID_AUTHENTICATION_ERROR_TITLE = @"Error";

static NSString * const TouchID_NOT_AVAILABLE_TITLE = @"Efield Login";

static NSString * const Biometrics_NOT_AVAILABLE_MSG = @"Your device cannot authenticate using Biometrics.";
static NSString * const TouchID_NOT_AVAILABLE_MSG = @"Your device cannot authenticate using TouchID.";
static NSString * const FaceID_NOT_AVAILABLE_MSG = @"Your device cannot authenticate using FaceID.";

static NSString * const FaceID_ALERT_TITLE = @"Face ID Authentication";
static NSString * const TouchID_ALERT_TITLE = @"Touch ID Authentication";
static NSString * const TouchID_ALERT_MESSAGE = @"Do you want to enable TouchID for this app ?";
static NSString * const FaceID_ALERT_MESSAGE = @"Do you want to enable Face ID for this app ?";

static NSString * const TouchID_ALERT_OKBTN = @"Yes";
static NSString * const TouchID_ALERT_CANCELBTN = @"No";


#endif /* Constants_h */
