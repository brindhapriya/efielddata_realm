//
//  DownloadManager.h
//  EatHalal
//
//  Created by Tech Basics on 14/09/15.
//  Copyright (c) 2015 Tech Basics. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DownloadManagerDelegate <NSObject>

@required

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key;
-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key;

@end

@interface DownloadManager : NSObject

@property(nonatomic,strong) id<DownloadManagerDelegate> delegate;

-(void)callServerWithURL:(NSString *)serverUrl andParameter:(NSDictionary *)parameter andMethod:(NSString *)method andDelegate:(id)callDelegate andKey:(NSString *)key;
-(void)callServerWithURLAuthentication:(NSString *)serverUrl andParameter:(NSDictionary *)parameter andMethod:(NSString *)method andDelegate:(id)callDelegate andKey:(NSString *)key;
+(void)updateDashboardCount:(NSDictionary *)dict;
@end
