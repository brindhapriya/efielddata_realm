//
//  AlertListVC.h
//  Efield
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertListTableViewCell.h"
#import "ViewController.h"

@interface edit_addTimesheet : ViewController {
    __weak IBOutlet UITableView *alertTable;
    __weak IBOutlet UISearchBar *searchbar;
    NSArray *tempArr;
    NSMutableArray *timesheet_list;

    NSMutableArray *workorder_list;
    IBOutlet UITextField *jobdate;
    IBOutlet UITextField *workorder;
    NSMutableArray *checkBoxArr;
    NSInteger index;
    NSDictionary *dummyDic;
    NSMutableDictionary *alertListDic;
    UIRefreshControl *refreshControl;
    __weak IBOutlet UIView *tableFooterView;
    __weak IBOutlet UIBarButtonItem *markAllButton;
}
- (IBAction)pdf_action:(id)sender;
- (IBAction)add_action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *submit;
- (IBAction)submit_action:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *pdf; 
@property (weak, nonatomic) IBOutlet UIView *popupview;
@property (weak, nonatomic) IBOutlet UILabel *alertcnt;
- (IBAction)timesheetdate_action:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UITextField *timesheet_date;
@property (weak, nonatomic) IBOutlet UITextField *timesheet_absence;
@property (weak, nonatomic) IBOutlet UITextField *timesheet_hours;
@property (weak, nonatomic) IBOutlet UITextField *timesheet_notes;
- (IBAction)absence_action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *timesheet_btn;
- (IBAction)timesheetcancel_action:(id)sender;
- (IBAction)timesheetsave_action:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *companynametxt;
//@property (weak, nonatomic) IBOutlet UIButton *editIcon;
//@property(nonatomic, retain)NSString *workorderID;
//@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
- (IBAction)logoutAction:(id)sender; 
@property (weak, nonatomic) IBOutlet UIButton *closebtn;
@property (weak, nonatomic) IBOutlet UITableView *workorder_table;
@property (weak, nonatomic) IBOutlet UITableView *timesheettable;
@property (weak, nonatomic) IBOutlet UITextField *datefield;
@property (weak, nonatomic) IBOutlet UILabel *timefield;
@property (weak, nonatomic) IBOutlet UIButton *add_btn;

- (IBAction)alertaction:(id)sender; 
- (IBAction)dateaction:(id)sender;

- (void)updateData;
@end
