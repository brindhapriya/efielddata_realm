//
//  AppDelegate.h
//  Efield
//
//  Created by iPhone on 09/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ViewController.h"
#import "PendingVC.h"
#import "LoginViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSTimer     *logoutTimer;
    UIAlertView *alertSession;
    
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIView *notifView;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)authenticateUser;

@end

