//
//  TIMERUIApplication.m
//  PMI
//
//  Created by MyMac1 on 2/28/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "TIMERUIApplication.h"

@implementation TIMERUIApplication

-(void)sendEvent:(UIEvent *)event
{
    @try{
    if (!myidleTimer)
    {
        [self resetIdleTimer];
    }
    
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0)
    {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
  
             if (phase == UITouchPhaseBegan || phase == UITouchPhaseEnded)
        {
            [self resetIdleTimer];
        }
        
    }
    [super sendEvent:event];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception => %@",exception);
    }
}
//as labeled...reset the timer
-(void)resetIdleTimer
{
    if (myidleTimer)
    {
        [myidleTimer invalidate];
        
        
    }
    //convert the wait period into minutes rather than seconds
    int timeout =  kApplicationTimeoutInMinutes * 60;
    
    myidleTimer = [NSTimer scheduledTimerWithTimeInterval:timeout target:self selector:@selector(idleTimerExceeded) userInfo:nil repeats:YES];
    
}

-(void)invalidateTimer
{
    [myidleTimer invalidate];
    myidleTimer = nil;
    
    
}

//if the timer reaches the limit as defined in kApplicationTimeoutInMinutes, post this notification
-(void)idleTimerExceeded
{
    NSLog(@"Post event");
 
    [[NSNotificationCenter defaultCenter] postNotificationName:kApplicationDidTimeoutNotification object:nil];
}
@end
