//
//  TIMERUIApplication.h
//  PMI
//
//  Created by MyMac1 on 2/28/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#define kApplicationTimeoutInMinutes 30
//30
#define kApplicationDidTimeoutNotification @"AppTimeOut"

@interface TIMERUIApplication : UIApplication
{
    NSTimer     *myidleTimer;
}

-(void)resetIdleTimer;
-(void)invalidateTimer;
@end
