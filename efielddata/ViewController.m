//
//  ViewController.m
//  Efield
//
//  Created by iPhone on 09/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];

    // Do any additional setup after loading the view, typically from a nib.
}
- (NSString *)updateALERTcount
{
    NSString *string ;
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efield Message" message:@"No network, please check your internet connection"];
        return @"";
    }else{
        
      
        NSMutableURLRequest *request= [[NSMutableURLRequest alloc]init];
        NSString *str=
        
        //[NSString stringWithFormat:@"%@GetIOSEditAssessment?AssessmentMasterid=%@&NoteId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_assessmentMasterId,_WorkorderId];
        [NSString stringWithFormat:@"%@GetAllCurrentUserAlertCountForApp?UserName=%@&RoleName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[NSUserDefaults standardUserDefaults] objectForKey:userRole]];
        NSLog(@"url%@",[NSString stringWithFormat:@"%@GetAllCurrentUserAlertCountForApp?UserName=%@&RoleName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[NSUserDefaults standardUserDefaults] objectForKey:userRole]]);
        [request setURL:[NSURL URLWithString:str]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
     //   string =[NSString stringWithUTF8String:[urlData bytes]];
        
      // string = [NSString stringWithUTF8String:[urlData bytes]];

      //ascii codes

    
        //[[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding] ;
       string = [NSString stringWithContentsOfURL:[NSURL URLWithString:str] encoding:NSUTF8StringEncoding error:&error];
        string= [string stringByReplacingOccurrencesOfString: @"\"" withString:@""];

        NSLog(@"urlData%@",string);
        //
        //        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        //        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        //
        //   [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@GetAllCurrentUserAlertCountForApp?UserName=%@&RoleName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[NSUserDefaults standardUserDefaults] objectForKey:userRole]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetAllCurrentUserAlertCountForApp"];
       
    }
    ;
     return [NSString stringWithFormat:@" %@ ",string];
    
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phoneNumber];
}

- (BOOL)isiPad
{
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (BOOL)isNetworkAvailable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - API Delegate

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    if([key isEqualToString:@"GetPhysicianDashboard"])
    {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:details];
        detailsDic = [[NSMutableDictionary alloc]initWithDictionary:response];
        NSData* data=[NSKeyedArchiver archivedDataWithRootObject:response];
        [[NSUserDefaults standardUserDefaults]setObject:data forKey:details];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (0 == buttonIndex && alertView.tag == 55)
    {
        UIApplication *app = [UIApplication sharedApplication];
        [app performSelector:@selector(suspend)];
        
        //wait 2 seconds while app is going background
        [NSThread sleepForTimeInterval:2.0];

        exit(0);
    }
}
- (void)showAlertno_network:(NSString*)title message:(NSString*)message
{
    
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    callAlert.tag = 55;
    [callAlert show];
     
}


- (void)showAlertWithTitle:(NSString*)title message:(NSString*)message
{
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
   // callAlert.tag = 55;
   
   // [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    NSArray *subviewArray = callAlert.subviews;
    for(int x = 0; x < [subviewArray count]; x++){
        
        if([[[subviewArray objectAtIndex:x] class] isSubclassOfClass:[UILabel class]]) {
            UILabel *label = [subviewArray objectAtIndex:x];
            label.textAlignment = UITextAlignmentLeft;
        }
    }
     [callAlert show];
    
}

- (void)logoutUser
{
//    [[NSUserDefaults standardUserDefaults]removeObjectForKey:username];
//    [[NSUserDefaults standardUserDefaults]removeObjectForKey:password];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:remeberme];
//    [[NSUserDefaults standardUserDefaults]removeObjectForKey:userRole];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:details];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
}

- (void)changePassword
{ 

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
