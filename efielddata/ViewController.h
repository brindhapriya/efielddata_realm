//
//  ViewController.h
//  Efield
//
//  Created by iPhone on 09/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DownloadManager.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#define UIColorFromRGBA(rgbValue, alphaValue) ([UIColor colorWithRed:((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0 \
green:((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0 \
blue:((CGFloat)(rgbValue & 0xFF)) / 255.0 \
alpha:alphaValue])
#define remeberme @"remeberme"
#define username @"username"
#define fakeusername @"fakeusername"
#define login_password @"password"
#define isLoggedin @"isloggedin"
#define AuthorizationId @"AuthorizationId"
#define userRole @"userRole"
#define companyname @"companyname"
#define details @"details"
#define kdeviceToken @"deviceToken"
#define isPatientUpdatedNeed @"isPatientUpdatedNeed"
#define ACCESSCODEURL @"http://efielddatadynamic.vconnexservices.com/api/WorkOrderApi/"
#define SERVERURL @"SERVERURL"

@interface ViewController : UIViewController
{
    NSMutableDictionary *detailsDic;
    BOOL showMoreFlag;
}
- (BOOL)isiPad;
- (void)showAlertno_network:(NSString*)title message:(NSString*)message;
- (void)showAlertWithTitle:(NSString*)title message:(NSString*)message;
- (void)logoutUser;
- (NSString *)updateALERTcount;
- (void)changePassword;
- (void)updateAPI;
- (BOOL)isNetworkAvailable;
- (BOOL)validatePhone:(NSString *)phoneNumber;
@end
