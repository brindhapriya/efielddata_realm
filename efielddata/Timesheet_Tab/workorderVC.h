//
//  AlertListVC.h
//  Efield
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PendingTableViewCell.h"
#import "ViewController.h"

@interface workorderVC : ViewController {
    __weak IBOutlet UITableView *alertTable;
    __weak IBOutlet UISearchBar *searchbar;
    NSArray *tempArr;
    NSMutableArray *alertList;
    __weak IBOutlet UITextField *projectid;
    IBOutlet UITextField *jobdate;
    IBOutlet UITextField *workorder;
    NSMutableArray *checkBoxArr;
    NSInteger index;
    NSDictionary *dummyDic;
    NSMutableDictionary *alertListDic;
    UIRefreshControl *refreshControl;
    __weak IBOutlet UIView *tableFooterView;
    __weak IBOutlet UIBarButtonItem *markAllButton;
}
@property (weak, nonatomic) IBOutlet UIButton *cloasebtn;
@property (weak, nonatomic) IBOutlet UILabel *alertcnt;
@property (weak, nonatomic) IBOutlet UILabel *date;
- (IBAction)close:(id)sender;
- (IBAction)selectproject:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *closebtn2;
@property (weak, nonatomic) IBOutlet UILabel *headerlbl;

- (IBAction)clearproject_action:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *companynametxt;
@property (weak, nonatomic) IBOutlet UIButton *editIcon;
@property(nonatomic, retain)NSString *workorderID;
@property (weak, nonatomic) IBOutlet UIButton *filter_icon;
@property (weak, nonatomic) IBOutlet UIButton *caliender_icon;

@property(nonatomic, retain)NSString *status;

@property(nonatomic, retain)NSString *statusreport;

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
- (IBAction)logoutAction:(id)sender;
- (IBAction)changepasswordAction:(id)sender;
- (IBAction)showMoreAction:(id)sender;
- (IBAction)editAction:(id)sender;
- (IBAction)jobdateaction:(id)sender;
- (IBAction)markAllAction:(id)sender;
- (IBAction)Specialization:(id)sender;
- (IBAction)add_patient:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addpatient_button;
@property (weak, nonatomic) IBOutlet UIButton *cancel_btn;

- (IBAction)alertaction:(id)sender;

- (void)updateData;
@end
