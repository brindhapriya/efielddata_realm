//
//  multiselectTableViewCell.h
//  Efield
//
//  Created by MyMac1 on 2/7/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface timesheettableheadercell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *date_lbl;
@property (weak, nonatomic) IBOutlet UILabel *absence_lbl;
@property (weak, nonatomic) IBOutlet UILabel *hours_lbl;
@end
