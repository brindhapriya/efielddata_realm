//
//  multiselectTableViewCell.h
//  Efield
//
//  Created by MyMac1 on 2/7/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface workordertimesheetcell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *date_val;
@property (weak, nonatomic) IBOutlet UILabel *workorder_value;
@property (weak, nonatomic) IBOutlet UILabel *hours_value;
@property (weak, nonatomic) IBOutlet UIButton *edit_Timesheet;


@end
