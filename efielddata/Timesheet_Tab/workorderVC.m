                             //
//  AlertListVC.m
//  Efielddata
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import "workorderVC.h"
#import "Constants.h"  
#import "DynamicNoteFormVC.h"
#import "NoRecordsTableViewCell.h"
#import "TestTypeselectVC.h"
#import "AlertListVC.h"
#import "cancell_infieldVC.h"
#import "completedTableViewCell.h"
#import "project_filter.h"

//#import "UITabBarItem+CustomBadge.h"
@interface workorderVC ()<UITextFieldDelegate,UIWebViewDelegate>
{
    BOOL isOnEditing;
    NSString *cancellworkorderIDstr;
    BOOL isMarkAll;
    __weak IBOutlet UILabel *titleLbl;
    __weak IBOutlet UITableView *alertSearchTable;
    NSMutableArray *searchResultArray;
    NSMutableArray *searchFullResultArray;
    NSInteger selectedRowIndex;
    int selectedUtilityIndex;
    NSString *places;
    CGFloat  lastScale;
    BOOL isAll;
    NSString *IsChange,*TestTypeId;
    NSString *TaskName,*JobNumber,*ProjectName;
    NSString *loadingmsg,*wono,*ReportStatus,*datestring,*ProjectId,*emailworkid,*emailjobnumber;
   }
@end

@implementation workorderVC

UIButton *webview_completedbuttonwo;
UIWebView *webview_completedwo;
NSURL *targetURLwo ;
bool  showpdfwo;
UIActivityIndicatorView *activityViewwo; 

- (void)viewDidLoad {
      
    [super viewDidLoad];
    showpdfwo=false;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    //[self.view addGestureRecognizer:tap];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"ProjectId"];
    
    wono=@"";
     datestring=@"";
     loadingmsg=@"Loading...";
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"ProjectId"];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"ProjectName"];
    
    _companynametxt.text=[[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];
    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
    [currentDTFormatter1 setDateFormat:@"MM/dd/yyyy"];
    NSString *todayDateStr,*eventDateStr;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"Display_canceledJobDateText"] length]>0)
    {
        
        self->jobdate.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"Display_canceledJobDateText"];
        wono=[[NSUserDefaults standardUserDefaults] objectForKey:@"Display_canceledJobDateText"];
     }
    else
    {
        todayDateStr = [currentDTFormatter1 stringFromDate:[NSDate date]];
        eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
        
        self->jobdate.text=todayDateStr;
        datestring=todayDateStr;
        NSLog(@"%@", eventDateStr);
    
    }
        _date.text=eventDateStr;
    self.navigationController.navigationBar.hidden = YES;
    alertList = [[NSMutableArray alloc]init];
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor whiteColor];
    refreshControl.tintColor = [UIColor blackColor];
   
    projectid.text=@"";
    if([_statusreport isEqualToString:@"All Completed WO"])
    {
        isAll=true;
        ReportStatus=@"DR";
        _headerlbl.text=@"All Completed Work Orders";
    }
   else if([_statusreport isEqualToString:@"All Pending WO"])
    {
          isAll=true;
        ReportStatus=@"NW";
        _headerlbl.text=@"All Pending Work Orders";
    }
   else if([_statusreport isEqualToString:@"All Cancelled Reports"])
   {
         isAll=true;
       ReportStatus=@"CF";
        _headerlbl.text=_statusreport;
   }
   else if([_statusreport isEqualToString:@"All Reviewed Reports"])
   {
         isAll=true;
       ReportStatus=@"RV";
        _headerlbl.text=_statusreport;
   }
   else if([_statusreport isEqualToString:@"My Cancelled Reports"])
   {
         isAll=false;
       ReportStatus=@"CF";
        _headerlbl.text=_statusreport;
   }
   else if([_statusreport isEqualToString:@"My Reviewed Reports"])
   {
        isAll=false;
       ReportStatus=@"RV";
        _headerlbl.text=_statusreport;
   }
   
    [refreshControl addTarget:self
                       action:@selector(updateDatarefresh)
             forControlEvents:UIControlEventValueChanged];
    [alertTable addSubview:refreshControl];
  _alertcnt.text=[self updateALERTcount];
     self->jobdate.tag=101;
    
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 20)];
     
     self->jobdate.leftView = paddingView;
     self->jobdate.leftViewMode = UITextFieldViewModeAlways;
  
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 20)];
    
    self->projectid.enabled=false;
      self->projectid.leftView = paddingView1;
    self->projectid.leftViewMode = UITextFieldViewModeAlways;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateData)
                                                 name:@"updateDataAlert"
                                               object:nil];
    [self->jobdate  setDelegate:self];
    
    [self->jobdate addTarget:self action:@selector(updateData1) forControlEvents:UIControlEventEditingDidEnd];
    [self->jobdate addTarget:self action:@selector(enable_closebtn) forControlEvents:UIControlEventEditingChanged];
    
    [self->projectid  setDelegate:self];
    
    [self->projectid addTarget:self action:@selector(projectdata) forControlEvents:UIControlEventEditingDidEnd];
      //[self->workorder addTarget:self action:@selector(updateData1) forControlEvents:UIControlEventEditingChanged];

//    [[NSUserDefaults standardUserDefaults]setObject:  @"" forKey:@"predicateString"];
//    if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"IsShowAddPatient"] isEqualToString:@"1"]){
//        _addpatient_button.hidden=NO;
//        _addpatient_button.userInteractionEnabled=YES;
//    }else{
//        _addpatient_button.hidden=YES;
//        _addpatient_button.userInteractionEnabled=NO;
//    }
    // Do any additional setup after loading the view.
//}
//       if([[[NSUserDefaults standardUserDefaults] objectForKey:userRole] isEqualToString:@"Admin"]){
//        
//        [self->searchbar setImage:[UIImage imageNamed:@"patientlist"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateNormal];
//        [self->searchbar setImage:[UIImage imageNamed:@"patientlist"] forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateSelected];
//        self->searchbar.showsBookmarkButton=YES;
//    }
//    else{
//    self->searchbar.showsBookmarkButton=NO;}
//    
}
//
//- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar
//{
//    
//    [self performSegueWithIdentifier:@"showlogfile" sender:nil];
//}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    //   [aTextField resignFirstResponder];
}


-(void)progress
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    
}

//- (void)updateDeviceTokentoServer
//{
//     
//    if (![self isNetworkAvailable]) {
//        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
//        return;
//    }else{
//        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kdeviceToken]);
//    DownloadManager *downloadObj = [[DownloadManager alloc]init];
//        NSDictionary *json ;
//        NSString *deviceoken=[[NSUserDefaults standardUserDefaults] objectForKey:kdeviceToken];
//        if ([deviceoken isKindOfClass:[NSNull class]]||[deviceoken isEqualToString:@"null"]||deviceoken==nil)
//        {
//            json = @{
//                     
//                     @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
//                     @"AppId" :@""
//                     
//                     };
//        }else
//        {
//            json = @{
//                     @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
//                     @"AppId" : deviceoken
//                     };
//               }
//        
//        NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
//        [dictEntry setObject:json forKey:[NSString stringWithFormat:@"UserModel"]];
//        
//       // [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@UpdateUserAppId?UserName=%@&AppId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[NSUserDefaults standardUserDefaults] objectForKey:kdeviceToken]] andParameter:nil andMethod:@"POST" andDelegate:self andKey:@"UpdateUserAppId"];
//     [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@IOSUpdateUserAppId",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"UpdateUserAppId"];
//    }
//}
- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
//    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"gotopatientlist"] isEqualToString:@"true"]){
//        
//        [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"gotopatientlist"];
//        UINavigationController *nav      =[[self.tabBarController childViewControllers] objectAtIndex:3];
//        
//        PatientListVC *myController = (PatientListVC *)nav.topViewController;
//        self.tabBarController.selectedIndex = 3;
//    }
//    else{
  //  _alertPatientID = nil;
    //}
}
- (void)viewWillAppear:(BOOL)animated
{
    
    
        [super viewWillAppear:animated];
    _alertcnt.text=[self updateALERTcount];

    [[NSUserDefaults standardUserDefaults]setBool:true forKey:isLoggedin];
    [[NSUserDefaults standardUserDefaults]setBool:true forKey:isPatientUpdatedNeed];
    [[NSUserDefaults standardUserDefaults] synchronize];
   
    //self->jobdate.text=@"";
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];
    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
    [currentDTFormatter1 setDateFormat:@"MM/dd/yyyy"];
  NSString *eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];

    eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];

    _date.text=eventDateStr;

    ProjectId=[[NSUserDefaults standardUserDefaults] objectForKey:@"ProjectId"];
    projectid.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"ProjectName"];
    [self updateData];
  
}
- (void)loadAlertData
{
    NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:details];
    NSDictionary* json = [NSKeyedUnarchiver unarchiveObjectWithData:data];
   
        alertList = [[NSMutableArray alloc]initWithArray:[json valueForKeyPath:@"Data"]];
        tempArr = [[NSArray alloc]initWithArray:[json valueForKeyPath:@"Data"]];
   
    
         [alertTable reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGFloat)calculateStringHeigth:(CGSize)size text:(NSString *)text andFont:(UIFont *)font {
    NSLog(@"text : %@",text);
    
    
    NSLog(@"Font : %@", font);
    NSLog(@"size : %@",NSStringFromCGSize(size));
    
    if([text isEqual:[NSNull null]]) {
        text = @"";
    }
    
    if(![text length])
        return 0;
    
    CGSize labelSize = [text sizeWithFont:font
                        constrainedToSize:size
                            lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat labelHeight = labelSize.height;
    NSLog(@"labelHeight : %f",labelHeight);
    return labelHeight;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([alertList count]==0)
    {
        return 40;
        
    }else{
        //     if(![[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isEqualToString:@"New"]){
        //               return 205;
        //     }else{
        if([_statusreport isEqualToString:@"All Pending WO"]){
            
            
            CGSize size = alertTable.frame.size;
            size.width = size.width - 16;
            CGFloat  height = [self calculateStringHeigth:size text:[[alertList valueForKey:@"ProjectInfo"] objectAtIndex:indexPath.row] andFont:[UIFont systemFontOfSize:13]];
            height=height+140;
            
            return height;
            
        }
        else{
            return 200;
            
        }
        //}
    }
}
//
// - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if([alertList count]==0)
//    {
//        return 40;
//
//    }else{
////     if(![[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isEqualToString:@"New"]){
////               return 205;
////     }else{
//          if([_statusreport isEqualToString:@"All Pending WO"]){
//
//
//                  CGSize size = alertTable.frame.size;
//                  size.width = size.width - 16;
//                CGFloat  height = [self calculateStringHeigth:size text:[[alertList valueForKey:@"ProjectInfo"] objectAtIndex:indexPath.row] andFont:[UIFont systemFontOfSize:13]];
//              height=height+140;
//
//            return 145;
//
//        }
//        else{
//            return 200;
//
//        }
//     //}
//    }
//}


- (void) handlelocationtapFrom: (UITapGestureRecognizer *)recognizer
{
    
    UIView *parentCell = recognizer.view.superview;
    
    while (![parentCell isKindOfClass:[UITableViewCell class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentCell = parentCell.superview;
    }
    
    UIView *parentView = parentCell.superview;
    
    while (![parentView isKindOfClass:[UITableView class]]) {
        // iOS 7 onwards the table cell hierachy has changed.
        parentView = parentView.superview;
    }
    
    
    UITableView *tableView = (UITableView *)parentView;
    NSIndexPath *indexPath = [tableView indexPathForCell:(UITableViewCell *)parentCell];
    
    NSString *ProjectAddress = [[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row] description];
//
    
     ProjectAddress = [ProjectAddress stringByReplacingOccurrencesOfString:@" " withString:@"+"];
      NSString *url = [NSString stringWithFormat: @"https://www.google.com/maps/dir/Tierra+South+Florida,+Inc,+2765+Vista+Pkwy+%2310,+West+Palm+Beach,+FL+33411,+United+States/%@",ProjectAddress];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: url]];

    
}


- (void) handleNameLblTapFrom: (UITapGestureRecognizer *)recognizer
{
    
    UIView *parentCell = recognizer.view.superview;
    
    while (![parentCell isKindOfClass:[UITableViewCell class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentCell = parentCell.superview;
    }
    
    UIView *parentView = parentCell.superview;
    
    while (![parentView isKindOfClass:[UITableView class]]) {
        // iOS 7 onwards the table cell hierachy has changed.
        parentView = parentView.superview;
    }
    
    
    UITableView *tableView = (UITableView *)parentView;
    NSIndexPath *indexPath = [tableView indexPathForCell:(UITableViewCell *)parentCell];
    
    NSLog(@"indexPath = %@", indexPath);
    
    NSString *phoneStr = [[[alertList valueForKey:@"Phone"] objectAtIndex:indexPath.row] description];
    phoneStr = [phoneStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:phoneStr]];
    NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneStr]];
    
    if (![phoneStr isEqualToString:@""]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Call from Efielddata"
                                      message:@"Please click OK to call"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl])
                                 {
                                     [UIApplication.sharedApplication openURL:phoneFallbackUrl];
                                 }
                                 else if ([UIApplication.sharedApplication canOpenURL:phoneUrl])
                                 {
                                     [UIApplication.sharedApplication openURL:phoneUrl];
                                 }
                                 else
                                 {
                                     [self showAlertWithTitle:@"Efielddata Message" message:@"Your device not supported for phone calls"];
                                 }
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([alertList count]==0)
       {
           NoRecordsTableViewCell *cell = (NoRecordsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:noRecordsResuableIdentifier];
           cell.noRecordsLabel.text = loadingmsg;
           return cell;
           
       }
    
    
    if([_statusreport isEqualToString:@"All Pending WO"]){
  
           NSString *cellIdentifier = @"cell";
           PendingTableViewCell *cell = (PendingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
           cell.orderview.backgroundColor = [UIColor whiteColor];
        for (UIGestureRecognizer *gesture in cell.contact_no.gestureRecognizers) {
            [cell.contact_no removeGestureRecognizer:gesture];
        }
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleNameLblTapFrom:)];
        cell.contact_no.userInteractionEnabled = YES;
        [cell.contact_no addGestureRecognizer:tapGestureRecognizer];
    cell.cancel_infield.tag = indexPath.row;
      cell.contact_no.tag = indexPath.row;
//  int value =  [[[alertList valueForKey:@"TimeSpent"] objectAtIndex:indexPath.row] intValue];
    if([[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isEqualToString:@"New"]){
     
                cell.status.textColor= [UIColor  colorWithRed:236/255.0f green:114/255.0f blue:103/255.0f alpha:1.0];
   // [cell.cancel_infield addTarget:self action:@selector(cancellfield_clicked:) forControlEvents:UIControlEventTouchUpInside];
        //cell.cancel_infield.hidden=false;
      //  cell.dividerlabel.hidden=false;
    }
           
    else{
        cell.status.textColor=[UIColor grayColor];
      //  cell.cancel_infield.hidden=true;
        //cell.dividerlabel.hidden=true;

    }
           cell.cancel_infield.hidden=true;
           cell.dividerlabel.hidden=true;

    
    for (UIGestureRecognizer *gesture in cell.joblocation.gestureRecognizers) {
        [cell.joblocation removeGestureRecognizer:gesture];
    }
    
    UITapGestureRecognizer *tapGestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlelocationtapFrom:)];
    cell.joblocation.userInteractionEnabled = YES;
    [cell.joblocation addGestureRecognizer:tapGestureRecognizer1];
    
    
        if ([[[alertList valueForKey:@"TaskName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
            cell.task_name.text = [[alertList valueForKey:@"TaskName"] objectAtIndex:indexPath.row];
        }
        else
        {
            cell.task_name.text = @"";
        }
    
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if ([[[alertList valueForKey:@"JobDateTimeDisplayUTCText"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.job_scheduleddate.text =[[alertList valueForKey:@"JobDateTimeDisplayUTCText"] objectAtIndex:indexPath.row];
        //[NSString stringWithFormat:@"Job Date Scheduled: %@",[[alertList valueForKey:@"JobDateTimeText"] objectAtIndex:indexPath.row]];
     //   NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.job_scheduleddate.text];
    //    [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 19)];
      //  cell.job_scheduleddate.attributedText = contactString;
    }
    else
    {
//        cell.job_scheduleddate.text = @"Job Date Scheduled";
//        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.job_scheduleddate.text];
//        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 19)];
//        cell.job_scheduleddate.attributedText = contactString;
        cell.job_scheduleddate.text =@"";
  }
    if ([[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.status.text =[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row];
        //[NSString stringWithFormat:@"Status: %@",[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row]];
//        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.status.text];
//        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 8)];
//        cell.status.attributedText = contactString;
    }
    else
    {
//
        cell.status.text =@"";
    }
    if ([[[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.workorder.text =[[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row];
   
    }
    else
    {        cell.workorder.text =@"";
     }
    if ([[[alertList valueForKey:@"ProjectName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.project.text =[[alertList valueForKey:@"ProjectName"] objectAtIndex:indexPath.row];
        
 
    }
    else
    {
        cell.project.text =@"";
    }
    if ([[[alertList valueForKey:@"ProjectInfo"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.taskdetails.text =[[alertList valueForKey:@"ProjectInfo"] objectAtIndex:indexPath.row];
        
//
//        cell.taskdetails.numberOfLines = 3;
//        [cell.taskdetails sizeToFit];
//        [cell.taskdetails setNeedsDisplay];
    
    }
    else
    {
        cell.taskdetails.text =@"";
    }
    if ([[[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.assignedto.text =[[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row];
 
    }
    else
    {
        cell.assignedto.text =@"";
    }
    if ([[[alertList valueForKey:@"Contact"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.contactname.text =[[alertList valueForKey:@"Contact"] objectAtIndex:indexPath.row];
   
    }
    else
    {
        cell.contactname.text =@"";
    }
    if ([[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.joblocation.text =[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row];
 
    }
    else
    {
        cell.joblocation.text =@"";
    }
    if ([[[alertList valueForKey:@"Instructions"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.direction.text =[[alertList valueForKey:@"Instructions"] objectAtIndex:indexPath.row];
      
    }
    else
    {
        cell.direction.text =@"";
    }
        if ([[[alertList valueForKey:@"Phone"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
    {
        cell.contact_no.text =[[alertList valueForKey:@"Phone"] objectAtIndex:indexPath.row];
        
        }
        else
        {  cell.contact_no.text =@"";
//
        }
           
           cell.copworkorder.tag = indexPath.row;

           UITapGestureRecognizer *copytapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(copytapgestureaction:)];

           cell.copworkorder.userInteractionEnabled = YES;
           [cell.copworkorder addGestureRecognizer:copytapGestureRecognizer];
        
        return cell;

       }
    
    
    else
        
      //  if([_statusreport isEqualToString:@"All Completed WO"])
        {
            NSString *cellIdentifier = @"cell1";
            completedTableViewCell *cell = (completedTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            cell.orderview.backgroundColor = [UIColor whiteColor];
            
            //    CGFloat borderWidth = 1.0f;
            //
            //    cell.orderview.layer.borderColor = [UIColor colorWithRed:0.0/255.0 green:156.0/255.0 blue:21.0/255.0 alpha:1].CGColor;
            //    cell.orderview.layer.borderWidth = borderWidth;
            //
            UITapGestureRecognizer *pdfAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pdfClickwo:)];
            
            pdfAction.delegate =self;
            pdfAction.numberOfTapsRequired = 1;
            cell.pdflbl.userInteractionEnabled = YES;
            for (UITapGestureRecognizer *reco in cell.pdflbl.gestureRecognizers) {
                [cell.pdflbl removeGestureRecognizer:reco];
            }
            cell.pdflbl.tag=indexPath.row;
            
            [cell.pdflbl addGestureRecognizer:pdfAction];
            
            UITapGestureRecognizer *pdfviewAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pdfClickwo:)];
            
            pdfviewAction.delegate =self;
            pdfviewAction.numberOfTapsRequired = 1;
            
            cell.pdfview.userInteractionEnabled = YES;
            for (UITapGestureRecognizer *reco in cell.pdfview.gestureRecognizers) {
                [cell.pdfview removeGestureRecognizer:reco];
            }
            cell.pdfview.tag=indexPath.row;
            
            [cell.pdfview addGestureRecognizer:pdfviewAction];
            
            
            
            
            
            
            
            UITapGestureRecognizer *pdfAction1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pdfClickwo:)];
            
            pdfAction1.delegate =self;
            pdfAction1.numberOfTapsRequired = 1;
            
            cell.pdfimg.userInteractionEnabled = YES;
            for (UITapGestureRecognizer *reco in cell.pdfimg.gestureRecognizers) {
                [cell.pdfimg removeGestureRecognizer:reco];
            }
            cell.pdfimg.tag=indexPath.row;
            
            [cell.pdfimg addGestureRecognizer:pdfAction1];
            
            
            
            UITapGestureRecognizer *emailAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emailClickwo:)];
            
            emailAction.delegate =self;
            emailAction.numberOfTapsRequired = 1;
            cell.email.userInteractionEnabled = YES;
            for (UITapGestureRecognizer *reco in cell.email.gestureRecognizers) {
                [cell.email removeGestureRecognizer:reco];
            }
            
            cell.email.tag=indexPath.row;
            
            [cell.email addGestureRecognizer:emailAction];
            
            
            UITapGestureRecognizer *emailviewAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emailClickwo:)];
            
            emailviewAction.delegate =self;
            emailviewAction.numberOfTapsRequired = 1;
            
            cell.emailview.userInteractionEnabled = YES;
            for (UITapGestureRecognizer *reco in cell.emailview.gestureRecognizers) {
                [cell.emailview removeGestureRecognizer:reco];
            }
            
            cell.emailview.tag=indexPath.row;
            
            [cell.emailview addGestureRecognizer:emailviewAction];
            
            
            
            UITapGestureRecognizer *emailActionimg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emailClickwo:)];
            
            emailActionimg.delegate =self;
            emailActionimg.numberOfTapsRequired = 1;
            
            cell.emailimg.userInteractionEnabled = YES;
            for (UITapGestureRecognizer *reco in cell.emailimg.gestureRecognizers) {
                [cell.emailimg removeGestureRecognizer:reco];
            }
            
            cell.emailimg.tag=indexPath.row;
            
            [cell.emailimg addGestureRecognizer:emailActionimg];
            
            
            
            
            
            //        if([[[[alertList valueForKey:@"IsEmailSend"] objectAtIndex:indexPath.row]description] isEqualToString:@"0"])
            //
            //        {
            cell.emailimg.hidden=true;
            cell.email.hidden=true;
            cell. emailview.hidden=true;
            //        }else{
            //            cell.emailimg.hidden=false;
            //            cell.email.hidden=false;
            //            cell. emailview.hidden=true;
            //
            //
            //        }
            //
            
            
            // cell.orderview.layer.cornerRadius = 5.0f;
            
            for (UIGestureRecognizer *gesture in cell.contact_no.gestureRecognizers) {
                [cell.contact_no removeGestureRecognizer:gesture];
            }
            
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleNameLblTapFrom:)];
            cell.contact_no.userInteractionEnabled = YES;
            [cell.contact_no addGestureRecognizer:tapGestureRecognizer];
            
            
            
            for (UIGestureRecognizer *gesture in cell.joblocation.gestureRecognizers) {
                [cell.joblocation removeGestureRecognizer:gesture];
            }
            
            UITapGestureRecognizer *tapGestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlelocationtapFrom:)];
            cell.joblocation.userInteractionEnabled = YES;
            [cell.joblocation addGestureRecognizer:tapGestureRecognizer1];
            
            
            if ([[[alertList valueForKey:@"TaskName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
                cell.task_name.text = [[alertList valueForKey:@"TaskName"] objectAtIndex:indexPath.row];
            }
            else
            {
                cell.task_name.text = @"";
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if ([[[alertList valueForKey:@"JobDateTimeDisplayUTCText"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
            {
                cell.job_scheduleddate.text =[[alertList valueForKey:@"JobDateTimeDisplayUTCText"] objectAtIndex:indexPath.row];
                
                //        [NSString stringWithFormat:@"Job Date Scheduled: %@",[[alertList valueForKey:@"JobDateTimeText"] objectAtIndex:indexPath.row]];
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.job_scheduleddate.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 19)];
                //        cell.job_scheduleddate.attributedText = contactString;
            }
            else
            {
                //        cell.job_scheduleddate.text = @"Job Date Scheduled";
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.job_scheduleddate.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 19)];
                //        cell.job_scheduleddate.attributedText = contactString;
            }
            if ([[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
            {
                
                if([[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isEqualToString:@"Cancelled in Field"]){
                    
                    cell.status.textColor= [UIColor  colorWithRed:236/255.0f green:114/255.0f blue:103/255.0f alpha:1.0];
                }
                else{
                    cell.status.textColor=[UIColor grayColor];
                }
            //    cell.status.text =[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row];
                if ([[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isEqualToString:@"Completed"])
                {
                cell.status.text =[NSString stringWithFormat:@"%@-%@",[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row],[[alertList valueForKey:@"CompletedDateText"] objectAtIndex:indexPath.row]];
                    cell.status.textColor= [UIColor  colorWithRed:67/255.0f green:160/255.0f blue:39/255.0f alpha:1.0];

                }else{
                    
                   cell.status.text =[NSString stringWithFormat:@"%@",[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row]];
                }
                
                //                if([[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row] isEqualToString:@"Cancelled in Field"]){
                //                    cell.status.textColor=;
                //
                //                }else{
                //
                //                    cell.status.textColor=;
                //
                //                }
                //[NSString stringWithFormat:@"Status: %@",[[alertList valueForKey:@"StatusText"] objectAtIndex:indexPath.row]];
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.status.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 8)];
                //        cell.status.attributedText = contactString;
            }
            else
            {
                //        cell.status.text = @"Status:";
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.status.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 8)];
                //        cell.status.attributedText = contactString;
            }
            if ([[[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
            {
                cell.workorder.text =[[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row];
                //[NSString stringWithFormat:@"Work Order#: %@",[[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row]];
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.workorder.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,11)];
                //        cell.workorder.attributedText = contactString;
            }
            else
            {
                //        cell.workorder.text = @"Work Order#:";
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.workorder.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,11)];
                //        cell.workorder.attributedText = contactString;
            }
            if ([[[alertList valueForKey:@"ProjectName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
            {
                cell.project.text =[[alertList valueForKey:@"ProjectName"] objectAtIndex:indexPath.row];
                //        [NSString stringWithFormat:@"Project/Sub Project: %@",[[alertList valueForKey:@"ProjectName"] objectAtIndex:indexPath.row]];
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.project.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 19)];
                //        cell.project.attributedText = contactString;
            }
            else
            {
                //        cell.project.text = @"Project/Sub Project:";
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.project.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 19)];
                //        cell.project.attributedText = contactString;
            }
            if ([[[alertList valueForKey:@"ProjectInfo"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
            {
                cell.taskdetails.text =[[alertList valueForKey:@"ProjectInfo"] objectAtIndex:indexPath.row];
             }
            else
            {
             }
            if ([[[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
            {
                cell.assignedto.text =[[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row];
                
                //        [NSString stringWithFormat:@"Assigned to: %@",[[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row]];
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.assignedto.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 12)];
                //        cell.assignedto.attributedText = contactString;
            }
            else
            {
                //        cell.assignedto.text = @"Assigned to:";
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.assignedto.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 12)];
                //        cell.assignedto.attributedText = contactString;
            }
            if ([[[alertList valueForKey:@"Contact"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
            {
                cell.contactname.text =[[alertList valueForKey:@"Contact"] objectAtIndex:indexPath.row];
                //        [NSString stringWithFormat:@"Contact Name: %@",[[alertList valueForKey:@"Contact"] objectAtIndex:indexPath.row]];
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.contactname.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 13)];
                //        cell.contactname.attributedText = contactString;
            }
            else
            {
                //        cell.contactname.text = @"Contact Name:";
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.contactname.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 13)];
                //        cell.contactname.attributedText = contactString;
            }
            if ([[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
            {
                cell.joblocation.text =[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row];
                //        [NSString stringWithFormat:@"Job Location: %@",[[alertList valueForKey:@"ProjectAddress"] objectAtIndex:indexPath.row]];
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.joblocation.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 13)];
                //
                //        cell.joblocation.attributedText = contactString;
            }
            else
            {
                //        cell.joblocation.text = @"Job Location:";
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.joblocation.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 13)];
                //        cell.joblocation.attributedText = contactString;
            }
            if ([[[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
            {
                cell.assignedto.text =[[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row];
                
            }
            else
            {
                cell.assignedto.text =@"";
            }
            if ([[[alertList valueForKey:@"Instructions"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
            {
                cell.direction.text =[[alertList valueForKey:@"Instructions"] objectAtIndex:indexPath.row];
                //        //[NSString stringWithFormat:@"Directions: %@",[[alertList valueForKey:@"Instructions"] objectAtIndex:indexPath.row]];
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.direction.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 11)];
                //        cell.direction.attributedText = contactString;
            }
            else
            {
                //        cell.direction.text = @"Directions:";
                //        NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.direction.text];
                //        [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 11)];
                //        cell.direction.attributedText = contactString;
            }
            if ([[[alertList valueForKey:@"Phone"] objectAtIndex:indexPath.row] isKindOfClass:[NSString class]])
            {
                cell.contact_no.text =[[alertList valueForKey:@"Phone"] objectAtIndex:indexPath.row];
                
                //        [NSString stringWithFormat:@"Contact phone: %@",[[alertList valueForKey:@"Phone"] objectAtIndex:indexPath.row]];
                //            NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.contact_no.text];
                //         [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor  blackColor] range:NSMakeRange(0, 14)];
                //        //     [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.0/255.0 green:112.0/255.0 blue:193.0/255.0 alpha:1] range:NSMakeRange(14,25)];
                //            cell.contact_no.attributedText = contactString;
            }
            else
            {
                //            cell.contact_no.text = @"Contact phone:";
                //            NSMutableAttributedString * contactString = [[NSMutableAttributedString alloc] initWithString:cell.contact_no.text];
                //            [contactString addAttribute:NSForegroundColorAttributeName value:[UIColor  blackColor] range:NSMakeRange(0, 14)];
                //
                //            cell.contact_no.attributedText = contactString;
            }
            
            return cell;
        }
}
 


- (void)handlePinchGesture:(UIPinchGestureRecognizer *)gestureRecognizer {
    
    if([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
        // Reset the last scale, necessary if there are multiple objects with different scales.
        lastScale = [gestureRecognizer scale];
    }
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan ||
        [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        
        CGFloat currentScale = [[[gestureRecognizer view].layer valueForKeyPath:@"transform.scale"] floatValue];
        
        // Constants to adjust the max/min values of zoom.
        const CGFloat kMaxScale = 2.0;
        const CGFloat kMinScale = 1.0;
        
        CGFloat newScale = 1 -  (lastScale - [gestureRecognizer scale]); // new scale is in the range (0-1)
        newScale = MIN(newScale, kMaxScale / currentScale);
        newScale = MAX(newScale, kMinScale / currentScale);
        CGAffineTransform transform = CGAffineTransformScale([[gestureRecognizer view] transform], newScale, newScale);
        [gestureRecognizer view].transform = transform;
        
        lastScale = [gestureRecognizer scale];  // Store the previous. scale factor for the next pinch gesture call
    }
}



- (void)copypopViewController {
    
    
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
    
            
            DownloadManager *downloadObj = [[DownloadManager alloc]init];
            //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            
            [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@CopyWorkOrderForApp?WorkOrderId=%@&UserName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],cancellworkorderIDstr,[[NSUserDefaults standardUserDefaults] objectForKey:username]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"CopyWorkOrderForApp"];
 
}



- (void)pdfClickwo:(UITapGestureRecognizer *)tapGesture {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetWoPdfUrl?AuthorizationId=%@&UserName=%@&WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:AuthorizationId],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[alertList valueForKey:@"WorkOrderId"] objectAtIndex:tapGesture.view.tag]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetWoPdfUrl"];
    
    //
    //    NSLog(@"getpdf%@",[NSString stringWithFormat:@"%@GetWoPdfUrl?AuthorizationId=%@&UserName=%@&WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:AuthorizationId],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[alertList valueForKey:@"WorkOrderId"] objectAtIndex:tapGesture.view.tag]]);
    //
    //        NSURLSession *session = [NSURLSession sharedSession];
    //        [[session dataTaskWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@GetWoPdfUrl?AuthorizationId=%@&UserName=%@&WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:AuthorizationId],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[alertList valueForKey:@"WorkOrderId"] objectAtIndex:tapGesture.view.tag]]]
    //                completionHandler:^(NSData *data,
    //                                    NSURLResponse *response,
    //                                    NSError *error) {
    //                    // handle response
    //                    if(data != nil)
    //                    {
    //                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    //                        NSString *string =  [NSString stringWithUTF8String:[data bytes]];
    //                        dispatch_async(dispatch_get_main_queue(), ^{
    //
    //                           targetURL = [NSURL fileURLWithPath:string];
    //
    //                               [self showpdfview_com];
    //
    //   });
    //                        NSLog(@"data%@",string);
    //
    //                    }
    //                    else
    //                    {
    //                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    //
    //                    }
    //
    //                }] resume];
    //
    
    
}

-(void)copytapgestureaction:(UITapGestureRecognizer*)recognizer
{
    
    
    UIView *parentCell = recognizer.view.superview;
    
    while (![parentCell isKindOfClass:[UITableViewCell class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentCell = parentCell.superview;
    }
    
    UIView *parentView = parentCell.superview;
    
    while (![parentView isKindOfClass:[UITableView class]]) {
        // iOS 7 onwards the table cell hierachy has changed.
        parentView = parentView.superview;
    }
    
    
    UITableView *tableView = (UITableView *)parentView;
    NSIndexPath *indexPath = [tableView indexPathForCell:(UITableViewCell *)parentCell];
    
    cancellworkorderIDstr = [[[alertList valueForKey:@"WorkOrderId"] objectAtIndex:indexPath.row]description];
    
    [self performSelector:@selector(copypopViewController) withObject:nil afterDelay:0.1];
    
    
}


-(void)cancellfield_clicked:(UIButton*)cancellworkorderID
{
    _workorderID=[[[alertList valueForKey:@"WorkOrderId"] objectAtIndex:cancellworkorderID.tag]description];
    TaskName=  [[[alertList valueForKey:@"TaskName"] objectAtIndex:cancellworkorderID.tag]description];
    JobNumber=    [[[alertList valueForKey:@"JobNumber"] objectAtIndex:cancellworkorderID.tag]description];
    ProjectName= [[[alertList valueForKey:@"ProjectName"] objectAtIndex:cancellworkorderID.tag]description];
//    cancellworkorderIDstr = [[[alertList valueForKey:@"WorkOrderId"] objectAtIndex:cancellworkorderID.tag]description];
//
//    [self performSelector:@selector(cancelpopViewController) withObject:nil afterDelay:0.1];
//    //
    [self performSegueWithIdentifier:@"cancelinfield_segue" sender:nil];


    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == alertTable)
    {
        return 1;
    }
    else
    {
        
        
        return [searchResultArray count];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == alertTable)
    {
        NSLog(@"[alertList count] %lu",(unsigned long)[alertList count]);
        if([alertList count]==0)
        {
             return  1;
        }else{
        return [alertList count];
        }
    }
    else
    {
        if([[[searchResultArray objectAtIndex:section] objectForKey:@"childObject"] count]==0)
        {
            return  1;
        }else{
        return [[[searchResultArray objectAtIndex:section] objectForKey:@"childObject"] count];
        }
    }
}
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if([alertList count]==0)
//    {}else{
//    _workorderID=[[[alertList valueForKey:@"WorkOrderId"] objectAtIndex:indexPath.row]description];
//   TaskName=  [[[alertList valueForKey:@"TaskName"] objectAtIndex:indexPath.row]description];
//   JobNumber=    [[[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row]description];
//    ProjectName= [[[alertList valueForKey:@"ProjectName"] objectAtIndex:indexPath.row]description];
//    TestTypeId=[[[alertList valueForKey:@"ProjectTestTypeId"] objectAtIndex:indexPath.row]description];
//    IsChange=[[[alertList valueForKey:@"IsChange"] objectAtIndex:indexPath.row]description];
//if([IsChange isEqualToString:@"1"])
//{
//
//    [self performSegueWithIdentifier:@"testtypesegue" sender:nil];
//
//
//}else{
//    [self performSegueWithIdentifier:@"dynamicform" sender:nil];
//}
//    }
//}

 //
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 57)
    {
        self.tabBarController.selectedIndex = 0;
    }
    if (alertView.tag == 58)
    {
     
    }
}



- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    
    
    webview_completedwo.scrollView.delegate = self; // set delegate method of UISrollView
    webview_completedwo.scrollView.maximumZoomScale = 20; // set as you want.
    webview_completedwo.scrollView.minimumZoomScale = 1; // set as you want.
    
    //// Below two line is for iOS 6, If your app only supported iOS 7 then no need to write this.
    webview_completedwo.scrollView.zoomScale = 2;
    webview_completedwo.scrollView.zoomScale = 1;
    [activityViewwo stopAnimating];
    [activityViewwo removeFromSuperview];
    
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    webview_completedwo.scrollView.maximumZoomScale = 20; // set similar to previous.
}


-(void)showpdfview_wo
{
    
    if(showpdfwo)
    {

        self->jobdate.enabled=NO;
        _cancel_btn.enabled=NO;
        _filter_icon.enabled=NO;
        _caliender_icon.enabled=NO;
        [webview_completedwo removeFromSuperview];
    
    [webview_completedbuttonwo removeFromSuperview];
    webview_completedwo=[[UIWebView alloc]initWithFrame:CGRectMake( 0, 60, self.view.frame.size.width-30, self.view.frame.size.height-100)];
    webview_completedwo.center = CGPointMake( self.view.frame.size.width/2, self.view.frame.size.height/2+70);
    
    webview_completedwo.scalesPageToFit=YES;
    NSLog(@"urlData%@",targetURLwo);
    
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:targetURLwo];
    //  webview_completedwo.opaque = NO;
    // webview_completedwo.delegate=self;
    //    webview_completedwo.backgroundColor = [UIColor clearColor];
    [webview_completedwo loadRequest:request];
    //    UIPinchGestureRecognizer *pgr = [[UIPinchGestureRecognizer alloc]
    //                                     initWithTarget:self action:@selector(handlePinchGesture:)];
    //    pgr.delegate = self;
    //    [ webview_completedwo addGestureRecognizer:pgr];
    [self.view addSubview:webview_completedwo];
    webview_completedbuttonwo = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [webview_completedbuttonwo setFrame: CGRectMake(webview_completedwo.frame.origin.x+15,webview_completedwo.frame.origin.y+5,50, 30)];
    [webview_completedbuttonwo setTitle:@"Close" forState:UIControlStateNormal];
    webview_completedbuttonwo.userInteractionEnabled = YES;
    webview_completedbuttonwo.titleLabel.lineBreakMode   = UILineBreakModeTailTruncation;
    webview_completedbuttonwo.titleLabel.textColor =[UIColor blueColor];
    webview_completedbuttonwo.showsTouchWhenHighlighted = YES;
    webview_completedbuttonwo.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin|
    UIViewAutoresizingFlexibleTopMargin|
    UIViewAutoresizingFlexibleHeight|
    UIViewAutoresizingFlexibleBottomMargin;
    [webview_completedwo setDelegate:self];
    
    [webview_completedbuttonwo addTarget:self action:@selector(closepdfwo:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:webview_completedbuttonwo];
    activityViewwo=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-15, self.view.frame.size.height/2, 30, 30)];
    [activityViewwo setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    
    activityViewwo.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height/2.0);
    [activityViewwo startAnimating];
    activityViewwo.tag = 100;
    [self.view addSubview:activityViewwo];
    }
    
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    if(showpdfwo)
    {
        
        [self showpdfview_wo];
    }
}
- (IBAction)closepdfwo:(id)sender {
    self->jobdate.enabled=YES;
_filter_icon.enabled=YES;
    _caliender_icon.enabled=YES;
    _cancel_btn.enabled=YES;
    showpdfwo=false;
    [activityViewwo stopAnimating];
    [activityViewwo removeFromSuperview];
    [webview_completedwo removeFromSuperview];
    [webview_completedbuttonwo removeFromSuperview];
    
}

#pragma mark - API Delegate

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
    //[[UIApplication sharedApplication] endIgnoringInteractionEvents];
     UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:response delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    callAlert.tag = 57;
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    if ([key isEqualToString:@"GetDashboard"])
    {
           loadingmsg=@"No Data Found!";
       
      //  [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        alertTable.tableFooterView.hidden = YES;
        detailsDic = [[NSMutableDictionary alloc]initWithDictionary:response];
        NSData* data=[NSKeyedArchiver archivedDataWithRootObject:response];
        [[NSUserDefaults standardUserDefaults]setObject:data forKey:details];
        [[NSUserDefaults standardUserDefaults]synchronize];
        NSLog(@"data%@",response);
        
        [self loadAlertData];
    }
    //
else    if ([key isEqualToString:@"GetAllPendingWO"])
    {
        loadingmsg=@"No Data Found!";
        
        //  [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        alertTable.tableFooterView.hidden = YES;
        detailsDic = [[NSMutableDictionary alloc]initWithDictionary:response];
        NSData* data=[NSKeyedArchiver archivedDataWithRootObject:response];
        [[NSUserDefaults standardUserDefaults]setObject:data forKey:details];
        [[NSUserDefaults standardUserDefaults]synchronize];
        NSLog(@"data%@",response);
        
        [self loadAlertData];
    }
    //CopyWorkOrderForApp
    else if ([key isEqualToString:@"CopyWorkOrderForApp"])
    {   [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        [self updateData];
        //[self viewDidLoad];
        //  [self viewWillAppear:YES];
    }
    else if ([key isEqualToString:@"SubmitCancelWoForApp"])
    {   [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        [self updateData];
        //[self viewDidLoad];
      //  [self viewWillAppear:YES];
    }
    else if ([key isEqualToString:@"GetWoPdfUrl"])
    {        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSLog(@"GetWoPdfUrldata%@",response);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString  *url=[response valueForKeyPath:@"Data"];
            //  url=  @"http://www.google.com";// @"http://efielddatadynamic.vconnexservices.com/WorkOrder/WorkOrderReportListByIdPrint?WorkOrderId=394";
            targetURLwo = [NSURL URLWithString:url];
            if([url length]>0){
            showpdfwo=true;
            }else{
                showpdfwo=false;

            }
            [self showpdfview_wo];
        });
        
        
    }
    else if ([key isEqualToString:@"GetSendEmail"])
    {
        
        // if ([MFMailComposeViewController canSendMail])
        //{
        
        //            MFMailComposeViewController *mail =
        //            [[MFMailComposeViewController alloc] init];
        //            mail.mailComposeDelegate = self;
        //            [mail setSubject:[response valueForKeyPath:@"Data.subject"]];
        //            [mail setMessageBody:[response valueForKeyPath:@"Data.body"] isHTML:YES];
        //            NSArray *emailwords = [[response valueForKeyPath:@"Data.toEmail"] componentsSeparatedByString: @","];
        //  NSArray *ccwords = [[response valueForKeyPath:@"Data.CC"] componentsSeparatedByString: @","];
        //            //  NSArray *bccwords = [[response valueForKeyPath:@"Data.BCC"] componentsSeparatedByString: @","];
        //             [mail setToRecipients:emailwords];
        //            [mail setCcRecipients:ccwords];
        //   [mail setBccRecipients:bccwords];
        
        NSDictionary *json = @{
                               
                               @"workOrderId" : emailworkid,
                               
                               @"JobNumber" :emailjobnumber,
                               
                               @"body" : [response valueForKeyPath:@"Data.body"],
                               @"toEmail" :[response valueForKeyPath:@"Data.toEmail"],
                               @"CC" : [response valueForKeyPath:@"Data.CC"],
                               @"subject":[response valueForKeyPath:@"Data.subject"]
                               
                               
                               };
        
        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        NSLog(@"sendmail%@",json);
        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@UpdateSendEmail",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"UpdateSendEmail"];
        
        
        //  [self presentViewController:mail animated:YES completion:NULL];
        
        // }
        //        else
        //        {
        //            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Efield Message" message:@"Email cannot be sent." preferredStyle:UIAlertControllerStyleAlert];
        //            UIAlertAction* actionCancel = [UIAlertAction
        //                                           actionWithTitle:@"Ok"
        //                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        //
        //                                           }];
        //            [alert addAction:actionCancel];
        //            [self presentViewController:alert animated:YES completion:nil];
        //        }
        NSLog(@"response%@",response);
        
    }
    else if ([key isEqualToString:@"UpdateUserAppId"])
    {
        
        NSLog(@"app id uploaded response %@",response);
    }
    else if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efielddata Message" message:@"Please try again later"];
        }
    }
   
}

-(void) showDatePicker: (UIDatePickerMode) modeDatePicker currentLbl:(UILabel *)lbl
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                     if(modeDatePicker == UIDatePickerModeDate) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                                         datestring= [formatter stringFromDate:picker.date];
                                         _cloasebtn.hidden=false;
                               jobdate.text=datestring;
                                         [self updateData];
//                                         NSArray *temp;
//                                         temp = [tempArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"JobDateTimeText CONTAINS[c] %@",datestring]];
//                                     alertList = (NSMutableArray*)temp;
//                                     [alertTable reloadData];
                                     }
                                     
                                     
                                 }];
    [alertController addAction:doneAction];
    
    
    UIAlertAction *clearAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                    
                                      self->jobdate.text=@"";
                                      datestring = @"";
                                      [self updateData];
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    [alertController addAction:cancelAction];
    
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController  animated:YES completion:nil]; }


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)projectdata
{
    
    if([projectid.text length]==0){
        self.closebtn2.hidden=true;

    }else     if([projectid.text length]>=1){
        self.closebtn2.hidden=false;
     
    }
    
}

- (void)emailClickwo:(UITapGestureRecognizer *)tapGesture {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    emailworkid= [[alertList valueForKey:@"WorkOrderId"] objectAtIndex:tapGesture.view.tag];
    emailjobnumber=[[alertList valueForKey:@"JobNumber"] objectAtIndex:tapGesture.view.tag];
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetSendEmail?WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[alertList valueForKey:@"WorkOrderId"] objectAtIndex:tapGesture.view.tag]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetSendEmail"];
    
    
}
- (void)enable_closebtn
{
    if([jobdate.text length]==0){
        self.cloasebtn.hidden=true;
    }else{
        self.cloasebtn.hidden=false;

    }
}
- (void)updateData1
{
    
    if([jobdate.text length]==0){
        self.cloasebtn.hidden=true;
        wono=@"";
        datestring=@"";
        [self updateData];
    }else     if([jobdate.text length]>=3){
        self.cloasebtn.hidden=false;
        wono=jobdate.text;
               datestring=@"";
   [self updateData];
        
    }
  
}

- (IBAction)alertaction:(id)sender {
    [self performSegueWithIdentifier:@"alertsegue" sender:self];

}
- (void)updateDatarefresh
{
    
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
        return;
    }else{
        _cloasebtn.hidden=false;
        NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];
        [currentDTFormatter1 setDateFormat:@"MM/dd/yyyy"];

        NSString *todayDateStr = [currentDTFormatter1 stringFromDate:[NSDate date]];
        self->jobdate.text=todayDateStr;
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
      //  wono = @"";
   
     
        NSDictionary *json = @{
                               
                               @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
                               
                               @"RoleName" :[[NSUserDefaults standardUserDefaults] objectForKey:userRole],
                               @"ProjectId":@"",
                               @"ReportStatus" : ReportStatus,
                               @"Wono" : wono,
                               @"FromJobDateTime" :todayDateStr
                               
                               
                               };
      //  if( [_statusreport containsString:@"All Completed WO"])
      //  {
//            NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
//            [dictEntry setObject:json forKey:[NSString stringWithFormat:@"FilterWorkOrder"]];
//
//            DownloadManager *downloadObj = [[DownloadManager alloc]init];
//            //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
//
//            [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetDashboard",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"GetDashboard"];
//
//       // }
          if([_statusreport containsString:@"All"] )
        {
            NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
            [dictEntry setObject:json forKey:[NSString stringWithFormat:@"FilterWorkOrder"]];
            
            DownloadManager *downloadObj = [[DownloadManager alloc]init];
            //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            
            [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetAllPendingWO",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"GetAllPendingWO"];
            
        }else{
        NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
        [dictEntry setObject:json forKey:[NSString stringWithFormat:@"FilterWorkOrder"]];
        
        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetDashboard",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"GetDashboard"];
        
        }
    }
    [refreshControl performSelector:@selector(endRefreshing) withObject:nil afterDelay:1.0f];
    [refreshControl endRefreshing];


}
- (void)updateData
{
    
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
        return;
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
     //   wono = @"";
        
      
        
        if(ProjectId == nil)
        {
            ProjectId=@"";
            _closebtn2.hidden=true;

        }
        else    if([ProjectId isEqualToString:@""] )
        {
        }
        else{
            _closebtn2.hidden=false;

        }
        
        NSDictionary *json = @{
                               
                               @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
                               
                               @"RoleName" :[[NSUserDefaults standardUserDefaults] objectForKey:userRole],
                               @"ProjectId":ProjectId,
                               @"ReportStatus" : ReportStatus,
                               @"Wono" : wono,
                               @"FromJobDateTime" : datestring
                               
                               
                               };
        
//        if( [_statusreport containsString:@"All Completed WO"])
//        {
//            NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
//            [dictEntry setObject:json forKey:[NSString stringWithFormat:@"FilterWorkOrder"]];
//
//            DownloadManager *downloadObj = [[DownloadManager alloc]init];
//            //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
//
//            [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetDashboard",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"GetDashboard"];
//
//        }
//       else
        if([_statusreport containsString:@"All"] )
        {
            NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
            [dictEntry setObject:json forKey:[NSString stringWithFormat:@"FilterWorkOrder"]];
            
            DownloadManager *downloadObj = [[DownloadManager alloc]init];
            //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            
            [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetAllPendingWO",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"GetAllPendingWO"];
            
        }else{
            NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
            [dictEntry setObject:json forKey:[NSString stringWithFormat:@"FilterWorkOrder"]];
            
            DownloadManager *downloadObj = [[DownloadManager alloc]init];
            //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            
            [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetDashboard",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"GetDashboard"];
            
        }
    }
}
-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {
}
- (IBAction)logoutAction:(id)sender
{
//    if (![self isNetworkAvailable]) {
//        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
//        return;
//    }else{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    });
//    DownloadManager *downloadObj = [[DownloadManager alloc]init];
//    //[downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@IOSResetUserAppId?UserName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username]] andParameter:nil andMethod:@"POST" andDelegate:self andKey:@"logout"];
//        NSDictionary *json = @{
//                               
//                               @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username]
//                               
//                               };
//        NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
//        [dictEntry setObject:json forKey:[NSString stringWithFormat:@"UserModel"]];
//        
//        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@IOSResetUserAppId",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"logout"];
    
         [self logoutUser];
        
   // }
}

- (IBAction)changepasswordAction:(id)sender {
    [self changePassword];
}
- (IBAction)cancel_action:(id)sender {
    
    //[self.navigationController popViewControllerAnimated:YES];
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"Display_canceledJobDateText"] length]>0)
    {
   [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];

    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

- (void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil]
    ;
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Display_canceledJobDateText"];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];
    
    //    if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"IsDefaultRoundList"] isEqualToString:@"1"]){
    //
    //
    //        rootViewController.selectedIndex = 3;
    //
    //    }else{
    
    rootViewController.selectedIndex = 0;
    
    // }
    [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

    NSString *name;
    NSString *room ;

    if ([segue.identifier isEqualToString:@"alertsegue"]) {

        AlertListVC *vc = [segue destinationViewController];

    }
    else if ([segue.identifier isEqualToString:@"cancelinfield_segue"]) {

        cancell_infieldVC *vc = [segue destinationViewController];
        vc.WorkorderId =_workorderID;

        vc.TaskName=TaskName;
        vc.ProjectName=ProjectName;
        vc.JobNumber=JobNumber;
    }
    //
    else


    if ([segue.identifier isEqualToString:@"dynamicform"]) {
        //dynamicform
        DynamicNoteFormVC *vc = [segue destinationViewController];
        vc.WorkorderId =_workorderID;

        vc.TaskName=TaskName;
        vc.ProjectName=ProjectName;
         vc.JobNumber=JobNumber;
    }
    //projectfilter_segue
    else if ([segue.identifier isEqualToString:@"projectfilter_segue"]) {
        //dynamicform
        project_filter *vc = [segue destinationViewController];
        vc.IsAll=isAll;
    }
   else if ([segue.identifier isEqualToString:@"testtypesegue"]) {
        //dynamicform
        TestTypeselectVC *vc = [segue destinationViewController];
        vc.WorkorderId =_workorderID;
   
        vc.TaskName=TaskName;
        vc.ProjectName=ProjectName;
        vc.JobNumber=JobNumber;
       vc.TesttypeId=TestTypeId;
    }

}

- (IBAction)jobdateaction:(id)sender {
    
    [self showDatePicker:UIDatePickerModeDate currentLbl:self->jobdate];
}


- (IBAction)close:(id)sender {
    self->jobdate.text=@"";wono=@"";
    datestring=@"";
    self.cloasebtn.hidden=true;
    [self updateData];
}

- (IBAction)selectproject:(id)sender {
 //   projectfilter_segue
    
      [self performSegueWithIdentifier:@"projectfilter_segue" sender:self];
}
- (IBAction)clearproject_action:(id)sender {
    ProjectId=@"";
    projectid.text=@"";
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"ProjectId"];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"ProjectName"];
    
    self.closebtn2.hidden=true;
    [self updateData];
}
@end
