                             //
//  AlertListVC.m
//  Efielddata
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import "Timesheet.h"
#import "Constants.h"  
#import "DynamicNoteFormVC.h"

#import "TestTypeselectVC.h"
#import "NoRecordsTableViewCell.h"
#import "workordertimesheetheadercell.h"
#import "workordertimesheetcell.h"
#import "timesheettableheadercell.h"
#import "timesheettablecell.h"

#import "timsheet_edit.h"
#import "signature.h"
#import "MPBSignatureViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AlertListVC.h"
//#import "UITabBarItem+CustomBadge.h"
@interface Timesheet () <UITableViewDelegate, UITableViewDataSource,UITextViewDelegate,UITextFieldDelegate,UIPickerViewDataSource, UIPickerViewDelegate,UIWebViewDelegate>
{
    signature *signView;
    UIView *mainview;
    BOOL isOnEditing,opensignatureview;
    UIPickerView *picker;
    BOOL isMarkAll,isSignture;
    __weak IBOutlet UILabel *titleLbl;
    __weak IBOutlet UITableView *alertSearchTable;
    NSMutableArray *searchResultArray;
    NSMutableArray *searchFullResultArray;
    NSInteger selectedRowIndex,deletetag,workorderindex;
    int selectedUtilityIndex;
    NSString *places,*editworkorderIDstr,*edittimesheet_IDstr;
    UIAlertController * timeshtthours ;
  NSString * edittimesheet_absence,*hours,
    
    *edittimesheet_date,
    *edittimesheet_notes,
    *edittimesheet_hours;
    NSString *IsChange,*TestTypeId;
    CGFloat  lastScale;
    int timesheettimespent_hours;   // integer division to get the hours part
    int timesheettimespent_minutes;
    NSString  *timesheettimespent_str,
    *timesheettimespent_save;
    NSString *TaskName,*JobNumber,*ProjectName;
    NSString *datestr,*loadingmsg;
    UIImage *signatureimg;
    NSData *ttbresponse;
    NSString   *pdfurlstring;
    NSString *eventDateStr,*end_curweekdate;
   }
@end

@implementation Timesheet
NSString *Startdate, *enddate;

bool  showpdf_timesheet;
UIButton *Timesheet_webview_completedbutton ;
UIWebView *Timesheet_webview_completed;
NSURL *Timesheet_targetURL ;
UIActivityIndicatorView *activity_View;

- (void)viewDidLoad {
      
    [super viewDidLoad];
    
    hours=@"";
    showpdf_timesheet=false;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
  //  [self.view addGestureRecognizer:tap];
    _timesheet_absence.enabled = NO;

    loadingmsg=@"";
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
   // [self updateALERTcount];
//    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"searchText"];
//    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"searchText_alert"];
//    
//    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"patientlistflag"];
    _companynametxt.text=[[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];
    _popupview.hidden=true;

    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
     [currentDTFormatter1 setDateFormat:@"MM/dd/yyyy"];

    self.navigationController.navigationBar.hidden = YES;

 eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@", eventDateStr);
    self.date.text=eventDateStr;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 20)];

    [_timesheet_date setDelegate:self];
    [_timesheet_hours setDelegate:self];
    [_timesheet_notes  setDelegate:self];
    _timesheet_hours.keyboardType = UIKeyboardTypeDecimalPad;
    _datefield.leftView = paddingView;
    _datefield.leftViewMode = UITextFieldViewModeAlways;
    NSString *todayDateStr = [currentDTFormatter1 stringFromDate:[NSDate date]];
    _datefield.text=todayDateStr;
    [_datefield setDelegate:self];
    
    
    [_datefield addTarget:self action:@selector(updateDatacompleted) forControlEvents:UIControlEventEditingChanged];
    
    workorder_list = [[NSMutableArray alloc]init];
    _datefield.delegate=self;
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor whiteColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self
                       action:@selector(updateDatacompleted)
             forControlEvents:UIControlEventValueChanged];
    [_workorder_table addSubview:refreshControl];
      [_timesheettable addSubview:refreshControl];
    [self updateDatacompleted];
    _workorder_table.dataSource = self;
    _timesheettable.dataSource = self;
    _workorder_table.allowsSelection = NO;
    
    _timesheettable.allowsSelection = NO;
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *now = [NSDate date];
    NSDate *startOfTheWeek;
    NSDate *endOfWeek;
    NSTimeInterval interval;
    [cal rangeOfUnit:NSWeekCalendarUnit
           startDate:&startOfTheWeek
            interval:&interval
             forDate:now];
    //startOfWeek holds now the first day of the week, according to locale (monday vs. sunday)
    
    endOfWeek = [startOfTheWeek dateByAddingTimeInterval:interval];
    end_curweekdate=[currentDTFormatter1 stringFromDate:endOfWeek];
    NSLog(@"endOfWeek%@",end_curweekdate);

}

-(void)dismissKeyboard
{
      [self.view endEditing:YES];
  //  [aTextField resignFirstResponder];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
        return 35;
        
  
    
    
}




- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
        if (tableView == _workorder_table)
        {
            if([workorder_list count]==0)
            {
                return  2;
            }else{
                return [workorder_list count]+1;
            }
        }
        else
        {
            if([timesheet_list count]==0)
            {
                return  2;
            }else{
                return [timesheet_list count]+1;
            }
        }
   
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(tableView==_workorder_table)
    {
        if(indexPath.row==0)
        {
            workordertimesheetheadercell *cell = (workordertimesheetheadercell *)[tableView dequeueReusableCellWithIdentifier:@"workorderheadercell"];
             cell.backgroundColor = [UIColor  colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0];
            return cell;
            
        }
        else
        {
            if([workorder_list count]==0)
            {
                NoRecordsTableViewCell *cell = (NoRecordsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:noRecordsResuableIdentifier];
                cell.noRecordsLabel.text =loadingmsg;
                return cell;
                
            }
            else {
                workordertimesheetcell *cell = (workordertimesheetcell *)[tableView dequeueReusableCellWithIdentifier:@"workordertimesheetcell"];
                cell.date_val.text=[[workorder_list valueForKey:@"JobDateText"] objectAtIndex:indexPath.row-1];
                  cell.workorder_value.text=[[workorder_list valueForKey:@"Wono"] objectAtIndex:indexPath.row-1];
                  cell.hours_value.text=[[[workorder_list valueForKey:@"HoursSpent"] objectAtIndex:indexPath.row-1]description];
                
                
                
                if([[[workorder_list valueForKey:@"IsTimeSheet"] objectAtIndex:0]boolValue])
                {
                    
                     cell.edit_Timesheet.hidden=true;
                    
                }
                else{
                    cell.edit_Timesheet.hidden=false;

                }
                cell.edit_Timesheet.tag = indexPath.row-1;

                [cell.edit_Timesheet addTarget:self action:@selector(editworkorder_clicked:) forControlEvents:UIControlEventTouchUpInside];

                if(indexPath.row % 2 == 0)
                    cell.backgroundColor = [UIColor  colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0];
                else
                    cell.backgroundColor = [UIColor  whiteColor];
                 return cell;
                
            }
            
        }
    }
    else
    {
        if(indexPath.row==0)
        {
            timesheettableheadercell *cell = (timesheettableheadercell *)[tableView dequeueReusableCellWithIdentifier:@"timeheadercell"];
               cell.backgroundColor = [UIColor  colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0];
            return cell;
            
            
        }
        else   {
            if([timesheet_list count]==0)
            {
                NoRecordsTableViewCell *cell = (NoRecordsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:noRecordsResuableIdentifier];
                cell.noRecordsLabel.text =loadingmsg;
                return cell;
                
            }
            else{
                timesheettablecell *cell = (timesheettablecell *)[tableView dequeueReusableCellWithIdentifier:@"timesheetcell"];
                cell.date_val.text=[[timesheet_list valueForKey:@"JobDateText"] objectAtIndex:indexPath.row-1];
                cell.absence_value.text=[[timesheet_list valueForKey:@"Type"] objectAtIndex:indexPath.row-1];
                NSLog(@"hrsspent%@",[[timesheet_list valueForKey:@"HrsSpent"] objectAtIndex:indexPath.row-1]);
                cell.hours_value.text=[[[timesheet_list valueForKey:@"HrsSpent"] objectAtIndex:indexPath.row-1]description];
                
                cell.edit_timesheet_details.tag = indexPath.row-1;
                if([[[timesheet_list valueForKey:@"IsTimeSheet"] objectAtIndex:0]boolValue])
                {
                    
                    cell.edit_timesheet_details.hidden=true;
                      cell.delete_timesheet_details.hidden=true;
                }
                else{
                    cell.edit_timesheet_details.hidden=false;
                       cell.delete_timesheet_details.hidden=false;
                }
                [cell.edit_timesheet_details addTarget:self action:@selector(editworkorderdetail_clicked:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.delete_timesheet_details.tag = indexPath.row-1;
                
                [cell.delete_timesheet_details addTarget:self action:@selector(deleteworkorderdetail_clicked:) forControlEvents:UIControlEventTouchUpInside];

                if(indexPath.row % 2 == 0)
                    cell.backgroundColor = [UIColor  colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0];
                else
                         cell.backgroundColor = [UIColor  whiteColor];
                return cell;
                
                
            }
            
        }
    }
   
}

- (void)updateDatacompleted
{
     [self.view endEditing:YES];
    if([_datefield.text length]==0){
        self.closebtn.hidden=true;
     
    }else{
        self.closebtn.hidden=false;
      
        
    }
    
    
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
        return;
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
     
        NSDictionary *json = @{
                               
                               @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
                               
                               @"RoleName" :[[NSUserDefaults standardUserDefaults] objectForKey:userRole],
                             
                               @"FromJobDateTime" : _datefield.text
                               
                               
                               };
        
        
        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        
        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@IOSTimeSheetWorkOrderList",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"IOSTimeSheetWorkOrderList"];
        
        [refreshControl performSelector:@selector(endRefreshing) withObject:nil afterDelay:1.0f];
        
    }
}
- (IBAction)logoutAction:(id)sender
{
    [self logoutUser];
    
    
    //  }
}

- (IBAction)alertaction:(id)sender {
    [self performSegueWithIdentifier:@"alertsegue" sender:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
  
    
    [super viewWillDisappear:animated];
}

- (void)deallocwebview {
    [Timesheet_webview_completed setDelegate:nil];
    [Timesheet_webview_completed stopLoading];
}



// ARC - Before iOS6 as its deprecated from it.
- (void)viewWillUnload {
    [Timesheet_webview_completed setDelegate:nil];
    [Timesheet_webview_completed stopLoading];
}
- (void)viewWillAppear:(BOOL)animated
{
    
    
    [super viewWillAppear:animated];
    [mainview removeFromSuperview];
    
    opensignatureview=false;
    [self enableallbutton];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"DisplayJobDateText"];

      [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Display_canceledJobDateText"];
    _popupview.hidden=true;

    NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];
 [currentDTFormatter1 setDateFormat:@"MM/dd/yyyy"];
   // NSString *todayDateStr = [currentDTFormatter1 stringFromDate:[NSDate date]];

  //  _datefield.text=todayDateStr;
    _alertcnt.text=[self updateALERTcount];
    showpdf_timesheet=false;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"datereplace"]isEqualToString:@""])
    {
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
        NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];
 
        [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
        [currentDTFormatter1 setDateFormat:@"MM/dd/yyyy"];

        NSString *todayDateStr = [currentDTFormatter1 stringFromDate:[NSDate date]];
        _datefield.text=todayDateStr;
        [_datefield setDelegate:self];
        

    }
    [self updateDatacompleted];

    [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"datereplace"];

    [Timesheet_webview_completed removeFromSuperview];
    [Timesheet_webview_completedbutton removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) showDatePickertextfield: (UIDatePickerMode) modeDatePicker currentLbl:(UITextField *)lbl
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    _closebtn.hidden=false;

    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"MM/dd/yyyy"];
 
    if([ _timesheet_date.text isEqualToString:@""]){
    }else{
        NSDate *mydate;

        mydate = [formatter1 dateFromString: _timesheet_date.text];
        [picker setDate:mydate animated:NO];
       
    }
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                     if(modeDatePicker == UIDatePickerModeDate) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                                         
                                         NSString *datestring= [formatter stringFromDate:picker.date];
                                         
                                         lbl.text=datestring;
                                    
                                     }
                                     
                                     
                                 }];
    [alertController addAction:doneAction];
    
    
    UIAlertAction *clearAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                      
                                lbl.text=@"";
                                   self.closebtn.hidden=true;
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    [alertController addAction:cancelAction];
    
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController  animated:YES completion:nil]; }





-(void) showDatePicker: (UIDatePickerMode) modeDatePicker currentLbl:(UILabel *)lbl
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    _closebtn.hidden=false;
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"MM/dd/yyyy"];
    NSDate *enddte=[formatter1 dateFromString:end_curweekdate];
    [picker setMaximumDate:enddte];
  
    if([ _datefield.text isEqualToString:@""]){
    }else{
        NSDate *mydate;
        NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
        [formatter1 setDateFormat:@"MM/dd/yyyy"];
        mydate = [formatter1 dateFromString: _datefield.text];
        [picker setDate:mydate animated:NO];
    }
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                     if(modeDatePicker == UIDatePickerModeDate) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                               
                                         NSString *datestring= [formatter stringFromDate:picker.date];
                                         
                                       _datefield.text=datestring;
                                         [self updateDatacompleted];
                                     }
                                     
                                     
                                 }];
    [alertController addAction:doneAction];
    
    
    UIAlertAction *clearAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                      
                                      _datefield.text=@"";
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    [alertController addAction:cancelAction];
    
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController  animated:YES completion:nil]; }





-(void)deleteworkorderdetail_clicked:(UIButton*)deleteworkorderID
{
    
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:@"Are you sure to delete this item?"  delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil];
    callAlert.tag = 56;
    [callAlert show];
    deletetag=deleteworkorderID.tag;
    
    
}
-(void)editworkorderdetail_clicked:(UIButton*)editworkorderID
{
    edittimesheet_IDstr=[[[timesheet_list valueForKey:@"TimeSheetId"] objectAtIndex:editworkorderID.tag]description];
//     if(_popupview.hidden)
//     {      _popupview.hidden=false;
//     }else{
//           _popupview.hidden=true;
//     }
//    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_popupview];

edittimesheet_absence=[[[timesheet_list valueForKey:@"Type"] objectAtIndex:editworkorderID.tag]description];
  

     edittimesheet_date=[[[timesheet_list valueForKey:@"JobDateText"] objectAtIndex:editworkorderID.tag]description];
     edittimesheet_notes=[[[timesheet_list valueForKey:@"Notes"] objectAtIndex:editworkorderID.tag]description];
        edittimesheet_hours=[[[timesheet_list valueForKey:@"HrsSpent"] objectAtIndex:editworkorderID.tag]description];
    [[NSUserDefaults standardUserDefaults]setObject:edittimesheet_absence forKey:@"absencetxt"];

//timesheet_editsegue
    [self performSegueWithIdentifier:@"timesheet_editsegue" sender:self];

    //[self performSelector:@selector(editdetailstimesheet_popViewController) withObject:nil afterDelay:0.1];
    
    
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 5;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *item ;
    if(row==0)
    {item=@"Administration";}
    else    if(row==1)
    {item=@"Holiday";}
    else    if(row==2)
    {item=@"Sick";}
    else    if(row==3)
    {item=@"Training";}
    else
    {item=@"Vacation";}
    return item;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // perform some action
    
    if(row==0)
    {_timesheet_absence.text=@"Administration";}
    else    if(row==1)
    {_timesheet_absence.text=@"Holiday";}
    else    if(row==2)
    {_timesheet_absence.text=@"Sick";}
    else    if(row==3)
    {_timesheet_absence.text=@"Training";}
    else
    {_timesheet_absence.text=@"Vacation";}
    [picker removeFromSuperview];

}


- (void)createPicker1 {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-250, screenWidth, 44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:self
                                                                     action:@selector(doneClicked)];
    toolBar.items = @[flex, barButtonDone];
    barButtonDone.tintColor = [UIColor blackColor];
    
  picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, toolBar.frame.size.height, screenWidth, 200)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.showsSelectionIndicator = YES;
    
    
    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, toolBar.frame.size.height + picker.frame.size.height)];
    inputView.backgroundColor = [UIColor clearColor];
    [inputView addSubview:picker];
    [inputView addSubview:toolBar];
    
    _timesheet_absence.inputView = inputView;
    
    [self.view addSubview:inputView];
}
- (void)createPicker  {
    
//    UIView *pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 216)];
//    pickerView.backgroundColor = [UIColor colorWithRed:109.0/255.0 green:110.0/255.0 blue:120.0/255.0 alpha:1];
//
    
 picker = [[UIPickerView alloc] initWithFrame:CGRectMake(self.view.center.x/2,self.view.center.y+60,self.view.bounds.size.width/2, 250)];
    picker.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1];
 
    picker.showsSelectionIndicator = YES;
    picker.dataSource = self;
    picker.delegate = self;
 _timesheet_absence.inputView = picker;
 
    // add a toolbar with Cancel & Done button
    UIToolbar *toolBar  = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,320,44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;

    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTouched:)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTouched:)];

    // the middle button is to make the Done button align to right
    [toolBar setItems:[NSArray arrayWithObjects:cancelButton, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton, nil]];
_timesheet_absence.inputAccessoryView = toolBar;
    
    [self.view addSubview:picker];

 
}




-(void)SubmitTimeSheetForApp
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    NSString *ids=@"";
    for(int i=0;i<[workorder_list count];i++)
    {
        if([ids length]>0)
        {
            NSString *idsval=[[[workorder_list valueForKey:@"WorkOrderId"] objectAtIndex:0]description];
            ids=[NSString stringWithFormat:@"%@,%@",ids,idsval];
        }
        else{
            ids=[[[workorder_list valueForKey:@"WorkOrderId"] objectAtIndex:0]description];
        }
        
        
    }
    NSDictionary *json = @{
                           @"List" :workorder_list,
                           @"Details" :timesheet_list,
                           @"WonoText": ids,
                           @"JobDate": _datefield.text,
                           @"JobDateText": _datefield.text,
                           @"Techname": [[NSUserDefaults standardUserDefaults] objectForKey:username]
                           
                           };
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetForApp"];
}


-(void)editworkorder_clicked:(UIButton*)editworkorderID
{
    editworkorderIDstr=[[[workorder_list valueForKey:@"WorkOrderId"] objectAtIndex:editworkorderID.tag]description];
        hours=[[[workorder_list valueForKey:@"HoursSpent"] objectAtIndex:editworkorderID.tag]description];
    workorderindex=editworkorderID.tag;
    timesheettimespent_str =
    hours;
    timesheettimespent_save =
    hours;
    
    if ([timesheettimespent_str rangeOfString:@"."].location == NSNotFound) {
        
        timesheettimespent_str =
        [NSString stringWithFormat:@"%@ hrs 00 mins", timesheettimespent_str];
    }
    timesheettimespent_str =
    [timesheettimespent_str stringByReplacingOccurrencesOfString:@".5"
                                                      withString:@" hrs 30 mins"];
    
    timesheettimespent_str =
    [timesheettimespent_str stringByReplacingOccurrencesOfString:@".25"
                                                      withString:@" hrs 15 mins"];
    timesheettimespent_str =
    [timesheettimespent_str stringByReplacingOccurrencesOfString:@".75"
                                                      withString:@" hrs 45 mins"];
  [self performSelector:@selector(edittimesheet_popViewController) withObject:nil afterDelay:0.1];
    
    
}


-(void) textFieldDidChange:(UITextField *)textField
{
    [timeshtthours dismissViewControllerAnimated:YES completion:nil];

  //  timeshtthours.di
    [self showTimePicker:UIDatePickerModeTime
              currentLbl:textField];
    
//
//    if(textField.text.length == 0){
//        textField.textColor = [UIColor lightGrayColor];
//        textField.placeholder  = @"Click Enter to hours";
//        hours=@"";
//     }
//
//    else{
////        NSString *codeString = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
////
////        NSString *firstLetter = [codeString substringFromIndex:0];
////        if([firstLetter isEqualToString:@"."])
////        {
////
////            hours= [NSString stringWithFormat:@"0%@",codeString];
////        }else{
////            hours=codeString;
////
////        }
////        textField.text =  hours;
//    }
//
 
    
}
- (NSDate *)roundToNearestQuarterHour:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |
    NSCalendarUnitDay | NSCalendarUnitHour |
    NSCalendarUnitMinute | NSCalendarUnitWeekday |
    NSCalendarUnitWeekdayOrdinal | NSCalendarUnitWeekOfYear;
    NSDateComponents *components = [calendar components:unitFlags fromDate:date];
    NSInteger roundedToQuarterHour = round((components.minute / 15.0)) * 15;
    components.minute = roundedToQuarterHour;
    return [calendar dateFromComponents:components];
}

- (void)showTimePicker:(UIDatePickerMode)modeDatePicker
            currentLbl:(UITextField *)lbl {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
    [picker setLocale:locale];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    
    if ([timesheettimespent_str isEqualToString:@""] ||[timesheettimespent_str isEqualToString:@"0 hrs 00 mins"]||
        [timesheettimespent_str isKindOfClass:[NSNull class]]) {
        picker.date = [self roundToNearestQuarterHour:[NSDate date]];
        
    } else {
        
        NSString *times = [timesheettimespent_str stringByReplacingOccurrencesOfString:@" hrs"
                                                                   withString:@":"];
        times = [times stringByReplacingOccurrencesOfString:@" mins"
                                                 withString:@""];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"HH:mm";
        NSDate *date = [dateFormatter dateFromString:times];
        NSString *pmamDateString = [dateFormatter stringFromDate:date];
        NSData *date_hrs = [formatter dateFromString:pmamDateString];
        [picker setDate:date];
    }
    picker.minuteInterval = 15;
    UIAlertAction *doneAction = [UIAlertAction
                                 actionWithTitle:@"Done"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action) {
                                     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                     [formatter setDateFormat:@"HH:mm"];
                                     NSString *datestring = [formatter stringFromDate:picker.date];
                                     
                                     NSCalendar *calendar = [NSCalendar currentCalendar];
                                     NSDateComponents *components = [calendar
                                                                     components:(NSCalendarUnitHour | NSCalendarUnitMinute)
                                                                     fromDate:picker.date];
                                     
                                     timesheettimespent_hours = [components hour];
                                     timesheettimespent_minutes = [components minute];
                                     
                                     NSString *timeDiff;
                                     if (timesheettimespent_minutes == 0) {
                                         timeDiff = [NSString
                                                     stringWithFormat:@"%d hrs %ld0 mins", timesheettimespent_hours,
                                                     (long)timesheettimespent_minutes];
                                         
                                     } else {
                                         timeDiff = [NSString
                                                     stringWithFormat:@"%d hrs %ld mins", timesheettimespent_hours,
                                                     (long)timesheettimespent_minutes];
                                     }
                                     timesheettimespent_str = timeDiff;
                                     timesheettimespent_save = timeDiff;
                                    // if (lbl.tag == indexpathrow.row) {
                                         lbl.text = timeDiff;
                                    // }
                                      [self performSelector:@selector(edittimesheet_popViewController) withObject:nil afterDelay:0.1];
                                 }];
    [alertController addAction:doneAction];
    
    UIAlertAction *clearAction =
    [UIAlertAction actionWithTitle:@"Clear"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action) {
                               lbl.text = @"";
                               timesheettimespent_str = @"";
                               timesheettimespent_save = @"";
                               [self performSelector:@selector(edittimesheet_popViewController) withObject:nil afterDelay:0.1];
                               
                               
                           }];
    
    UIAlertAction *cancelAction =
    [UIAlertAction actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction *action) {
                               NSLog(@"Cancel action");
                               [self performSelector:@selector(edittimesheet_popViewController) withObject:nil afterDelay:0.1];
                               
                               
                           }];
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController =
    alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect =
    CGRectMake(self.view.bounds.size.width / 2.0,
               self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController animated:YES completion:nil];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
}
- (void)edittimesheet_popViewController {
   
    
  NSString *message=  [NSString stringWithFormat:@"Please update time spent on this %@ - %@",[[[workorder_list valueForKey:@"Wono"] objectAtIndex:workorderindex]description],
                       [[[workorder_list valueForKey:@"TaskName"] objectAtIndex:workorderindex]description]];
    
    
    // if (![timesheettimespent_str i) {
    
  //  hours=timesheettimespent_str;
    timeshtthours = [UIAlertController alertControllerWithTitle: @"Timesheet"
                                                                              message: message preferredStyle:UIAlertControllerStyleAlert];
    [timeshtthours addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder =@"Click Enter to hours";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
        textField.text= timesheettimespent_str;//hours;
      textField.delegate=self;
 //textField.enabled=false;
         //       textField.userInteractionEnabled = false;
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventTouchDown];
         //ntEditingChanged];
        //        textField.placeholder = @"Enter hours";
        //        textField.textColor = [UIColor blueColor];
        //        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        //        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
  
    [timeshtthours addAction:[UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
    }]];
    [timeshtthours addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = timeshtthours.textFields;
        UITextField * hrsfield = textfields[0];
        
        
        if (timesheettimespent_minutes != -1) {
            int send_minutes = 0;
            if (timesheettimespent_minutes == 15) {
                send_minutes = 25;
            } else if (timesheettimespent_minutes == 30) {
                send_minutes = 5;
            } else if (timesheettimespent_minutes == 45) {
                send_minutes = 75;
            }
            
            timesheettimespent_save =
            [NSString stringWithFormat:@"%d.%d", timesheettimespent_hours, send_minutes];
        }
        
//        NSString *codeString = [hrsfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//
//        NSString *firstLetter = [codeString substringFromIndex:0];
//        if([firstLetter isEqualToString:@"."])
//        {
//
//            codeString= [NSString stringWithFormat:@"0%@",codeString];
//        }else{
//            codeString=codeString;
//
//        }
//        hrsfield.text =  codeString;
        NSLog(@"hrsfield %@",hrsfield.text );
      //  NSString *str=[[[workorder_list valueForKey:@"TimeSpent"] objectAtIndex:workorderindex]description];
    //    hrsfield.text=str;
        timesheettimespent_str=hrsfield.text;
//        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//        formatter.numberStyle = NSNumberFormatterDecimalStyle;
//        NSNumber *myNumber = [formatter numberFromString:hours];
        hrsfield.tag=1002;
        if([timesheettimespent_str length]==0){
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please Enter hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
            
        }
        
//        else if ([hrsfield.text rangeOfCharacterFromSet:notDigits].location == NSNotFound)
//
//        {
 else     {
     
     
                
            float b = [timesheettimespent_save floatValue];
     
//     if([timesheettimespent_str  containsString:@" "]){
//         UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please enter valid hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//         [callAlert show];
//     }else{
            if(b<24){
                
                
//                NSString *stringToBeTested = hrsfield.text;
//
//                NSString *mobileNumberPattern = @"^[0-9]+(?:\\.[0-9]{2})?$";
//                NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
//
//
//
//                NSString *mobileNumberPattern2 = @"^[0-9]+(?:\\.[0-9]{1})?$";
//                NSPredicate *mobileNumberPred1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern2];
//
//
//                NSString *mobileNumberPattern3 = @"^[0-9]+(?:\\.[0-9]{3})?$";
//                NSPredicate *mobileNumberPred2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern3];
//
//                BOOL matched = [mobileNumberPred evaluateWithObject:stringToBeTested];
//                BOOL matched2 = [mobileNumberPred2 evaluateWithObject:stringToBeTested];
//
//                BOOL matched1 = [mobileNumberPred1 evaluateWithObject:stringToBeTested];
//           //     NSLog(@"matched %@ matched2 %@ matched1 %@",matched, matched2,matched1);
//                if(matched||matched1||matched2){
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            NSDictionary *json = @{
                                   
                                   @"WorkOrderId" : editworkorderIDstr,
                                   @"TimeSpent" : timesheettimespent_save
                                   
                                   };
          
                
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:workorder_list[workorderindex]];
                //[dic setObject:hrsfield.text forKey:@"TextAnswer"];
                NSLog(@"dic : %@",dic);
              //  [dataList replaceObjectAtIndex:textField.tag withObject:dic];
            DownloadManager *downloadObj = [[DownloadManager alloc]init];
            //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            
            [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetWoForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetWoForApp"];
                }
//                else{
//                    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please enter valid hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//                    [callAlert show];
//                }
           // }
     
            else
            {
               UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please enter valid hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [callAlert show];
            }
  //   }
 
    }
//        else
//        {
//
//
//            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please enter valid hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            [callAlert show];
//        }
        
    }]];
    [self presentViewController:timeshtthours animated:YES completion:nil];
    
}





- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

{
    
    
    if (0 == buttonIndex && alertView.tag == 1001)
    {
        
        [self SubmitTimeSheetForApp];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        });
//                NSString *ids=@"";
//                for(int i=0;i<[workorder_list count];i++)
//                {
//                    if([ids length]>0)
//                    {
//                        NSString *idsval=[[[workorder_list valueForKey:@"WorkOrderId"] objectAtIndex:0]description];
//                        ids=[NSString stringWithFormat:@"%@,%@",ids,idsval];
//                    }
//                    else{
//                        ids=[[[workorder_list valueForKey:@"WorkOrderId"] objectAtIndex:0]description];
//                    }
//
//
//                }
//                NSDictionary *json = @{
//                                       @"List" :workorder_list,
//                                       @"Details" :timesheet_list,
//                                       @"WonoText": ids,
//                                       @"JobDate": _datefield.text,
//                                       @"JobDateText": _datefield.text,
//                                       @"Techname": [[NSUserDefaults standardUserDefaults] objectForKey:username]
//
//                                       };
//
//                DownloadManager *downloadObj = [[DownloadManager alloc]init];
//                //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
//
//                [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetForApp"];
     }else
    if (0 == buttonIndex && alertView.tag == 55)
    {
        [self updateDatacompleted];
    }
  else  if (1 == buttonIndex && alertView.tag == 56)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        
        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@DeleteTimeSheet?TimeSheetId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[[timesheet_list valueForKey:@"TimeSheetId"] objectAtIndex:deletetag]description]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"DeleteTimeSheet"];
    }
}
- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}




-(void)successmessage{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    //    NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
    //
    //    for (int i=0; i<img_count;i++ )
    //    {
    //
    //        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:[NSString stringWithFormat:@"image%d", img_count]];
    //
    //    }
    //    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"img_count"];
    //
    NSError *error1;
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:ttbresponse
                                                              options:kNilOptions
                                                                error:&error1];
    
    if ([[response1 valueForKeyPath:@"Response"] isEqualToString:@"Success"])
    {
        [self SubmitTimeSheetForApp];
    }else{
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:[[response1 valueForKeyPath:@"Response"] description] message:[[response1 valueForKeyPath:@"Message"] description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
   // callAlert.tag=1001;
    [callAlert show];
 
    }
 
    
    
    // [self performSelector:@selector(popViewController) withObject:nil afterDelay:1.5];
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    
    if ([key isEqualToString:@"SubmitTimeSheetWoForApp"])
    {
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        //Open alert here
        
        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
        {
            
            
//            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            callAlert.tag = 55;
//            [callAlert show];
 [self updateDatacompleted];
        }
        else{
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //  callAlert.tag = 55;
            [callAlert show];
        }
    }
    
    else if ([key isEqualToString:@"DeleteTimeSheet"])
    {
        _popupview.hidden=true;
        [picker removeFromSuperview];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        //Open alert here
        
        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
        {
            
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            callAlert.tag = 55;
            [callAlert show];
            
        }   else{
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //  callAlert.tag = 55;
            [callAlert show];
        }
    }
    else if([key isEqualToString:@"SubmitTimeSheetForApp"]){
        NSLog(@"SubmitTimeSheetForApp%@", response);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
        {
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
           
            [callAlert show];
            [self updateDatacompleted];

            
            
        }
        else{
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //  callAlert.tag = 55;
            [callAlert show];
        }
    }
    
    else if([key isEqualToString:@"CheckSubmitTimeSheetForApp"]){
        
     //   if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
       // {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        NSLog(@"CheckSubmitTimeSheetForAppLog%@", response);
        if([[response valueForKeyPath:@"Data.Response"]boolValue])
        {
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            
            [callAlert show];
            
        }
        
        else
            
        {
            [self show_signatureview];
        }
        
        
    }
    else if ([key isEqualToString:@"GetTimeSheetPdfUrl"])
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        NSLog(@"responseLog%@", response);
    }
   else if ([key isEqualToString:@"SubmitTimeSheetDetailsWoForApp"])
    {
        _popupview.hidden=true;
        [picker removeFromSuperview];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        //Open alert here
        
        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
        {
            
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            callAlert.tag = 55;
            [callAlert show];
            
        }   else{
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //  callAlert.tag = 55;
            [callAlert show];
        }
    }
    //SubmitTimeSheetDetailsWoForApp
    else
    if ([key isEqualToString:@"IOSTimeSheetWorkOrderList"])
    {
        
        //[[UIApplication sharedApplication] endIgnoringInteractionEvents];
        loadingmsg=@"No Data Found!";
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        //Open alert here
        
        workorder_list=[response valueForKeyPath:@"Data.List"] ;
     timesheet_list=[response valueForKeyPath:@"Data.DetailList"] ;
        NSLog(@"response%@",response);
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[response valueForKeyPath:@"Data.TimePeriod"]];
       Startdate=[response valueForKeyPath:@"Data.StartDate"];
       enddate=[response valueForKeyPath:@"Data.EndDate"];

        [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0,11)];
        [string addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.0] range:NSMakeRange(0,11)];
      _timefield.attributedText = string;
        
      
        if([workorder_list count]>0)
        {
            
            if([[[workorder_list valueForKey:@"IsTimeSheetnew"] objectAtIndex:0]boolValue])
            {
                isSignture=false;
            }else{
                isSignture=true;

            }
            if([[[workorder_list valueForKey:@"IsPdfShow"] objectAtIndex:0]boolValue])
            {
                _pdf.hidden=false;
                _pdfimg.hidden=false;
                
                
            }
            else{
                _pdf.hidden=true;
                _pdfimg.hidden=true;
            }
            
            if([[[workorder_list valueForKey:@"IsTimeSheet"] objectAtIndex:0]boolValue])
            {
                _submit.hidden=true;
                 _add_btn.hidden=true;
                
            }
            else{
                _submit.hidden=false;
                _add_btn.hidden=false;

            }
        }
       else if([timesheet_list count]>0)
       {
           
           if([[[timesheet_list valueForKey:@"IsTimeSheetnew"] objectAtIndex:0]boolValue])
           {
               isSignture=false;
           }else{
               isSignture=true;
               
           }
           if([[[timesheet_list valueForKey:@"IsPdfShow"] objectAtIndex:0]boolValue])
           {
               _pdf.hidden=false;
               
               _pdfimg.hidden=false;

           }
           else{
               _pdf.hidden=true;
                _pdfimg.hidden=true;
           }
           
           if([[[timesheet_list valueForKey:@"IsTimeSheet"] objectAtIndex:0]boolValue])
           {
             //  _submit.hidden=true;
               _add_btn.hidden=true;
               
           }
           else{
              // _submit.hidden=false;
               _add_btn.hidden=false;
               
           }
           
       }
       else{
        //_submit.hidden=true;
          // _submit.hidden=false;
           _add_btn.hidden=false;
           
           _pdf.hidden=true;
           _pdfimg.hidden=true;
       }
        if([workorder_list count]==0&&[timesheet_list count]==0)
        {
            _submit.hidden=true;
        }else{
            if([timesheet_list count]>0)
            {
            if([[[timesheet_list valueForKey:@"IsTimeSheet"] objectAtIndex:0]boolValue])
            {
                _submit.hidden=true;
            }else
            {
                
                _submit.hidden=false;
            }
            }
           else if([workorder_list count]>0)
            {
                if([[[workorder_list valueForKey:@"IsTimeSheet"] objectAtIndex:0]boolValue])
                {
                    _submit.hidden=true;
                }else
                {
                    
                    _submit.hidden=false;
                }
            }
        }
        [_workorder_table reloadData];
        [_timesheettable reloadData];
        
    }
}
 
- (IBAction)dateaction:(id)sender {
    [self showDatePicker:UIDatePickerModeDate currentLbl:self->jobdate];

}
- (IBAction)close:(id)sender {
//    self->jobdate.text=@"";
//    date=@"";
//    Wono=@"";
_datefield.text=@"";
    self.closebtn.hidden=true;
 [self deallocwebview];
    //alertList = (NSMutableArray*)tempArr;
    // [_alert_table reloadData];
}
- (IBAction)absence_action:(id)sender {
     [self createPicker];
}
- (IBAction)timesheetdate_action:(id)sender {
        [picker removeFromSuperview];
    [self showDatePickertextfield:UIDatePickerModeDate currentLbl:_timesheet_date];


}
- (IBAction)timesheetcancel_action:(id)sender {
    _popupview.hidden=true;
        [picker removeFromSuperview];
}

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Server may be busy. Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    //callAlert.tag = 56;
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
   
    if ([[segue identifier] isEqualToString:@"timesheet_editsegue"])
    {
        
         timsheet_edit *vc = [segue destinationViewController];
        vc.edittimesheet_IDstr=edittimesheet_IDstr;
        vc.edittimesheet_date=edittimesheet_date;
        vc.edittimesheet_hours=edittimesheet_hours;
        vc.edittimesheet_notes=edittimesheet_notes;
        vc.startdate=Startdate;
        vc.enddate=enddate;
    }
  
}


- (IBAction)timesheetsave_action:(id)sender {
    
 
 
       
         float myNumber = [ _timesheet_hours.text  floatValue];
     
        if([_timesheet_date.text length]==0){
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please Enter Date" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
            
        }
        else
        if([_timesheet_absence.text length]==0){
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please Enter Absence" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
            
        }
        else
        if([_timesheet_hours.text length]==0){
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please Enter hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
            
        }
        
        //        else if ([hrsfield.text rangeOfCharacterFromSet:notDigits].location == NSNotFound)
        //
        //        {
        else   if (myNumber !=0.0) {
            
            
            if([_timesheet_hours.text  containsString:@" "])
            {
                
                
                UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please enter valid hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [callAlert show];
            }else{
            if(myNumber < 24.0){
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                
          NSDictionary *json = @{
                    @"TimeSheetId" :edittimesheet_IDstr,
                    @"JobDate" : _timesheet_date.text,
                    @"TechName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
                    @"Type" : _timesheet_absence.text,
                    @"Notes" : _timesheet_notes.text,
                    @"HrsSpent" :_timesheet_hours.text
                    };
              
                DownloadManager *downloadObj = [[DownloadManager alloc]init];
                //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                
                [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetDetailsWoForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetDetailsWoForApp"];
                
        
            }
            else
            {
                
                
                UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please enter valid hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [callAlert show];
            }
        }
   
        }
 
        else
        {
            
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please enter valid hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
        }
        
    
 
}


- (IBAction)pdf_action:(id)sender {
    
    //GetTimeSheetPdfUrl
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc]init];
    NSString *str=
    
     [NSString stringWithFormat:@"%@GetTimeSheetPdfUrl?AuthorizationId=%@&UserName=%@&jobDate=%@&IsPrint=true",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:AuthorizationId],[[NSUserDefaults standardUserDefaults] objectForKey:username],  _datefield.text];
    NSLog(@"url%@",[NSString stringWithFormat:@"%@GetTimeSheetPdfUrl?AuthorizationId=%@&UserName=%@&jobDate=%@&IsPrint=true",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:AuthorizationId],[[NSUserDefaults standardUserDefaults] objectForKey:username],  _datefield.text]);
    [request setURL:[NSURL URLWithString:str]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
 pdfurlstring = [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding] ;
   pdfurlstring= [pdfurlstring stringByReplacingOccurrencesOfString:@"\""
                                   withString:@""];
   // pdfurlstring=[[NSString alloc] initWithData:pdfurlstring encoding:NSUTF8StringEncoding];
    NSLog(@"urlData%@",pdfurlstring);
    showpdf_timesheet=true;
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    [self showpdfview];
//    DownloadManager *downloadObj = [[DownloadManager alloc]init];
//    //   [
//    [UIApplication sharedApplication] beginIgnoringInteractionEvents];
//    
//    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetTimeSheetPdfUrl?AuthorizationId=%@&UserName=%@&jobDate=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:AuthorizationId],[[NSUserDefaults standardUserDefaults] objectForKey:username],  _datefield.text] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetTimeSheetPdfUrl"];
}




-(void)showpdfview
{
     [self.view endEditing:YES];
[self disableallbutton];
    [Timesheet_webview_completed removeFromSuperview];
    
    [Timesheet_webview_completedbutton removeFromSuperview];
    Timesheet_webview_completed=[[UIWebView alloc]initWithFrame:CGRectMake( 0, 60, self.view.frame.size.width-30, self.view.frame.size.height-100)];
    Timesheet_webview_completed.center = CGPointMake( self.view.frame.size.width/2, self.view.frame.size.height/2+70);
    
    Timesheet_webview_completed.scalesPageToFit=YES;
    NSLog(@"urlData%@",pdfurlstring);
    Timesheet_targetURL = [NSURL URLWithString:pdfurlstring];

    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:Timesheet_targetURL];
    //  Timesheet_webview_completed.opaque = NO;
    // Timesheet_webview_completed.delegate=self;
    //    Timesheet_webview_completed.backgroundColor = [UIColor clearColor];
    [Timesheet_webview_completed loadRequest:request];
        UIPinchGestureRecognizer *pgr = [[UIPinchGestureRecognizer alloc]
                                         initWithTarget:self action:@selector(handlePinchGesture:)];
        pgr.delegate = self;
        [ Timesheet_webview_completed addGestureRecognizer:pgr];
    [self.view addSubview:Timesheet_webview_completed];
    Timesheet_webview_completedbutton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [Timesheet_webview_completedbutton setFrame: CGRectMake(Timesheet_webview_completed.frame.origin.x+15,Timesheet_webview_completed.frame.origin.y+5,50, 30)];
    [Timesheet_webview_completedbutton setTitle:@"Close" forState:UIControlStateNormal];
    Timesheet_webview_completedbutton.userInteractionEnabled = YES;
    Timesheet_webview_completedbutton.titleLabel.lineBreakMode   = UILineBreakModeTailTruncation;
    Timesheet_webview_completedbutton.titleLabel.textColor =[UIColor blueColor];
    Timesheet_webview_completedbutton.showsTouchWhenHighlighted = YES;
    Timesheet_webview_completedbutton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin|
    UIViewAutoresizingFlexibleTopMargin|
    UIViewAutoresizingFlexibleHeight|
    UIViewAutoresizingFlexibleBottomMargin;
    [Timesheet_webview_completed setDelegate:self];
    
    [Timesheet_webview_completedbutton addTarget:self action:@selector(closepdf1:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:Timesheet_webview_completedbutton];
    activity_View=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-15, self.view.frame.size.height/2, 30, 30)];
    [activity_View setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    
    activity_View.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height/2.0);
    [activity_View startAnimating];
    activity_View.tag = 100;
    [self.view addSubview:activity_View];
    
}
- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    
    
    Timesheet_webview_completed.scrollView.delegate = self; // set delegate method of UISrollView
    Timesheet_webview_completed.scrollView.maximumZoomScale = 20; // set as you want.
    Timesheet_webview_completed.scrollView.minimumZoomScale = 1; // set as you want.
    
    //// Below two line is for iOS 6, If your app only supported iOS 7 then no need to write this.
    Timesheet_webview_completed.scrollView.zoomScale = 2;
    Timesheet_webview_completed.scrollView.zoomScale = 1;
    [activity_View stopAnimating];
    [activity_View removeFromSuperview];
    
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    Timesheet_webview_completed.scrollView.maximumZoomScale = 20; // set similar to previous.
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)gestureRecognizer {
    
    if([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
        // Reset the last scale, necessary if there are multiple objects with different scales.
        lastScale = [gestureRecognizer scale];
    }
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan ||
        [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        
        CGFloat currentScale = [[[gestureRecognizer view].layer valueForKeyPath:@"transform.scale"] floatValue];
        
        // Constants to adjust the max/min values of zoom.
        const CGFloat kMaxScale = 2.0;
        const CGFloat kMinScale = 1.0;
        
        CGFloat newScale = 1 -  (lastScale - [gestureRecognizer scale]); // new scale is in the range (0-1)
        newScale = MIN(newScale, kMaxScale / currentScale);
        newScale = MAX(newScale, kMinScale / currentScale);
        CGAffineTransform transform = CGAffineTransformScale([[gestureRecognizer view] transform], newScale, newScale);
        [gestureRecognizer view].transform = transform;
        
        lastScale = [gestureRecognizer scale];  // Store the previous. scale factor for the next pinch gesture call
    }
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    if(showpdf_timesheet)
    {
        
        [self showpdfview];
    }
    
    if(opensignatureview)
    {
        [self show_signatureview];
        
    }
}
- (IBAction)closepdf1:(id)sender {
    showpdf_timesheet=false;
    [self enableallbutton];
    [activity_View stopAnimating];
    [activity_View removeFromSuperview];
    [Timesheet_webview_completed removeFromSuperview];
    [Timesheet_webview_completedbutton removeFromSuperview];
    
}
- (IBAction)add_action:(id)sender {
    
//   self.timesheet_date.text=@"";
//   self.timesheet_absence.text=@"";
//    self.timesheet_hours.text=@"";
//    self.timesheet_notes.text=@"";
//    edittimesheet_IDstr=@"0";
//    _popupview.hidden=false;
//       [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_popupview];
    
    
    edittimesheet_IDstr=@"0";
    //     if(_popupview.hidden)
    //     {      _popupview.hidden=false;
    //     }else{
    //           _popupview.hidden=true;
    //     }
    //    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_popupview];
    
    edittimesheet_absence=@"";
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"absencetxt"];

    edittimesheet_date=@"";
    edittimesheet_notes=@"";
    edittimesheet_hours=@"";
    //timesheet_editsegue
    [self performSegueWithIdentifier:@"timesheet_editsegue" sender:self];


}
- (IBAction)submit_action:(id)sender {
    
    if([workorder_list count]>0){
      //  CheckSubmitTimeSheetForApp
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSDictionary *json = @{
                               @"TimeSheet" :workorder_list,
                       @"Details" :timesheet_list
                               };
        
        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        NSLog(@"json%@",json);
        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@CheckSubmitTimeSheetForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"CheckSubmitTimeSheetForApp"];
    }
    else if([timesheet_list count]>0)
        
    {
        [self show_signatureview];
    }
}

-(void)show_signatureview
{
    
     [self disableallbutton];
    
    opensignatureview=true;
    [mainview removeFromSuperview];
    mainview=[[UIView alloc]initWithFrame: CGRectMake(20,  self.view.frame.size.height/2-200, self.view.frame.size.width-40, 500)];
    signView= [[ signature alloc] initWithFrame: CGRectMake(0, 35, self.view.frame.size.width-40, 450)];
    [signView setBackgroundColor:[UIColor whiteColor]];
      [mainview setBackgroundColor:[UIColor whiteColor]];
    mainview.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    mainview.layer.borderWidth = 1.0;
    [mainview addSubview:signView];
    [self.view addSubview:mainview];
    CGRect frame = CGRectMake(5, 5, 60, 30);
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    button.frame = frame;
    
    [button setTitle:(NSString *)@"Cancel" forState:(UIControlState)UIControlStateNormal];
    
    [button addTarget:self action:@selector(signcancel:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect frame1 = CGRectMake(mainview.frame.size.width/2-20, 5, 60, 30);
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    button1.frame = frame1;
    
    [button1 setTitle:(NSString *)@"Clear" forState:(UIControlState)UIControlStateNormal];
    
    [button1 addTarget:self action:@selector(signclear:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect frame2 = CGRectMake(mainview.frame.size.width-50, 5, 50, 30);
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    button2.frame = frame2;
    
    [button2 setTitle:(NSString *)@"Save" forState:(UIControlState)UIControlStateNormal];
    
    [button2 addTarget:self action:@selector(signsave:) forControlEvents:UIControlEventTouchUpInside];
    button.titleLabel.font = [UIFont systemFontOfSize:17.0f];
    button1.titleLabel.font = [UIFont systemFontOfSize:17.0f];
    button2.titleLabel.font = [UIFont systemFontOfSize:17.0f];

    [mainview addSubview:button];
    [mainview addSubview:button1];
    [mainview addSubview:button2];
    
}
 -(void)signsave:(id)sender {
      [self.view endEditing:YES];
     UIGraphicsBeginImageContext(signView.bounds.size);
     [[signView.layer presentationLayer] renderInContext:UIGraphicsGetCurrentContext()];
  signatureimg = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     
                 NSString *boundary = [self generateBoundaryString];
     
                 // configure the request
     
                 NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@UpdateTechnicianSign",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]]]];
                 [request setHTTPMethod:@"POST"];
     
                 // set content type
     
                 NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
                 [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
     
                 // create body
                 NSMutableData *httpBody = [NSMutableData data];
     
                 // add params (all params are strings)
                 NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
                 [parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:username] forKey:@"UserName"];
     
                 [parameters setValue:[NSString stringWithFormat:@"%d",1]forKey:@"EntryCount" ];
     [parameters setValue:enddate forKey:@"WeekEndDay" ];
     NSLog(@"parameters%@",parameters);
                 [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
                     [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                     [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
                     [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
                 }];
     
     
                 NSLog(@"request = %@", [NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@%@\"\r\n", @"FilePath1", @"image1",@".png"]  );
     
     
     
                 [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                 [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@%@\"\r\n", @"FilePath1", @"image1",@".png"] dataUsingEncoding:NSUTF8StringEncoding]];
     
                 [httpBody appendData:[@"Content-Type:image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                 [httpBody appendData: UIImageJPEGRepresentation(signatureimg, 0.5)];
                 //[httpBody appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                 // [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
                 //  [httpBody appendData:data];
                 [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
     
     
                 [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                 NSURLSession *session = [NSURLSession sharedSession];  // use sharedSession or create your own
     
                 NSURLSessionTask *task = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                     if (error) {
                         NSLog(@"error = %@", error);
                         return;
                     }
     
                     NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                     NSLog(@"result = %@", result);
                     ttbresponse=data;
     
                     dispatch_async(dispatch_get_main_queue(), ^{
     
                         [self successmessage];
                     });
                     [mainview removeFromSuperview];
                     opensignatureview=false;
                     [self enableallbutton];
                 }];
                 [task resume];
    
     
     //        [self dismissViewControllerAnimated:YES completion:nil];
     
     
     
  //   NSData *postData = UIImageJPEGRepresentation(viewImage, 1.0);
     }
-(void) enableallbutton
{
   
    _submit.enabled=YES;
    _calender_icon.enabled=YES;
    
    _add_btn.enabled=YES;
    [_add_btn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    [_calender_icon setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    
    [_submit setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
}
-(void) disableallbutton
{
    _submit.enabled=NO;
    _calender_icon.enabled=NO;
    _add_btn.enabled=NO;
     [_add_btn setTitleColor:[UIColor  colorWithRed:118/255.0f green:118/255.0f blue:118/255.0f alpha:1.0] forState:UIControlStateNormal];
    [_calender_icon setTitleColor:[UIColor  colorWithRed:118/255.0f green:118/255.0f blue:118/255.0f alpha:1.0] forState:UIControlStateNormal];
    
    [_submit setTitleColor:[UIColor  colorWithRed:118/255.0f green:118/255.0f blue:118/255.0f alpha:1.0] forState:UIControlStateNormal];
    
}
 -(void)signclear:(id)sender {
  
       [signView erase];
    }
-(void)signcancel:(id)sender {
    
    [self enableallbutton];
    
         [mainview removeFromSuperview];
    opensignatureview=false;
    }
    
    
    
    
    
//    {
//      //  if(isSignture)
//      //  {
//    MPBSignatureViewControllerConfiguration *configuration = [[MPBSignatureViewControllerConfiguration alloc] initWithFormattedAmount:@""];
//
//    static MPBSignatureViewControllerConfigurationScheme scheme = MPBSignatureViewControllerConfigurationSchemeGhLink;
//    configuration.scheme = scheme;
//
//    MPBSignatureViewController* signatureViewController = [[MPBSignatureViewController alloc] initWithConfiguration:configuration];
//
//    signatureViewController.modalPresentationStyle = UIModalPresentationPopover;
//
//    signatureViewController.continueBlock = ^(UIImage *signature) {
//     //   [self showImage: signature];
//
//    signatureimg= signature;
//
//            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//
//            NSString *boundary = [self generateBoundaryString];
//
//            // configure the request
//
//            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@UpdateTechnicianSign",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]]]];
//            [request setHTTPMethod:@"POST"];
//
//            // set content type
//
//            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
//            [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
//
//            // create body
//            NSMutableData *httpBody = [NSMutableData data];
//
//            // add params (all params are strings)
//            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
//            [parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:username] forKey:@"UserName"];
//
//            [parameters setValue:[NSString stringWithFormat:@"%d",1]forKey:@"EntryCount" ];
//
//            [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
//                [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//                [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
//                [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
//            }];
//
//
//            NSLog(@"request = %@", [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@%@\"\r\n", @"FilePath1", @"image1",@".png"] dataUsingEncoding:NSUTF8StringEncoding]);
//
//
//
//            [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//            [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@%@\"\r\n", @"FilePath1", @"image1",@".png"] dataUsingEncoding:NSUTF8StringEncoding]];
//
//            [httpBody appendData:[@"Content-Type:image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//            [httpBody appendData: UIImageJPEGRepresentation(signatureimg, 0.5)];
//            //[httpBody appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//            // [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
//            //  [httpBody appendData:data];
//            [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//
//
//            [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//            NSURLSession *session = [NSURLSession sharedSession];  // use sharedSession or create your own
//
//            NSURLSessionTask *task = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                if (error) {
//                    NSLog(@"error = %@", error);
//                    return;
//                }
//
//                NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//                NSLog(@"result = %@", result);
//                ttbresponse=data;
//
//                dispatch_async(dispatch_get_main_queue(), ^{
//
//                    [self successmessage];
//                });
//            }];
//            [task resume];
//
//
//        [self dismissViewControllerAnimated:YES completion:nil];
//
//
//
//    };
//
//
//    signatureViewController.cancelBlock = ^ {
//        [self dismissViewControllerAnimated:YES completion:nil];
//    };
//
//    [self presentViewController:signatureViewController animated:YES completion:nil];
////    }
////        else{
////            [self SubmitTimeSheetForApp];
////
////        }
//    }
    
    
//}
@end
