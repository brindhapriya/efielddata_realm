//
//  multiselectViewController.m
//  Efield
//
//  Created by MyMac1 on 2/7/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "AbsenceVC.h"
#import "DynamicNoteFormVC.h"
#import "multiselectTableViewCell.h"

@interface AbsenceVC ()
{
    NSString *
    selectedvalue,*selectedid,*Specialist,*locationname
    ;
}


@end

@implementation AbsenceVC{
    NSString *previous;
    NSMutableArray *arSelectedRows;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _companynametxt.text=[[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];
    
    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
    [currentDTFormatter1 setDateFormat:@"dd/MM/YYYY"];
    
    self. navigationController.navigationBar.hidden = YES;
    
    NSString *eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@", eventDateStr);
    self.date.text=eventDateStr;
    self.taskname_lbl.text= _ProjectName   ;
  //  self.jobnumber.text=[NSString stringWithFormat:@"%@ - %@", _JobNumber,_TaskName];
     selectedvalue=@"";
     selectedid=@"";
    arSelectedRows = [[NSMutableArray alloc] init];
 
    previous=[[NSUserDefaults standardUserDefaults] objectForKey:@"absencetxt"];
    int j;
    if([previous isEqualToString:@"Administration"])
    { j=0;}
    else    if ([previous isEqualToString:@"Holiday"])
    {j=1;}
    else   if([previous isEqualToString:@"Sick"])
    {j=2;}
    else  if([previous isEqualToString:@"Training"])
    {j=3;}
    else if([previous isEqualToString:@"Vacation"])
    {j=4;}
    else{
        j=10;
    }
    if(j==10){}else{
    NSIndexPath* selectedCellIndexPath= [NSIndexPath indexPathForRow:j inSection:0];
    [arSelectedRows addObject:selectedCellIndexPath];
    
    [self tableView:self.tableView didSelectRowAtIndexPath:selectedCellIndexPath];
    [self.tableView selectRowAtIndexPath:selectedCellIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        //[self.savebtn setEnabled:NO];
 
    }
    
}



- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];

    
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
    self.alertcnt.text=[self updateALERTcount];
  
}



 

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Server may be busy. Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
   // callAlert.tag = 56;
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

{
    if (0 == buttonIndex && alertView.tag == 55)
    {
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//
//        [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];
// });
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"multislectcell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"multislectcell"] ;
    }
   
//    if([testtypelist count]==0)
//    {
//        cell.textLabel.text =@"No Record Found" ;
//        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
//
//    }else{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *absencetxt=  [[NSUserDefaults standardUserDefaults] objectForKey:@"absencetxt"];
    
    
    
    
        
    if(indexPath.row==0)
    {cell.textLabel.text=@"Administration";}
    else    if(indexPath.row==1)
    {cell.textLabel.text=@"Holiday";}
    else    if(indexPath.row==2)
    {cell.textLabel.text=@"Sick";}
    else    if(indexPath.row==3)
    {cell.textLabel.text=@"Training";}
    else
    {cell.textLabel.text=@"Vacation";}
    cell.textLabel.font = [UIFont systemFontOfSize:14.0];
   
    
    if([cell.textLabel.text isEqualToString:absencetxt])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [arSelectedRows addObject:indexPath];
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines=0;
        [  cell.textLabel sizeToFit];

   
    
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    
    self.savebtn.enabled = YES;
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    // if([_selected_index_multi isEqualToString:@"0"]||[_selected_index_multi isEqualToString:@"1"]){
 //   if([_selected_index_multi isEqualToString:@"0"]||[_selected_index_multi isEqualToString:@"1"])
 //   {
        if(arSelectedRows.count) {
            
            //  for (int i=0; i< [arSelectedRows count]; i++) {
            
            
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:arSelectedRows[0]];
            cell.accessoryType = UITableViewCellAccessoryNone;
            NSIndexPath *indexpath=arSelectedRows[0];
            [arSelectedRows removeObject:arSelectedRows[0]];
            // [arSelectedRows removeAllObjects];
            cell.accessoryType = UITableViewCellAccessoryNone;

            // }
        }
    //}
//    if([_selected_index_multi isEqualToString:@"0"]||[_selected_index_multi isEqualToString:@"1"])
//    {
//        [_tableView deselectRowAtIndexPath:indexPath animated:YES];
//    }
    if(cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        //  if(arSelectedRows.count) {
        
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        [arSelectedRows removeObject:indexPath];
      
        
        
    }else   {
        [arSelectedRows addObject:indexPath];
        NSString *absencetxt;
        if(indexPath.row==0)
        {absencetxt=@"Administration";}
        else    if(indexPath.row==1)
        {absencetxt=@"Holiday";}
        else    if(indexPath.row==2)
        {absencetxt=@"Sick";}
        else    if(indexPath.row==3)
        {absencetxt=@"Training";}
        else
        {absencetxt=@"Vacation";}
        [[NSUserDefaults standardUserDefaults]setObject:absencetxt forKey:@"absencetxt"];
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
       
 
    }
    NSArray *selectedRows = [tableView indexPathsForSelectedRows];
    for(NSIndexPath *i in selectedRows)
    {
        if(![i isEqual:indexPath])
        {
            [tableView deselectRowAtIndexPath:i animated:NO];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 35;
    
    
    
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//    if([_selected_index_multi isEqualToString:@"0"]||[_selected_index_multi isEqualToString:@"1"])
  //  {
        if([arSelectedRows count]>0)
        {
        NSInteger    theRow       = arSelectedRows[0];
        NSIndexPath *theIndexPath = [NSIndexPath indexPathForRow:theRow inSection:0];
        [self.tableView selectRowAtIndexPath:theIndexPath
                                    animated:NO
                              scrollPosition:UITableViewScrollPositionNone];
        }
   // }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
//    self.savebtn.enabled = YES;
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if(cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        
        
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        // [arSelectedRows removeAllObjects];
 [arSelectedRows removeObject:indexPath];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"absencetxt"];

    
        
    }
}

- (IBAction)cancel:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    [[NSUserDefaults standardUserDefaults]setObject:previous forKey:@"absencetxt"];

    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    location_dischargeViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"loc_discharge"];
    //
    //    viewController.patientId = self.patientId;
    //    viewController.providesPresentationContextTransitionStyle = YES;
    //    viewController.definesPresentationContext = YES;
    //
    //    [viewController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    //    [self presentViewController:viewController animated:YES completion:nil];
    
}

- (IBAction)save:(id)sender {
    
    
    [self.navigationController popViewControllerAnimated:YES];

    
    
 
    
}

- (IBAction)logoutAction:(id)sender {
    [self logoutUser];
    
}

- (IBAction)changepasswordAction:(id)sender {
    [self changePassword];
}

@end
