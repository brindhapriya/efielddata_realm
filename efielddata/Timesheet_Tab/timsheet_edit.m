//
//  multiselectViewController.m
//  Efield
//
//  Created by MyMac1 on 2/7/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "timsheet_edit.h" 
#import "multiselectTableViewCell.h"
#import "AbsenceVC.h"

@interface timsheet_edit ()
{
    NSString *
    re_schedule,*hours,*notes,*datestr;
    ;
    int timesheetedit_timespent_hours;   // integer division to get the hours part
    int timesheetedit_timespent_minutes;
    NSString  *timesheetedit_timespent_str,
    *timesheetedittimespent_save;
    UIAlertController * timeshtthours ;

}


@end

@implementation timsheet_edit{
    
    NSMutableArray *arSelectedRows;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _savebtn.enabled=NO;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
   // [self.view addGestureRecognizer:tap];
    datestr=@"";
    notes=@"";
    re_schedule=@"false";
    hours=@"";
 //   _companynametxt.text=[[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];
    
    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
    [currentDTFormatter1 setDateFormat:@"dd/MM/YYYY"];
    
    self.navigationController.navigationBar.hidden = YES;
    
    NSString *eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@", eventDateStr);
    self.date.text=eventDateStr;
   // self.taskname_lbl.text= @"Timesheet"   ;
//        self.jobnumber.text=[NSString stringWithFormat:@"%@ - %@", _JobNumber,_TaskName];
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];
    [_tableView setAllowsSelection:YES];
    datestr= _edittimesheet_date;
    notes=_edittimesheet_notes;
    hours=_edittimesheet_hours;
    timesheetedit_timespent_str =
    hours;
    if([hours isEqualToString:@""])
    {
    timesheetedittimespent_save =@"-1";
    }else
    {
        timesheetedittimespent_save =
        hours;
    }
    NSLog(@"hours%@",hours);
    if([timesheetedit_timespent_str length]==0)
    {
        timesheetedit_timespent_str =
        [timesheetedit_timespent_str stringByReplacingOccurrencesOfString:@""
                                                               withString:@"0 hrs 30 mins"];
        
    }else{
    if ([timesheetedit_timespent_str rangeOfString:@"."].location == NSNotFound) {
        
        timesheetedit_timespent_str =
        [NSString stringWithFormat:@"%@ hrs 00 mins", timesheetedit_timespent_str];
    }
    timesheetedit_timespent_str =
    [timesheetedit_timespent_str stringByReplacingOccurrencesOfString:@".5"
                                                      withString:@" hrs 30 mins"];
    
    timesheetedit_timespent_str =
    [timesheetedit_timespent_str stringByReplacingOccurrencesOfString:@".25"
                                                      withString:@" hrs 15 mins"];
    timesheetedit_timespent_str =
    [timesheetedit_timespent_str stringByReplacingOccurrencesOfString:@".75"
                                                      withString:@" hrs 45 mins"];
    
    }
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    //   [aTextField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
        [_tableView reloadData];

    
}

 

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Server may be busy. Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    callAlert.tag = 56;
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

{
    if (0 == buttonIndex && alertView.tag == 55)
    {
        
        [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];

    }
}
- (void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil]
    ;
    
 }


-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"jsonString:\n%@", response);
    if ([key isEqualToString:@"SubmitTimeSheetDetailsWoForApp"])
    {
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        //Open alert here
   
        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
        {
//            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            callAlert.tag = 55;
//            [callAlert show];
           // [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];

            [self.navigationController popViewControllerAnimated:YES];
            [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil]
            ;
        }
        else{
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //  callAlert.tag = 55;
            [callAlert show];
        }
    }
    
    
    else if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efield Message" message:@"Please try again later"];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}


-(NSMutableAttributedString *)AddRequiredField:(NSString *)title andIsRequired:(BOOL)required {
    
    if(required)
    {
        NSString *string = [NSString stringWithFormat:@"%@ *", title];
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
        NSRange range = [string rangeOfString:@"*"];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        return attString;
    }
    else
    {
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:title];
        return attString;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.row==0)
//    {
//        DescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
//
//
//            cell.lblTitle.hidden=NO;
//        cell.lblTitle.text =@"Please enter admin hours including sick, vacation, holiday or professional training.";
//       // cell.lblTitle.font
//
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
//    else
    if(indexPath.row==1)
    { MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
        
        if (cell == nil) {
            cell = [[MultySelectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MultySelectionCell"];
        }
        cell.lblTitle.attributedText = [self AddRequiredField:@"Absence Type" andIsRequired:true];
        
      
            cell.lblValue.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"absencetxt"];
       
        [cell.lblValue sizeToFit];
        cell.lblValue.numberOfLines = 0;
        [  cell.lblValue setNeedsDisplay];
        //
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else  if(indexPath.row==2)
        
    {
        
        DateAndTimeTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"
                                        forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[DateAndTimeTableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:@"DateAndTimeCell"];
        }
        
        cell.lblValue.tag = indexPath.row;
        
                         cell.lblValue.text=timesheetedit_timespent_str;
        
        cell.lblTitle.attributedText = [self AddRequiredField:@"Time Spent (hours)" andIsRequired:true];
           cell.date_img.tag= indexPath.row;
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
        
     
        
        [tapRecognizer setNumberOfTapsRequired:1];
        
        [cell.date_img setUserInteractionEnabled:YES];
        [  cell.date_img addGestureRecognizer:tapRecognizer];
        // cell.txtValue.text = [NSString
        // stringWithFormat:@"%.f",[dataList[indexPath.row][@"TimeSpent"]
        // floatValue]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
//    {
//        TextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
//        // if(_isFromEditNote && !_IsEditNote)
//        //  cell.userInteractionEnabled = NO;
//        cell.lblTitle.attributedText =[self AddRequiredField:@"Time Spent (hours)" andIsRequired:true];
//        [cell.txtValue setKeyboardType:UIKeyboardTypeDecimalPad];
//        cell.txtValue.enabled=NO;
//        if( [cell.txtValue.text isEqualToString:@""])
//        {
//             cell.txtValue.text=_edittimesheet_hours;
//        }else{
//
//        }
//  //      @"Enter Hours";
//  cell.txtValue.tag = indexPath.row;
//        cell.txtValue.delegate = self;
//
//        [cell.txtValue addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventTouchDown];
//
//            cell.txtValue.placeholder = @"Click here to enter hours...";
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
  
    else if(indexPath.row==0)
    {
        DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
         cell.lblValue.tag = indexPath.row;
       
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
        
        cell.date_img.tag= indexPath.row;

        [tapRecognizer setNumberOfTapsRequired:1];
        [cell.date_img setUserInteractionEnabled:YES];
        [  cell.date_img addGestureRecognizer:tapRecognizer];
        cell.lblTitle.attributedText =  [self AddRequiredField:@"Date" andIsRequired:true] ;
        if( [cell.lblValue.text isEqualToString:@""])
        {
            cell.lblValue.text=_edittimesheet_date;

        }else{
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        TextViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextViewCell"];
        // if(_isFromEditNote && !_IsEditNote)
        //  cell.userInteractionEnabled = NO;
        cell.lblTitle.text =@"Notes";
        //[self AddRequiredField:@"Enter notes" andIsRequired:true]; 
        cell.txtViewValue.delegate = self;
        if( [cell.txtViewValue.text isEqualToString:@""])
        {
            
            cell.txtViewValue.text=_edittimesheet_notes;

        }else{

        }
        //      @"Enter Hours";
        
       // cell.txtViewValue.delegate = self;
       // cell.txtViewValue.tag = indexPath.row;
       cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
         
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
    
 if(indexPath.row==0)
 {
        DateAndTimeTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        [self showDatePicker:UIDatePickerModeDate currentLbl:cell.lblValue];
}
    else if(indexPath.row==1)
    {
          [self performSegueWithIdentifier:@"absencesegue" sender:self];
    }
    else if(indexPath.row==2)
    {
        DateAndTimeTableViewCell *cell =
        [tableView cellForRowAtIndexPath:indexPath];
        cell.lblValue.tag = indexPath.row;
        [self showTimePicker:UIDatePickerModeTime
                  currentLbl:cell.lblValue
               indexpathvalue:indexPath];

    }
}


-(void) showDatePicker: (UIDatePickerMode) modeDatePicker currentLbl:(UILabel *)lbl
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *startdte=[dateFormatter dateFromString:_startdate];
    NSDate *enddte=[dateFormatter dateFromString:_enddate];

    [picker setMinimumDate:startdte];
    [picker setMaximumDate:enddte];

    [alertController.view addSubview:picker];
    
    
    if([lbl.text isEqualToString:@""]){
    }else{
        NSDate *mydate;
        NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
        [formatter1 setDateFormat:@"MM/dd/yyyy"];
        mydate = [formatter1 dateFromString:lbl.text];
        [picker setDate:mydate animated:NO];
    }
  
    
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                     if(modeDatePicker == UIDatePickerModeDate) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                                         NSString *datestring= [formatter stringFromDate:picker.date];
                                         
                                         lbl.text=datestring;
                                           datestr=datestring;
                                         [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
                                         self.savebtn.enabled = YES;
                                        
                                     }
                                    
                                     
                                 }];
    [alertController addAction:doneAction];
    
    
    UIAlertAction *clearAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                      lbl.text=@"";
                                      NSLog(@"Clear action");
                                      datestr=@"";
                                      [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
                                      self.savebtn.enabled = YES;
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    [alertController addAction:cancelAction];
    
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController  animated:YES completion:nil]; }
- (void)switchValueChanges:(id)sender {
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        NSLog(@"its on!");
          re_schedule=@"true";
    } else {
        NSLog(@"its off!");
          re_schedule=@"false";
    }

}

- (IBAction)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
 if([textField.text isEqualToString:@"Type here ..."]){
        textField.text = @"";
        textField.textColor = [UIColor lightGrayColor];}
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
 
    
        [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
        self.savebtn.enabled = YES;
    
    hours=textField.text;

    // [_tblForm reloadData];
}


-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender {
    
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
    NSIndexPath *path = [NSIndexPath indexPathForRow:sender.view.tag inSection:0];


    DateAndTimeTableViewCell *cell = [_tableView cellForRowAtIndexPath:path];
     if(sender.view.tag == 0)
     {
    [self showDatePicker:UIDatePickerModeDate currentLbl:cell.lblValue];
     }else{
         [self showTimePicker:UIDatePickerModeTime
                   currentLbl:cell.lblValue
               indexpathvalue:path];
     }
    
    
}
-(void) textViewDidChange:(UITextView *)textView
{
    
    
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
 
    self.savebtn.enabled = YES;
  notes= textView.text;
    
}
- (NSDate *)roundToNearestQuarterHour:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |
    NSCalendarUnitDay | NSCalendarUnitHour |
    NSCalendarUnitMinute | NSCalendarUnitWeekday |
    NSCalendarUnitWeekdayOrdinal | NSCalendarUnitWeekOfYear;
    NSDateComponents *components = [calendar components:unitFlags fromDate:date];
    NSInteger roundedToQuarterHour = round((components.minute / 15.0)) * 15;
    components.minute = roundedToQuarterHour;
    return [calendar dateFromComponents:components];
}

- (void)showTimePicker:(UIDatePickerMode)modeDatePicker
            currentLbl:(UILabel *)lbl
  indexpathvalue:(NSIndexPath *)indexpathrow {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    NSDateFormatter *formatter24 = [[NSDateFormatter alloc] init];
    [formatter24 setDateFormat:@"HH:mm"];
    NSString *Maxdate= [formatter24 stringFromDate:[NSDate date]];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
    NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: [formatter24 dateFromString:Maxdate]];
    [components setHour: 20];
    [components setMinute: 0];
    [components setSecond: 0];
    NSDate *maxDate = [gregorian dateFromComponents: components];

    //[picker setMaximumDate:maxDate];
    [alertController.view addSubview:picker];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"NL"];
    [picker setLocale:locale];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    
    if ([timesheetedit_timespent_str isEqualToString:@""] ||[timesheetedit_timespent_str isEqualToString:@"0 hrs 00 mins"]||
        [timesheetedit_timespent_str isKindOfClass:[NSNull class]]) {
       // picker.date = [self roundToNearestQuarterHour:[NSDate date]];
        [picker setDate:[self roundToNearestQuarterHour:[NSDate date]]];
        
    } else {
        
        NSString *times = [timesheetedit_timespent_str stringByReplacingOccurrencesOfString:@" hrs"
                                                                            withString:@":"];
        times = [times stringByReplacingOccurrencesOfString:@" mins"
                                                 withString:@""];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"HH:mm";
        NSDate *date = [dateFormatter dateFromString:times];
        NSString *pmamDateString = [dateFormatter stringFromDate:date];
        NSData *date_hrs = [formatter dateFromString:pmamDateString];
        [picker setDate:date];
    }
    [picker setMaximumDate:maxDate];
    picker.minuteInterval = 15;
    UIAlertAction *doneAction = [UIAlertAction
                                 actionWithTitle:@"Done"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action) {
                                     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                     [formatter setDateFormat:@"HH:mm"];
                                     NSString *datestring = [formatter stringFromDate:picker.date];
                                     
                                     NSCalendar *calendar = [NSCalendar currentCalendar];
                                     NSDateComponents *components = [calendar
                                                                     components:(NSCalendarUnitHour | NSCalendarUnitMinute)
                                                                     fromDate:picker.date];
                                     
                                     timesheetedit_timespent_hours = [components hour];
                                     timesheetedit_timespent_minutes = [components minute];
                                     
                                     NSString *timeDiff;
                                     if (timesheetedit_timespent_minutes == 0) {
                                         timeDiff = [NSString
                                                     stringWithFormat:@"%d hrs %ld0 mins", timesheetedit_timespent_hours,
                                                     (long)timesheetedit_timespent_minutes];
                                         
                                     } else {
                                         timeDiff = [NSString
                                                     stringWithFormat:@"%d hrs %ld mins", timesheetedit_timespent_hours,
                                                     (long)timesheetedit_timespent_minutes];
                                     }
                                     timesheetedit_timespent_str = timeDiff;
                                     timesheetedittimespent_save = timeDiff;
                                 if (lbl.tag == indexpathrow.row) {
                                     lbl.text = timeDiff;
                            }
                                    
                                 }];
    [alertController addAction:doneAction];
    
    UIAlertAction *clearAction =
    [UIAlertAction actionWithTitle:@"Clear"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action) {
                               lbl.text = @"";
                               timesheetedit_timespent_str = @"";
                               timesheetedittimespent_save = @"-1";
                               
                               
                           }];
    
    UIAlertAction *cancelAction =
    [UIAlertAction actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction *action) {
                               NSLog(@"Cancel action");
                            
                               
                           }];
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController =
    alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect =
    CGRectMake(self.view.bounds.size.width / 2.0,
               self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController animated:YES completion:nil];
}


//
//-(void) textFieldDidChange:(UITextField *)textField
//{
//
//
//    if(textField.text.length == 0){
//        textField.textColor = [UIColor lightGrayColor];
//        textField.placeholder = @"Type here ...";
//        hours=@"";
//     }
//
//    else{
//
//        NSString *codeString = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//
//        NSString *firstLetter = [codeString substringFromIndex:0];
//        if([firstLetter isEqualToString:@"."])
//        {
//
//            hours= [NSString stringWithFormat:@"0%@",codeString];
//        }else{
//            hours=codeString;
//
//        }
//        textField.text =  hours;
//    }
//
//
//}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
    // if([self substring:@"null" existsInString:_admitingdiagnosis]) {
    if([textView.text isEqualToString:@"Type here ..."]){
        textView.text = @"";
        textView.textColor = [UIColor lightGrayColor];}
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
   return YES;
    //    }
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    
    if(![textView.text isEqualToString:@""]){
        notes= textView.text;   }else{
        textView.text =@"Type here ...";
    }
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
  
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height;
 
    if(indexPath.row==3)
             height = 150;
        else
       
            height = 55;
    
   
    return height;
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//    if([_selected_index_multi isEqualToString:@"0"]||[_selected_index_multi isEqualToString:@"1"])
  //  {
        if([arSelectedRows count]>0)
        {
        NSInteger    theRow       = arSelectedRows[0];
        NSIndexPath *theIndexPath = [NSIndexPath indexPathForRow:theRow inSection:0];
        [self.tableView selectRowAtIndexPath:theIndexPath
                                    animated:NO
                              scrollPosition:UITableViewScrollPositionNone];
        }
   // }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{}

- (IBAction)cancel:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    location_dischargeViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"loc_discharge"];
    //
    //    viewController.patientId = self.patientId;
    //    viewController.providesPresentationContextTransitionStyle = YES;
    //    viewController.definesPresentationContext = YES;
    //
    //    [viewController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    //    [self presentViewController:viewController animated:YES completion:nil];
    
}


- (IBAction)save:(id)sender {
    
  
        float myNumber = [timesheetedittimespent_save floatValue];
        
        if([datestr isEqualToString:@""]){
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please Enter Date" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
            
        }
        else
            if([[[NSUserDefaults standardUserDefaults] objectForKey:@"absencetxt"]isEqualToString:@""]){
                UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please Enter Absence" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [callAlert show];
                
            }
    
            else
                if([timesheetedittimespent_save isEqualToString:@"-1"]){
                    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please Enter hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [callAlert show];
                    
              
        
    
                        }else{
                            
                            if (timesheetedit_timespent_minutes != -1) {
                                int send_minutes = 0;
                                if (timesheetedit_timespent_minutes == 15) {
                                    send_minutes = 25;
                                } else if (timesheetedit_timespent_minutes == 30) {
                                    send_minutes = 5;
                                } else if (timesheetedit_timespent_minutes == 45) {             send_minutes = 75;
                                }
                                
                                timesheetedittimespent_save =
                                [NSString stringWithFormat:@"%d.%d", timesheetedit_timespent_hours, send_minutes];
                            }
                            
                            
                        
                        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        
                        
                        NSDictionary *json = @{
                                               @"TimeSheetId" :_edittimesheet_IDstr,
                                               @"JobDate" : datestr,
                                               @"TechName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
                                               @"Type" : [[NSUserDefaults standardUserDefaults] objectForKey:@"absencetxt"],
                                               @"Notes" : notes,
                                               @"HrsSpent" :timesheetedittimespent_save
                                               };
                        
                        DownloadManager *downloadObj = [[DownloadManager alloc]init];
                        //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                        
                        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetDetailsWoForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetDetailsWoForApp"];
                        }

    
        
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    AbsenceVC *vc = [segue destinationViewController];
    vc.edittimesheet_IDstr=_edittimesheet_IDstr;
    vc.edittimesheet_notes=_edittimesheet_notes;
     vc.edittimesheet_hours=_edittimesheet_hours;
    vc.edittimesheet_date=_edittimesheet_date;
     vc.edittimesheet_IDstr=_edittimesheet_IDstr;
}
- (IBAction)logoutAction:(id)sender {
    [self logoutUser];
    
}

- (IBAction)changepasswordAction:(id)sender {
    [self changePassword];
}

@end
