//
//  TaptoBillVC.m
//  Efield
//
//  Created by Praveen Kumar on 12/02/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "dynamiccameraVC.h"

 #import "multiplettb.h"

//
//  HomeViewController.m
//  LLSimpleCameraExample
//
//  Created by Ömer Faruk Gül on 29/10/14.
//  Copyright (c) 2014 Ömer Faruk Gül. All rights reserved.
//

#import "ViewUtils.h"

@interface dynamiccameraVC ()
@property (strong, nonatomic) LLSimpleCamera *camera;
@property (strong, nonatomic) UILabel *errorLabel;
@property (strong, nonatomic) UIButton *snapButton,*imagepicker;
@property (strong, nonatomic) UIButton *switchButton,*appicon,*Multiple;
@property (strong, nonatomic) UIButton *flashButton;

@property   UIView *topbar,*bottombar;
@property (strong, nonatomic) UISegmentedControl *segmentedControl;
@end

@implementation dynamiccameraVC{

    
    UIImage *single_img1;
    CGRect screenRect ;
    NSMutableArray *imgListList;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
NSArray   *imgdummy=[[NSArray alloc]init];

 imgListList=[[NSMutableArray alloc]init];
    NSData *imgListdata = [[NSUserDefaults standardUserDefaults] objectForKey:@"imgList"];
    if([imgListdata length]==0)
    {
        
    }else{
        imgdummy = [NSKeyedUnarchiver unarchiveObjectWithData:imgListdata];
    }
    imgListList=[imgdummy mutableCopy];
    NSLog(@"imgListList%@",imgListList);
        self.view.backgroundColor = [UIColor blackColor];
        [self.navigationController setNavigationBarHidden:YES animated:NO];
    
        screenRect = [[UIScreen mainScreen] bounds];
    
        // ----- initialize camera -------- //
    
        // create camera vc
        self.camera = [[LLSimpleCamera alloc] initWithQuality:AVCaptureSessionPresetHigh
                                                     position:LLCameraPositionRear
                                                 videoEnabled:YES];
        
        // attach to a view controller
        [self.camera attachToViewController:self withFrame:CGRectMake(0, 0, screenRect.size.width, screenRect.size.height)];
        
        // read: http://stackoverflow.com/questions/5427656/ios-uiimagepickercontroller-result-image-orientation-after-upload
        // you probably will want to set this to YES, if you are going view the image outside iOS.
        self.camera.fixOrientationAfterCapture = NO;
        
        // take the required actions on a device change
        __weak typeof(self) weakSelf = self;
        [self.camera setOnDeviceChange:^(LLSimpleCamera *camera, AVCaptureDevice * device) {
            
            NSLog(@"Device changed.");
            
            // device changed, check if flash is available
            if([camera isFlashAvailable]) {
                weakSelf.flashButton.hidden = NO;
                
                if(camera.flash == LLCameraFlashOff) {
                    weakSelf.flashButton.selected = NO;
                }
                else {
                    weakSelf.flashButton.selected = YES;
                }
            }
            else {
                weakSelf.flashButton.hidden = YES;
            }
        }];
        // ----- camera buttons -------- //
        self.bottombar=[UIView new];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        self.bottombar.frame = CGRectMake(0, 0, screenRect.size.width+4000
                                          ,100.0f);
    }else{
        self.bottombar.frame = CGRectMake(0, 0, screenRect.size.width+4000
                                          ,80.0f);
    }
        self.bottombar.backgroundColor =[UIColor  colorWithRed:220/255.0f green:236/255.0f blue:243/255.0f alpha:1.0];
        [self.view addSubview:self.bottombar];
    
    
    
    
    self.imagepicker = [UIButton buttonWithType:UIButtonTypeCustom];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        self.imagepicker.frame = CGRectMake(0, 0, 60.0f, 60.0f);
    }else{
        self.imagepicker.frame = CGRectMake(0, 0, 50.0f, 50.0f);
    }
    self.imagepicker.clipsToBounds = YES;
    
    UIImage *buttonImage = [UIImage imageNamed:@"photolib"];
    [ self.imagepicker setImage:buttonImage forState:UIControlStateNormal];
    
    self.imagepicker.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.imagepicker.layer.shouldRasterize = YES;
    [self.imagepicker addTarget:self action:@selector(imagepickerPressed:) forControlEvents:UIControlEventTouchUpInside];
  //  [self.view addSubview:self.imagepicker];
    
    
    
    
        // snap button to capture image
        self.snapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        self.snapButton.frame = CGRectMake(0, 0, 70.0f, 70.0f);
    }else{
       self.snapButton.frame = CGRectMake(0, 0, 50.0f, 50.0f);
    }
        self.snapButton.clipsToBounds = YES;
        self.snapButton.layer.cornerRadius = self.snapButton.width / 2.0f;
        self.snapButton.layer.borderColor = [UIColor whiteColor].CGColor;
        self.snapButton.layer.borderWidth = 2.0f;
        self.snapButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:1.0];
        self.snapButton.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.snapButton.layer.shouldRasterize = YES;
        [self.snapButton addTarget:self action:@selector(snapButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.snapButton];
        
        // button to toggle flash
//        self.topbar=[UIView new];
//    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
//    {
//        self.topbar.frame = CGRectMake(0,0, screenRect.size.width+4000 , 65.0f);
//    }else{
//    
//        self.topbar.frame = CGRectMake(0,0, screenRect.size.width+4000 , 50.0f);
//        
//    }
//        self.topbar.backgroundColor =[UIColor  colorWithRed:220/255.0f green:236/255.0f blue:243/255.0f alpha:1.0];
        self.flashButton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.flashButton.frame = CGRectMake(0, 0, 16.0f + 15.0f, 24.0f + 15.0f);
        self.flashButton.tintColor = [UIColor blackColor];
        [self.flashButton setImage:[UIImage imageNamed:@"camera-flash.png"] forState:UIControlStateNormal];
        self.flashButton.imageEdgeInsets = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
        [self.flashButton addTarget:self action:@selector(flashButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
      //  [self.view addSubview:self.topbar];
        [self.view addSubview:self.flashButton];
        
        //if([LLSimpleCamera isFrontCameraAvailable] && [LLSimpleCamera isRearCameraAvailable]) {
        // button to toggle camera positions
        self.switchButton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.switchButton.frame = CGRectMake(5.0f, 0, 29.0f + 40.0f, 22.0f + 20.0f);
        self.switchButton.tintColor = [UIColor whiteColor];
        [self.switchButton setTitle: @"Cancel" forState:UIControlStateNormal];
        self.switchButton .titleLabel.font = [UIFont systemFontOfSize:17];
        
        [self.switchButton setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
        //  [self.switchButton setImage:[UIImage imageNamed:@"camera-switch.png"] forState:UIControlStateNormal];
        self.switchButton.imageEdgeInsets = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
        [self.switchButton addTarget:self action:@selector(switchButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.switchButton];
 
//
//    // button to toggle camera positions
//    self.Multiple = [UIButton buttonWithType:UIButtonTypeSystem];
//    self.Multiple.frame = CGRectMake(5.0f, 0, 29.0f + 40.0f, 22.0f + 20.0f);
//    self.Multiple.tintColor = [UIColor whiteColor];
//    [self.Multiple setTitle: @"Multiple" forState:UIControlStateNormal];
//    self.Multiple .titleLabel.font = [UIFont systemFontOfSize:17];
//
//    [self.Multiple setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
//    //  [self.switchButton setImage:[UIImage imageNamed:@"camera-switch.png"] forState:UIControlStateNormal];
//
//
//       // self.segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Multiple",@"Single"]];
//        self.Multiple.frame = CGRectMake (12.0f, screenRect.size.height - 47.0f, 120.0f, 30.0f);
////    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"multiplettb"] isEqualToString:@"true"]){
////        self.segmentedControl.selectedSegmentIndex = 0;
////
////    }else{
////        self.segmentedControl.selectedSegmentIndex = 1;
////    }
//      //  self.segmentedControl.tintColor = [UIColor  colorWithRed:75/255.0f green:83/255.0f blue:102/255.0f alpha:1.0];
//
//        [self.Multiple  addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
//        [self.view addSubview:self.Multiple];
      //  [self.camera start];
    }
//- (void)viewWillAppear:(BOOL)animated
//{
//    
//    [self.camera start];
//}
- (void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil]
    ;}

- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");

    NSString *alertText;
    NSString *alertButton;

    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings)
    {
        alertText = @"Denied camera access";

        alertButton = @"Settings";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:alertText
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:alertButton, nil
                            ];
        alert.tag = 3491832;
        [alert show];
 
    }
    else
    {
        alertText = @"Denied camera access";

        alertButton = @"OK";
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:alertText
                              delegate:self
                              cancelButtonTitle:alertButton
                              otherButtonTitles:nil];
        alert.tag = 34918321;
        [alert show];
     }

   
}

//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
 - (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
 
{
    if (alertView.tag == 34918321)
    {
       // [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.1];

        [self.navigationController popViewControllerAnimated:YES];
        [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
    }
    
    else if (alertView.tag == 3491832)
    {
        if(buttonIndex==1){
        BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
        if (canOpenSettings)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
       //     [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.1];
            
            [self.navigationController popViewControllerAnimated:YES];
            [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];

//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];
//            rootViewController.selectedIndex = 1;
//            [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
            
        }else{
          //  [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.1];
            
            NSLog(@"%@", @"Denied camera access");

            [self.navigationController popViewControllerAnimated:YES];
            [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];

//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];
//            rootViewController.selectedIndex = 1;
//            [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
            
        }
    } else if (buttonIndex==0)
    {
        NSLog(@"%@", @"Denied camera access");

     //   [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.1];
        [self.navigationController popViewControllerAnimated:YES];
        [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
        
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];
//            rootViewController.selectedIndex = 1;
//            [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
        
      
    }

}
}

- (void)segmentedControlValueChanged:(UISegmentedControl *)control
{
    NSLog(@"Segment value changed!");
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *mediaType = AVMediaTypeVideo;
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        NSLog(@"%@", @"You have camera access");
    }
    else if(authStatus == AVAuthorizationStatusDenied)
    {
        NSLog(@"%@", @"Denied camera access");
        
        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
            if(granted){
             } else {
     dispatch_async(dispatch_get_main_queue(), ^{[self camDenied];                });
             
            }
        }];
    }
    else if(authStatus == AVAuthorizationStatusRestricted)
    {
        NSLog(@"%@", @"Restricted, normally won't happen");
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
            if(granted){
           
            } else {
               
                dispatch_async(dispatch_get_main_queue(), ^{
//                    [[[UIAlertView alloc] initWithTitle:@"status not determined!"
//                                                message:@"AVCam doesn't have permission to use Camera, please change privacy settings"
//                                               delegate:self
//                                      cancelButtonTitle:@"OK"
//                                      otherButtonTitles:nil] show];
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil]
                    ;
//                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                    UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];
//                    rootViewController.selectedIndex = 1;
//                    [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
                   
                });
            }
        }];
    }
    else
    {
        NSLog(@"%@", @"Camera access unknown error.");
    }
    [self.camera start];
}

/* camera button methods */

- (void)switchButtonPressed:(UIButton *)button
{
    //[self.camera togglePosition];
//    NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
//    
//    for (int i=0; i<img_count;i++ )
//    {
//        
//        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:[NSString stringWithFormat:@"image%d", img_count]];
//        
//    }
//    
//    NSInteger    img_count1=0;
//    
//    [[NSUserDefaults standardUserDefaults] setInteger:img_count1 forKey:@"img_count"];
    
    
    
    
//
    
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"false" forKey:@"fromcamera"];

        [self.navigationController popViewControllerAnimated:YES];
        [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil]
        ;
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];
//        rootViewController.selectedIndex = 1;
//        [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
    
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)flashButtonPressed:(UIButton *)button
{
    if(self.camera.flash == LLCameraFlashOff) {
        BOOL done = [self.camera updateFlashMode:LLCameraFlashOn];
        if(done) {
            self.flashButton.selected = YES;
            self.flashButton.tintColor = [UIColor yellowColor];
        }
    }
    else {
        BOOL done = [self.camera updateFlashMode:LLCameraFlashOff];
        if(done) {
            self.flashButton.selected = NO;
            self.flashButton.tintColor = [UIColor whiteColor];
        }
    }
}

//-(void)imagePickerController:(UIImagePickerController *)picker
//didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    [self dismissViewControllerAnimated:YES completion:^{
//        image = [info valueForKey:UIImagePickerControllerOriginalImage];
//        imageControllerView.image = image;
//        [self presentViewController:imageControllerView animated:YES completion:nil];
//    }];
//}
- (void)imagepickerPressed:(UIButton *)button
{
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)imagePicker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    //    You can retrieve the actual UIImage
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    //    Or you can get the image url from AssetsLibrary
    NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    
    single_img1=image;
   NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"img_count"] ==nil){
        img_count=1;
    }else{
        img_count=img_count+1;
    }
    [[NSUserDefaults standardUserDefaults] setInteger:img_count forKey:@"img_count"];
    
    [[NSUserDefaults standardUserDefaults]setObject:UIImageJPEGRepresentation(single_img1, 0.0)  forKey:[NSString stringWithFormat:@"image%d", img_count]];
    
    NSLog(@"img_arraycount%d",img_count);
    [imagePicker dismissViewControllerAnimated:NO completion:nil];
    
   // [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];
//    [self performSegueWithIdentifier:@"multiplettb_show" sender:self];
       }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)imagePicker
{
    
    
    [imagePicker dismissViewControllerAnimated:NO completion:nil];//Or call YES if you want the nice dismissal animation
}
- (void)snapButtonPressed:(UIButton *)button
{
    __weak typeof(self) weakSelf = self;
    
    //if(self.segmentedControl.selectedSegmentIndex == 0) {
    // capture
    [self.camera capture:^(LLSimpleCamera *camera, UIImage *image, NSDictionary *metadata, NSError *error) {
        if(!error) {
         
            single_img1=image;
            
           
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            
            
            NSString  *datestring =[dateFormatter stringFromDate:[NSDate date]];
            datestring=[datestring stringByReplacingOccurrencesOfString:@":" withString:@""];
            datestring=[datestring stringByReplacingOccurrencesOfString:@"-" withString:@""];
            datestring=[datestring stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            
         
            NSDictionary *json = @{
                                   @"DetailsImageListId": @"",
                                   @"WorkorderAttachmentsId": @"",
                                   @"WorkOrderId":_WorkorderId,
                                   @"ImageName": [NSString stringWithFormat:@"image%@.jpeg",datestring],
                                   @"FilePath": @"",
                                   @"Comments": @"",
                                   @"QuestionValue": @"true",
                                   @"ImageDiscription": @"",
                                   @"NSData_value": [self imageToNSString:single_img1]
                                 
                                   };
  
  //    NSDictionary *json =@{
//
//                                   @"AnswerId": @"0",
//                                   @"AnswerDisplayOrder": @"0",
//                                   @"QuestionId": @"0",
//                                   @"AnswerValue": [NSString stringWithFormat:@"image%@.jpg",datestring],
//                                   @"QuestionValue": @"true",
//                                   @"NSData_value": [self imageToNSString:single_img1]
//                                   };
            [imgListList addObject:json];
            
            
            //       [[NSUserDefaults standardUserDefaults] setObject:UIImageJPEGRepresentation(image, 0.5) forKey:@"image"];
            
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"imgList"];
            
            NSData *imgListtdata = [NSKeyedArchiver archivedDataWithRootObject:imgListList];
            [[NSUserDefaults standardUserDefaults] setObject:imgListtdata forKey:@"imgList"];
            [[NSUserDefaults standardUserDefaults] setObject:@"true" forKey:@"fromcamera"];
            
            
            
            
            
            
          
         //   [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil];

            [self.navigationController popViewControllerAnimated:YES];
            [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil]
            ;
                  //  [self performSegueWithIdentifier:@"multiplettb_show" sender:self];
  
        }
        else {
            NSLog(@"An error has occured: %@", error);
        }
    } exactSeenImage:YES];
   
}
- (NSString *)imageToNSString:(UIImage *)image
{
   NSData *imageData =UIImageJPEGRepresentation(image, 0.5);
    
    return   [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (NSData *)stringToUIImage:(NSString *)string
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:string
                                                      options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    return data;
}
/* other lifecycle methods */

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.camera.view.frame = self.view.contentBounds;
    
    self.snapButton.center = self.view.contentCenter;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
    self.snapButton.bottom = self.view.height - 15.0f;
    } else  if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                
                
            case 2436:
                self.snapButton.bottom = self.view.height - 20.0f;
                break;
            default:
                self.snapButton.bottom = self.view.height - 10.0f;
        }
    }
    
    
  //  self.imagepicker.center = self.view.contentCenter;
    self.imagepicker.left=20;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        self.imagepicker.bottom = self.view.height - 20.0f;
    }
    else  if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                
                
            case 2436:
                self.imagepicker.bottom = self.view.height - 20.0f;
                break;
            default:
                self.imagepicker.bottom = self.view.height - 10.0f;
        }
    }
    
  
    
    // self.flashButton.center = self.view.contentCenter;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
    self.flashButton.top = 15.0f;
    }else{
     self.flashButton.top = 10.0f;
    }
    self.topbar.top = 0.0f;
    
    self.bottombar.bottom = self.view.height;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
    self.switchButton.top = 15.0f;
           }else{
        self.switchButton.top = 10.0f;
       
    }
    self.flashButton.right = self.view.width;
    
    //   self.segmentedControl.left = 12.0f;
   // self.segmentedControl.center = self.view.contentCenter;
    self.segmentedControl.right = self.view.width-5;

    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
    self.segmentedControl.bottom =  self.view.height - 30.0f;
        }else{
            
            self.segmentedControl.bottom = self.view.height - 20.0f;

             }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"multiplettb_show"]) {
            multiplettb *viewController = [segue destinationViewController];
            viewController.multiimg_img=single_img1;
        viewController.dynamicnote=true;
        viewController.noteType = _noteType;

            
        }
        }



@end



