//
//  multiselectViewController.m
//  Efield
//
//  Created by MyMac1 on 2/7/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "multiplettb.h"
 @interface multiplettb ()
{
    NSString *
    selectedvalue,*selectedid,*Specialist;
    NSMutableArray *img_array;
   
}

@end

@implementation multiplettb{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    img_array=[[NSMutableArray alloc]init];
    
    if(_dynamicnote){
        
        _lbltitle.text=_noteType;

    }else{
        
        _lbltitle.text=@"Tap-To-Bill";
    }
    
 }

-(void)viewWillAppear:(BOOL)animated{
    self.img_view.image=_multiimg_img;
    
 
  
//    for(int val=0;val<[img_array count];val++){
//        
//        NSString *s = [NSString stringWithFormat:@"image%d", val];
//        
//        [[NSUserDefaults standardUserDefaults] setObject:img_array forKey:s];
//    }


}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */



- (IBAction)logoutAction:(id)sender {}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    if ([key isEqualToString:@"logout"])
    {
        
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efield Message" message:@"Please try again later"];
        }
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  
}
- (IBAction)changepasswordAction:(id)sender {
    [self changePassword];
}

- (IBAction)Cancel:(id)sender {
   
        NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
        
        for (int i=0; i<img_count;i++ )
        {
            
            [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:[NSString stringWithFormat:@"image%d", img_count]];
            
        }
        
        NSInteger    img_count1=0;
        
        [[NSUserDefaults standardUserDefaults] setInteger:img_count1 forKey:@"img_count"];
        
        
        [self performSegueWithIdentifier:@"dynamicform" sender:self];
 
    //

}


- (IBAction)retake:(id)sender {
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil]
    ;
     [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (IBAction)next:(id)sender {
     NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"img_count"] ==nil){}else{
        img_count=img_count+1;
    }
    [[NSUserDefaults standardUserDefaults] setInteger:img_count forKey:@"img_count"];

    [[NSUserDefaults standardUserDefaults]setObject:UIImageJPEGRepresentation(_multiimg_img, 0.0)  forKey:[NSString stringWithFormat:@"image%d", img_count]];

       NSLog(@"img_arraycount%d",img_count);
//    
//    
//    NSData *imageData = UIImageJPEGRepresentation(_multiimg_img, 0.5);
//    NSMutableArray *dummy=[[NSMutableArray alloc]init];
//    if([[NSUserDefaults standardUserDefaults] objectForKey:@"img_array"] ==nil){}else{
//    [dummy addObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"img_array"]];
//        NSLog(@"dummycount%d",[dummy count]);}
//    NSMutableArray *img_array = [[NSMutableArray alloc]init];
//     [img_array addObject:dummy];
//      [img_array addObject:imageData];
//    
//    NSLog(@"img_arraycount%d",[img_array count]);
//    [[NSUserDefaults standardUserDefaults] setObject:img_array forKey:@"img_array"];

    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil]
    ;
    [self.navigationController popViewControllerAnimated:YES];
    

}


-(UIImage *)compressImage:(UIImage *)image{
    
    NSData *imgData = UIImageJPEGRepresentation(image, 1); //1 it represents the quality of the image.
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imgData length]);
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imageData length]);
    
    return [UIImage imageWithData:imageData];
}


- (IBAction)done:(id)sender {
    if(_dynamicnote){
        
        
        NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"img_count"] ==nil){
            img_count=1;
        }else{
            img_count=img_count+1;
        }
        [[NSUserDefaults standardUserDefaults] setInteger:img_count forKey:@"img_count"];
        _multiimg_img = [self compressImage:_multiimg_img];
       
        [[NSUserDefaults standardUserDefaults]setObject:UIImageJPEGRepresentation(_multiimg_img, 0.5)  forKey:[NSString stringWithFormat:@"image%d", img_count]];
        
        NSLog(@"img_arraycount%d",img_count);
        [self performSegueWithIdentifier:@"dynamicform" sender:self];

    }else{
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue0"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue2"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue3"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue4"];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue5"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue0id"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue10id"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue10"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"LocationName"];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue2id"];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"selectedvalue3id"];
    
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"MM/dd/yyyy"];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:[formatter1 stringFromDate:[NSDate date]] forKey:@"selectedvalue1"];
    
//    NSData *imageData = UIImageJPEGRepresentation(_multiimg_img, 0.5);
//    NSMutableArray *dummy=[[NSMutableArray alloc]init];
//    if([[NSUserDefaults standardUserDefaults] objectForKey:@"img_array"] ==nil){}else{
//        [dummy addObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"img_array"]];
//        NSLog(@"dummycount%d",[dummy count]);}
//    NSMutableArray *img_array = [[NSMutableArray alloc]init];
//    
//    [img_array addObject:dummy];
//    [img_array addObject:imageData];
//    NSLog(@"img_arraycount%d",[img_array count]);
//    [[NSUserDefaults standardUserDefaults] setObject:img_array forKey:@"img_array"];
//
    
    NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"img_count"] ==nil){
        img_count=1;
    }else{
        img_count=img_count+1;
    }
    [[NSUserDefaults standardUserDefaults] setInteger:img_count forKey:@"img_count"];
    
    [[NSUserDefaults standardUserDefaults]setObject:UIImageJPEGRepresentation(_multiimg_img, 0.0)  forKey:[NSString stringWithFormat:@"image%d", img_count]];

        NSLog(@"img_arraycount%d",img_count);
    
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil]
    ;
    //[self dismissModalViewControllerAnimated:YES];
    
    [[NSUserDefaults standardUserDefaults]setObject:@"false" forKey:@"multiplettb"];

     [self.navigationController popViewControllerAnimated:YES];
    [self performSegueWithIdentifier:@"multiple_addsegue" sender:self];
    

    }
}
@end
