//
//  AlertListVC.h
//  Efield
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface completedVC : ViewController {
    __weak IBOutlet UITableView *alertTable;
    __weak IBOutlet UISearchBar *searchbar;
    NSArray *tempArr;
    NSMutableArray *alertList;
    IBOutlet UITextField *jobdate;
    IBOutlet UITextField *workorder;
    NSMutableArray *checkBoxArr;
    NSInteger index;
    NSDictionary *dummyDic;
    NSMutableDictionary *alertListDic;
    UIRefreshControl *refreshControl;
    __weak IBOutlet UIView *tableFooterView;
    __weak IBOutlet UIBarButtonItem *markAllButton;
}
@property (weak, nonatomic) IBOutlet UIButton *cloasebtn;
@property (weak, nonatomic) IBOutlet UILabel *alertcnt;
@property (weak, nonatomic) IBOutlet UILabel *date;
- (IBAction)close:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *companynametxt;
@property (weak, nonatomic) IBOutlet UIButton *editIcon;
@property(nonatomic, retain)NSString *workorderID;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
- (IBAction)logoutAction:(id)sender;
- (IBAction)changepasswordAction:(id)sender;
- (IBAction)showMoreAction:(id)sender;
- (IBAction)editAction:(id)sender;
- (IBAction)jobdateaction:(id)sender;
- (IBAction)markAllAction:(id)sender;
- (IBAction)Specialization:(id)sender;
- (IBAction)add_patient:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *calender_btn;
@property (weak, nonatomic) IBOutlet UIButton *addpatient_button;
void *NewBase64Decode(
                      const char *inputBuffer,
                      size_t length,
                      size_t *outputLength);

char *NewBase64Encode(
                      const void *inputBuffer,
                      size_t length,
                      bool separateLines,
                      size_t *outputLength);

 
+ (NSData *)dataFromBase64String:(NSString *)aString;
- (NSString *)base64EncodedString;

// added by Hiroshi Hashiguchi
- (NSString *)base64EncodedStringWithSeparateLines:(BOOL)separateLines;

- (IBAction)alertaction:(id)sender;
- (IBAction)add_WOaction:(id)sender;

- (void)updateData;
@end
