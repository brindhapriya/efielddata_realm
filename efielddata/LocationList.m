                             //
//  _LocationList_arrVC.m
//  Efielddata
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import "LocationList.h"

#import "Location_Cell.h"
#import "Constants.h"
#import "DynamicNoteFormVC.h"
#import "PlacementFormVC.h"
//#import "UITabBarItem+CustomBadge.h"
@interface LocationList ()
{
    NSString *placeworkorderID,*placeTaskName,*placeJobNumber,*placeProjectName,*placeTestTypeId;

   }
@end

@implementation LocationList
NSDictionary *placement_dict;
NSInteger delete_index;

- (void)viewDidLoad {
      
    [super viewDidLoad];
  
        _Header_lbl.text=_childname;
 
    self.jobnumber.text=[NSString stringWithFormat:@"%@ - %@", _JobNumber,_TaskName];
}


-(void)getEditNote
{
if (![self isNetworkAvailable]) {
    [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
    return;
}else{
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc]init];
    NSString *str=
    
    //[NSString stringWithFormat:@"%@GetIOSEditAssessment?AssessmentMasterid=%@&NoteId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_assessmentMasterId,_WorkorderId];
    [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId];
    NSLog(@"url%@",[NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId]);
    [request setURL:[NSURL URLWithString:str]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];


    placement_dict= [NSJSONSerialization JSONObjectWithData:urlData
                                              options:kNilOptions error:&error];
    _LocationList_arr=[[NSArray alloc]init];
   _LocationList_arr= [placement_dict valueForKeyPath:@"Data.ConcreteWorkOrderList"];

    
    self.taskname_lbl.text=[placement_dict valueForKeyPath:@"Data.ProjectName"] ;
    [_location_list reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
}
 

- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
 
}
- (void)viewWillAppear:(BOOL)animated
{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    if(_isFromEditNote )
    [self performSelector:@selector(getEditNote) withObject:nil afterDelay:0.1];
     
}
//- (void)loadAlertData
//{
//    NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:details];
//    NSDictionary* json = [NSKeyedUnarchiver unarchiveObjectWithData:data];
//
//        _LocationList_arr = [[NSMutableArray alloc]initWithArray:[json valueForKeyPath:@"Data"]];
//        tempArr = [[NSArray alloc]initWithArray:[json valueForKeyPath:@"Data"]];
//
//
//         [Location_Table reloadData];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        return 50;
    
}


#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _LocationList_arr.count;
    
    
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        
        
        delete_index=indexPath.row;
        UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:@"Are you sure to delete this item?"  delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil];
        callAlert.tag = 56;
        [callAlert show];
        
        
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
    {
        
        
        static NSString *simpleTableIdentifier = @"locationcell";
        
        Location_Cell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        [cell.contentView clearsContextBeforeDrawing];
        
        
        if (cell == nil) {
            cell = [[Location_Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        cell.Job_number.text =   [NSString stringWithFormat:@"%@,%@" ,  [[_LocationList_arr valueForKey:@"JobNumber"] objectAtIndex:indexPath.row],[[_LocationList_arr valueForKey:@"TaskName"] objectAtIndex:indexPath.row] ];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return  cell;
    }


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    placeTaskName=[[_LocationList_arr valueForKey:@"TaskName"] objectAtIndex:indexPath.row];
  //  placeProjectName[[_LocationList_arr valueForKey:@"WorkOrderId"] objectAtIndex:indexPath.row];
   placeJobNumber=[[_LocationList_arr valueForKey:@"JobNumber"] objectAtIndex:indexPath.row];
    placeworkorderID=[[_LocationList_arr valueForKey:@"WorkOrderId"] objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"placement_segue" sender:nil];
    
}

 //
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 57)
    {
        self.tabBarController.selectedIndex = 0;
    }
  else  if (alertView.tag == 58)
    {
     
    }
    
    else  if (1 == buttonIndex && alertView.tag == 56)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        
        
        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        
        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@DisableWorkOrderForApp?WorkOrderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[_LocationList_arr valueForKey:@"WorkOrderId"] objectAtIndex:delete_index]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"DisableWorkOrderForApp"];
      
        
    }
}

-(void)copyplaceViewController {
    
    
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    NSString *url=[NSString stringWithFormat:@"%@CopyPlacementWorkOrderForApp?WorkOrderId=%@&UserName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId,[[NSUserDefaults standardUserDefaults] objectForKey:username]];
    NSLog(@"url%@",url);
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@CopyPlacementWorkOrderForApp?WorkOrderId=%@&UserName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId,[[NSUserDefaults standardUserDefaults] objectForKey:username]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"CopyPlacementWorkOrderForApp"];
    
}

#pragma mark - API Delegate

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{{
    NSLog(@"CallBackFailure");
   // [[UIApplication sharedApplication] endIgnoringInteractionEvents];
     UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:response delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    callAlert.tag = 57;
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
    {
        if ([key isEqualToString:@"DisableWorkOrderForApp"])
        {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            //    if(_isFromEditNote )
            [self performSelector:@selector(getEditNote) withObject:nil afterDelay:0.1];
            
            
        }
        
    else    if ([key isEqualToString:@"CopyPlacementWorkOrderForApp"])
        {   [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            NSLog(@"data%@",response);
            
            // if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
            // {
            NSArray *ConcreteWorkOrderList=[[NSArray alloc]init];
            ConcreteWorkOrderList=[response valueForKeyPath:@"Data.ConcreteWorkOrderList"];
            
            placeworkorderID=[[ConcreteWorkOrderList valueForKey:@"WorkOrderId"] objectAtIndex:[ConcreteWorkOrderList count]-1];
            
            placeTaskName=  [[ConcreteWorkOrderList valueForKey:@"TaskName"] objectAtIndex:[ConcreteWorkOrderList count]-1];
       
            placeJobNumber=[[ConcreteWorkOrderList valueForKey:@"JobNumber"] objectAtIndex:[ConcreteWorkOrderList count]-1];
       
            placeTestTypeId=[[[ConcreteWorkOrderList valueForKey:@"ProjectTestTypeId"] objectAtIndex:[ConcreteWorkOrderList count]-1]description];
            [self performSegueWithIdentifier:@"placement_segue" sender:nil];

        
        }
    }



  

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
 
    
    if ([[segue identifier] isEqualToString:@"placement_segue"])
    {
        NSIndexPath *indexpath = (NSIndexPath *)sender;
        PlacementFormVC *vc = [segue destinationViewController];
        vc.TaskName=placeTaskName;
        vc.childname=_childname;
        vc.JobNumber=placeJobNumber;
        
        vc.WorkorderId = placeworkorderID;
    }
    
}
- (IBAction)cancelAction:(id)sender {
    
   [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)add_newaction:(id)sender {
      [self copyplaceViewController];
}
@end
