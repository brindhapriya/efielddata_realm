//
//  AlertListVC.h
//  Efield
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
 
#import "ViewController.h"

@interface LocationList : ViewController {
    
}

@property (weak, nonatomic) IBOutlet UITableView *location_list;
@property (nonatomic, retain) NSString *TaskName,*JobNumber,*ProjectName,*childname;



@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *Header_lbl;

@property (weak, nonatomic) IBOutlet UILabel *companynametxt;
@property (weak, nonatomic) IBOutlet UILabel *taskname_lbl;
@property (weak, nonatomic) IBOutlet UILabel *jobnumber;
@property(nonatomic, retain)NSArray *LocationList_arr;
@property (nonatomic, retain)NSString *WorkorderId;
- (IBAction)add_newaction:(id)sender;

@end
