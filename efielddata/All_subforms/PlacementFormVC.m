
//
//  Created by brindha on 12/02/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "PlacementFormVC.h"
#import "Image_SelectionViewController.h"
#import "imageviewTableviewcell.h"
#import "dynamiccameraVC.h"
#import "Suborder_listVC.h"
#import "childform_list.h"
#import "RadioTableViewCell.h"
#import "DLRadioButton.h"
#import "LocationTableViewCell.h"
#import "sampledSelectionViewController.h"


@interface PlacementFormVC ()<UpdateSelectedItemsDelegate,UITextViewDelegate,UITextFieldDelegate>{
    Boolean iscomplete1;
    NSString *timespent1;
    NSInteger rowOfTheCell1;
 //    UITextField *txtfield;
    NSIndexPath *editingIndexPath1;
// NSInteger indexpath;
}
//@property (strong, nonatomic) IBOutlet UISlider *timespentslider;
@property (nonatomic, retain) IBOutlet UITableView *tblForm1;


@end

@implementation PlacementFormVC
NSDictionary *jsonDict1;
- (void)viewDidLoad {
    [super viewDidLoad];
    _childname = [_childname stringByReplacingOccurrencesOfString:@"+"
                                                       withString:@""];
    _Header_lbl.text=_childname;
    //self.title = @"Efield";
    // Do any additional setup after loading the view, typically from a nib.
//     _sampledataview.hidden=YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
 //   [self.view addGestureRecognizer:tap];
   // NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
    _companynametxt.text=[[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
     self.jobnumber.text=[NSString stringWithFormat:@"%@ - %@", _JobNumber,_TaskName];
    
    NSString *eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@", eventDateStr);
            [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"SampleSourceText"];

    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"sampled_array"];
    _date.text=eventDateStr;
//    for (int i=1; i<=img_count;i++ )
//    {
//
//        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:[NSString stringWithFormat:@"image%d", img_count]];
//
//    }
//    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"imgList"];
    [_tblForm1 setDataSource:self];
    [_tblForm1 setDelegate:self];
   // [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"img_count"];
// [self updateALERTcount];
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
    _alertcnt.text=[self updateALERTcount];
    
    _timespentslider.minimumValue=0.0;
    _timespentslider.maximumValue=20.0;
    
     if(_isFromEditNote&& !_IsEditNote)
    {
        _saveBtn.hidden = YES;
    }
    else
    {
        _saveBtn.hidden = NO;
    }
    
     _saveBtn.enabled = NO;
    
   // lblTitle.text = [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    if(_isFromEditNote )
    [self performSelector:@selector(getEditNote) withObject:nil afterDelay:0.1];
//    else
   //     [self performSelector:@selector(getAddNote) withObject:nil afterDelay:0.1];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
   
}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    //   [aTextField resignFirstResponder];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   //  _sampled_date.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"SampleSourceText"];

//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
 //   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
  
 
    
    
    
    // Disable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
 //   if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0);
//    } else {
//        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
//    }
    
    self.tblForm1.contentInset = contentInsets;
    self.tblForm1.scrollIndicatorInsets = contentInsets;
    [self.tblForm1 scrollToRowAtIndexPath: editingIndexPath1 atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    self.tblForm1.contentInset = UIEdgeInsetsZero;
    self.tblForm1.scrollIndicatorInsets = UIEdgeInsetsZero;
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
  //  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

-(void)gestureHandlerMethod1:(UITapGestureRecognizer*)sender {
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    
    [self performSegueWithIdentifier:@"samplesegue" sender:0];
 
    
}
-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender {
 
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
      //  if(sender.view.tag==2) {
            
            if([dataList1[sender.view.tag][@"ChildTestType"] isEqualToString:@"ChildTestTypeName"])
            {
            //do something here
            [self performSegueWithIdentifier:@"suborderlist_segue" sender:self];
            
        }
    else
        if([dataList1[sender.view.tag][@"ChildTestType"] isEqualToString:@"Child2TestTypeName"])
        {
            //do something here
            [self performSegueWithIdentifier:@"childform_segue" sender:self];
            
        }

    }
-(void)getEditNote
{
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
        return;
    }else{
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc]init];
    NSString *str=
        
        //[NSString stringWithFormat:@"%@GetIOSEditAssessment?AssessmentMasterid=%@&NoteId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_assessmentMasterId,_WorkorderId];
        [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId];
        NSLog(@"url%@",[NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId]);
    [request setURL:[NSURL URLWithString:str]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
  jsonDict1= [NSJSONSerialization JSONObjectWithData:urlData
                                                            options:kNilOptions error:&error];
    
    
    //    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"document" ofType:@"json"];
    //    NSLog(@"%@ filePath",filePath);
    //    NSData *data = [NSData dataWithContentsOfFile:filePath];
    //
    //    NSDictionary *jsonDict1= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions  error:&error];
     // NSLog(@"jsonDict1 :%@ %@",str,jsonDict1);
     
  //  dataList1 = [[NSMutableArray alloc] initWithArray:jsonDict1[@"Data.List"]];
   //     NSLog(@"dataList1 :%@",dataList1);
          NSLog(@"jsonDict1 :%@",jsonDict1);
   // NSMutableArray *tempAry = [[NSMutableArray alloc] initWithArray:jsonDict1[@"Data.List"]];
        dataList1 = [[NSMutableArray alloc]initWithArray:[jsonDict1 valueForKeyPath:@"Data.List"]];
        IsAddMoreDisplay1  =[[jsonDict1 valueForKeyPath:@"Data.IsAddMoreDisplay1"]description];
        Is2AddMoreDisplay1  =[[jsonDict1 valueForKeyPath:@"Data.Is2AddMoreDisplay1"]description];

        NSString  *iscomplete1  =  [[jsonDict1 valueForKeyPath:@"Data.iscomplete1"]description];
  //      float  timespent1 =  [[jsonDict1 valueForKeyPath:@"Data.timespent1"]f];
//    _timespentslider.value = [[jsonDict1 valueForKeyPath:@"Data.timespent1"]floatValue];
//        _timespentfield.text=[[jsonDict1 valueForKeyPath:@"Data.timespent1"]description];
//        if([iscomplete1 isEqualToString:@"1"])
//        {
//            [_completeswitch setOn:true];
//
//
//            _completeswitch.enabled=false;
//
//        }else
//        {
//            [_completeswitch setOn:false];
//
//            _completeswitch.enabled=true;
//
//
//        }
//        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
//     _locationlink.userInteractionEnabled = YES;
//
//        [_locationlink addGestureRecognizer:tapRecognizer];
//        _locationlink.tag=2;
        
      //  _timespentslider.value=timespent1;
//    if([IsAddMoreDisplay1 isEqualToString:@"1"])
//    {
//        _dividerlbl.hidden=NO;
//        _locationlbl.hidden=NO;
//          _locationlink.hidden=NO;
        ChildTestTypeId1  =[[jsonDict1 valueForKeyPath:@"Data.ChildTestTypeId11"]description];
        Child2TestTypeId1=[[jsonDict1 valueForKeyPath:@"Data.Child2TestTypeId11"]description];
   TestTypeId1=[[jsonDict1 valueForKeyPath:@"Data.TestTypeId1"]description];
        
            self.taskname_lbl.text=[jsonDict1 valueForKeyPath:@"Data.ProjectName"] ;
        _TaskName = [jsonDict1 valueForKeyPath:@"Data.ProjectName"];
//        SubWorkOrderList1=[[jsonDict1 valueForKeyPath:@"Data.SubWorkOrderList1"]description];
//
//    }else{
        //  _dividerlbl.hidden=YES;
        //     _locationlbl.hidden=YES;
       
    
   // }
        //
        
//        if([TestTypeId1 isEqualToString:@"1011"])
//        {
//
//            NSLog(@"sample id%@",[jsonDict1 valueForKeyPath:@"Data.SampleSourceText"]);
//
//            [[NSUserDefaults standardUserDefaults]setObject:[jsonDict1 valueForKeyPath:@"Data.SampleSourceText"] forKey:@"SampleSourceText"];
//            _sampled_date.text=[[jsonDict1 valueForKeyPath:@"Data.SampleSourceText"]description];
//             _sampledataview.hidden=NO;
//
//
//            [[NSUserDefaults standardUserDefaults]setObject:  [jsonDict1 valueForKeyPath:@"Data.SampleIdArray"]   forKey:@"sampled_array"];
//
//            UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod1:)];
//            _sampledataview.userInteractionEnabled = YES;
//
//            [_sampledataview addGestureRecognizer:tapRecognizer];
//            _sampledataview.tag=2;
//        }
//        else{
//                 _sampledataview.hidden=YES;
//        }
      
        
        //[[NSMutableArray alloc] initWithArray:jsonDict1[@"Data.List"]];
        //    [_tblForm1 reloadData];
        //          NSLog(@"dataList1 : %@",dataList1);
        //    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
          NSMutableArray *tempAry = [[NSMutableArray alloc]initWithArray:[jsonDict1 valueForKeyPath:@"Data.List"]];
        
     //   NSMutableArray *tempAry = [[NSMutableArray alloc] initWithArray:jsonDict1[@"Data.List"]];
        NSLog(@"dataList1 :%@",dataList1);
        
    int index = 0;
    
    for(NSDictionary *dic in dataList1) {
        
        if(([dic[@"DataType"] isEqualToString:@"Dropdown"]) || ([dic[@"DataType"] isEqualToString:@"Radio"]))
        {
            if([dic[@"AnswerId"] intValue] != 0)
            {
              [tempAry replaceObjectAtIndex:index withObject:[self singleCheckEditValues:dic]];
//                                NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"AnswerId = %@",dic[@"AnswerId"]]];
//                                NSArray* filteredData = [dic[@"AnswersList"] filteredArrayUsingPredicate:predicate];
//                                NSLog(@"Filter : %@",filteredData);
//                                if(filteredData.count)
//                                {
//                                    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
//                                    [tempDic setObject:filteredData[0][@"AnswerValue"] forKey:@"QuestionValue"];
//                                    [tempAry replaceObjectAtIndex:index withObject:tempDic];
//                                }
            }
        }
        else if([dic[@"DataType"] isEqualToString:@"NumericTextBox"] )
        {
            
            if([[dic[@"NumericAnswer"] description] isEqualToString:@"-1"])
            {
                
                NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
                [tempDic setObject:@"" forKey:@"NumericAnswer"];
                [tempAry replaceObjectAtIndex:index withObject:tempDic];
                
            }
        }
        else if([dic[@"DataType"] isEqualToString:@"Dropdown(Multiple)"])
        {
            NSArray *selectAnswer = [[NSArray alloc] initWithArray:dic[@"MultiAnswers"]];
            NSString *questionValue = @"";
            
            for(NSString *answerStr in selectAnswer)
            {
                if([answerStr intValue] != 0)
                {
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"AnswerId = %@",answerStr]];
                    NSArray* filteredData = [dic[@"AnswersList"] filteredArrayUsingPredicate:predicate];
                    NSLog(@"Filter : %@",filteredData);
                    if(filteredData.count)
                    {
                        if(questionValue.length)
                            questionValue = [NSString stringWithFormat:@"%@, %@",questionValue,filteredData[0][@"AnswerValue"]];
                        else
                            questionValue = filteredData[0][@"AnswerValue"];
                    }
                }
            }
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
            [tempDic setObject:questionValue forKey:@"QuestionValue"];
            [tempAry replaceObjectAtIndex:index withObject:tempDic];
        }
        else if([dic[@"DataType"] isEqualToString:@"Checkbox(Multiple)"])
        {
            NSArray *selectAnswer = [[NSArray alloc] initWithArray:dic[@"MultiCheckBoxAnswers"]];
            NSString *questionValue = @"";
            
            for(NSString *answerStr in selectAnswer)
            {
                if([answerStr intValue] != 0)
                {
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"AnswerId = %@",answerStr]];
                    NSArray* filteredData = [dic[@"AnswersList"] filteredArrayUsingPredicate:predicate];
                    NSLog(@"Filter : %@",filteredData);
                    if(filteredData.count)
                    {
                        if(questionValue.length)
                            questionValue = [NSString stringWithFormat:@"%@, %@",questionValue,filteredData[0][@"AnswerValue"]];
                        else
                            questionValue = filteredData[0][@"AnswerValue"];
                    }
                }
            }
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
            [tempDic setObject:questionValue forKey:@"QuestionValue"];
            [tempAry replaceObjectAtIndex:index withObject:tempDic];
        }
 
        //        [_selectedItem addObjectsFromArray:_listInfo[@"MultiSelectAnswers"]];
        //        else if([_listInfo[@"DataType"] isEqualToString:@"Checkbox(Multiple)"])
        //            [_selectedItem addObjectsFromArray:_listInfo[@"MultiCheckBoxAnswers"]];
        
        index++;
    }
        if([IsAddMoreDisplay1 isEqualToString:@"1"])
            
        {
             //ChildTestTypeName
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] init];
            [tempDic setObject:@"locationlist" forKey:@"DataType"];
            [tempDic setObject: @"ChildTestTypeName"  forKey:@"ChildTestType"];

            [tempDic setObject:@"Please click the link below to add multiple lines of data" forKey:@"QuestionDisplayName"];
            [tempDic setObject:[[jsonDict1 valueForKeyPath:@"Data.ChildTestTypeName"]description] forKey:@"QuestionValue"];
            
            [tempAry addObject:tempDic];
            
        }
        if([Is2AddMoreDisplay1 isEqualToString:@"1"])
            
        {
        
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc]init];
            [tempDic setObject:@"locationlist" forKey:@"DataType"];
            [tempDic setObject: @"Child2TestTypeName"  forKey:@"ChildTestType"];

            [tempDic setObject:@"Please click the link below to add multiple lines of data" forKey:@"QuestionDisplayName"];
            [tempDic setObject:[[jsonDict1 valueForKeyPath:@"Data.Child2TestTypeName"]description] forKey:@"QuestionValue"];
            
            [tempAry addObject:tempDic];
            
        }
    
    dataList1 = [tempAry mutableCopy];
    [_tblForm1 reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
}

- (NSDictionary *)singleCheckEditValues:(NSDictionary *)dic
{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"AnswerId = %@",[dic[@"AnswerId"]description]]];
    NSArray* filteredData = [dic[@"AnswersList"] filteredArrayUsingPredicate:predicate];
    NSLog(@"Filter : %@",filteredData);
    if(filteredData.count)
    {
        NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
        [tempDic setObject:filteredData[0][@"AnswerValue"] forKey:@"QuestionValue"];
        return tempDic;
    }
    return dic;
}



-(void)updateSelectedItems:(NSDictionary *)updatedItem questionID:(NSString *)questionID {
    NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"QuestionId = %@",questionID]];
    NSArray* filteredData = [dataList1 filteredArrayUsingPredicate:predicate];
    NSLog(@"Filter : %@",filteredData);
    if(filteredData.count) {
        
        NSInteger index = [dataList1 indexOfObject:filteredData[0]];
        NSLog(@"Index : %ld",(long)index);
        [dataList1 replaceObjectAtIndex:index withObject:updatedItem];
        [_tblForm1 reloadData];
    }
    NSLog(@"Came");
    NSLog(@"dataList1 : %@",dataList1);

    
}

-(CGFloat)calculateStringHeigth:(CGSize)size text:(NSString *)text andFont:(UIFont *)font {
    NSLog(@"text : %@",text);
    
    
    NSLog(@"Font : %@", font);
    NSLog(@"size : %@",NSStringFromCGSize(size));
    
    if([text isEqual:[NSNull null]]) {
        text = @"";
    }
    
    if(![text length])
        return 0;
    
    CGSize labelSize = [text sizeWithFont:font
                        constrainedToSize:size
                            lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat labelHeight = labelSize.height;
    NSLog(@"labelHeight : %f",labelHeight);
    return labelHeight;
}

-(NSMutableAttributedString *)AddRequiredField:(NSString *)title andIsRequired:(BOOL)required {
    
    if(required)
    {
        NSString *string = [NSString stringWithFormat:@"%@ *", title];
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
        NSRange range = [string rangeOfString:@"*"];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        return attString;
    }
    else
    {
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:title];
        return attString;
    }
}




#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
 
        return dataList1.count;

 
 
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   // if(dataList1.count<indexPath.row){'
    
    if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"locationlist"]) {
        
        
        //@"Field Image Attachments";
        
        
        static NSString *simpleTableIdentifier = @"location2cell";
        
        LocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        [cell.contentView clearsContextBeforeDrawing];
        
        
        if (cell == nil) {
            cell = [[LocationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        [cell.lblTitle sizeToFit];
        cell.lblTitle.numberOfLines = 0;
        cell.lblTitle.text = dataList1[indexPath.row][@"QuestionDisplayName"] ;
        
        
        //  if([self.taptobilledit isEqualToString:@"true"]){
        
        
        //                if([dataList1[indexPath.row][@"TextAnswer"] length]>0){
        //                    cell.ttbimage_txt.text = @"View Images";
        //                    cell.ttbimage_txt.textColor=[UIColor blueColor];
        //
        //
        //                    UITapGestureRecognizer *viewimag_tap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewimag_taping:)];
        //                    cell.ttbimage_txt.tag=indexPath.row;
        //                    [viewimag_tap setNumberOfTapsRequired:1];
        //                    [cell.ttbimage_txt setUserInteractionEnabled:YES];
        //
        //                    [  cell.ttbimage_txt addGestureRecognizer:viewimag_tap];
        //
        //                }else{
        //
        //                    cell.ttbimage_txt.text = @"";
        //
        //                }
        
        
        // tap gesture for camera
        //  cell.ttbimage_txt.textColor=[UIColor blueColor];
        
          UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
        
        cell.lblValue.tag=indexPath.row;
             cell.lblValue.text=dataList1[indexPath.row][@"QuestionValue"] ;
        [tapRecognizer setNumberOfTapsRequired:1];
        [cell.lblValue setUserInteractionEnabled:YES];
        
        [  cell.lblValue addGestureRecognizer:tapRecognizer];
        //
        //            UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
        //            [singleTap setNumberOfTapsRequired:1];
        //            [cell.ttbimage_txt setUserInteractionEnabled:YES];
        //
        //            [  cell.ttbimage_txt addGestureRecognizer:singleTap];
        //
        //
        // tap gesture to view images
        //  NSInteger height=10;
        //        if(_isFromEditNote)
        //        {
        //
        //
        //            [cell.ttbimage_txt sizeToFit];
        //            cell.ttbimage_txt.numberOfLines = 0;
        //        cell.ttbimage_txt.text=dataList1[indexPath.row][@"TextAnswer"];
        //              CGSize size = self.tblForm1.frame.size;
        //                height = [self calculateStringHeigth:size text:dataList1[indexPath.row][@"TextAnswer"] andFont:[UIFont systemFontOfSize:14]];
        //
        //
        //        }
        
        // CGRect imageFrame = CGRectMake(10,height+20,200, 100);
        cell.accessoryType=UITableViewCellAccessoryNone;
        //            UIImageView *customImage = [[UIImageView alloc] initWithFrame:imageFrame] ;
        //
        //            NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
        //
        //            if(img_count>0){
        //                for(int i=1;i<=img_count;i++){
        //
        //                    //   customImage.image=_single_img;
        //                    customImage.image= [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"image%d", i]]];
        //                }
        //                customImage.center = CGPointMake(cell.contentView.bounds.size.width/2,cell.contentView.bounds.size.height/2+10);
        //                customImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        //
        //
        //                [cell.contentView addSubview:customImage];
        //
        //            }
        //
        return cell;
        
        
    }else
    if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Image"]) {
        
      
      //@"Field Image Attachments";
 
        
            static NSString *simpleTableIdentifier = @"imageviewcell";
            
            imageviewTableviewcell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            [cell.contentView clearsContextBeforeDrawing];
            
            
            if (cell == nil) {
                cell = [[imageviewTableviewcell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
            }
            [cell.ttbimage_lbl sizeToFit];
            cell.ttbimage_lbl.numberOfLines = 0;
            cell.ttbimage_lbl.text = @"Field Image Attachments";
            
            
            //  if([self.taptobilledit isEqualToString:@"true"]){
        
                
//                if([dataList1[indexPath.row][@"TextAnswer"] length]>0){
//                    cell.ttbimage_txt.text = @"View Images";
//                    cell.ttbimage_txt.textColor=[UIColor blueColor];
//
//
//                    UITapGestureRecognizer *viewimag_tap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewimag_taping:)];
//                    cell.ttbimage_txt.tag=indexPath.row;
//                    [viewimag_tap setNumberOfTapsRequired:1];
//                    [cell.ttbimage_txt setUserInteractionEnabled:YES];
//
//                    [  cell.ttbimage_txt addGestureRecognizer:viewimag_tap];
//
//                }else{
//
//                    cell.ttbimage_txt.text = @"";
//
//                }
        
            
            // tap gesture for camera
       //  cell.ttbimage_txt.textColor=[UIColor blueColor];
        
        
                            UITapGestureRecognizer *viewimag_tap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewimag_taping:)];
                            cell.ttbimage_txt.tag=indexPath.row;
                            [viewimag_tap setNumberOfTapsRequired:1];
                            [cell.ttbimage_txt setUserInteractionEnabled:YES];
        
                            [  cell.ttbimage_txt addGestureRecognizer:viewimag_tap];
        //
//            UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
//            [singleTap setNumberOfTapsRequired:1];
//            [cell.ttbimage_txt setUserInteractionEnabled:YES];
//
//            [  cell.ttbimage_txt addGestureRecognizer:singleTap];
//
//
            // tap gesture to view images
          //  NSInteger height=10;
            //        if(_isFromEditNote)
            //        {
            //
            //
            //            [cell.ttbimage_txt sizeToFit];
            //            cell.ttbimage_txt.numberOfLines = 0;
            //        cell.ttbimage_txt.text=dataList1[indexPath.row][@"TextAnswer"];
            //              CGSize size = self.tblForm1.frame.size;
            //                height = [self calculateStringHeigth:size text:dataList1[indexPath.row][@"TextAnswer"] andFont:[UIFont systemFontOfSize:14]];
            //
            //
            //        }
            
           // CGRect imageFrame = CGRectMake(10,height+20,200, 100);
            cell.accessoryType=UITableViewCellAccessoryNone;
//            UIImageView *customImage = [[UIImageView alloc] initWithFrame:imageFrame] ;
//
//            NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
//
//            if(img_count>0){
//                for(int i=1;i<=img_count;i++){
//
//                    //   customImage.image=_single_img;
//                    customImage.image= [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"image%d", i]]];
//                }
//                customImage.center = CGPointMake(cell.contentView.bounds.size.width/2,cell.contentView.bounds.size.height/2+10);
//                customImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
//
//
//                [cell.contentView addSubview:customImage];
//
//            }
//
            return cell;
            
            
        }
       
     else
    if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Category"]) {
        DescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
        
        
        //   cell.lblTitle.text = dataList1[indexPath.row][@"CategoryName"];
        
        //  if(dataList1[indexPath.row][@"CategoryDescription"] != [NSNull null])
        [cell.lblTitle sizeToFit];
        cell.lblTitle.numberOfLines = 1;
        [cell.lblDiscribtion sizeToFit];
        cell.lblDiscribtion.numberOfLines = 0;
        
        if([dataList1[indexPath.row][@"CategoryName"] isKindOfClass:[NSNull class]]){
            cell.lblTitle.hidden=YES;
            
        }else
        {
            cell.lblTitle.hidden=NO;
            cell.lblTitle.text = dataList1[indexPath.row][@"CategoryName"];
        }
        if([dataList1[indexPath.row][@"CategoryDescription"] isKindOfClass:[NSNull class]]){
            cell.lblDiscribtion.hidden=YES;
            
        }else
        {
            cell.lblDiscribtion.hidden=NO;
            cell.lblDiscribtion.text = dataList1[indexPath.row][@"CategoryDescription"];
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"NumericTextBox"])
    //{
        
        {
            TextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
            // if(_isFromEditNote && !_IsEditNote)
            //  cell.userInteractionEnabled = NO;
            if (cell == nil) {
                cell= [[TextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TextFieldCell"];
            }
            cell.lblTitle.attributedText = [self AddRequiredField:dataList1[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList1[indexPath.row][@"IsRequired"] boolValue]];
            cell.txtValue.delegate = self;
            cell.txtValue.tag = indexPath.row;
            [cell.txtValue setKeyboardType:UIKeyboardTypeDecimalPad];

              NSLog(@"NumericAnswer value%@",[NSString stringWithFormat:@"%.f",[dataList1[indexPath.row][@"NumericAnswer"] floatValue]]);
            if([dataList1[indexPath.row][@"NumericAnswer"] floatValue] < 0)
         {       // }
                cell.txtValue.placeholder = @"Type here ...";
    
                 cell.txtValue.text = @"";
       
         }else
                {
                  
                        cell.txtValue.text =[NSString stringWithFormat:@"%.f",[dataList1[indexPath.row][@"NumericAnswer"] floatValue]];}
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.txtValue addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

            return cell;
        }
        
      //  SliderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SliderCell"];
//
//if(_isFromEditNote  && !_IsEditNote)
//            cell.userInteractionEnabled = NO;
//
//        cell.lblTitle.attributedText = [self AddRequiredField:dataList1[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList1[indexPath.row][@"IsRequired"] boolValue]];
//        cell.sliderValue.minimumValue = 0.0;
//        cell.sliderValue.maximumValue = [dataList1[indexPath.row][@"MaxValue"] floatValue];
//        cell.sliderValue.value = [dataList1[indexPath.row][@"NumericAnswer"] floatValue];
//
//        if([dataList1[indexPath.row][@"NumericAnswer"] floatValue] < 0)
//            cell.txtValue.text = @"";
//        else
//            cell.txtValue.text = [NSString stringWithFormat:@"%.f",[dataList1[indexPath.row][@"NumericAnswer"] floatValue]];
//
//        cell.sliderValue.tag = indexPath.row;
//
//        [cell.sliderValue addTarget:self action:@selector(sliderValueChanges:) forControlEvents:UIControlEventValueChanged];
//
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
   // }
    else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Dropdown"]) {
        MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
      
        if (cell == nil) {
            cell = [[MultySelectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MultySelectionCell"];
        }
        if(_isFromEditNote   && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        cell.lblTitle.attributedText = [self AddRequiredField:dataList1[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList1[indexPath.row][@"IsRequired"] boolValue]];
        
        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm1.frame));
//         [cell.lblValue sizeToFit];
//        cell.lblValue.numberOfLines = 0;
//        [  cell.lblValue setNeedsDisplay];
//
        if([dataList1[indexPath.row][@"QuestionValue"]isEqualToString:@""])
        {
            cell.lblValue.text = @"";
        }else{
        cell.lblValue.text = dataList1[indexPath.row][@"QuestionValue"];
        }
        [cell.lblValue sizeToFit];
                cell.lblValue.numberOfLines = 0;
                [  cell.lblValue setNeedsDisplay];
        //
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Checkbox"])
    {
        SwitchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
        if(_isFromEditNote  && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        cell.lblTitle.attributedText = [self AddRequiredField:dataList1[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList1[indexPath.row][@"IsRequired"] boolValue]];
        [cell.switchValue addTarget:self action:@selector(switchValueChanges:) forControlEvents:UIControlEventValueChanged];
        cell.switchValue.tag = indexPath.row;
        [cell.switchValue setOn:[dataList1[indexPath.row][@"BooleanAnswer"] boolValue]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"TextBoxMultiline"])
    {
        TextViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextViewCell"];
        if (cell == nil) {
            cell= [[TextViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TextViewCell"];
        }
        cell.lblTitle.attributedText = [self AddRequiredField:dataList1[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList1[indexPath.row][@"IsRequired"] boolValue]];
        [cell.txtViewValue setKeyboardType:UIKeyboardTypeAlphabet];

        cell.txtViewValue.delegate = self;
        [cell.txtViewValue  sizeToFit];
 
//            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
//            {
//          cell.txtViewValue.textContainerInset = UIEdgeInsetsZero;
//        }else {
//      cell.txtViewValue.contentInset = UIEdgeInsetsMake(-11,-8,0,0);
//        }
        cell.txtViewValue.tag = indexPath.row;
        if(dataList1[indexPath.row][@"TextAnswer"] == [NSNull null]||[dataList1[indexPath.row][@"TextAnswer"]isEqualToString:@""]){
            cell.txtViewValue.text = @"Type here ...";
            cell.txtViewValue.textColor = [UIColor lightGrayColor];
        }else  if(dataList1[indexPath.row][@"TextAnswer"] != [NSNull null]){
            cell.txtViewValue.text = dataList1[indexPath.row][@"TextAnswer"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"TextBox"])
    {
        TextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
        if (cell == nil) {
            cell= [[TextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TextFieldCell"];
        }
        // if(_isFromEditNote && !_IsEditNote)
        //  cell.userInteractionEnabled = NO;
        cell.lblTitle.attributedText = [self AddRequiredField:dataList1[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList1[indexPath.row][@"IsRequired"] boolValue]];
                cell.txtValue.delegate = self;
        cell.txtValue.tag = indexPath.row;
        [cell.txtValue setKeyboardType:UIKeyboardTypeAlphabet];
//
                [cell.txtValue addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
//        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
//         cell.txtValue.leftView = paddingView;
//         cell.txtValue.leftViewMode = UITextFieldViewModeAlways;
//                    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
//                    {
//                  cell.txtValue.textContainerInset = UIEdgeInsetsZero;
//                }else {
//              cell.txtValue.contentInset = UIEdgeInsetsMake(-11,-8,0,0);
//                }
        if(dataList1[indexPath.row][@"TextAnswer"] == [NSNull null]||[dataList1[indexPath.row][@"TextAnswer"]isEqualToString:@""]){
            cell.txtValue.placeholder = @"Type here ...";
            cell.txtValue.text = @"";
            
        }else
              //  if(dataList1[indexPath.row][@"TextAnswer"] != [NSNull null])
        {
                    cell.txtValue.text = dataList1[indexPath.row][@"TextAnswer"];
            
        }
        
    
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Dropdown(Multiple)"])
    {
        MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
        if(_isFromEditNote && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        cell.lblTitle.attributedText = [self AddRequiredField:dataList1[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList1[indexPath.row][@"IsRequired"] boolValue]];
        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm1.frame));
        cell.lblValue.text = dataList1[indexPath.row][@"QuestionValue"];
        [cell.lblTitle sizeToFit];
        [cell.lblValue sizeToFit];
        //[  cell.lblValue setNeedsDisplay];

        cell.lblValue.numberOfLines = 0;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Checkbox(Multiple)"]) {
        
        NSMutableDictionary *dic1 = [[NSMutableDictionary alloc] initWithDictionary:dataList1[indexPath.row]];
        
        NSMutableArray   *selectedItem1 = [[NSMutableArray alloc] init];
        [dic1 setObject:selectedItem1 forKey:@"MultiSelectCheckBoxAnswers"];
        [dataList1 replaceObjectAtIndex:indexPath.row withObject:dic1];
        
        MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
        if(_isFromEditNote  && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        cell.lblValue.text = dataList1[indexPath.row][@"QuestionValue"];
        cell.lblTitle.attributedText = [self AddRequiredField:dataList1[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList1[indexPath.row][@"IsRequired"] boolValue]];
        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm1.frame));
        [cell.lblTitle sizeToFit];
        [cell.lblValue sizeToFit];
        [  cell.lblValue setNeedsDisplay];
        
        cell.lblValue.numberOfLines = 0;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Date"]) {
        DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
        if (cell == nil) {
            cell= [[DateAndTimeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DateAndTimeCell"];
        }
        if(_isFromEditNote && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        cell.lblValue.tag = indexPath.row;
         if(dataList1[indexPath.row][@"DateAnswerText"] == [NSNull null]||[dataList1[indexPath.row][@"DateAnswerText"]isEqualToString:@""]){
                 cell.lblValue.text = @"";
        }else{
            cell.lblValue.text = dataList1[indexPath.row][@"DateAnswerText"];
        }
        cell.lblTitle.attributedText = [self AddRequiredField:dataList1[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList1[indexPath.row][@"IsRequired"] boolValue]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Radio"]) {
        RadioTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"radiocell"];
        
        if (cell == nil) {
            cell= [[RadioTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"radiocell"];
        }
        if(_isFromEditNote   && !_IsEditNote)
            cell.userInteractionEnabled = NO;
      //  cell.lblValue.text = dataList1[indexPath.row][@"QuestionValue"];
        cell.lblTitle.attributedText = [self AddRequiredField:dataList1[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList1[indexPath.row][@"IsRequired"] boolValue]];
        //[cell.lblValue sizeToFit];
      //  cell.lblValue.numberOfLines = 0;
   NSString    *selectedbtn=    [dataList1[indexPath.row][@"AnswerId"]description];
        NSArray  *radiolist=dataList1[indexPath.row][@"AnswersList"];
//        cell.button1.layer.cornerRadius = 10;
//        cell.button1.layer.borderColor = [[UIColor blackColor] CGColor];
//
//        cell.button2.layer.cornerRadius = 10;
//        cell.button2.layer.borderColor = [[UIColor blackColor] CGColor];
//
//        cell.button3.layer.cornerRadius = 10;
//        cell.button3.layer.borderColor = [[UIColor blackColor] CGColor];
//        [ cell.button1 setTitle:@" " forState:UIControlStateNormal];
//        [ cell.button2 setTitle:@" " forState:UIControlStateNormal];
//        [ cell.button3  setTitle:@" " forState:UIControlStateNormal];
       
        NSLog(@"count%d",[radiolist count]);
        [cell.button3 setSelected:YES];
        
        
 
        [cell.button3 setSelected:NO];
        if([radiolist count]!=3)
        {
            
                 cell.button3.hidden=YES;
        }else{
         cell.button3.hidden=NO;
        }
     if([radiolist count]<=2)
     {
      //   cell.button3.hidden=YES;

        for(int i=0;i<[radiolist count];i++)
        {
            if(i==0)
            {
                if([selectedbtn isEqualToString:[radiolist[0][@"AnswerId"]description]])
                {
                   
                     [cell.button1 setSelected:YES];
 

                }
                else{
                    [cell.button1 setSelected:NO];
                    

                }
                [    cell.button1 setTitle: radiolist[0][@"AnswerValue"]forState: UIControlStateNormal];
                cell.button1.tag=indexPath.row;
                //cell.button1.text=radiolist[i][@"AnswerValue"];
            }
         else   if(i==1)
            {
                if([selectedbtn isEqualToString:[radiolist[1][@"AnswerId"]description]])
                {
                    
                    [cell.button2 setSelected:YES];
                    
                    
                }
                else{
                    [cell.button2 setSelected:NO];
                    
                    
                }
                   [    cell.button2 setTitle: radiolist[1][@"AnswerValue"]forState: UIControlStateNormal];
              //  cell.button1.titleLabel.text=radiolist[i][@"AnswerValue"];
                cell.button2.tag=indexPath.row;
            }
 
        }
        }
     else     if([radiolist count]>2)
     {
        // cell.button3.hidden=NO;

         for(int i=0;i<[radiolist count];i++)
         {
             if(i==0)
             {
                 if([selectedbtn isEqualToString:[radiolist[0][@"AnswerId"]description]])
                 {
                     
                     [cell.button1 setSelected:YES];
                     
                     
                 }
                 else{
                     [cell.button1 setSelected:NO];
                     
                     
                 }
                 [    cell.button1 setTitle: radiolist[0][@"AnswerValue"]forState: UIControlStateNormal];
                 cell.button1.tag=indexPath.row;
                 //cell.button1.text=radiolist[i][@"AnswerValue"];
             }
             else   if(i==1)
             {
                 if([selectedbtn isEqualToString:[radiolist[1][@"AnswerId"]description]])
                 {
                     
                     [cell.button2 setSelected:YES];
                     
                     
                 }
                 else{
                     [cell.button2 setSelected:NO];
                     
                     
                 }
                 [    cell.button2 setTitle: radiolist[1][@"AnswerValue"]forState: UIControlStateNormal];
                 //  cell.button1.titleLabel.text=radiolist[i][@"AnswerValue"];
                 cell.button2.tag=indexPath.row;
             }
             
             else   if(i==2)
             {
                 if([selectedbtn isEqualToString:[radiolist[2][@"AnswerId"]description]])
                 {
                     
                     [cell.button3 setSelected:YES];
                     
                     
                 }
                 else{
                     [cell.button3 setSelected:NO];
                     
                     
                 }
                 [    cell.button3 setTitle: radiolist[2][@"AnswerValue"]forState: UIControlStateNormal];
                 // cell.button1.titleLabel.text=radiolist[i][@"AnswerValue"];
                 cell.button3.tag=indexPath.row;
             }
         }
     }
        [cell.button1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cell.button2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

        [cell.button3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [cell.button1 setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
//        [cell.button2 setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
//
//        [cell.button3 setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
  [cell.button1 addTarget:self action:@selector(logSelectedButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell.button2 addTarget:self action:@selector(logSelectedButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell.button3 addTarget:self action:@selector(logSelectedButton:) forControlEvents:UIControlEventTouchUpInside];

        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm1.frame));
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
 
        cell.accessoryType = UITableViewCellAccessoryNone ;
        return cell;
    }
      else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Time"])
    {
        //if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Time"]) {
        DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
        if(_isFromEditNote  && !_IsEditNote)
            cell.userInteractionEnabled = NO;
        cell.lblValue.tag = indexPath.row;
        if(dataList1[indexPath.row][@"TimeAnswerText"] == [NSNull null]||[dataList1[indexPath.row][@"TimeAnswerText"]isEqualToString:@""]){
            cell.lblValue.text = @"";
        }else{
            cell.lblValue.text = dataList1[indexPath.row][@"TimeAnswerText"];
        }
        cell.lblTitle.attributedText = [self AddRequiredField:dataList1[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList1[indexPath.row][@"IsRequired"] boolValue]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
//    else {
//        DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
//        if(_isFromEditNote && !_IsEditNote)
//            cell.userInteractionEnabled = NO;
//       cell.lblTitle.attributedText = [self AddRequiredField:dataList1[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList1[indexPath.row][@"IsRequired"] boolValue]];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
//    else{
//      if([Is2AddMoreDisplay1 isEqualToString:@"1"])
//
//    {
//                LocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"location2cell"];
//        cell.lblValue.text=[[jsonDict1    valueForKeyPath:@"Data.Child2TestTypeName"]description];
//        cell.lblTitle.text=[[jsonDict1 valueForKeyPath:@"Data.Child2TestTypeName"]description];
//
//
//    }
//    else  if([IsAddMoreDisplay1 isEqualToString:@"1"])
//
//    {
//
//        LocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"location2cell"];
//        cell.lblValue.text=[[jsonDict1    valueForKeyPath:@"Data.ChildTestTypeName"]description];
//        cell.lblTitle.text=[[jsonDict1 valueForKeyPath:@"Data.ChildTestTypeName"]description];
//
//    }
//    }
    return nil;
    
}
- (IBAction)logSelectedButton:(DLRadioButton *)radioButton {
    if (radioButton.isMultipleSelectionEnabled) {
        for (DLRadioButton *button in radioButton.selectedButtons) {
            NSLog(@"%@ is selected.\n", button.titleLabel.text);
        }
    } else {
        [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
        self.saveBtn.enabled = YES;
        NSLog(@"%@ is selected.\n", radioButton.selectedButton.titleLabel.text);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[radioButton.tag]];
        [dic setObject: radioButton.selectedButton.titleLabel.text forKey:@"QuestionValue"];
    
        NSArray  *radiolist=dataList1[radioButton.tag][@"AnswersList"];
      
            for(int i=0;i<[radiolist count];i++)
            {
                if( [[radiolist[i][@"AnswerValue"]description]isEqualToString:radioButton.selectedButton.titleLabel.text])
                {
                    [dic setObject: radiolist[i][@"AnswerId"] forKey:@"AnswerId"];

                }
            }
             NSLog(@"dic : %@",dic);
        
       // [dataList1 replaceObjectAtIndex:radioButton.tag withObject:dic];
     //   [updateSelectedItems:dic questionID:dataList1[radioButton.tag][@"QuestionId"]];

        
        NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"QuestionId = %@",dataList1[radioButton.tag][@"QuestionId"]]];
        NSArray* filteredData = [dataList1 filteredArrayUsingPredicate:predicate];
        NSLog(@"Filter : %@",filteredData);
        if(filteredData.count) {
            
            NSInteger index = [dataList1 indexOfObject:filteredData[0]];
            NSLog(@"Index : %ld",(long)index);
            [dataList1 replaceObjectAtIndex:index withObject:dic];
         }
        NSLog(@"Came");
        NSLog(@"dataList1 : %@",dataList1);

        
          NSLog(@"dic : %@",dataList1);
        
    }
}

- (void)sliderValueChanges:(id)sender {
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    UISlider *slider = (UISlider *)sender;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[slider.tag]];
    [dic setObject:[NSString stringWithFormat:@"%.f",slider.value] forKey:@"NumericAnswer"];
    NSLog(@"dic : %@",dic);
    [dataList1 replaceObjectAtIndex:slider.tag withObject:dic];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:slider.tag inSection:0];
    
    SliderTableViewCell *cell = [_tblForm1 cellForRowAtIndexPath:indexPath];
    cell.txtValue.text = [NSString stringWithFormat:@"%.f",slider.value];
    
}
- (void)switchValueChanges:(id)sender {
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    UIButton *btn = (UIButton *)sender;
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[btn.tag]];
    [dic setObject:[sender isOn] ? [NSNumber numberWithBool:true]:[NSNumber numberWithBool:false] forKey:@"BooleanAnswer"];
    NSLog(@"dic : %@",dic);
    [dataList1 replaceObjectAtIndex:btn.tag withObject:dic];
}

- (IBAction)textFieldDidBeginEditing:(UITextField *)textField
 {
editingIndexPath1= [NSIndexPath indexPathForRow:textField.tag inSection:0];

}

//
//- (void)NumericTextBoxedit:(UITextField *)textField {
//
//        [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
//        self.saveBtn.enabled = YES;
//        //  [dataList1[indexPath.row][@"MaxValue"] floatValue];
//        float inte = [textField.text floatValue];
//        if (inte <=[dataList1[textField.tag][@"MaxValue"] floatValue])
//            //&& inte >=[dataList1[textField.tag][@"MinValue"] floatValue])
//        {
//            [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
//            self.saveBtn.enabled = YES;
//            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[textField.tag]];
//            [dic setObject:textField.text forKey:@"NumericAnswer"];
//            NSLog(@"dic : %@",dic);
//            [dataList1 replaceObjectAtIndex:textField.tag withObject:dic];
//        }
//        else
//        {
//            //  textField.textColor = [UIColor lightGrayColor];
//            textField.text = @"";
//            // [textField resignFirstResponder];
//
//        }
//
//
//    // [_tblForm1 reloadData];
//}



- (void)textFieldDidEndEditing:(UITextField *)textField {
  
        
        
 // indexpath = 0;
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[textField.tag]];
    [dic setObject:textField.text forKey:@"TextAnswer"];
    NSLog(@"dic : %@",dic);
    [dataList1 replaceObjectAtIndex:textField.tag withObject:dic];
 
    // [_tblForm1 reloadData];
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    
    if(textView.text.length == 0){
        textView.textColor = [UIColor lightGrayColor];
        textView.text = @"Type here ...";
        NSLog(@"textView : %ld",(long)textView.tag);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[textView.tag]];
        [dic setObject:textView.text forKey:@"TextAnswer"];
        NSLog(@"dic : %@",dic);
        [dataList1 replaceObjectAtIndex:textView.tag withObject:dic];
        
        [textView resignFirstResponder];
    }
    
    else{
        NSLog(@"textView : %ld",(long)textView.tag);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[textView.tag]];
        [dic setObject:textView.text forKey:@"TextAnswer"];
        NSLog(@"dic : %@",dic);
        [dataList1 replaceObjectAtIndex:textView.tag withObject:dic];
    }
    self.saveBtn.enabled = YES;
    
}


-(void) textFieldDidChange:(UITextField *)textField
{
    if([dataList1[textField.tag][@"DataType"] isEqualToString:@"NumericTextBox"])
    {
        
        [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
        self.saveBtn.enabled = YES;
        //  [dataList1[indexPath.row][@"MaxValue"] floatValue];
        float inte = [textField.text floatValue];
        if (inte <=[dataList1[textField.tag][@"MaxValue"] floatValue])
            //&& inte >=[dataList1[textField.tag][@"MinValue"] floatValue])
        {
            [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
            self.saveBtn.enabled = YES;
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[textField.tag]];
            [dic setObject:textField.text forKey:@"NumericAnswer"];
            NSLog(@"dic : %@",dic);
            [dataList1 replaceObjectAtIndex:textField.tag withObject:dic];
        }
      
        else
        {
            //  textField.textColor = [UIColor lightGrayColor];
            textField.text = @"";
            // [textField resignFirstResponder];
            
        }
        
        
        // [_tblForm1 reloadData];
    }else{

     [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
 
    NSLog(@"textView : %ld",(long)textField.tag);
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[textField.tag]];
    [dic setObject:textField.text forKey:@"TextAnswer"];
    NSLog(@"dic : %@",dic);
    [dataList1 replaceObjectAtIndex:textField.tag withObject:dic];
    }


}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    editingIndexPath1= [NSIndexPath indexPathForRow:textView.tag inSection:0];

    // if([self substring:@"null" existsInString:_admitingdiagnosis]) {
    if([textView.text isEqualToString:@"Type here ..."]){
        textView.text = @"";
        textView.textColor = [UIColor lightGrayColor];}
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
        [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
       self.saveBtn.enabled = YES;
    if([dataList1[textField.tag][@"DataType"] isEqualToString:@"NumericTextBox"])
    {
//         [dataList1[indexPath.row][@"MaxValue"] floatValue];
//        float inte = [textField.text floatValue];
//        if (inte <[dataList1[textField.tag][@"MaxValue"] floatValue] && inte > 0.0)
//        {
//            return YES;
//        }
//        else
//        {
//            textField.textColor = [UIColor lightGrayColor];
//            textField.text = @"Type here ...";
//            [textField resignFirstResponder];
//
//            return NO;
//        }
        [textField resignFirstResponder];

             return NO;
    }
     else{

    NSLog(@"textView : %ld",(long)textField.tag);
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[textField.tag]];
    [dic setObject:textField.text forKey:@"TextAnswer"];
    NSLog(@"dic : %@",dic);
    [dataList1 replaceObjectAtIndex:textField.tag withObject:dic];
    return YES;
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    
    if(![textView.text isEqualToString:@""]){
        textView.text =   dataList1[textView.tag][@"TextAnswer"];;
    }else{
        textView.text =@"Type here ...";
    }
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    NSLog(@"textView : %ld",(long)textView.tag);
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[textView.tag]];
    [dic setObject:textView.text forKey:@"TextAnswer"];
    NSLog(@"dic : %@",dic);
    [dataList1 replaceObjectAtIndex:textView.tag withObject:dic];
    //  [_tblForm1 reloadData];
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    //    if([text isEqualToString:@"\n"]) {
    //        [textView resignFirstResponder];
    //        return NO;
    //    }
    
    return YES;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat height = 80;
    if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Image"])
    {
        height=65;
    }
    
     else
        if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"placementlist"])
        {
            height=80;
        }else
            if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Category"])
            {
                CGSize size = self.tblForm1.frame.size;
                size.width = size.width - 16;
                height = [self calculateStringHeigth:size text:dataList1[indexPath.row][@"CategoryDescription"] andFont:[UIFont systemFontOfSize:13]];
                CGFloat height1;
                height1 = [self calculateStringHeigth:size text:dataList1[indexPath.row][@"CategoryName"] andFont:[UIFont systemFontOfSize:13]];
                if(![dataList1[indexPath.row][@"CategoryName"] length]&&![dataList1[indexPath.row][@"CategoryDescription"] length]){
                    
                    
                    
                    height = height + 37;
                }
                
                else
                    if([dataList1[indexPath.row][@"CategoryName"] isKindOfClass:[NSNull class]]||[dataList1[indexPath.row][@"CategoryName"] isEqualToString:@""]){
                        
                        
                        height = height + 12;
                    }
                    else {
                        
                        height = height +15+20;}
                
            }
            else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"NumericTextBox"])
                height = 65;
            else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Dropdown"]) {
                //height = 51;
                CGSize size = self.tblForm1.frame.size;
                size.width = size.width - 78;
                NSString *string = [dataList1[indexPath.row][@"IsRequired"] boolValue] ? [NSString stringWithFormat:@"%@ *",dataList1[indexPath.row][@"QuestionDisplayName"]] : dataList1[indexPath.row][@"QuestionDisplayName"];
                height = [self calculateStringHeigth:size text:string andFont:[UIFont systemFontOfSize:15]];
                
                
                NSString *string1 =  dataList1[indexPath.row][@"QuestionValue"];
                height =height+ [self calculateStringHeigth:size text:string1 andFont:[UIFont systemFontOfSize:15]];
                height = 60;
                NSLog(@"height%f",height);
            }
            else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Checkbox"])
                height = 54;
            else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"TextBoxMultiline"])
                height = 150;
            else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"TextBox"])
                height = 60;
            else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Dropdown(Multiple)"]) {
                CGSize size = self.tblForm1.frame.size;
                size.width = size.width - 78;
                NSString *string = [dataList1[indexPath.row][@"IsRequired"] boolValue] ? [NSString stringWithFormat:@"%@ *",dataList1[indexPath.row][@"QuestionDisplayName"]] : dataList1[indexPath.row][@"QuestionDisplayName"];
                height = [self calculateStringHeigth:size text:string andFont:[UIFont systemFontOfSize:15]];
                
                NSString *string1 =  dataList1[indexPath.row][@"QuestionValue"];
                height =height+ [self calculateStringHeigth:size text:string1 andFont:[UIFont systemFontOfSize:14]];
                //        if (height > 39.0f)
                //        {
                //            //we know exactly what will happen
                //            height = height+5 ;
                //        }
                //        else{
                height = 60;
                // }
                
                
                NSLog(@"Dropdown(Multiple)height%d",height);
            }
    //height = 51;
            else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Checkbox(Multiple)"]) {
                CGSize size = self.tblForm1.frame.size;
                size.width = size.width - 78;
                NSString *string = [dataList1[indexPath.row][@"IsRequired"] boolValue] ? [NSString stringWithFormat:@"%@ *",dataList1[indexPath.row][@"QuestionDisplayName"]] : dataList1[indexPath.row][@"QuestionDisplayName"];
                height = [self calculateStringHeigth:size text:string andFont:[UIFont systemFontOfSize:14]];
                
                NSString *string1 =  dataList1[indexPath.row][@"QuestionValue"];
                height =height+ [self calculateStringHeigth:size text:string1 andFont:[UIFont systemFontOfSize:14]];
                
                if (height > 39.0f)
                {
                    //we know exactly what will happen
                    height = height+5 ;
                }
                else{
                    height = 60;
                }
                
                
            }
    //height = 51;
            else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Date"])
                height = 60;
            else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Time"])
                height = 60;
            else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Radio"]) {
                CGSize size = self.tblForm1.frame.size;
                size.width = size.width - 78;
                NSString *string = dataList1[indexPath.row][@"QuestionDisplayName"];
                height = [self calculateStringHeigth:size text:string andFont:[UIFont systemFontOfSize:14]];
                
                NSString *string1 =   dataList1[indexPath.row][@"QuestionValue"];
                height =height+ [self calculateStringHeigth:size text:string1 andFont:[UIFont systemFontOfSize:14]];
                
                height = 62;
            }
    //height = 51;
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.view endEditing:YES];
    self.saveBtn.enabled = YES;
    editingIndexPath1=indexPath;
    rowOfTheCell1=indexPath.row;
  if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Image"])
    {
//
//        [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
//        self.saveBtn.enabled = YES;
//
//        [self performSegueWithIdentifier:@"ImageselectionSegue" sender:self];
        
    }
    if(([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Dropdown(Multiple)"]) || ([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Checkbox(Multiple)"]) || ([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Dropdown"])  ) {
        
        
        [self performSegueWithIdentifier:@"selectionsegue" sender:indexPath];
    }
    else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Date"])
    {
        DateAndTimeTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        [self showDatePicker:UIDatePickerModeDate currentLbl:cell.lblValue];
    }
    else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Time"])
    {
        DateAndTimeTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        [self showDatePicker:UIDatePickerModeTime currentLbl:cell.lblValue];
        
    }
    else if([dataList1[indexPath.row][@"DataType"] isEqualToString:@"Checkbox"]) {
        
    }
}


-(void)location_taping:(UITapGestureRecognizer *)recognizer {
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    rowOfTheCell1=recognizer.view.tag;
    [self performSegueWithIdentifier:@"ImageselectionSegue" sender:self];
    
    // [self performSegueWithIdentifier:@"dynamiccamera" sender:indexPath];
    
}


-(void)viewimag_taping:(UITapGestureRecognizer *)recognizer {
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    rowOfTheCell1=recognizer.view.tag;
    [self performSegueWithIdentifier:@"ImageselectionSegue" sender:self];
    
    // [self performSegueWithIdentifier:@"dynamiccamera" sender:indexPath];
    
}



-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    [self performSegueWithIdentifier:@"dynamiccamera" sender:self];
    
    // [self performSegueWithIdentifier:@"dynamiccamera" sender:indexPath];
    
}



-(void) showDatePicker: (UIDatePickerMode) modeDatePicker currentLbl:(UILabel *)lbl
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    if(modeDatePicker == UIDatePickerModeDate) {
       
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd/yyyy"];
        NSString *dateanswerstr=dataList1[rowOfTheCell1][@"DateAnswerText"];
        if([dateanswerstr isEqualToString:@""]||[dateanswerstr isKindOfClass:[NSNull class]]){}else{
   NSData *date=[formatter dateFromString:dateanswerstr];
            [picker setDate:date];}
    }else{
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
     [formatter setDateFormat:@"hh:mm a"];
        NSString *dateanswerstr=dataList1[rowOfTheCell1][@"TimeAnswerText"];;
        if([dateanswerstr isEqualToString:@""]||[dateanswerstr isKindOfClass:[NSNull class]]){}else{
            NSData *date=[formatter dateFromString:dateanswerstr];
            [picker setDate:date];}
    }
    
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                     if(modeDatePicker == UIDatePickerModeDate) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                                        NSString *datestring= [formatter stringFromDate:picker.date];
                                         
                                         lbl.text=datestring;
                                         
                                         NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[lbl.tag]];
                                         [dic setObject:[formatter stringFromDate:picker.date] forKey:@"DateAnswerText"];
                                         [dic setObject:[formatter stringFromDate:picker.date]forKey:@"DateAnswer"];
                                         [dic setObject:[formatter stringFromDate:picker.date] forKey:@"DisplayDateAnswerText"];
                                         NSLog(@"dic : %@",dic);
                                         [dataList1 replaceObjectAtIndex:lbl.tag withObject:dic];
                                     }
                                     else {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"hh:mm a"];
                                         NSString *datestring= [formatter stringFromDate:picker.date];
                                         
                                         lbl.text=datestring;
                                         
                                         
                                         NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[lbl.tag]];
                                         
                                         [dic setObject:[formatter stringFromDate:picker.date]forKey:@"TimeAnswer"];
                                         [dic setObject:lbl.text forKey:@"TimeAnswerText"];
                                         
                                         [dic setObject:lbl.text forKey:@"DisplayTimeAnswerText"];
                                         NSLog(@"dic : %@",dic);
                                         [dataList1 replaceObjectAtIndex:lbl.tag withObject:dic];
                                     }
                                     
                                 }];
    [alertController addAction:doneAction];
    
    
    UIAlertAction *clearAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                      if(modeDatePicker == UIDatePickerModeDate) {
                                          
                                          NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[lbl.tag]];
                                          [dic setObject:@"" forKey:@"DateAnswer"];

                                          [dic setObject:@"" forKey:@"DateAnswerText"];
                                          [dic setObject:@"" forKey:@"DisplayDateAnswerText"];
                                          [dataList1 replaceObjectAtIndex:lbl.tag withObject:dic];
                                          
                                      }else{
                                          
                                          
                                          NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList1[lbl.tag]];
                                          [dic setObject:@""forKey:@"TimeAnswer"];

                                          [dic setObject:@""forKey:@"TimeAnswerText"];
                                          
                                          [dic setObject:@"" forKey:@"DisplayTimeAnswerText"];
                                          [dataList1 replaceObjectAtIndex:lbl.tag withObject:dic];
                                          
                                      }
                                      NSLog(@"Clear action");
                                      lbl.text=@"";
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    [alertController addAction:cancelAction];

    
    [alertController addAction:clearAction];

    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController  animated:YES completion:nil]; }

-(void)setSelectedDateInField
{
    NSLog(@"date :: %@",datePicker1.date.description);
    //set Date formatter
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"MMMM dd, yyyy"];
    
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"childform_segue"])
    {
        
        NSIndexPath *indexpath = (NSIndexPath *)sender;
        childform_list *vc = [segue destinationViewController];
        vc.TaskName=_TaskName;
        vc.ProjectName=_ProjectName;
        vc.JobNumber=_JobNumber;
        vc.Testtype_ID= Child2TestTypeId1;
        vc.WorkorderId = _WorkorderId;
    }else
    if ([[segue identifier] isEqualToString:@"suborderlist_segue"])
    {
        
        NSIndexPath *indexpath = (NSIndexPath *)sender;
        Suborder_listVC *vc = [segue destinationViewController];
        vc.TaskName=_TaskName;
        vc.ProjectName=_ProjectName;
        vc.JobNumber=_JobNumber;
        vc.Testtype_ID=ChildTestTypeId1;
          vc.WorkorderId = _WorkorderId;
    }else
    if ([[segue identifier] isEqualToString:@"ImageselectionSegue"])
    {
        
        NSIndexPath *indexpath = (NSIndexPath *)sender;
        Image_SelectionViewController *vc = [segue destinationViewController];
        vc.listInfo = dataList1[rowOfTheCell1];
        vc.WorkorderId = _WorkorderId;
        vc.TaskName=_TaskName;
        vc.ProjectName=_ProjectName;
        vc.JobNumber=_JobNumber;
    }
    
    
    else
        if ([[segue identifier] isEqualToString:@"samplesegue"])
        {
            
            NSIndexPath *indexpath = (NSIndexPath *)sender;
            sampledSelectionViewController *vc = [segue destinationViewController];
             vc.delegate = self;
            vc.TaskName=_TaskName;
            vc.ProjectName=_ProjectName;
            vc.JobNumber=_JobNumber;
            
            vc.selectedList=[[NSUserDefaults standardUserDefaults] valueForKey:@"sampled_array"];
          //  [jsonDict1 valueForKeyPath:@"Data.SampleIdArray"];
        }
    else
    if ([[segue identifier] isEqualToString:@"selectionsegue"])
    {
  
        NSIndexPath *indexpath = (NSIndexPath *)sender;
        SelectionViewController *vc = [segue destinationViewController];
        vc.listInfo = dataList1[indexpath.row];
        vc.delegate = self;
        vc.TaskName=_TaskName;
        vc.ProjectName=_TaskName;
        vc.JobNumber=_JobNumber;
        
        vc.Workorder_type=_childname;
    }
    else if([[segue identifier] isEqualToString:@"dynamiccamera"]){
        dynamiccameraVC *vc = [segue destinationViewController];
        vc.TaskName=_TaskName;
        vc.ProjectName=_ProjectName;
        vc.JobNumber=_JobNumber;
    }
}


 

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 - (IBAction)cancelAction:(id)sender {
     DownloadManager *downloadObj = [[DownloadManager alloc]init];
     
     [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@DeletePlacementRecord?WorkOrderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"DeletePlacementRecord"];
 [self.navigationController popViewControllerAnimated:YES];
   //  [self performSegueWithIdentifier:@"backtoorders" sender:self];
     
}

- (IBAction)alertaction:(id)sender {
    
    [self performSegueWithIdentifier:@"alertsegue" sender:self];
    
}
-(BOOL) isValidatenumeric:(NSArray *)list {
    
    for(NSDictionary *dic in list) {
        
        
        if([dic[@"DataType"] isEqualToString:@"NumericTextBox"])
        {
            if([[NSString stringWithFormat:@"%@",dic[@"NumericAnswer"]] length]  >0){
                NSLog(@"Numerictext%@%@",dic,dic[@"NumericAnswer"] );
                
                if([dic[@"NumericAnswer"] floatValue] < [dic[@"MinValue"] floatValue])
                {
                    return NO;
                }
                else if([dic[@"NumericAnswer"] floatValue] > [dic[@"MaxValue"] floatValue])
                {
                    return NO;
                }
                else if([dic[@"NumericAnswer"] floatValue]==0)
                {
                    return YES;
                }
            }
            
            //
            //            else{
            //                return NO;
            //            }
            
        }
    }
    
    return YES;
    
}

-(BOOL) isValidate:(NSArray *)list {
    
    for(NSDictionary *dic in list) {
        
        if([dic[@"IsRequired"] boolValue])
        {
            NSLog(@"boolValue%@",dic);

            if([dic[@"DataType"] isEqualToString:@"NumericTextBox"])
            {
                if([[NSString stringWithFormat:@"%@",dic[@"NumericAnswer"]] length]  >0){
                    NSLog(@"Numerictext%@%@",dic,dic[@"NumericAnswer"] );

                    if([dic[@"NumericAnswer"] floatValue] < [dic[@"MinValue"] floatValue])
                    {
                        return YES;
                    }
                    else if([dic[@"NumericAnswer"] floatValue] > [dic[@"MaxValue"] floatValue])
                    {
                        return YES;
                    }
                    
                }
                
                
                else{
                    return NO;
                }
                
            }else            if([dic[@"DataType"] isEqualToString:@"Dropdown"])
            {
                if(![dic[@"QuestionValue"] length] ){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"TextBoxMultiline"])
            {
                if(![dic[@"TextAnswer"] length] ){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"TextBox"])
            {
                if(![dic[@"TextAnswer"] length] ){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"Checkbox"])
            {
                // if(!dic[@"BooleanAnswer"] ){
                if(![dic[@"BooleanAnswer"] boolValue]){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"Dropdown(Multiple)"])
            {
                if(![dic[@"QuestionValue"] length] ){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"Checkbox(Multiple)"])
            {
                if(![dic[@"QuestionValue"] length] ){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"Radio"])
            {
                if(![dic[@"QuestionValue"] length] ){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"Date"])
            {
                NSLog(@"DateAnswer%@",dic[@"DateAnswer"]);
                if(![dic[@"DateAnswer"] isKindOfClass:[NSString class]]||[dic[@"DateAnswer"] isKindOfClass:[NSNull class]]||dic[@"DateAnswer"]==nil)
                {
                    return NO;
                }else{
                if(![dic[@"DateAnswer"] length] ){
                    return NO;
                }
                }
                
            }
            else if([dic[@"DataType"] isEqualToString:@"Time"])
            {
                
                if(![dic[@"TimeAnswer"] isKindOfClass:[NSString class]]||[dic[@"TimeAnswer"] isKindOfClass:[NSNull class]]||dic[@"TimeAnswer"]==nil)
                {
                    return NO;
                }else{
                if(![dic[@"TimeAnswer"] length] ){
                    return NO;
                }
                }
              
            }
        }
    }
    
    return YES;
    
}
 
- (IBAction)saveAction:(id)sender {
    
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
        return;
    }else{
        
        Boolean locationbool;
        NSMutableArray  *alertList;
 
        if([IsAddMoreDisplay1 isEqualToString:@"1"])
            
        {
            NSMutableURLRequest *request= [[NSMutableURLRequest alloc]init];
            NSString *str=   [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId];
            NSLog(@"url%@",[NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId]);
            [request setURL:[NSURL URLWithString:str]];
            [request setHTTPMethod:@"GET"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            NSError *error;
            NSURLResponse *response;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            NSDictionary *jsonDict1= [NSJSONSerialization JSONObjectWithData:urlData
                                                                    options:kNilOptions error:&error];
            NSLog(@"jsonDict1%@",jsonDict1);
            
          alertList=[[NSMutableArray alloc] initWithArray:[jsonDict1 valueForKeyPath:@"Data.SubWorkOrderList1"]];
            
            if([alertList count]>0)
            {
                locationbool=true;
            }
            else{
                  locationbool=false;
            }
            
            
        }
        else{
           locationbool=true;
        }
        
        if(locationbool){
    if([self isValidate:dataList1]){
         if([self isValidatenumeric:dataList1]){
       if (![self isNetworkAvailable]) {
            [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
            return;
        }else{
            [[self view]endEditing:YES];
            NSString *iscompletestr;
            if(iscomplete1){
            iscompletestr=@"true";
            }else{
            iscompletestr=@"false";
            }
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            if([[[NSUserDefaults standardUserDefaults] objectForKey:@"FileName"] length]>0){
              int index = 0;
            NSMutableArray *tempAry = [[NSMutableArray alloc] initWithArray:dataList1];
            

            for(NSDictionary *dic in dataList1) {
                
                if([dic[@"DataType"] isEqualToString:@"Image"])
                {
                    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
                    NSString *aString1=dataList1[index][@"TextAnswer"];
                    NSString *aString;
                    
                    
                    aString = [aString1 stringByAppendingFormat:@",%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"FileName"]];
                    
                    [tempDic setObject:aString forKey:@"TextAnswer"];
                    [tempAry replaceObjectAtIndex:index withObject:tempDic];
                }
                index++;
            }
            
            dataList1  = [[NSMutableArray alloc] initWithArray:tempAry];
            }
                [self.saveBtn setTitleColor:[UIColor  colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1.0] forState:UIControlStateNormal];
              ///  NSLog(@"json%@",dataList1);
            NSDictionary *json ;
         
            json = @{
                                     
                                       @"RoleName": [[NSUserDefaults standardUserDefaults] objectForKey:userRole],
                                       @"UserName": [[NSUserDefaults standardUserDefaults] objectForKey:username],
                                       @"iscomplete" : @true,
                                       @"WorkOrderId" :_WorkorderId,
                                       @"timespent" : @"0",
                                       @"List" :dataList1
                                       
                                       };
           // }
                NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
                [dictEntry setObject:json forKey:[NSString stringWithFormat:@"WorkOrderModel"]];
            NSLog(@"dictEntry %@",dictEntry);

                DownloadManager *downloadObj = [[DownloadManager alloc]init];
                [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@UpdateWorkOrderDetails",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"UpdateWorkOrderDetails"];
         }
         }  else {
           UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please fill values within range" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
           [callAlert show];
       }
    }
    else {
         UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please fill required fields" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
               [callAlert show];
    }
        }
        else {
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"No density test results are added. Minimum one result is needed to complete the report. Please check." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
        }
        
    
    
}
}
#pragma mark - API Delegate

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Server may be busy. Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


- (IBAction)logoutAction:(id)sender {
       [self logoutUser];
    
}

//- (IBAction)changepasswordAction:(id)sender {
//    [self changePassword];
//}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (0 == buttonIndex && alertView.tag == 55)
    {
//            NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
//
//            for (int i=1; i<=img_count;i++ )
//            {
//
//                [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:[NSString stringWithFormat:@"image%d", img_count]];
//
//            }
//            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"img_count"];
//        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"imgList"];
         [self.navigationController popViewControllerAnimated:YES];
 
    }
}

- (void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
    [[self presentingViewController] dismissViewControllerAnimated:NO completion:nil]
    ;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];
    
    //    if( [[[NSUserDefaults standardUserDefaults] objectForKey:@"IsDefaultRoundList"] isEqualToString:@"1"]){
    //
    //
    //        rootViewController.selectedIndex = 3;
    //
    //    }else{
    
    rootViewController.selectedIndex = 1;
    
    // }
    [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
    [self.navigationController popViewControllerAnimated:YES];

   // [self performSegueWithIdentifier:@"backtoorders" sender:self];
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"response : %@",response);
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    if ([key isEqualToString:@"UpdateWorkOrderDetails"])
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FileName"];
            
//            NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
//
//            for (int i=1; i<=img_count;i++ )
//            {
//
//                [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:[NSString stringWithFormat:@"image%d", img_count]];
//
//            }
//            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"img_count"];
//            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"imgList"];
            [self.navigationController popViewControllerAnimated:YES];
            
//            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//              callAlert.tag = 55;
//            [callAlert show];
         
        }
        else{
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //  callAlert.tag = 55;
            [callAlert show];
        }
    }
    
    
    else if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efielddata Message" message:@"Please try again later"];
        }
    }
    
}
//
//- (IBAction)iscomplete:(id)sender {
//    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
//    self.saveBtn.enabled = YES;
//    UIButton *btn = (UIButton *)sender;
//    if([sender isOn]){
//        iscomplete1=true;
//    } else{
//        iscomplete1=false;
//    }
//}
//
//- (IBAction)time_spent:(id)sender {
//
//    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
//    self.saveBtn.enabled = YES;
//    UISlider *slider = (UISlider *)sender;
//    float roundedValue;
//    float increment = 0.25;
//  if ([sender isEqual:slider]){ //remove if you only have one slider
//
////        print((float)((int)((sender.value + 2.5) / 2.5) * 2.5));
////        sender.setValue((Float)((Int)((sender.value + 2.5) / 5) * 5), animated: false)
////        float newValue = slider.value /increment;
//        // slider.value= ((float)((int)(slider.value  / 0.25) * 0.25));
//
//
//   roundedValue = roundf((slider.value +0.25) / 0.25f) * 0.25f;
//      slider.value=roundedValue;
//    }
//    NSLog(@"Current value of slider is %.2f",   slider.value);
//
//
//    _timespentfield.text = [NSString stringWithFormat:@"%.2f",  slider.value];
//}
@end
