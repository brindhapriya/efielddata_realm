//
//  DynamicNoteFormVC.h
//  Efield
//
//  Created by Praveen Kumar on 12/02/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextViewTableViewCell.h"
#import "TextFieldTableViewCell.h"
#import "SwitchTableViewCell.h"
#import "SingleSelectionTableViewCell.h"
#import "MultySelectionTableViewCell.h"
#import "SelectionViewController.h"
#import "DateAndTimeTableViewCell.h"
#import "DescriptionTableViewCell.h"
#import "SliderTableViewCell.h"
#import "ViewController.h"

@interface Suborder_FormVC : ViewController
{
    NSMutableArray *dataList,*SubWorkOrderList;
    NSString *IsAddMoreDisplay;
    NSString *ChildTestTypeId;
    __weak IBOutlet UILabel *lblTitle;
    UIDatePicker *datePicker;

}

@property (weak, nonatomic) IBOutlet UILabel *Header_lbl;

@property (weak, nonatomic) IBOutlet UILabel *taskname_lbl;
@property (weak, nonatomic) IBOutlet UILabel *jobnumber;
@property(nonatomic)BOOL *isFromEditNote;
@property (nonatomic) BOOL IsEditNote;

@property (nonatomic, retain)NSString *assessmentMasterId;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UILabel *alertcnt;

@property(nonatomic, retain)NSString *WorkorderId,*Sub_WorkorderId,*Testtype_ID;
 
@property (weak, nonatomic) IBOutlet UIView *location;
@property (nonatomic, retain)NSString *TaskName,*JobNumber,*ProjectName,*childname;

- (IBAction)saveAction:(id)sender;
- (IBAction)cancelAction:(id)sender;
- (IBAction)alertaction:(id)sender;

- (IBAction)logoutAction:(id)sender;
 
@end
