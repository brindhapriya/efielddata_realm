//
//  DynamicNoteFormVC.h
//  Efield
//
//  Created by Praveen Kumar on 12/02/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextViewTableViewCell.h"
#import "TextFieldTableViewCell.h"
#import "SwitchTableViewCell.h"
#import "SingleSelectionTableViewCell.h"
#import "MultySelectionTableViewCell.h"
#import "SelectionViewController.h"
#import "DateAndTimeTableViewCell.h"
#import "DescriptionTableViewCell.h"
#import "SliderTableViewCell.h"
#import "ViewController.h"


@interface PlacementFormVC : ViewController<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *dataList1,*SubWorkOrderList1;
    NSString *IsAddMoreDisplay1,*Is2AddMoreDisplay1;
    NSString *ChildTestTypeId1,*Child2TestTypeId1,*TestTypeId1;
    __weak IBOutlet UILabel *lblTitle1;
    UIDatePicker *datePicker1;

}


 
-(void)updateSelectedItems:(NSDictionary *)updatedItem questionID:(NSString *)questionID;
 
//@property (weak, nonatomic) IBOutlet UIView *sampledataview;
//@property (weak, nonatomic) IBOutlet UILabel *sampled_date;
@property (weak, nonatomic) IBOutlet UILabel *date;

@property (weak, nonatomic) IBOutlet UILabel *companynametxt;
@property (weak, nonatomic) IBOutlet UILabel *taskname_lbl;
@property (weak, nonatomic) IBOutlet UILabel *jobnumber;
- (IBAction)iscomplete:(id)sender;
- (IBAction)time_spent:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *timespentfield;
@property (weak, nonatomic) IBOutlet UILabel *Header_lbl;
@property(nonatomic)BOOL *isFromEditNote;
@property (nonatomic) BOOL IsEditNote;

@property (nonatomic, retain)NSString *assessmentMasterId;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UILabel *alertcnt;
@property (nonatomic, retain) NSString *TaskName,*JobNumber,*ProjectName,*childname;
@property (weak, nonatomic) IBOutlet UISlider *timespentslider;
 
@property (weak, nonatomic) IBOutlet UISwitch *completeswitch;
@property (weak, nonatomic) IBOutlet UILabel *company_name;
@property (weak, nonatomic) IBOutlet UILabel *locationlink;
@property (weak, nonatomic) IBOutlet UILabel *locationlbl;
@property (weak, nonatomic) IBOutlet UILabel *dividerlbl;

@property (nonatomic, retain)NSString *WorkorderId;
@property (nonatomic, retain)NSString *noteTitle;
@property (weak, nonatomic) IBOutlet UIView *location;

- (IBAction)saveAction:(id)sender;
- (IBAction)cancelAction:(id)sender;
- (IBAction)alertaction:(id)sender;

- (IBAction)logoutAction:(id)sender;
 
@end
