                             //
//  AlertListVC.m
//  Efielddata
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import "Suborder_listVC.h"
#import "Constants.h"  
#import "Suborder_FormVC.h"
 #import "suborderlistTableViewCell.h"
@interface Suborder_listVC ()
{
    BOOL isOnEditing;
    BOOL isMarkAll;
    __weak IBOutlet UILabel *titleLbl;
     NSMutableArray *searchResultArray;
    NSMutableArray *searchFullResultArray;
    NSInteger selectedRowIndex,deletetag;
    int selectedUtilityIndex;
    NSString *places;
   }
@end

@implementation Suborder_listVC

- (void)viewDidLoad {
      
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    _childname = [_childname stringByReplacingOccurrencesOfString:@"+"
                                                       withString:@""];
      _Header_lbl.text=_childname;
  //  [self.view addGestureRecognizer:tap];
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
   // [self updateALERTcount];
//    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"searchText"];
//    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"searchText_alert"];
//    
//    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"patientlistflag"];
    self.taskname_lbl.text= _ProjectName   ;
    self.jobnumber.text=[NSString stringWithFormat:@"%@ - %@", _JobNumber,_TaskName];
    
    self.navigationController.navigationBar.hidden = YES;
    alertList = [[NSMutableArray alloc]init];
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor whiteColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self
                       action:@selector(updateData)
             forControlEvents:UIControlEventValueChanged];
    [suborder_listTable addSubview:refreshControl];
   suborder_listTable.separatorStyle = UITableViewCellSeparatorStyleNone;

}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    //   [aTextField resignFirstResponder];
}
- (void)viewWillAppear:(BOOL)animated
{
    
 
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
 
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@AutoUpdateSubWorkOrderList?WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"AutoUpdateSubWorkOrderList"];
    
         
     
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        return 40;
    
}




- (void)copyClick:(UITapGestureRecognizer *)tapGesture {
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@CopySubWorkOrder?SubWorkorderId=%@&UserName=%@&TestTypeId=%@&WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL], [[alertList valueForKey:@"SubWorkorderId"] objectAtIndex:tapGesture.view.tag],[[NSUserDefaults standardUserDefaults] objectForKey:username],_Testtype_ID,_WorkorderId] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"CopySubWorkOrder"];
    
    
                    
    
    
    
    
}



- (void)deleteClick:(UITapGestureRecognizer *)tapGesture {
    
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:@"Are you sure to delete this item?"  delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil];
    callAlert.tag = 56;
    [callAlert show];
    deletetag=tapGesture.view.tag;
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
 
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        NSString *cellIdentifier = @"subordercell";
        suborderlistTableViewCell *cell = (suborderlistTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(indexPath.row % 2 == 0)
    {
        cell.backgroundColor = [UIColor  clearColor];
        cell.orderview.backgroundColor = [UIColor whiteColor];
      
    }
    else
    {
        cell.backgroundColor = [UIColor  clearColor];
        cell.orderview.backgroundColor = [UIColor  colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0];
    }
 
    
    CGFloat borderWidth = 1.0f;
    if(indexPath.row==0)
    {
        cell.header_view.hidden=NO;
    }
    else
    {
        cell.header_view.hidden=YES;
    }
//    //
//    cell.orderview.layer.borderColor = [UIColor colorWithRed:0.0/255.0 green:156.0/255.0 blue:21.0/255.0 alpha:1].CGColor;
//    cell.orderview.layer.borderWidth = borderWidth;
//    //
//    cell.orderview.layer.cornerRadius = 5.0f;
    UITapGestureRecognizer *deleteAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteClick:)];
    
    deleteAction.delegate =self;
    deleteAction.numberOfTapsRequired = 1;
  cell.delete_btn.userInteractionEnabled = YES;
    for (UITapGestureRecognizer *reco in cell.delete_btn.gestureRecognizers) {
        [cell.delete_btn removeGestureRecognizer:reco];
    }
cell.delete_btn.tag=indexPath.row;
    
    [cell.delete_btn addGestureRecognizer:deleteAction];
    
    
    UITapGestureRecognizer *copyAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(copyClick:)];
    
    
    
    
    
    copyAction.delegate =self;
    copyAction.numberOfTapsRequired = 1;
    cell.copyview.userInteractionEnabled = YES;
    for (UITapGestureRecognizer *reco in cell.copyview.gestureRecognizers) {
        [cell.copyview removeGestureRecognizer:reco];
    }
    cell.copyview.tag=indexPath.row;
    
    [cell.copyview addGestureRecognizer:copyAction];
    copyAction.delegate =self;
copyAction.numberOfTapsRequired = 1;
    cell.copybtn.userInteractionEnabled = YES;
    for (UITapGestureRecognizer *reco in cell.copybtn.gestureRecognizers) {
        [cell.copybtn removeGestureRecognizer:reco];
    }
    cell.copybtn.tag=indexPath.row;
    
    [cell.copybtn addGestureRecognizer:copyAction];
    
    
    cell.count.text=[NSString stringWithFormat:@"%d",indexPath.row+1];
    
    
    NSArray *SubWorkOrderDetailDisplayList = [[alertList valueForKey:@"SubWorkOrderDetailDisplayList"]objectAtIndex:indexPath.row];
    cell.locationname.text =@"";
    for(int i=0;i<[SubWorkOrderDetailDisplayList count];i++)
    {
        if(i<=3)
        {
            
//            if([[[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:i] isKindOfClass:[NSNull class]] &&  [[[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:i] isEqualToString:@""])
//            {
//                cell.locationname.text =cell.locationname.text;
//            }else{
                if(    [cell.locationname.text  isEqualToString:@""])
                {
                    if([[[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:i] isKindOfClass:[NSNull class]]|| [[[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:i] isEqualToString:@""])
                    {
                    }else{
                    cell.locationname.text   =  [[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:i];
                    }
                }else{
                    if([[[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:i] isKindOfClass:[NSNull class]]|| [[[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:i] isEqualToString:@""])
                    {
                    }else{
                    cell.locationname.text   = [NSString stringWithFormat:@"%@, %@",cell.locationname.text,[[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:i]];
                    }
                }
          //  }
//            if([[[SubWorkOrderDetailDisplayList valueForKey:@"Question"] objectAtIndex:0] isKindOfClass:[NSNull class]])
//            {
//                cell.locationname.text =@"";
//            }else{
//        cell.locationname.text  = [[SubWorkOrderDetailDisplayList valueForKey:@"Question"] objectAtIndex:0];
//            }
//            if([[[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:0] isKindOfClass:[NSNull class]])
//            {
//                cell.locationname.text =cell.locationname.text;
//            }else{
//              cell.locationname.text   = [NSString stringWithFormat:@"%@,%@",cell.locationname.text,[[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:0]];
//            }
//            }
//        else  if(i==1)
//        {
//
//            if([[[SubWorkOrderDetailDisplayList valueForKey:@"Question"] objectAtIndex:1] isKindOfClass:[NSNull class]])
//            {
//                cell.locationname.text =cell.locationname.text;
//            }else{
//            cell.Question2_lbl.text   = [[SubWorkOrderDetailDisplayList valueForKey:@"Question"] objectAtIndex:1];
//            }
//            if([[[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:1] isKindOfClass:[NSNull class]])
//            {
//                cell.Question2_value.text =@"";
//            }else{
//            cell.Question2_value.text   = [[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:1];
//            }
//        }else  if(i==2)
//        {
//            if([[[SubWorkOrderDetailDisplayList valueForKey:@"Question"] objectAtIndex:2] isKindOfClass:[NSNull class]])
//            {
//                cell.Question3_lbl.text =@"";
//            }else{
//            cell.Question3_lbl.text   = [[SubWorkOrderDetailDisplayList valueForKey:@"Question"] objectAtIndex:2];
//            }
//            if([[[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:2] isKindOfClass:[NSNull class]])
//            {
//                cell.Question3_value.text =@"";
//            }else{
//            cell.Question3_value .text  = [[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:2];
//            }
//        }else if(i==3)
//        {
//            if([[[SubWorkOrderDetailDisplayList valueForKey:@"Question"] objectAtIndex:3] isKindOfClass:[NSNull class]])
//            {
//                cell.Question4_lbl.text =@"";
//            }else{
//            cell.Question4_lbl .text  = [[SubWorkOrderDetailDisplayList valueForKey:@"Question"] objectAtIndex:3];
//            }
//            if([[[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:3] isKindOfClass:[NSNull class]])
//            {
//                cell.Question4_lbl.text =@"";
//            }else{
//            cell.Question4_value.text   = [[SubWorkOrderDetailDisplayList valueForKey:@"Answer"] objectAtIndex:3];
//            }
       }
}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;

    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        NSLog(@"[alertList count] %lu",(unsigned long)[alertList count]);
        return [alertList count];
 
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _Sub_WorkorderId=[[[alertList valueForKey:@"SubWorkorderId"] objectAtIndex:indexPath.row]description];
    
    [self performSegueWithIdentifier:@"sub_form_segue" sender:nil];
    
}

 //
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 57)
    {
        self.tabBarController.selectedIndex = 0;
    }
  else  if ( alertView.tag == 56)
    {
        if (1 == buttonIndex)
        {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        
        
        NSURLSession *session = [NSURLSession sharedSession];
        
        [[session dataTaskWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@DeleteSubLocation?SubWorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL], [[alertList valueForKey:@"SubWorkorderId"] objectAtIndex:deletetag]]]
                completionHandler:^(NSData *data,
                                    NSURLResponse *response,
                                    NSError *error) {
                    // handle response
                    if (error) {
                        
                        
                        UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Server Error - Please try again" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        callAlert.tag = 57;
                        [callAlert show];
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    }else
                    if(data != nil)
                    {
                        //[MBProgressHUD hideHUDForView:self.view animated:YES];
                        //       [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        
                        [self updateData];
                        
                    }
                    else
                    {
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                        
                    }
                    
                }] resume];
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            });
            
        }
        
    }
else    if (alertView.tag == 58)
    {
     
    }
}


#pragma mark - API Delegate

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
   // [[UIApplication sharedApplication] endIgnoringInteractionEvents];
     UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:response delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    callAlert.tag = 57;
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    if ([key isEqualToString:@"GetSubWorkOrderDetails"])
    {
       
    //    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
     
        alertList=[[NSMutableArray alloc]initWithArray:[response valueForKeyPath:@"Data.List"]];
    }
    if ([key isEqualToString:@"AutoUpdateSubWorkOrderList"])
    {
        [self updateData];
    }
    if ([key isEqualToString:@"CopySubWorkOrder"])
    {
        
        //    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        _Sub_WorkorderId=   [response valueForKeyPath:@"Data.SubWorkOrderId"];
          [self performSegueWithIdentifier:@"sub_form_segue" sender:nil];
    
    }
    //CopySubWorkOrder
     else if ([key isEqualToString:@"SubmitCancelWoForApp"])
    {   [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    
        [self viewDidLoad];
        [self viewWillAppear:YES];
    }
    else if ([key isEqualToString:@"UpdateUserAppId"])
    {
        
        NSLog(@"app id uploaded response %@",response);
    }
    else if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efielddata Message" message:@"Please try again later"];
        }
    }
   
}

-(void) showDatePicker: (UIDatePickerMode) modeDatePicker currentLbl:(UILabel *)lbl
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                     if(modeDatePicker == UIDatePickerModeDate) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                                         NSString *datestring= [formatter stringFromDate:picker.date];
                                         
                                         self->jobdate.text=datestring;
                                         NSArray *temp;
                                         temp = [tempArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"JobDateTimeText CONTAINS[c] %@",datestring]];
                                     alertList = (NSMutableArray*)temp;
                                     [suborder_listTable reloadData];
                                     }
                                     
                                     
                                 }];
    [alertController addAction:doneAction];
    
    
    UIAlertAction *clearAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                    
                                      self->jobdate.text=@"";
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    [alertController addAction:cancelAction];
    
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController  animated:YES completion:nil]; }


- (IBAction)cancelAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)add_action:(id)sender {
    _Sub_WorkorderId=@"0";
   // [self performSegueWithIdentifier:@"suborderform_segue" sender:self];
    [self performSegueWithIdentifier:@"sub_form_segue" sender:self];

    
}

- (IBAction)cancel_action:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    

}

- (IBAction)alertaction:(id)sender {
    [self performSegueWithIdentifier:@"alertsegue" sender:self];

}
 
- (void)updateData
{
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
        return;
    }else{
        NSMutableURLRequest *request= [[NSMutableURLRequest alloc]init];
        NSString *str=   [NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId];
        NSLog(@"url%@",[NSString stringWithFormat:@"%@GetWorkOrderDetails?WorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId]);
        [request setURL:[NSURL URLWithString:str]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        NSError *error;
        NSURLResponse *response;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSDictionary *jsonDict= [NSJSONSerialization JSONObjectWithData:urlData
                                                                options:kNilOptions error:&error];
        NSLog(@"jsonDict%@",jsonDict);
        
            alertList=[[NSMutableArray alloc] initWithArray:[jsonDict valueForKeyPath:@"Data.SubWorkOrderList"]];
            
  
        
        //    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"document" ofType:@"json"];
        //    NSLog(@"%@ filePath",filePath);
        //    NSData *data = [NSData dataWithContentsOfFile:filePath];
        //
//            NSDictionary *jsonDict= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions  error:&error];
//         NSLog(@"jsonDict :%@ %@",str,jsonDict);
//
//          dataList = [[NSMutableArray alloc] initWithArray:jsonDict[@"Data.List"]];
//             NSLog(@"dataList :%@",dataList);
//
        // NSMutableArray *tempAry = [[NSMutableArray alloc] initWithArray:jsonDict[@"Data.List"]];
    //    alertList=[[NSMutableArray alloc] initWithArray:jsonDict[@"Data.SubWorkOrderList"]];
        //[[NSMutableArray alloc]initWithArray:[response valueForKeyPath:@"Data.SubWorkOrderList"]];

        dispatch_async(dispatch_get_main_queue(), ^{
  [suborder_listTable reloadData];
            });
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [refreshControl performSelector:@selector(endRefreshing) withObject:nil afterDelay:1.0f];

    }
}
-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {
}
- (IBAction)logoutAction:(id)sender
{ 
         [self logoutUser];
        
   // }
}

- (IBAction)changepasswordAction:(id)sender {
    [self changePassword];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSString *name;
    NSString *room ;
    
 
   // if ([segue.identifier isEqualToString:@"sub_form_segue"]) {
        //dynamicform
        if ([segue.identifier isEqualToString:@"sub_form_segue"]) {
            
        
      Suborder_FormVC *vc = [segue destinationViewController];
        vc.WorkorderId =_WorkorderId;
        vc.Testtype_ID =_Testtype_ID;
        vc.Sub_WorkorderId =_Sub_WorkorderId;
        vc.TaskName=_TaskName;
        vc.ProjectName=_ProjectName;
        vc.JobNumber=_JobNumber;
            vc.childname=_childname;
    }
    
    
}
- (IBAction)jobdateaction:(id)sender {
    
    [self showDatePicker:UIDatePickerModeDate currentLbl:self->jobdate];
}


@end
