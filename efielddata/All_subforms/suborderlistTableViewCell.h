
//  Efield
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface suborderlistTableViewCell :  UITableViewCell 
@property (weak, nonatomic) IBOutlet UIView *orderview;
@property (weak, nonatomic) IBOutlet UIButton *delete_btn;
@property (weak, nonatomic) IBOutlet UILabel *locationname;
@property (weak, nonatomic) IBOutlet UIButton *copybtn;
@property (weak, nonatomic) IBOutlet UILabel *count;
@property (weak, nonatomic) IBOutlet UIView *copyview;

@property (weak, nonatomic) IBOutlet UIView *header_view;

//
//@property (weak, nonatomic) IBOutlet UILabel *Question1_value;
//@property (weak, nonatomic) IBOutlet UILabel *Question2_lbl;
//@property (weak, nonatomic) IBOutlet UILabel *Question2_value;
//@property (weak, nonatomic) IBOutlet UILabel *Question3_lbl;
//@property (weak, nonatomic) IBOutlet UILabel *Question3_value;
//@property (weak, nonatomic) IBOutlet UILabel *Question4_lbl;
//@property (weak, nonatomic) IBOutlet UILabel *Question4_value;
//@property (weak, nonatomic) IBOutlet UILabel *Question1_lbl;

@end
