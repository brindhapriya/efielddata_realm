
//
//  Created by brindha on 12/02/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "Suborder_FormVC.h"
#import "Image_SelectionViewController.h"
#import "imageviewTableviewcell.h"
#import "dynamiccameraVC.h"
#import "Suborder_listVC.h"
@interface Suborder_FormVC ()<UpdateSelectedItemsDelegate,UITextViewDelegate,UITextFieldDelegate>{
    
    NSInteger rowOfTheCell;
    NSIndexPath *editingsubIndexPath;

}
@property (strong, nonatomic) IBOutlet UISlider *timespentslider;
@property (nonatomic, retain) IBOutlet UITableView *tblForm;
@end

@implementation Suborder_FormVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    
    _childname = [_childname stringByReplacingOccurrencesOfString:@"+"
                                         withString:@""];
    _Header_lbl.text=_childname;
   // [self.view addGestureRecognizer:tap];
    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"Dropdown_special_id"];
   
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
    _timespentslider.minimumValue=0.0;
    _timespentslider.maximumValue=1000.0;
    
    self.taskname_lbl.text= _ProjectName   ;
    self.jobnumber.text=[NSString stringWithFormat:@"%@ - %@", _JobNumber,_TaskName];
    
    if(_isFromEditNote&& !_IsEditNote)
    {
        _saveBtn.hidden = YES;
    }
    else
    {
        _saveBtn.hidden = NO;
    }
    
     _saveBtn.enabled = NO;
    
   
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    if(_isFromEditNote )
    [self performSelector:@selector(getEditNote) withObject:nil afterDelay:0.1];
//    else
   //     [self performSelector:@selector(getAddNote) withObject:nil afterDelay:0.1];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureHandlerMethod:)];
    [_location addGestureRecognizer:tapRecognizer];
    _location.tag=2;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
   // if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0);
//   } else {
//        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
//    }
    
    self.tblForm.contentInset = contentInsets;
    self.tblForm.scrollIndicatorInsets = contentInsets;
    [self.tblForm scrollToRowAtIndexPath: editingsubIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    self.tblForm.contentInset = UIEdgeInsetsZero;
    self.tblForm.scrollIndicatorInsets = UIEdgeInsetsZero;
}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    //   [aTextField resignFirstResponder];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
 
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"Dropdown_special_id"] isEqualToString:@""]||[[NSUserDefaults standardUserDefaults] objectForKey:@"Dropdown_special_id"]==nil)
    {
    }else{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetProctorItem?WorkOrderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:@"Dropdown_special_id"]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetProctorItem"];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Disable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    [super viewWillDisappear:animated];
   // [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
  //  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}
-(void)gestureHandlerMethod:(UITapGestureRecognizer*)sender {
 
        if(sender.view.tag==2) {
            //do something here
            [self performSegueWithIdentifier:@"suborderlist_segue" sender:self];
            
        }
    }
-(void)getEditNote
{
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
        return;
    }else{
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc]init];
    NSString *str=
        
 [NSString stringWithFormat:@"%@GetSubWorkOrderDetails?WorkorderId=%@&TestTypeId=%@&SubWorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId,_Testtype_ID,_Sub_WorkorderId];
        NSLog(@"url%@",[NSString stringWithFormat:@"%@GetSubWorkOrderDetails?WorkorderId=%@&TestTypeId=%@&SubWorkorderId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],_WorkorderId,_Testtype_ID,_Sub_WorkorderId]);
    [request setURL:[NSURL URLWithString:str]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSDictionary *jsonDict= [NSJSONSerialization JSONObjectWithData:urlData
                                                            options:kNilOptions error:&error];
    
    
    //    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"document" ofType:@"json"];
    //    NSLog(@"%@ filePath",filePath);
    //    NSData *data = [NSData dataWithContentsOfFile:filePath];
    //
    //    NSDictionary *jsonDict= [NSJSONSerialization JSONObjectWithData:data options:kNilOptions  error:&error];
     // NSLog(@"jsonDict :%@ %@",str,jsonDict);
     
  //  dataList = [[NSMutableArray alloc] initWithArray:jsonDict[@"Data.List"]];
   //     NSLog(@"dataList :%@",dataList);
        
   // NSMutableArray *tempAry = [[NSMutableArray alloc] initWithArray:jsonDict[@"Data.List"]];
        dataList = [[NSMutableArray alloc]initWithArray:[jsonDict valueForKeyPath:@"Data.List"]];
         _Sub_WorkorderId=[[jsonDict valueForKeyPath:@"Data.SubWorkorderId"]description];
       
 
        //[[NSMutableArray alloc] initWithArray:jsonDict[@"Data.List"]];
        //    [_tblForm reloadData];
        //          NSLog(@"dataList : %@",dataList);
        //    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
          NSMutableArray *tempAry = [[NSMutableArray alloc]initWithArray:[jsonDict valueForKeyPath:@"Data.List"]];
        
     //   NSMutableArray *tempAry = [[NSMutableArray alloc] initWithArray:jsonDict[@"Data.List"]];
        NSLog(@"dataList :%@",dataList);
        
    int index = 0;
    
    for(NSDictionary *dic in dataList) {
        
        if(([dic[@"DataType"] isEqualToString:@"Dropdown"])||([dic[@"DataType"] isEqualToString:@"Dropdown(Special)"]) || ([dic[@"DataType"] isEqualToString:@"Radio"]))
        {
            if([dic[@"AnswerId"] intValue] != 0)
            {
                [tempAry replaceObjectAtIndex:index withObject:[self singleCheckEditValues:dic]];
                //                NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"AnswerId = %@",dic[@"AnswerId"]]];
                //                NSArray* filteredData = [dic[@"AnswersList"] filteredArrayUsingPredicate:predicate];
                //                NSLog(@"Filter : %@",filteredData);
                //                if(filteredData.count)
                //                {
                //                    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
                //                    [tempDic setObject:filteredData[0][@"AnswerValue"] forKey:@"QuestionValue"];
                //                    [tempAry replaceObjectAtIndex:index withObject:tempDic];
                //                }
            }
        }
        else if([dic[@"DataType"] isEqualToString:@"Dropdown(Multiple)"])
        {
            NSArray *selectAnswer = [[NSArray alloc] initWithArray:dic[@"MultiAnswers"]];
            NSString *questionValue = @"";
            
            for(NSString *answerStr in selectAnswer)
            {
                if([answerStr intValue] != 0)
                {
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"AnswerId = %@",answerStr]];
                    NSArray* filteredData = [dic[@"AnswersList"] filteredArrayUsingPredicate:predicate];
                    NSLog(@"Filter : %@",filteredData);
                    if(filteredData.count)
                    {
                        if(questionValue.length)
                            questionValue = [NSString stringWithFormat:@"%@, %@",questionValue,filteredData[0][@"AnswerValue"]];
                        else
                            questionValue = filteredData[0][@"AnswerValue"];
                    }
                }
            }
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
            [tempDic setObject:questionValue forKey:@"QuestionValue"];
            [tempAry replaceObjectAtIndex:index withObject:tempDic];
        }
        else if([dic[@"DataType"] isEqualToString:@"Checkbox(Multiple)"])
        {
            NSArray *selectAnswer = [[NSArray alloc] initWithArray:dic[@"MultiCheckBoxAnswers"]];
            NSString *questionValue = @"";
            
            for(NSString *answerStr in selectAnswer)
            {
                if([answerStr intValue] != 0)
                {
                    NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"AnswerId = %@",answerStr]];
                    NSArray* filteredData = [dic[@"AnswersList"] filteredArrayUsingPredicate:predicate];
                    NSLog(@"Filter : %@",filteredData);
                    if(filteredData.count)
                    {
                        if(questionValue.length)
                            questionValue = [NSString stringWithFormat:@"%@, %@",questionValue,filteredData[0][@"AnswerValue"]];
                        else
                            questionValue = filteredData[0][@"AnswerValue"];
                    }
                }
            }
            NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
            [tempDic setObject:questionValue forKey:@"QuestionValue"];
            [tempAry replaceObjectAtIndex:index withObject:tempDic];
        }
        else if([dic[@"DataType"] isEqualToString:@"NumericTextBox"] )
        {
            
            if([[dic[@"NumericAnswer"] description] isEqualToString:@"-1"])
            {
                
                NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
                [tempDic setObject:@"" forKey:@"NumericAnswer"];
                [tempAry replaceObjectAtIndex:index withObject:tempDic];
                
            }
        }
        
        //        [_selectedItem addObjectsFromArray:_listInfo[@"MultiSelectAnswers"]];
        //        else if([_listInfo[@"DataType"] isEqualToString:@"Checkbox(Multiple)"])
        //            [_selectedItem addObjectsFromArray:_listInfo[@"MultiCheckBoxAnswers"]];
        
        index++;
    }
    
    dataList = [tempAry mutableCopy];
    [_tblForm reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
}

- (NSDictionary *)singleCheckEditValues:(NSDictionary *)dic
{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"AnswerId = %@",dic[@"AnswerId"]]];
    NSArray* filteredData = [dic[@"AnswersList"] filteredArrayUsingPredicate:predicate];
    NSLog(@"Filter : %@",filteredData);
    if(filteredData.count)
    {
        NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
        [tempDic setObject:filteredData[0][@"AnswerValue"] forKey:@"QuestionValue"];
        return tempDic;
    }
    return dic;
}



-(void)updateSelectedItems:(NSDictionary *)updatedItem questionID:(NSString *)questionID {
    NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"QuestionId = %@",questionID]];
    NSArray* filteredData = [dataList filteredArrayUsingPredicate:predicate];
    NSLog(@"Filter : %@",filteredData);
    if(filteredData.count) {
        
        NSInteger index = [dataList indexOfObject:filteredData[0]];
        NSLog(@"Index : %ld",(long)index);
        [dataList replaceObjectAtIndex:index withObject:updatedItem];
        [_tblForm reloadData];
    }
    NSLog(@"Came");
}

-(CGFloat)calculateStringHeigth:(CGSize)size text:(NSString *)text andFont:(UIFont *)font {
    NSLog(@"text : %@",text);
    
    
    NSLog(@"Font : %@", font);
    NSLog(@"size : %@",NSStringFromCGSize(size));
    
    if([text isEqual:[NSNull null]]) {
        text = @"";
    }
    
    if(![text length])
        return 0;
    
    CGSize labelSize = [text sizeWithFont:font
                        constrainedToSize:size
                            lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat labelHeight = labelSize.height;
    NSLog(@"labelHeight : %f",labelHeight);
    return labelHeight;
}

-(NSMutableAttributedString *)AddRequiredField:(NSString *)title andIsRequired:(BOOL)required {
    
    if(required)
    {
        NSString *string = [NSString stringWithFormat:@"%@ *", title];
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
        NSRange range = [string rangeOfString:@"*"];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        return attString;
    }
    else
    {
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:title];
        return attString;
    }
}




#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //IsAddMoreDisplay
 
    return dataList.count;
 
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Image"]) {
            
            
            //@"Field Image Attachments";
            
            
            static NSString *simpleTableIdentifier = @"imageviewcell";
            
            imageviewTableviewcell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            [cell.contentView clearsContextBeforeDrawing];
            
            
            if (cell == nil) {
                cell = [[imageviewTableviewcell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
            }
            [cell.ttbimage_lbl sizeToFit];
            cell.ttbimage_lbl.numberOfLines = 0;
            cell.ttbimage_lbl.text = @"Field Image Attachments";
            
            
            //  if([self.taptobilledit isEqualToString:@"true"]){
            
            
            //                if([dataList[indexPath.row][@"TextAnswer"] length]>0){
            //                    cell.ttbimage_txt.text = @"View Images";
            //                    cell.ttbimage_txt.textColor=[UIColor blueColor];
            //
            //
            //                    UITapGestureRecognizer *viewimag_tap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewimag_taping:)];
            //                    cell.ttbimage_txt.tag=indexPath.row;
            //                    [viewimag_tap setNumberOfTapsRequired:1];
            //                    [cell.ttbimage_txt setUserInteractionEnabled:YES];
            //
            //                    [  cell.ttbimage_txt addGestureRecognizer:viewimag_tap];
            //
            //                }else{
            //
            //                    cell.ttbimage_txt.text = @"";
            //
            //                }
            
            
            // tap gesture for camera
            //  cell.ttbimage_txt.textColor=[UIColor blueColor];
            
            
            UITapGestureRecognizer *viewimag_tap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewimag_taping:)];
            cell.ttbimage_txt.tag=indexPath.row;
            [viewimag_tap setNumberOfTapsRequired:1];
            [cell.ttbimage_txt setUserInteractionEnabled:YES];
            
            [  cell.ttbimage_txt addGestureRecognizer:viewimag_tap];
            //
            //            UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
            //            [singleTap setNumberOfTapsRequired:1];
            //            [cell.ttbimage_txt setUserInteractionEnabled:YES];
            //
            //            [  cell.ttbimage_txt addGestureRecognizer:singleTap];
            //
            //
            // tap gesture to view images
            //  NSInteger height=10;
            //        if(_isFromEditNote)
            //        {
            //
            //
            //            [cell.ttbimage_txt sizeToFit];
            //            cell.ttbimage_txt.numberOfLines = 0;
            //        cell.ttbimage_txt.text=dataList[indexPath.row][@"TextAnswer"];
            //              CGSize size = self.tblForm.frame.size;
            //                height = [self calculateStringHeigth:size text:dataList[indexPath.row][@"TextAnswer"] andFont:[UIFont systemFontOfSize:14]];
            //
            //
            //        }
            
            // CGRect imageFrame = CGRectMake(10,height+20,200, 100);
            cell.accessoryType=UITableViewCellAccessoryNone;
            //            UIImageView *customImage = [[UIImageView alloc] initWithFrame:imageFrame] ;
            //
            //            NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
            //
            //            if(img_count>0){
            //                for(int i=1;i<=img_count;i++){
            //
            //                    //   customImage.image=_single_img;
            //                    customImage.image= [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"image%d", i]]];
            //                }
            //                customImage.center = CGPointMake(cell.contentView.bounds.size.width/2,cell.contentView.bounds.size.height/2+10);
            //                customImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
            //
            //
            //                [cell.contentView addSubview:customImage];
            //
            //            }
            //
            return cell;
            
            
        }
    
        else
            if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Category"]) {
                DescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
                
                
                //   cell.lblTitle.text = dataList[indexPath.row][@"CategoryName"];
                
                //  if(dataList[indexPath.row][@"CategoryDescription"] != [NSNull null])
                [cell.lblTitle sizeToFit];
                cell.lblTitle.numberOfLines = 1;
                [cell.lblDiscribtion sizeToFit];
                cell.lblDiscribtion.numberOfLines = 0;
                
                if([dataList[indexPath.row][@"CategoryName"] isKindOfClass:[NSNull class]]){
                    cell.lblTitle.hidden=YES;
                    
                }else
                {
                    cell.lblTitle.hidden=NO;
                    cell.lblTitle.text = dataList[indexPath.row][@"CategoryName"];
                }
                if([dataList[indexPath.row][@"CategoryDescription"] isKindOfClass:[NSNull class]]){
                    cell.lblDiscribtion.hidden=YES;
                    
                }else
                {
                    cell.lblDiscribtion.hidden=NO;
                    cell.lblDiscribtion.text = dataList[indexPath.row][@"CategoryDescription"];
                }
                
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"NumericTextBox"])
                //{
                
            {
                TextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
                // if(_isFromEditNote && !_IsEditNote)
                //  cell.userInteractionEnabled = NO;
                if (cell == nil) {
                    cell= [[TextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TextFieldCell"];
                }
                cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
                cell.txtValue.delegate = self;
                cell.txtValue.tag = indexPath.row;
                [cell.txtValue setKeyboardType:UIKeyboardTypeDecimalPad];
                
                
                if([dataList[indexPath.row][@"NumericAnswer"] floatValue] < 0)
                {       // }
                    cell.txtValue.placeholder = @"Type here ...";
                    
                    cell.txtValue.text = @"";
                    
                }else
                {
                    cell.txtValue.text = [dataList[indexPath.row][@"NumericAnswer"] description];}
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell.txtValue addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
                
                return cell;
            }
    
    //  SliderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SliderCell"];
    //
    //if(_isFromEditNote  && !_IsEditNote)
    //            cell.userInteractionEnabled = NO;
    //
    //        cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
    //        cell.sliderValue.minimumValue = 0.0;
    //        cell.sliderValue.maximumValue = [dataList[indexPath.row][@"MaxValue"] floatValue];
    //        cell.sliderValue.value = [dataList[indexPath.row][@"NumericAnswer"] floatValue];
    //
    //        if([dataList[indexPath.row][@"NumericAnswer"] floatValue] < 0)
    //            cell.txtValue.text = @"";
    //        else
    //            cell.txtValue.text = [NSString stringWithFormat:@"%.f",[dataList[indexPath.row][@"NumericAnswer"] floatValue]];
    //
    //        cell.sliderValue.tag = indexPath.row;
    //
    //        [cell.sliderValue addTarget:self action:@selector(sliderValueChanges:) forControlEvents:UIControlEventValueChanged];
    //
    //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //        return cell;
    // }
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown"]||[dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown(Special)"]) {
                MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
                if(_isFromEditNote   && !_IsEditNote)
                    cell.userInteractionEnabled = NO;
                cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
                
                NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
                NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
                [cell.lblTitle sizeToFit];
             
                cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
//                if([cell.lblValue.text isEqualToString:@""])
//                {
//                    for(int i=0;i<[dataList[indexPath.row][@"AnswersList"] count];i++)
//                    {
//                        if([dataList[indexPath.row][@"AnswerId"] isEqualToString:dataList[indexPath.row][@"AnswersList.AnswerId"] ])
//                    {
//                        cell.lblValue.text =dataList[indexPath.row][@"AnswersList.AnswerValue"];
//
//                    }
//                    }
//                }
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Checkbox"])
            {
                SwitchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
                if(_isFromEditNote  && !_IsEditNote)
                    cell.userInteractionEnabled = NO;
                cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
                [cell.switchValue addTarget:self action:@selector(switchValueChanges:) forControlEvents:UIControlEventValueChanged];
                cell.switchValue.tag = indexPath.row;
                [cell.switchValue setOn:[dataList[indexPath.row][@"BooleanAnswer"] boolValue]];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"TextBoxMultiline"])
            {
                TextViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextViewCell"];
                if (cell == nil) {
                    cell= [[TextViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TextViewCell"];
                }
                cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
                [cell.txtViewValue setKeyboardType:UIKeyboardTypeAlphabet];
                
                cell.txtViewValue.delegate = self;
                [cell.txtViewValue  sizeToFit];
                
                cell.txtViewValue.tag = indexPath.row;
                if(dataList[indexPath.row][@"TextAnswer"] == [NSNull null]||[dataList[indexPath.row][@"TextAnswer"]isEqualToString:@""]){
                    cell.txtViewValue.text = @"Type here ...";
                    cell.txtViewValue.textColor = [UIColor lightGrayColor];
                }else  if(dataList[indexPath.row][@"TextAnswer"] != [NSNull null]){
                    cell.txtViewValue.text = dataList[indexPath.row][@"TextAnswer"];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"TextBox"])
            {
                TextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
                if (cell == nil) {
                    cell= [[TextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TextFieldCell"];
                }
                // if(_isFromEditNote && !_IsEditNote)
                //  cell.userInteractionEnabled = NO;
                cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
                cell.txtValue.delegate = self;
                cell.txtValue.tag = indexPath.row;
                [cell.txtValue setKeyboardType:UIKeyboardTypeAlphabet];
                //
                  [cell.txtValue addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
                if(dataList[indexPath.row][@"TextAnswer"] == [NSNull null]||[dataList[indexPath.row][@"TextAnswer"]isEqualToString:@""]){
                    cell.txtValue.placeholder = @"Type here ...";
                    cell.txtValue.text = @"";
                    
                }else
                    //  if(dataList[indexPath.row][@"TextAnswer"] != [NSNull null])
                {
                    cell.txtValue.text = dataList[indexPath.row][@"TextAnswer"];}
                
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown(Multiple)"])
            {
                MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
                if(_isFromEditNote && !_IsEditNote)
                    cell.userInteractionEnabled = NO;
                cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
                NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
                NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
                cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
                [cell.lblTitle sizeToFit];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                return cell;
            }
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Checkbox(Multiple)"]) {
                
                NSMutableDictionary *dic1 = [[NSMutableDictionary alloc] initWithDictionary:dataList[indexPath.row]];
                
                NSMutableArray   *selectedItem1 = [[NSMutableArray alloc] init];
                [dic1 setObject:selectedItem1 forKey:@"MultiSelectCheckBoxAnswers"];
                [dataList replaceObjectAtIndex:indexPath.row withObject:dic1];
                
                MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
                if(_isFromEditNote  && !_IsEditNote)
                    cell.userInteractionEnabled = NO;
                cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
                cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
                NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
                NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
                [cell.lblTitle sizeToFit];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                return cell;
            }
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Date"]) {
                DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
                if(_isFromEditNote && !_IsEditNote)
                    cell.userInteractionEnabled = NO;
                cell.lblValue.tag = indexPath.row;
                
                     if(dataList[indexPath.row][@"DateAnswerText"] == [NSNull null]||[dataList[indexPath.row][@"DateAnswerText"]isEqualToString:@""]){
                         cell.lblValue.text = @"";
                     }else{
                cell.lblValue.text = dataList[indexPath.row][@"DateAnswerText"];
                     }
                cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Radio"]) {
                MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
                if(_isFromEditNote   && !_IsEditNote)
                    cell.userInteractionEnabled = NO;
                cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
                cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
                NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
                NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                return cell;
            }
            else
                if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Date"]) {
                DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
                if(_isFromEditNote  && !_IsEditNote)
                    cell.userInteractionEnabled = NO;
                cell.lblValue.tag = indexPath.row;
                
                if(dataList[indexPath.row][@"DateAnswerText"] == [NSNull null]||[dataList[indexPath.row][@"DateAnswerText"]isEqualToString:@""]){
                    cell.lblValue.text = @"";
                }else{
                cell.lblValue.text = dataList[indexPath.row][@"DateAnswerText"];
                }
                cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else
                if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Time"]) {
                    DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
                    if(_isFromEditNote  && !_IsEditNote)
                        cell.userInteractionEnabled = NO;
                    cell.lblValue.tag = indexPath.row;
                    if(dataList[indexPath.row][@"TimeAnswerText"] == [NSNull null]||[dataList[indexPath.row][@"TimeAnswerText"]isEqualToString:@""]){
                        cell.lblValue.text = @"";
                    }else{
                        cell.lblValue.text = dataList[indexPath.row][@"TimeAnswerText"];
                    }                    cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                }
    //    else {
    //        DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
    //        if(_isFromEditNote && !_IsEditNote)
    //            cell.userInteractionEnabled = NO;
    //       cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
    //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //        return cell;
    //    }
    //    else{
    //      if([Is2AddMoreDisplay isEqualToString:@"1"])
    //
    //    {
    //                LocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"location2cell"];
    //        cell.lblValue.text=[[jsonDict    valueForKeyPath:@"Data.Child2TestTypeName"]description];
    //        cell.lblTitle.text=[[jsonDict valueForKeyPath:@"Data.Child2TestTypeName"]description];
    //
    //
    //    }
    //    else  if([IsAddMoreDisplay isEqualToString:@"1"])
    //
    //    {
    //
    //        LocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"location2cell"];
    //        cell.lblValue.text=[[jsonDict    valueForKeyPath:@"Data.ChildTestTypeName"]description];
    //        cell.lblTitle.text=[[jsonDict valueForKeyPath:@"Data.ChildTestTypeName"]description];
    //
    //    }
    //    }
    return nil;
    
}
//{
//
//
//    if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Image"]) {
//
//
//      //@"Field Image Attachments";
//
//
//            static NSString *simpleTableIdentifier = @"imageviewcell";
//
//            imageviewTableviewcell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
//            [cell.contentView clearsContextBeforeDrawing];
//
//
//            if (cell == nil) {
//                cell = [[imageviewTableviewcell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
//            }
//            [cell.ttbimage_lbl sizeToFit];
//            cell.ttbimage_lbl.numberOfLines = 0;
//            cell.ttbimage_lbl.text = @"Field Image Attachments";
//
//
//            //  if([self.taptobilledit isEqualToString:@"true"]){
//
//
//                if([dataList[indexPath.row][@"TextAnswer"] length]>0){
//                    cell.ttbimage_txt.text = @"View Images";
//                    cell.ttbimage_txt.textColor=[UIColor blueColor];
//
//
//                    UITapGestureRecognizer *viewimag_tap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewimag_taping:)];
//                    cell.ttbimage_txt.tag=indexPath.row;
//                    [viewimag_tap setNumberOfTapsRequired:1];
//                    [cell.ttbimage_txt setUserInteractionEnabled:YES];
//
//                    [  cell.ttbimage_txt addGestureRecognizer:viewimag_tap];
//
//                }else{
//
//                    cell.ttbimage_txt.text = @"";
//
//                }
//
//
//            // tap gesture for camera
//
////            UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
////            [singleTap setNumberOfTapsRequired:1];
////            [cell.camera_icon setUserInteractionEnabled:YES];
////
////            [  cell.camera_icon addGestureRecognizer:singleTap];
////
////
//            // tap gesture to view images
//            NSInteger height=10;
//            //        if(_isFromEditNote)
//            //        {
//            //
//            //
//            //            [cell.ttbimage_txt sizeToFit];
//            //            cell.ttbimage_txt.numberOfLines = 0;
//            //        cell.ttbimage_txt.text=dataList[indexPath.row][@"TextAnswer"];
//            //              CGSize size = self.tblForm.frame.size;
//            //                height = [self calculateStringHeigth:size text:dataList[indexPath.row][@"TextAnswer"] andFont:[UIFont systemFontOfSize:14]];
//            //
//            //
//            //        }
//
//            CGRect imageFrame = CGRectMake(10,height+20,200, 100);
//            cell.accessoryType=UITableViewCellAccessoryNone;
//            UIImageView *customImage = [[UIImageView alloc] initWithFrame:imageFrame] ;
//
//            NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
//
//            if(img_count>0){
//                for(int i=1;i<=img_count;i++){
//
//                    //   customImage.image=_single_img;
//                    customImage.image= [UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"image%d", i]]];
//                }
//                customImage.center = CGPointMake(cell.contentView.bounds.size.width/2,cell.contentView.bounds.size.height/2+10);
//                customImage.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
//
//
//                [cell.contentView addSubview:customImage];
//
//            }
//
//            return cell;
//
//
//        }
//
//     else
//    if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Category"]) {
//        DescriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
//
//        //   cell.lblTitle.text = dataList[indexPath.row][@"CategoryName"];
//
//        //  if(dataList[indexPath.row][@"CategoryDescription"] != [NSNull null])
//        [cell.lblTitle sizeToFit];
//        cell.lblTitle.numberOfLines = 1;
//        [cell.lblDiscribtion sizeToFit];
//        cell.lblDiscribtion.numberOfLines = 0;
//
//        if([dataList[indexPath.row][@"CategoryName"] isKindOfClass:[NSNull class]]){
//            cell.lblTitle.hidden=YES;
//
//        }else
//        {
//            cell.lblTitle.hidden=NO;
//            cell.lblTitle.text = dataList[indexPath.row][@"CategoryName"];
//        }
//        if([dataList[indexPath.row][@"CategoryDescription"] isKindOfClass:[NSNull class]]){
//            cell.lblDiscribtion.hidden=YES;
//
//        }else
//        {
//            cell.lblDiscribtion.hidden=NO;
//            cell.lblDiscribtion.text = dataList[indexPath.row][@"CategoryDescription"];
//        }
//
//
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
//    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"NumericTextBox"]) {
//        TextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
//        // if(_isFromEditNote && !_IsEditNote)
//        //  cell.userInteractionEnabled = NO;
//        cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
//        cell.txtValue.delegate = self;
//        cell.txtValue.tag = indexPath.row;
//        [cell.txtValue setKeyboardType:UIKeyboardTypeDecimalPad];
//
//        if([dataList[indexPath.row][@"NumericAnswer"] floatValue] < 0)
//        {       // }
//            cell.txtValue.placeholder = @"Type here ..."; }else
//            {
//                cell.txtValue.text =[NSString stringWithFormat:@"%.f",[dataList[indexPath.row][@"NumericAnswer"] floatValue]];}
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        [cell.txtValue addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
//
//        return cell;
//    }
//    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown(Special)"]) {
//        MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
//        if(_isFromEditNote   && !_IsEditNote)
//            cell.userInteractionEnabled = NO;
//        cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
//
//        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
//        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
//        [cell.lblTitle sizeToFit];
//
//        cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
//
//    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown"]) {
//        MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
//        if(_isFromEditNote   && !_IsEditNote)
//            cell.userInteractionEnabled = NO;
//        cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
//
//        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
//        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
//        [cell.lblTitle sizeToFit];
//
//        cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
//    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Checkbox"])
//    {
//        SwitchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
//        if(_isFromEditNote  && !_IsEditNote)
//            cell.userInteractionEnabled = NO;
//        cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
//        [cell.switchValue addTarget:self action:@selector(switchValueChanges:) forControlEvents:UIControlEventValueChanged];
//        cell.switchValue.tag = indexPath.row;
//        [cell.switchValue setOn:[dataList[indexPath.row][@"BooleanAnswer"] boolValue]];
//
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
//    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"TextBoxMultiline"])
//    {
//        TextViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextViewCell"];
//        cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
//
//        cell.txtViewValue.tag = indexPath.row;
//        cell.txtViewValue.delegate = self;
//        [cell.txtViewValue  sizeToFit];
//        if(dataList[indexPath.row][@"TextAnswer"] == [NSNull null]||[dataList[indexPath.row][@"TextAnswer"]isEqualToString:@""]){
//            cell.txtViewValue.text = @"Type here ...";
//            cell.txtViewValue.textColor = [UIColor lightGrayColor];
//        }else  if(dataList[indexPath.row][@"TextAnswer"] != [NSNull null]){
//            cell.txtViewValue.text = dataList[indexPath.row][@"TextAnswer"];
//        }
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
//    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"TextBox"])
//    {
//        TextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
//        // if(_isFromEditNote && !_IsEditNote)
//        //  cell.userInteractionEnabled = NO;
//        cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
//
//        cell.txtValue.tag = indexPath.row;
//          [cell.txtValue addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
//        cell.txtValue.delegate = self;      if(dataList[indexPath.row][@"TextAnswer"] == [NSNull null]||[dataList[indexPath.row][@"TextAnswer"]isEqualToString:@""]){
//            cell.txtValue.placeholder = @"Type here ..."; }else
//                if(dataList[indexPath.row][@"TextAnswer"] != [NSNull null]){
//                    cell.txtValue.text = dataList[indexPath.row][@"TextAnswer"];}
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
//    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown(Multiple)"])
//    {
//        MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
//        if(_isFromEditNote && !_IsEditNote)
//            cell.userInteractionEnabled = NO;
//        cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
//        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
//        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
//        cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
//        [cell.lblTitle sizeToFit];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        return cell;
//    }
//    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Checkbox(Multiple)"]) {
//
//        NSMutableDictionary *dic1 = [[NSMutableDictionary alloc] initWithDictionary:dataList[indexPath.row]];
//
//        NSMutableArray   *selectedItem1 = [[NSMutableArray alloc] init];
//        [dic1 setObject:selectedItem1 forKey:@"MultiSelectCheckBoxAnswers"];
//        [dataList replaceObjectAtIndex:indexPath.row withObject:dic1];
//
//        MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
//        if(_isFromEditNote  && !_IsEditNote)
//            cell.userInteractionEnabled = NO;
//        cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
//        cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
//        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
//        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
//        [cell.lblTitle sizeToFit];
//
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        return cell;
//    }
//    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Date"]) {
//        DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
//        if(_isFromEditNote && !_IsEditNote)
//            cell.userInteractionEnabled = NO;
//        cell.lblValue.tag = indexPath.row;
//        cell.lblValue.text = dataList[indexPath.row][@"DateAnswerText"];
//        cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
//
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
//    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Radio"]) {
//        MultySelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
//        if(_isFromEditNote   && !_IsEditNote)
//            cell.userInteractionEnabled = NO;
//        cell.lblValue.text = dataList[indexPath.row][@"QuestionValue"];
//        cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
//        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
//        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tblForm.frame));
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        return cell;
//    }
//    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Time"]) {
//        DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
//        if(_isFromEditNote  && !_IsEditNote)
//            cell.userInteractionEnabled = NO;
//        cell.lblValue.tag = indexPath.row;
//        cell.lblValue.text = dataList[indexPath.row][@"TimeAnswerText"];
//        cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
//    else {
//        DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
//        if(_isFromEditNote && !_IsEditNote)
//            cell.userInteractionEnabled = NO;
//       cell.lblTitle.attributedText = [self AddRequiredField:dataList[indexPath.row][@"QuestionDisplayName"] andIsRequired:[dataList[indexPath.row][@"IsRequired"] boolValue]];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
//    return nil;
//
//}

#pragma mark - keyboard movements
//- (void)keyboardWillShow:(NSNotification *)notification
//{
//    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//
//    [UIView animateWithDuration:0.3 animations:^{
//        CGRect f = self.view.frame;
//        f.origin.y = -keyboardSize.height;
//        self.view.frame = f;
//    }];
//}
//
//-(void)keyboardWillHide:(NSNotification *)notification
//{
//    [UIView animateWithDuration:0.3 animations:^{
//        CGRect f = self.view.frame;
//        f.origin.y = 0.0f;
//        self.view.frame = f;
//    }];
//}
- (void)sliderValueChanges:(id)sender {
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    UISlider *slider = (UISlider *)sender;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[slider.tag]];
    [dic setObject:[NSString stringWithFormat:@"%.f",slider.value] forKey:@"NumericAnswer"];
    NSLog(@"dic : %@",dic);
    [dataList replaceObjectAtIndex:slider.tag withObject:dic];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:slider.tag inSection:0];
    
    SliderTableViewCell *cell = [_tblForm cellForRowAtIndexPath:indexPath];
    cell.txtValue.text = [NSString stringWithFormat:@"%.f",slider.value];
    
}
- (void)switchValueChanges:(id)sender {
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    UIButton *btn = (UIButton *)sender;
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[btn.tag]];
    [dic setObject:[sender isOn] ? [NSNumber numberWithBool:true]:[NSNumber numberWithBool:false] forKey:@"BooleanAnswer"];
    NSLog(@"dic : %@",dic);
    [dataList replaceObjectAtIndex:btn.tag withObject:dic];
}

- (IBAction)textFieldDidBeginEditing:(UITextField *)textField
{
    editingsubIndexPath= [NSIndexPath indexPathForRow:textField.tag inSection:0];

    
}
- (void)NumericTextBoxedit:(UITextField *)textField {
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    //  [dataList[indexPath.row][@"MaxValue"] floatValue];
    float inte = [textField.text floatValue];
    if (inte <=[dataList[textField.tag][@"MaxValue"] floatValue])
        //&& inte >=[dataList[textField.tag][@"MinValue"] floatValue])
    {
        [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
        self.saveBtn.enabled = YES;
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[textField.tag]];
        [dic setObject:textField.text forKey:@"NumericAnswer"];
        NSLog(@"dic : %@",dic);
        [dataList replaceObjectAtIndex:textField.tag withObject:dic];
    }
    else
    {
        //  textField.textColor = [UIColor lightGrayColor];
        textField.text = @"";
        // [textField resignFirstResponder];
        
    }
    
    
    // [_tblForm reloadData];
}



- (void)textFieldDidEndEditing:(UITextField *)textField {
    if([dataList[textField.tag][@"DataType"] isEqualToString:@"NumericTextBox"])
    {
        [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
        self.saveBtn.enabled = YES;
        //  [dataList[indexPath.row][@"MaxValue"] floatValue];
        float inte = [textField.text floatValue];
        if (inte <=[dataList[textField.tag][@"MaxValue"] floatValue] )
            //&& inte >=[dataList[textField.tag][@"MinValue"] floatValue])
        {
            [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
            self.saveBtn.enabled = YES;
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[textField.tag]];
            [dic setObject:textField.text forKey:@"NumericAnswer"];
            NSLog(@"dic : %@",dic);
            [dataList replaceObjectAtIndex:textField.tag withObject:dic];
        }
        else
        {
            //  textField.textColor = [UIColor lightGrayColor];
            textField.text = @"";
            // [textField resignFirstResponder];
            
        }
    }
    else{
        
        
   //     indexpath = 0;
        [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
        self.saveBtn.enabled = YES;
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[textField.tag]];
        [dic setObject:textField.text forKey:@"TextAnswer"];
        NSLog(@"dic : %@",dic);
        [dataList replaceObjectAtIndex:textField.tag withObject:dic];
    }
    // [_tblForm reloadData];
}


-(void) textViewDidChange:(UITextView *)textView
{
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    
    if(textView.text.length == 0){
        textView.textColor = [UIColor lightGrayColor];
        textView.text = @"Type here ...";
        NSLog(@"textView : %ld",(long)textView.tag);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[textView.tag]];
        [dic setObject:textView.text forKey:@"TextAnswer"];
        NSLog(@"dic : %@",dic);
        [dataList replaceObjectAtIndex:textView.tag withObject:dic];
        
        [textView resignFirstResponder];
    }
    
    else{
        NSLog(@"textView : %ld",(long)textView.tag);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[textView.tag]];
        [dic setObject:textView.text forKey:@"TextAnswer"];
        NSLog(@"dic : %@",dic);
        [dataList replaceObjectAtIndex:textView.tag withObject:dic];
    }
    self.saveBtn.enabled = YES;
    
}



-(void) textFieldDidChange:(UITextField *)textField
{
    if([dataList[textField.tag][@"DataType"] isEqualToString:@"NumericTextBox"])
    {
        
        [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
        self.saveBtn.enabled = YES;
        //  [dataList[indexPath.row][@"MaxValue"] floatValue];
        float inte = [textField.text floatValue];
        if (inte <=[dataList[textField.tag][@"MaxValue"] floatValue])
            //&& inte >=[dataList[textField.tag][@"MinValue"] floatValue])
        {
            [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
            self.saveBtn.enabled = YES;
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[textField.tag]];
            [dic setObject:textField.text forKey:@"NumericAnswer"];
            NSLog(@"dic : %@",dic);
            [dataList replaceObjectAtIndex:textField.tag withObject:dic];
        }
        
        else
        {
            //  textField.textColor = [UIColor lightGrayColor];
            textField.text = @"";
            // [textField resignFirstResponder];
            
        }
        
        
        // [_tblForm reloadData];
    }else{
        
        [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
        self.saveBtn.enabled = YES;
        
        NSLog(@"textView : %ld",(long)textField.tag);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[textField.tag]];
        [dic setObject:textField.text forKey:@"TextAnswer"];
        NSLog(@"dic : %@",dic);
        [dataList replaceObjectAtIndex:textField.tag withObject:dic];
    }
    
    
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    editingsubIndexPath= [NSIndexPath indexPathForRow:textView.tag inSection:0];

     if([textView.text isEqualToString:@"Type here ..."]){
        textView.text = @"";
        textView.textColor = [UIColor lightGrayColor];}
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    if([dataList[textField.tag][@"DataType"] isEqualToString:@"NumericTextBox"])
    {
        //         [dataList[indexPath.row][@"MaxValue"] floatValue];
        //        float inte = [textField.text floatValue];
        //        if (inte <[dataList[textField.tag][@"MaxValue"] floatValue] && inte > 0.0)
        //        {
        //            return YES;
        //        }
        //        else
        //        {
        //            textField.textColor = [UIColor lightGrayColor];
        //            textField.text = @"Type here ...";
        //            [textField resignFirstResponder];
        //
        //            return NO;
        //        }
        [textField resignFirstResponder];
        
        return NO;
    }
    else{
        
        NSLog(@"textView : %ld",(long)textField.tag);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[textField.tag]];
        [dic setObject:textField.text forKey:@"TextAnswer"];
        NSLog(@"dic : %@",dic);
        [dataList replaceObjectAtIndex:textField.tag withObject:dic];
        return YES;
    }
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    
    if(![textView.text isEqualToString:@""]){
        textView.text =   dataList[textView.tag][@"TextAnswer"];;
    }else{
        textView.text =@"Type here ...";
    }
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    NSLog(@"textView : %ld",(long)textView.tag);
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[textView.tag]];
    [dic setObject:textView.text forKey:@"TextAnswer"];
    NSLog(@"dic : %@",dic);
    [dataList replaceObjectAtIndex:textView.tag withObject:dic];
    //  [_tblForm reloadData];
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    //    if([text isEqualToString:@"\n"]) {
    //        [textView resignFirstResponder];
    //        return NO;
    //    }
    
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat height = 80;
    if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Image"])
    {
        height=65;
    }
    
     else
        if([dataList[indexPath.row][@"DataType"] isEqualToString:@"placementlist"])
        {
            height=80;
        }else
            if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Category"])
            {
                CGSize size = self.tblForm.frame.size;
                size.width = size.width - 16;
                height = [self calculateStringHeigth:size text:dataList[indexPath.row][@"CategoryDescription"] andFont:[UIFont systemFontOfSize:13]];
                CGFloat height1;
                height1 = [self calculateStringHeigth:size text:dataList[indexPath.row][@"CategoryName"] andFont:[UIFont systemFontOfSize:13]];
                if(![dataList[indexPath.row][@"CategoryName"] length]&&![dataList[indexPath.row][@"CategoryDescription"] length]){
                    
                    
                    
                    height = height + 37;
                }
                
                else
                    if([dataList[indexPath.row][@"CategoryName"] isKindOfClass:[NSNull class]]||[dataList[indexPath.row][@"CategoryName"] isEqualToString:@""]){
                        
                        
                        height = height + 12;
                    }
                    else {
                        
                        height = height +15+20;}
                
            }
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"NumericTextBox"])
                height = 65;
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown"]) {
                //height = 51;
                CGSize size = self.tblForm.frame.size;
                size.width = size.width - 78;
                NSString *string = [dataList[indexPath.row][@"IsRequired"] boolValue] ? [NSString stringWithFormat:@"%@ *",dataList[indexPath.row][@"QuestionDisplayName"]] : dataList[indexPath.row][@"QuestionDisplayName"];
                height = [self calculateStringHeigth:size text:string andFont:[UIFont systemFontOfSize:15]];
                
                
                NSString *string1 =  dataList[indexPath.row][@"QuestionValue"];
                height =height+ [self calculateStringHeigth:size text:string1 andFont:[UIFont systemFontOfSize:15]];
                height = 60;
                NSLog(@"height%f",height);
            }
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Checkbox"])
                height = 54;
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"TextBoxMultiline"])
                height = 150;
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"TextBox"])
                height = 60;
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown(Multiple)"]) {
                CGSize size = self.tblForm.frame.size;
                size.width = size.width - 78;
                NSString *string = [dataList[indexPath.row][@"IsRequired"] boolValue] ? [NSString stringWithFormat:@"%@ *",dataList[indexPath.row][@"QuestionDisplayName"]] : dataList[indexPath.row][@"QuestionDisplayName"];
                height = [self calculateStringHeigth:size text:string andFont:[UIFont systemFontOfSize:15]];
                
                NSString *string1 =  dataList[indexPath.row][@"QuestionValue"];
                height =height+ [self calculateStringHeigth:size text:string1 andFont:[UIFont systemFontOfSize:14]];
                //        if (height > 39.0f)
                //        {
                //            //we know exactly what will happen
                //            height = height+5 ;
                //        }
                //        else{
                height = 60;
                // }
                
                
                NSLog(@"Dropdown(Multiple)height%d",height);
            }
    //height = 51;
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Checkbox(Multiple)"]) {
                CGSize size = self.tblForm.frame.size;
                size.width = size.width - 78;
                NSString *string = [dataList[indexPath.row][@"IsRequired"] boolValue] ? [NSString stringWithFormat:@"%@ *",dataList[indexPath.row][@"QuestionDisplayName"]] : dataList[indexPath.row][@"QuestionDisplayName"];
                height = [self calculateStringHeigth:size text:string andFont:[UIFont systemFontOfSize:14]];
                
                NSString *string1 =  dataList[indexPath.row][@"QuestionValue"];
                height =height+ [self calculateStringHeigth:size text:string1 andFont:[UIFont systemFontOfSize:14]];
                
                if (height > 39.0f)
                {
                    //we know exactly what will happen
                    height = height+5 ;
                }
                else{
                    height = 60;
                }
                
                
            }
    //height = 51;
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Date"])
                height = 60;
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Time"])
                height = 60;
            else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Radio"]) {
                CGSize size = self.tblForm.frame.size;
                size.width = size.width - 78;
                NSString *string = dataList[indexPath.row][@"QuestionDisplayName"];
                height = [self calculateStringHeigth:size text:string andFont:[UIFont systemFontOfSize:14]];
                
                NSString *string1 =   dataList[indexPath.row][@"QuestionValue"];
                height =height+ [self calculateStringHeigth:size text:string1 andFont:[UIFont systemFontOfSize:14]];
                
                height = 62;
            }
    //height = 51;
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    editingsubIndexPath=indexPath;

    rowOfTheCell=indexPath.row;
  if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Image"])
    {
//
//        [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
//        self.saveBtn.enabled = YES;
//
//        [self performSegueWithIdentifier:@"ImageselectionSegue" sender:self];
        
    }
    if(([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown(Multiple)"]) || ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Checkbox(Multiple)"]) || ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown"])||([dataList[indexPath.row][@"DataType"] isEqualToString:@"Dropdown(Special)"]) || ([dataList[indexPath.row][@"DataType"] isEqualToString:@"Radio"])) {
        
        
        [self performSegueWithIdentifier:@"selectionsegue" sender:indexPath];
    }
    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Date"])
    {
        DateAndTimeTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        [self showDatePicker:UIDatePickerModeDate currentLbl:cell.lblValue];
    }
    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Time"])
    {
        DateAndTimeTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        [self showDatePicker:UIDatePickerModeTime currentLbl:cell.lblValue];
        
    }
    else if([dataList[indexPath.row][@"DataType"] isEqualToString:@"Checkbox"]) {
        
    }
}



-(void)viewimag_taping:(UITapGestureRecognizer *)recognizer {
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    rowOfTheCell=recognizer.view.tag;
    [self performSegueWithIdentifier:@"ImageselectionSegue" sender:self];
    
    // [self performSegueWithIdentifier:@"dynamiccamera" sender:indexPath];
    
}



-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.saveBtn.enabled = YES;
    [self performSegueWithIdentifier:@"dynamiccamera" sender:self];
    
    // [self performSegueWithIdentifier:@"dynamiccamera" sender:indexPath];
    
}



-(void) showDatePicker: (UIDatePickerMode) modeDatePicker currentLbl:(UILabel *)lbl
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    if(modeDatePicker == UIDatePickerModeDate) {
       
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd/yyyy"];
        NSString *dateanswerstr=dataList[rowOfTheCell][@"DateAnswerText"];
        if([dateanswerstr isEqualToString:@""]||[dateanswerstr isKindOfClass:[NSNull class]]){}else{
   NSData *date=[formatter dateFromString:dateanswerstr];
            [picker setDate:date];}
    }else{
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
     [formatter setDateFormat:@"hh:mm a"];
        NSString *dateanswerstr=dataList[rowOfTheCell][@"TimeAnswerText"];;
        if([dateanswerstr isEqualToString:@""]||[dateanswerstr isKindOfClass:[NSNull class]]){}else{
            NSData *date=[formatter dateFromString:dateanswerstr];
            [picker setDate:date];}
    }
    
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                     if(modeDatePicker == UIDatePickerModeDate) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                                        NSString *datestring= [formatter stringFromDate:picker.date];
                                         
                                         lbl.text=datestring;
                                         
                                         NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[lbl.tag]];
                                         [dic setObject:[formatter stringFromDate:picker.date] forKey:@"DateAnswerText"];
                                         [dic setObject:[formatter stringFromDate:picker.date]forKey:@"DateAnswer"];
                                         [dic setObject:[formatter stringFromDate:picker.date] forKey:@"DisplayDateAnswerText"];
                                         NSLog(@"dic : %@",dic);
                                         [dataList replaceObjectAtIndex:lbl.tag withObject:dic];
                                     }
                                     else {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"hh:mm a"];
                                         NSString *datestring= [formatter stringFromDate:picker.date];
                                         
                                         lbl.text=datestring;
                                         
                                         
                                         NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[lbl.tag]];
                                         
                                         [dic setObject:lbl.text forKey:@"TimeAnswer"];
                                         [dic setObject:lbl.text forKey:@"TimeAnswerText"];
                                         
                                         [dic setObject:lbl.text forKey:@"DisplayTimeAnswerText"];
                                         NSLog(@"dic : %@",dic);
                                         [dataList replaceObjectAtIndex:lbl.tag withObject:dic];
                                     }
                                     
                                 }];
    [alertController addAction:doneAction];
    
    
    UIAlertAction *clearAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                      if(modeDatePicker == UIDatePickerModeDate) {
                                          
                                          NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[lbl.tag]];
                                          [dic setObject:@"" forKey:@"DateAnswerText"];
                                          [dic setObject:@"" forKey:@"DisplayDateAnswerText"];
                                          [dic setObject:@""forKey:@"DateAnswer"];

                                          [dataList replaceObjectAtIndex:lbl.tag withObject:dic];
                                          
                                      }else{
                                          
                                          
                                          NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[lbl.tag]];
                                          [dic setObject:@""forKey:@"TimeAnswer"];

                                          [dic setObject:@""forKey:@"TimeAnswerText"];
                                          
                                          [dic setObject:@"" forKey:@"DisplayTimeAnswerText"];
                                          [dataList replaceObjectAtIndex:lbl.tag withObject:dic];
                                          
                                      }
                                      NSLog(@"Clear action");
                                      lbl.text=@"";
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    [alertController addAction:cancelAction];

    
    [alertController addAction:clearAction];

    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController  animated:YES completion:nil]; }

-(void)setSelectedDateInField
{
    NSLog(@"date :: %@",datePicker.date.description);
    //set Date formatter
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"MMMM dd, yyyy"];
    
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//
//    if ([[segue identifier] isEqualToString:@"suborderlist_segue"])
//    {
//
//        NSIndexPath *indexpath = (NSIndexPath *)sender;
//        Suborder_listVC *vc = [segue destinationViewController];
//
//        vc.noteType = lblTitle.text;
//          vc.noteID = _WorkorderId;
//    }else
//    if ([[segue identifier] isEqualToString:@"ImageselectionSegue"])
//    {
//
//        NSIndexPath *indexpath = (NSIndexPath *)sender;
//        Image_SelectionViewController *vc = [segue destinationViewController];
//        vc.listInfo = dataList[rowOfTheCell];
//        //vc.delegate = self;
//        vc.noteType = lblTitle.text;
//    }else
    if ([[segue identifier] isEqualToString:@"selectionsegue"])
    {
  
        NSIndexPath *indexpath = (NSIndexPath *)sender;
        SelectionViewController *vc = [segue destinationViewController];
        vc.listInfo = dataList[indexpath.row];
        vc.delegate = self;
        vc.TaskName=_TaskName;
        vc.ProjectName=_ProjectName;
        vc.JobNumber=_JobNumber; 
        vc.Workorder_type=_childname;

    }
//    else if([[segue identifier] isEqualToString:@"dynamiccamera"]){
//        dynamiccameraVC *vc = [segue destinationViewController];
//        vc.noteType = lblTitle.text;
//
//    }
}


-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 - (IBAction)cancelAction:(id)sender {
     
   //  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     [self performSegueWithIdentifier:@"suborderVCUnwindSegue" sender:self];

     
    
}

- (IBAction)alertaction:(id)sender {
    
    [self performSegueWithIdentifier:@"alertsegue" sender:self];
    
}

-(BOOL) isValidate:(NSArray *)list {
    
    for(NSDictionary *dic in list) {
        
        if([dic[@"IsRequired"] boolValue])
        {
            if([dic[@"DataType"] isEqualToString:@"NumericTextBox"])
            {
                if([[NSString stringWithFormat:@"%@",dic[@"NumericAnswer"]] length]  >0){
                    NSLog(@"Numerictext%@",dic[@"NumericAnswer"] );
                    
                    if([dic[@"NumericAnswer"] floatValue] < [dic[@"MinValue"] floatValue])
                    {
                        return YES;
                    }
                    else if([dic[@"NumericAnswer"] floatValue] > [dic[@"MaxValue"] floatValue])
                    {
                        return YES;
                    }
                    
                }
                
                
                else{
                    return NO;
                }
                
            }else
            if([dic[@"DataType"] isEqualToString:@"Dropdown"])
            {
                if(![dic[@"QuestionValue"] length] ){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"TextBoxMultiline"])
            {
                if(![dic[@"TextAnswer"] length] ){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"TextBox"])
            {
                if(![dic[@"TextAnswer"] length] ){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"Checkbox"])
            {
                // if(!dic[@"BooleanAnswer"] ){
                if(![dic[@"BooleanAnswer"] boolValue]){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"Dropdown(Multiple)"])
            {
                if(![dic[@"QuestionValue"] length] ){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"Checkbox(Multiple)"])
            {
                if(![dic[@"QuestionValue"] length] ){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"Radio"])
            {
                if(![dic[@"QuestionValue"] length] ){
                    return NO;
                }
            }
            else if([dic[@"DataType"] isEqualToString:@"Date"])
            {
                
                if(![dic[@"DateAnswer"] length] ){
                    return NO;
                }
                
            }
            else if([dic[@"DataType"] isEqualToString:@"Time"])
            {
                if(![dic[@"TimeAnswer"] length] ){
                    return NO;
                }
            }
        }
    }
    
    return YES;
    
}

-(BOOL) isValidatenumeric:(NSArray *)list {
    
    for(NSDictionary *dic in list) {
        
        
        if([dic[@"DataType"] isEqualToString:@"NumericTextBox"])
        {
            NSLog(@"data%@",dic );

            if([[NSString stringWithFormat:@"%@",dic[@"NumericAnswer"]] length]  >0){
                NSLog(@"Numerictext%@",dic[@"NumericAnswer"] );
                
                if([dic[@"NumericAnswer"] floatValue] < [dic[@"MinValue"] floatValue])
                {
                    return NO;
                }
                else if([dic[@"NumericAnswer"] floatValue] > [dic[@"MaxValue"] floatValue])
                {
                    return NO;
                }
                else if([dic[@"NumericAnswer"] floatValue]==0)
                {
                    return YES;
                }
            }
            
            //
            //            else{
            //                return NO;
            //            }
            
        }
    }
    
    return YES;
    
}
- (IBAction)saveAction:(id)sender {
    
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
        return;
    }else{
    if([self isValidate:dataList]){
      if([self isValidatenumeric:dataList]){
       if (![self isNetworkAvailable]) {
            [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
            return;
        }else{
            [[self view]endEditing:YES];
       
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                [self.saveBtn setTitleColor:[UIColor  colorWithRed:85/255.0f green:85/255.0f blue:85/255.0f alpha:1.0] forState:UIControlStateNormal];
              ///  NSLog(@"json%@",dataList);
                NSDictionary *json = @{
                                     
                                       @"RoleName": [[NSUserDefaults standardUserDefaults] objectForKey:userRole],
                                       @"UserName": [[NSUserDefaults standardUserDefaults] objectForKey:username],
                                        @"WorkOrderId" :_WorkorderId,
                                       @"SubWorkorderId":_Sub_WorkorderId,
                                        @"List" :dataList
                                       };
                NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
                [dictEntry setObject:json forKey:[NSString stringWithFormat:@"SubWorkOrderModel"]];
            NSLog(@"dictEntry %@",dictEntry);

                DownloadManager *downloadObj = [[DownloadManager alloc]init];
                [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@UpdateSubWorkOrderDetails",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"UpdateSubWorkOrderDetails"];
         }
    }
      else {
          UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please fill values within range" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
          [callAlert show];
      }
    }
    else {
         UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please fill required fields" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
               [callAlert show];
    }
    
    
}
}
#pragma mark - API Delegate

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Server may be busy. Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


- (IBAction)logoutAction:(id)sender {
       [self logoutUser];
    
}

//- (IBAction)changepasswordAction:(id)sender {
//    [self changePassword];
//}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (0 == buttonIndex && alertView.tag == 55)
    {
        
        [self performSegueWithIdentifier:@"suborderVCUnwindSegue" sender:self];
        
    }
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"response : %@",response);
    
    [self.saveBtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
//    if ([key isEqualToString:@"AutoUpdateSubWorkOrderList"])
//    { //[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//
//        [self performSegueWithIdentifier:@"suborderVCUnwindSegue" sender:self];
//
//    }
//    else
        if ([key isEqualToString:@"GetProctorItem"])
    { [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         NSString   *VisualClassification =[[response valueForKeyPath:@"Data.VisualClassification"]description];
         NSString   *SampleSource =[[response valueForKeyPath:@"Data.SampleSource"]description];
         NSString   *MaximumDryDensity =[[response valueForKeyPath:@"Data.MaximumDryDensity"]description] ;
         NSString   *OptimumMoisture = [[response valueForKeyPath:@"Data.OptimumMoisture"]description];
     int index = 0;
        NSMutableArray *tempAry = [dataList mutableCopy];

        for(NSDictionary *dic in dataList) {
            
        //if(([dic[@"DataType"] isEqualToString:@"Dropdown(Special)"]) )
           // {
                
//
//                if (js.getString("QuestionId").equals("1199"))
//                    js.put("TextAnswer", VisualClassification);
//                else if (js.getString("QuestionId").equals("1200"))
//                    js.put("TextAnswer", SampleSource);
//                else if (js.getString("QuestionId").equals("1201"))
//                    js.put("NumericAnswer", MaximumDryDensity);
//                else if (js.getString("QuestionId").equals("1199"))
//                    js.put("NumericAnswer", OptimumMoisture);
                if([dic[@"QuestionId"] intValue] == 1199)
                {
                    
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[index]];
                    [dic setObject:VisualClassification forKey:@"TextAnswer"];
                    NSLog(@"dic : %@",dic);
                    [tempAry replaceObjectAtIndex:index withObject:dic];
                    
                }
                
                else   if([dic[@"QuestionId"] intValue] == 1200)
                {
                    
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[index]];
                    [dic setObject:SampleSource forKey:@"TextAnswer"];
                    NSLog(@"dic : %@",dic);
                    [tempAry replaceObjectAtIndex:index withObject:dic];
                    
                    
                }
                else   if([dic[@"QuestionId"] intValue] == 1201)
                {
                    
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[index]];
                    [dic setObject:MaximumDryDensity forKey:@"NumericAnswer"];
                    NSLog(@"dic : %@",dic);
                    [tempAry replaceObjectAtIndex:index withObject:dic];
                    
                }
                else   if([dic[@"QuestionId"] intValue] == 1202)
                {
                    
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:dataList[index]];
                    [dic setObject:OptimumMoisture forKey:@"NumericAnswer"];
                    NSLog(@"dic : %@",dic);
                    [tempAry replaceObjectAtIndex:index withObject:dic];
                    
                }
                  if([dic[@"DataType"] isEqualToString:@"NumericTextBox"] )
                {
                    
                    if([[dic[@"NumericAnswer"] description] isEqualToString:@"-1"])
                    {
                        
                        NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
                        [tempDic setObject:@"" forKey:@"NumericAnswer"];
                        [tempAry replaceObjectAtIndex:index withObject:tempDic];
                        
                    }
                }
                
           // }
            
            index++;

            }
        dataList = [tempAry mutableCopy];

   [_tblForm reloadData];
        
    }
    else
    if ([key isEqualToString:@"UpdateSubWorkOrderDetails"])
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
        {
            [self performSegueWithIdentifier:@"suborderVCUnwindSegue" sender:self];

//            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//              callAlert.tag = 55;
//            [callAlert show];
         
        }
        else{
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //  callAlert.tag = 55;
            [callAlert show];
        }
    }
    
    else if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efielddata Message" message:@"Please try again later"];
        }
    }
    
}
 
 
@end
