//
//  AlertListVC.h
//  Efield
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface childform_list : ViewController {
    __weak IBOutlet UITableView *suborder_listTable;
     NSArray *tempArr;
    NSMutableArray *alertList;
    IBOutlet UITextField *jobdate;
    IBOutlet UITextField *workorder;
    NSMutableArray *checkBoxArr;
    NSInteger index;
    NSDictionary *dummyDic;
    NSMutableDictionary *alertListDic;
    UIRefreshControl *refreshControl;
    __weak IBOutlet UIView *tableFooterView;
    __weak IBOutlet UIBarButtonItem *markAllButton;
}
 @property (weak, nonatomic) IBOutlet UILabel *alertcnt; 
@property (nonatomic, retain) NSString *TaskName,*JobNumber,*ProjectName;

@property (weak, nonatomic) IBOutlet UILabel *taskname_lbl;
@property (weak, nonatomic) IBOutlet UILabel *jobnumber;
@property(nonatomic, retain)NSString *WorkorderId,*Sub_WorkorderId,*Testtype_ID,*Testtype_number,*childname;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
- (IBAction)logoutAction:(id)sender;
- (IBAction)changepasswordAction:(id)sender;
- (IBAction)showMoreAction:(id)sender;
- (IBAction)editAction:(id)sender;
- (IBAction)jobdateaction:(id)sender;
- (IBAction)markAllAction:(id)sender;
- (IBAction)Specialization:(id)sender;
- (IBAction)add_patient:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *Header_lbl;
@property (weak, nonatomic) IBOutlet UIButton *addpatient_button;
- (IBAction)add_action:(id)sender;
- (IBAction)cancel_action:(id)sender;

- (void)updateData;
@end
