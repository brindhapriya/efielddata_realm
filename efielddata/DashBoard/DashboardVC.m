//
//  DashboardVC.m
//  Efield
//
//  Created by iPhone on 23/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import "DashboardVC.h"
 #import "Constants.h"
#import "workorderVC.h"


@interface DashboardVC ()<UIWebViewDelegate>

{
    Boolean isdashBoardTablehidden;
    NSUserDefaults * defaults;
    NSString *DraftwoTotal,*ReportwoTotal,*ReviewedwoTotal,*NewwoTotal;
    NSString *statusstring;
}
@end

@implementation DashboardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
  //  self.alertcnt.text=[self updateALERTcount];
    _alertcnt.text=[self updateALERTcount];

     self.navigationController.navigationBar.hidden = YES;
    _companynametxt.text=[[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
    
    NSString *eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@", eventDateStr);
    _date.text=eventDateStr;
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor whiteColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self
                            action:@selector(refreshData)
                  forControlEvents:UIControlEventValueChanged];
    [dashBoardTable addSubview:refreshControl];
    dashBoardTable.allowsSelection = NO;
// [self updateALERTcount];
}


 
- (void)viewWillAppear:(BOOL)animated
{
    
    
    [super viewWillAppear:animated];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"DisplayJobDateText"];

    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"datereplace"];

      _alertcnt.text=[self updateALERTcount];
    [self refreshData];
}
- (void)refreshData
{
         [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
      [downloadObj callServerWithURL:
       [NSString stringWithFormat:@"%@GetUserDashboard?UserName=%@&RoleName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[NSUserDefaults standardUserDefaults] objectForKey:userRole]]andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetPhysicianDashboard"];
    
    [refreshControl performSelector:@selector(endRefreshing) withObject:nil afterDelay:1.0f];
}
- (void)loadData
{
    
    
    dashBoardList = [[NSMutableArray alloc]init];
     
    NSDictionary *Hospital = @{@"title":@"New/InProgress Work Orders", @"icon":@"pending_report", @"bgcolor":@0xCEC1EF, @"count":NewwoTotal};
    NSDictionary *InpatientSNF = @{@"title":@"Completed Work Orders", @"icon":@"completed_report", @"bgcolor":@0xF4ADA1, @"count":DraftwoTotal};

    NSDictionary *OutpatientRehab = @{@"title":@"Reviewed Reports", @"icon":@"cancelled_report", @"bgcolor":@0xFADFA4, @"count":ReviewedwoTotal};
    NSDictionary *HomewithSelfCare = @{@"title":@"Reports Sent to client", @"icon":@"review_report", @"bgcolor":@0x9ED8ED, @"count":ReportwoTotal};
    
    [dashBoardList addObject:Hospital];
    [dashBoardList addObject:InpatientSNF];
   // [dashBoardList addObject:HomeHealth];
    [dashBoardList addObject:OutpatientRehab];
    [dashBoardList addObject:HomewithSelfCare];
    
    [dashBoardTable reloadData];
}

#pragma mark - API Delegate

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"success%@",response);

    if([key isEqualToString:@"GetPhysicianDashboard"])
    {
        DraftwoTotal=[[response valueForKeyPath:@"Data.DraftwoTotal"]description];
        ReportwoTotal=[[response valueForKeyPath:@"Data.CanceledTotal"]description];
        ReviewedwoTotal=[[response valueForKeyPath:@"Data.ReviewedwoTotal"]description];
        NewwoTotal=[[response valueForKeyPath:@"Data.NewwoTotal"]description];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self loadData];
    } else if([key isEqualToString:@"GetAllCurrentUserAlertCountForApp"])
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSLog(@"data%@",response);
    }
    else if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [self logoutUser];
        }
        else
        {
            [self showAlertWithTitle:@"Efield Message" message:@"Please try again later"];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dashBoardList.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    switch (orientation) {
        case 1:
        case 2:
            NSLog(@"portrait");
            // your code for portrait...
            //return  130;
       return    self.view.bounds.size.height/4-30;

            break;
            
        case 3:
        case 4:
            NSLog(@"landscape");
            // your code for landscape...
          //  return 130;
          return self.view.bounds.size.height/4+10;

            break;
        default:
            NSLog(@"other");
            // your code for face down or face up...
           // return  130;
           return self.view.bounds.size.height/4-35;

            break;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DashBoardTableViewCell *cell = nil;
    NSString *cellIdentifier = @"cell";
 cell = (DashBoardTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.bgView.layer.cornerRadius = 10;
    cell.bgview1.layer.cornerRadius = 10;

    if(indexPath.row==0){
    cell.bgView.backgroundColor = UIColorFromRGBA([@0xCEC1EF intValue], 1.0);
        cell.countLbl.text=NewwoTotal;
//        cell.iconimageview_1.image= [UIImage imageNamed:@"pending_report"];
//        cell.iconImageView.image= [UIImage imageNamed:@"all_pending"];
//
        cell.iconimageview_1.image= [UIImage imageNamed:@"all_pending"];
   cell.iconImageView.image= [UIImage imageNamed:@"all_pending"];
            cell.bgView.backgroundColor = UIColorFromRGBA([@0xccc0ed intValue], 1.0);
        cell.titleLbl1.text=@"Pending WO";
        cell.titleLbl.text=@"All Pending WO";
          cell.bgview1.layer.borderWidth = 3;
          cell.bgview1.layer.borderColor =[UIColorFromRGBA([@0xccc0ed intValue], 1.0) CGColor];
        //[UIColorFromRGBA([@0x804e76 intValue], 1.0) CGColor];

    }
    else
        if(indexPath.row==1){
            cell.bgView.backgroundColor = UIColorFromRGBA([@0xFADFA4 intValue], 1.0);
          //  cell.bgview1.backgroundColor = UIColorFromRGBA([@0x4C9EF5  intValue], 1.0);
           //   cell.bgview1.layer.borderColor = [UIColorFromRGBA([@0x4C9EF5  intValue], 1.0) CGColor];
            cell.bgview1.layer.borderWidth = 3;
            cell.bgview1.layer.borderColor =[UIColorFromRGBA([@0xFADFA4 intValue], 1.0) CGColor];
            //[UIColorFromRGBA([@0x4C9EF5 intValue], 1.0) CGColor];
            cell.iconimageview_1.image= [UIImage imageNamed:@"completed_report"];
            cell.titleLbl1.text=@"Completed WO";
            cell.titleLbl.text=@"All Completed WO";
      cell.iconImageView.image= [UIImage imageNamed:@"all_completed"];
            cell.iconimageview_1.image= [UIImage imageNamed:@"all_completed"];

            cell.countLbl.text=DraftwoTotal;
            //ReportwoTotal;
        }
 else   if(indexPath.row==2){
        cell.bgView.backgroundColor = UIColorFromRGBA([@0xF4ADA1 intValue], 1.0);
          cell.countLbl.text=ReportwoTotal;
     //fa6b7a
        cell.bgview1.layer.borderWidth = 3;
       cell.bgview1.layer.borderColor =[UIColorFromRGBA([@0xF4ADA1 intValue], 1.0)CGColor];
     //  cell.bgview1.layer.borderColor = [UIColorFromRGBA([@0xFA6B7A  intValue], 1.0) CGColor];
     // cell.bgview1.backgroundColor = UIColorFromRGBA([@0xFA6B7A  intValue], 1.0);
     cell.titleLbl1.text=@"Cancelled Reports";
     cell.titleLbl.text=@"All Cancelled Reports";
 cell.iconImageView.image= [UIImage imageNamed:@"All_cancelled"];
     cell.iconimageview_1.image= [UIImage imageNamed:@"cancelled_report"];
     cell.iconimageview_1.image= [UIImage imageNamed:@"All_cancelled"];

    }
 else   if(indexPath.row==3){
     
 
     cell.bgView.backgroundColor = UIColorFromRGBA([@0x9DD7EB intValue], 1.0);
   //  cell.bgview1.backgroundColor = UIColorFromRGBA([@0xFAA770   intValue], 1.0);
      cell.bgview1.layer.borderColor = [UIColorFromRGBA([@0x9DD7EB  intValue], 1.0) CGColor];
     // cell.bgview1.layer.borderColor = [UIColorFromRGBA([@0xFAA770  intValue], 1.0) CGColor];
        cell.bgview1.layer.borderWidth = 3;
     cell.iconimageview_1.image= [UIImage imageNamed:@"review_report"];
      cell.iconImageView.image= [UIImage imageNamed:@"all_review"];
     cell.iconimageview_1.image= [UIImage imageNamed:@"all_review"];

     cell.titleLbl1.text=@"Reviewed Reports";
     cell.titleLbl.text=@"All Reviewed Reports";
 
     cell.countLbl.text=  ReviewedwoTotal;

 }
    cell.bgView.tag=indexPath.row;
    cell.bgview1.tag=indexPath.row;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Myreports:)];
    cell.bgView.userInteractionEnabled = YES;
    [cell.bgView addGestureRecognizer:tapRecognizer];
    UITapGestureRecognizer *tapRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Allreports:)];
    cell.bgview1.userInteractionEnabled = YES;
    
    [ cell.bgview1 addGestureRecognizer:tapRecognizer1];
    return cell;
}


- (void)Allreports:(UITapGestureRecognizer *)tapGesture {
    
    if(tapGesture.view.tag==0){
        statusstring=@"All Pending WO";
        
    }
    else  if(tapGesture.view.tag==1){
          statusstring=@"All Completed WO";
        
    }
    else  if(tapGesture.view.tag==2){
          statusstring=@"All Cancelled Reports";
      
    }
    else  if(tapGesture.view.tag==3){
          statusstring=@"All Reviewed Reports";
       
    }
    [self performSegueWithIdentifier:@"workorder_segue" sender:nil];

}


- (void)Myreports:(UITapGestureRecognizer *)tapGesture {
    
   
    if(tapGesture.view.tag==0){
        
        UINavigationController *nav = [[self.tabBarController childViewControllers] objectAtIndex:0];
        
        self.tabBarController.selectedIndex = 0;
    }
    else  if(tapGesture.view.tag==1){
        UINavigationController *nav = [[self.tabBarController childViewControllers] objectAtIndex:1];
        
        self.tabBarController.selectedIndex = 1;
    }
    else  if(tapGesture.view.tag==2){
           statusstring=@"My Cancelled Reports";
        [self performSegueWithIdentifier:@"workorder_segue" sender:nil];

     
    }
    else  if(tapGesture.view.tag==3){
        statusstring=@"My Reviewed Reports";

        [self performSegueWithIdentifier:@"workorder_segue" sender:nil];

    }
   
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if ([[[dashBoardList objectAtIndex:indexPath.row] objectForKey:@"count"] intValue])
//    {
}
 
- (IBAction)logoutAction:(id)sender
{
    
    [self logoutUser];

}
//
//- (IBAction)changepasswordAction:(id)sender {
//    [self changePassword];
//}

- (IBAction)add_WOaction:(id)sender {
    //  [self performSegueWithIdentifier:@"addWO_segue" sender:self];
    [self performSegueWithIdentifier:@"WO_listsegue" sender:self];
    
    //WO_listsegue
    //addWO_segue
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"workorder_segue"]) {

        workorderVC *viewController = [segue destinationViewController];
        viewController.statusreport=statusstring;
    }
}
@end
