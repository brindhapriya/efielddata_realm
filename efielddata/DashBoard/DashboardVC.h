//
//  DashboardVC.h
//  Efield
//
//  Created by iPhone on 23/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashBoardTableViewCell.h"
#import "ViewController.h"

@interface DashboardVC : ViewController
{
    NSMutableArray *dashBoardList;
    __weak IBOutlet UITableView *dashBoardTable;
    UIRefreshControl *refreshControl;
    
    //__weak IBOutlet UIActivityIndicatorView *activityIndicator;
}


@property (weak, nonatomic) IBOutlet UILabel *companynametxt;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *alertcnt;
- (IBAction)logoutAction:(id)sender; 
- (void)loadData;
- (IBAction)add_WOaction:(id)sender;

@end
