//
//  LoginViewController.h
//  Efield
//
//  Created by iPhone on 23/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ViewController.h"  

@interface LoginViewController : ViewController
{
    NSString *usernameStr, *emailStr;
    __weak IBOutlet UIImageView *logoImageView;
    __weak IBOutlet UILabel *unableLoginLbl;
    __weak IBOutlet UITextField *passwordTxt;
    __weak IBOutlet UITextField *usernameTxt;
    __weak IBOutlet UIButton *rememberBtn;
    __weak IBOutlet UIButton *loginBtn;
    __weak IBOutlet UIView *loginView;
    __weak IBOutlet UIScrollView *mainScrollView;
    __weak IBOutlet NSLayoutConstraint *widthConstrain;
    __weak IBOutlet NSLayoutConstraint *leftSpacingContrain;
    __weak IBOutlet NSLayoutConstraint *topSpacingContrain;
    __weak IBOutlet UILabel *reset_accesscode;
    __weak IBOutlet UILabel *company_name;
}
- (IBAction)loginAction:(id)sender;
- (IBAction)rememberMeAction:(id)sender;
- (IBAction)reset_accesscode:(id)sender;
@end
