//
//  LoginViewController.m
//  Efield
//
//  Created by iPhone on 23/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "LoginViewController.h"

@import LocalAuthentication;

@interface LoginViewController () <MFMailComposeViewControllerDelegate>
@end

@implementation LoginViewController
LAContext *context;
BOOL success;
- (void)viewDidLoad {
    [super viewDidLoad];
    company_name.text =
    [[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    
    context = [[LAContext alloc] init];
    loginView.backgroundColor = [UIColor whiteColor];
   
    CGFloat borderWidth = 1.0f;
    
    loginView.layer.borderColor = [UIColor colorWithRed:0.0 / 255.0
                                                  green:156.0 / 255.0
                                                   blue:21.0 / 255.0
                                                  alpha:1]
    .CGColor;
    loginView.layer.borderWidth = borderWidth;
    
    loginView.layer.cornerRadius = 5.0f;
    logoImageView.layer.cornerRadius = 5.0f;
    loginBtn.layer.cornerRadius = 5.0f;
    rememberBtn.backgroundColor = [UIColor clearColor];
    [rememberBtn setImage:[UIImage imageNamed:@"uncheckedCheckbox"]
                 forState:UIControlStateNormal];
    [rememberBtn setImage:[UIImage imageNamed:@"checkedCheckbox"]
                 forState:UIControlStateSelected];
    
    unableLoginLbl.userInteractionEnabled = YES;
    
    UIApplicationState state =
    [[UIApplication sharedApplication] applicationState];
    if (state != UIApplicationStateBackground) {
        if ([TouchIDEnabled_Key
             isEqualToString:[[NSUserDefaults standardUserDefaults]
                              objectForKey:TouchIDEnabled]]) {
                 
                 if ([[NSUserDefaults standardUserDefaults] objectForKey:username] ==
                     [NSNull null] ||
                     [[[NSUserDefaults standardUserDefaults] objectForKey:username]
                      isEqualToString:@""] ||
                     [[NSUserDefaults standardUserDefaults] objectForKey:username] ==
                     nil ||
                     [[NSUserDefaults standardUserDefaults] objectForKey:login_password] ==
                     [NSNull null] ||
                     [[[NSUserDefaults standardUserDefaults] objectForKey:login_password]
                      isEqualToString:@""] ||
                     [[NSUserDefaults standardUserDefaults] objectForKey:login_password] ==
                     nil) {
                     
                 } else {
                     AppDelegate *appDelegate =
                     (AppDelegate *)[[UIApplication sharedApplication] delegate];
                     [appDelegate authenticateUser];
                 }
             }
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber =
    [[NSString stringWithFormat:@"0"] integerValue];
    // Do any additional setup after loading the view.
}

- (void)setConstrainForiPad {
    
    UIInterfaceOrientation orientation =
    [UIApplication sharedApplication].statusBarOrientation;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [mainScrollView removeConstraint:leftSpacingContrain];
        topSpacingContrain.constant =
        ([UIScreen mainScreen].bounds.size.height - 446) / 2;
        widthConstrain.constant = 350.0;
    } else if (UIInterfaceOrientationIsLandscape(orientation) &&
               [UIDevice currentDevice].userInterfaceIdiom ==
               UIUserInterfaceIdiomPhone) {
        [mainScrollView removeConstraint:leftSpacingContrain];
    } else {
    [loginView removeConstraint:widthConstrain];
    }
}
- (void)setFooterView {
    
    UIView *footerView;
    for (UIView *view in [mainScrollView subviews]) {
        if ([view isKindOfClass:[UIView class]] && view.tag == 1000) {
            [view removeFromSuperview];
            NSLog(@"Removed old view");
        }
    }
    UIInterfaceOrientation orientation =
    [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        if ([self isiPad]) {
            
            footerView = [[UIView alloc]
                          initWithFrame:CGRectMake(
                                                   0, [UIScreen mainScreen].bounds.size.height - 60,
                                                   [UIScreen mainScreen].bounds.size.width, 60)];
        } else {
            footerView = [[UIView alloc]
                          initWithFrame:CGRectMake( 0,[UIScreen mainScreen].bounds.size.height - 20,
                                                   [UIScreen mainScreen].bounds.size.width,
                                                   60)];
        }
        footerView.tag = 1000;
        logoImageView.contentMode = UIViewContentModeCenter;
    } else {
  
        logoImageView.contentMode = UIViewContentModeScaleAspectFit;
        if ([UIScreen mainScreen].bounds.size.height <= 480) {
            footerView = [[UIView alloc]
                          initWithFrame:CGRectMake(0, 540,
                                                   [UIScreen mainScreen].bounds.size.width,
                                                   60)];
        } else {
            if ([[UIDevice currentDevice] userInterfaceIdiom] ==
                UIUserInterfaceIdiomPhone) {
                
                switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                        
                    case 1136:
                        footerView = [[UIView alloc]
                                      initWithFrame:CGRectMake(
                                                               0,
                                                               [UIScreen mainScreen].bounds.size.height - 60,
                                                               [UIScreen mainScreen].bounds.size.width, 60)];
                        break;
                        
                    case 1334:
                        footerView = [[UIView alloc]
                                      initWithFrame:CGRectMake(
                                                               0,
                                                               [UIScreen mainScreen].bounds.size.height - 60,
                                                               [UIScreen mainScreen].bounds.size.width, 60)];
                        break;
                    case 1920:
                        footerView = [[UIView alloc]
                                      initWithFrame:CGRectMake(
                                                               0,
                                                               [UIScreen mainScreen].bounds.size.height - 60,
                                                               [UIScreen mainScreen].bounds.size.width, 60)];
                        break;
                        
                    case 2208:
                        footerView = [[UIView alloc]
                                      initWithFrame:CGRectMake(
                                                               0,
                                                               [UIScreen mainScreen].bounds.size.height - 60,
                                                               [UIScreen mainScreen].bounds.size.width, 60)];
                        break;
                        
                    case 2436:
                        footerView = [[UIView alloc]
                                      initWithFrame:CGRectMake(
                                                               0,
                                                               [UIScreen mainScreen].bounds.size.height - 100,
                                                               [UIScreen mainScreen].bounds.size.width, 60)];
                        break;
                        
                    case 2688:
                        footerView = [[UIView alloc]
                                      initWithFrame:CGRectMake(
                                                               0,
                                                               [UIScreen mainScreen].bounds.size.height - 100,
                                                               [UIScreen mainScreen].bounds.size.width, 60)];
                        break;
                        
                    case 1792:
                        footerView = [[UIView alloc]
                                      initWithFrame:CGRectMake(
                                                               0,
                                                               [UIScreen mainScreen].bounds.size.height - 100,
                                                               [UIScreen mainScreen].bounds.size.width, 60)];
                        break;
                        
                    default:
                        footerView = [[UIView alloc]
                                      initWithFrame:CGRectMake(
                                                               0,
                                                               [UIScreen mainScreen].bounds.size.height - 90,
                                                               [UIScreen mainScreen].bounds.size.width, 60)];
                        break;
                }
            }
            
            else {
                footerView = [[UIView alloc]
                              initWithFrame:CGRectMake(
                                                       0, [UIScreen mainScreen].bounds.size.height - 60,
                                                       [UIScreen mainScreen].bounds.size.width, 60)];
            }
        }
        
        footerView.tag = 1000;
       
    }
    footerView.backgroundColor = [UIColor clearColor];
    
    UILabel *copyRighstsLbl = [[UILabel alloc] init];
    copyRighstsLbl.frame = footerView.bounds;
    copyRighstsLbl.textAlignment = NSTextAlignmentCenter;
    copyRighstsLbl.numberOfLines = 0;
    copyRighstsLbl.backgroundColor = [UIColor clearColor];
    copyRighstsLbl.textColor = [UIColor blackColor];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    
    NSString *currentVersion = infoDictionary[@"CFBundleShortVersionString"];
    
    copyRighstsLbl.text = [NSString
                           stringWithFormat:@"EfieldData @2019, Version %@", currentVersion];
    //@"EfieldData @2018, Version 2.1";
    NSMutableAttributedString *string =
    [[NSMutableAttributedString alloc] initWithString:copyRighstsLbl.text];
    [string addAttribute:NSForegroundColorAttributeName
                   value:[UIColor blueColor]
                   range:NSMakeRange(0, 10)];
    [string addAttribute:NSFontAttributeName
                   value:[UIFont systemFontOfSize:13.0]
                   range:NSMakeRange(0, 10)];
    copyRighstsLbl.attributedText = string;
    
    copyRighstsLbl.font = [UIFont systemFontOfSize:13.0];
    copyRighstsLbl.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesturecpy = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self
                                             action:@selector(didTapLabelWithGesturecompany:)];
    [copyRighstsLbl addGestureRecognizer:tapGesturecpy];
 
    [footerView addSubview:copyRighstsLbl];
    UILabel *poweredLbl;
    poweredLbl = [[UILabel alloc] init];
    
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        if ([self isiPad]) {
            poweredLbl.frame = CGRectMake(0, footerView.bounds.size.height - 24,
                                          footerView.bounds.size.width, 24);
        } else {
            poweredLbl.frame = CGRectMake(0, footerView.bounds.size.height - 24,
                                          footerView.bounds.size.width, 24);
        }
        
    } else {
        
        if ([UIScreen mainScreen].bounds.size.height <= 480) {
            poweredLbl.frame = CGRectMake(0, 540, footerView.bounds.size.width, 24);
        } else {
            
            poweredLbl.frame = CGRectMake(0, footerView.bounds.size.height - 24,
                                          footerView.bounds.size.width, 24);
        }
    }
    
    poweredLbl.textAlignment = NSTextAlignmentCenter;
    poweredLbl.numberOfLines = 0;
    poweredLbl.backgroundColor = [UIColor clearColor];
    poweredLbl.textColor = [UIColor blackColor];
    poweredLbl.text = @"Powered by Vconnex Services, Inc.";
    
    NSMutableAttributedString *string1 =
    [[NSMutableAttributedString alloc] initWithString:poweredLbl.text];
    [string1 addAttribute:NSForegroundColorAttributeName
                    value:[UIColor blueColor]
                    range:NSMakeRange(11, 21)];
    [string1 addAttribute:NSFontAttributeName
                    value:[UIFont systemFontOfSize:13.0]
                    range:NSMakeRange(11, 21)];
    poweredLbl.attributedText = string1;
    
    poweredLbl.font = [UIFont systemFontOfSize:13.0];
    poweredLbl.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(didTapLabelWithGesture:)];
    [poweredLbl addGestureRecognizer:tapGesture];
    
    [footerView addSubview:poweredLbl];
    [mainScrollView addSubview:footerView];
}
#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize =
    [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey]
     CGRectValue]
    .size;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         CGRect f = self.view.frame;
                         f.origin.y = -100;
                         self.view.frame = f;
                     }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.3
                     animations:^{
                         CGRect f = self.view.frame;
                         f.origin.y = 0.0f;
                         self.view.frame = f;
                     }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification
     object:nil];
   // [self setConstrainForiPad];
    
    if ([self.navigationController
         respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification
     object:nil];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:isLoggedin];
    
    [self setConstrainForiPad];
     [self setFooterView];
     [[NSUserDefaults standardUserDefaults]setBool:false forKey:isLoggedin];
}
- (void)didRotateFromInterfaceOrientation:
(UIInterfaceOrientation)fromInterfaceOrientation {
  [self setFooterView];
  //  [self setConstrainForiPad];
}

- (void)didTapLabelWithGesturecompany:(UITapGestureRecognizer *)tapGesture {
    
    NSLog(@"url%@",
          [[NSUserDefaults standardUserDefaults] objectForKey:@"Domain_name"]);
    NSURL *url = [NSURL URLWithString:[[NSUserDefaults standardUserDefaults]
                                       objectForKey:@"Domain_name"]];
    
    if ([[UIApplication sharedApplication]
         respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [[UIApplication sharedApplication] openURL:url
                                           options:@{}
                                 completionHandler:NULL];
    } else {
        // Fallback on earlier versions
        [[UIApplication sharedApplication] openURL:url];
    }
}
- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
    
    NSURL *url = [NSURL URLWithString:@"https://vconnexservices.com/"];
    
    if ([[UIApplication sharedApplication]
         respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [[UIApplication sharedApplication] openURL:url
                                           options:@{}
                                 completionHandler:NULL];
    } else {
        // Fallback on earlier versions
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)handleTapFrom:(UITapGestureRecognizer *)recognizer {
    // Code to handle the gesture
    UIAlertView *alert =
    [[UIAlertView alloc] initWithTitle:@""
                               message:@"Enter User name & Email"
                              delegate:self
                     cancelButtonTitle:@"Cancel"
                     otherButtonTitles:@"Ok", nil];
    
    alert.tag = 897;
    alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    UITextField *alertTextField1 = [alert textFieldAtIndex:0];
    alertTextField1.keyboardType = UIKeyboardTypeDefault;
    alertTextField1.placeholder = @"User name";
    
    UITextField *alertTextField2 = [alert textFieldAtIndex:1];
    alertTextField2.keyboardType = UIKeyboardTypeEmailAddress;
    alertTextField2.placeholder = @"Email";
    alertTextField2.secureTextEntry = NO;
    
    [alert show];
}
- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView {
    if (alertView.tag == 1006) {
        return YES;
        
    } else if (alertView.tag != 1001) {
        NSString *inputText = [[alertView textFieldAtIndex:0] text];
        NSString *inputText1 = [[alertView textFieldAtIndex:1] text];
        if ([inputText length] >= 1 && [self isValidEmail:inputText1]) {
            return YES;
        } else {
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)isValidEmail:(NSString *)checkString {
    BOOL stricterFilter = NO;
    NSString *stricterFilterString =
    @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest =
    [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little
 preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)loginAction:(id)sender {
    
    [[self view] endEditing:YES];
    if (![self isNetworkAvailable]) {
        [self showAlertWithTitle:@"Efield Alert"
                         message:
         @"No network, please check your internet connection"];
        return;
    } else if ([self isNetworkAvailable]) {
        
        if ([usernameTxt hasText] && [passwordTxt hasText]) {
            
            loginBtn.enabled = NO;
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            usernameTxt.text = [usernameTxt.text
                                stringByTrimmingCharactersInSet:[NSCharacterSet
                                                                 whitespaceCharacterSet]];
            usernameStr = [usernameTxt.text stringByReplacingOccurrencesOfString:@" "
                                                                      withString:@""];
          DownloadManager *downloadObj = [[DownloadManager alloc] init];
            [downloadObj
             callServerWithURL:
             [NSString
              stringWithFormat:@"%@GetLoginUser?UserName=%@&Password=%@",
              [[NSUserDefaults standardUserDefaults]
               objectForKey:SERVERURL],
              usernameStr, passwordTxt.text]
             andParameter:nil
             andMethod:@"GET"
             andDelegate:self
             andKey:@"GetLoginUser"];
      }
        
        else if (![usernameTxt hasText]) {
            
            [self showAlertWithTitle:@"Efield Login"
                             message:@"Please enter user name"];
        } else if (![passwordTxt hasText]) {
            [self showAlertWithTitle:@"Efield Login"
                             message:@"Please enter password"];
        }
    }
}

//
//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//
//    [[NSNotificationCenter defaultCenter] addObserver:self
//    selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification
//    object:nil]; return YES;
//}
//
//
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
//    [[NSNotificationCenter defaultCenter] addObserver:self
//    selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification
//    object:nil];
//
//    [self.view endEditing:YES];
//    return YES;
//}
//
//
//- (void)keyboardDidShow:(NSNotification *)notification
//{
//    // Assign new frame to your view
//    [self.view setFrame:CGRectMake(0,-110,320,460)]; //here taken -110 for
//    example i.e. your view will be scrolled to -110. change its value
//    according to your requirement.
//
//}
//
//-(void)keyboardDidHide:(NSNotification *)notification
//{
//    [self.view setFrame:CGRectMake(0,0,320,460)];
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    UIInterfaceOrientation orientation =
    [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsLandscape(orientation) &&
        [UIDevice currentDevice].userInterfaceIdiom ==
        UIUserInterfaceIdiomPhone) {
        [mainScrollView
         setContentOffset:CGPointMake(0, textField.center.y + 40)
         animated:YES]; // you can set your  y cordinate as your req also
    }
}

#pragma mark - API Delegate

- (void)callBackWithFailureResponse:(NSString *)response
                             andKey:(NSString *)key {
    loginBtn.enabled = YES;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    //    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield
    //    Message" message:@"Server may be busy. Please try again later."
    //    delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    //
    
    UIAlertView *callAlert =
    [[UIAlertView alloc] initWithTitle:@"Efielddata Message"
                               message:response
                              delegate:self
                     cancelButtonTitle:@"Ok"
                     otherButtonTitles:nil];
    [callAlert show];
    NSLog(@"CallBackFailure");
}

- (void)callBackWithSuccessResponse:(NSDictionary *)response
                             andKey:(NSString *)key {
    if ([key isEqualToString:@"GetLoginUser"]) {
     
        NSLog(@"response %@", response);
        loginBtn.enabled = YES;
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:isLoggedin];
        NSLog(@"response %@", response);
        if ([[response valueForKeyPath:@"Data.Response"]
             isEqualToString:@"Success"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:usernameStr
                                                      forKey:username];
            [[NSUserDefaults standardUserDefaults]
             setObject:[response valueForKeyPath:@"Data.AuthorizationId"]
             forKey:AuthorizationId];
            
            [[NSUserDefaults standardUserDefaults]
             setObject:[response valueForKeyPath:@"Data.RoleName"]
             forKey:userRole];
            if (rememberBtn.selected) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:remeberme];
                
                [[NSUserDefaults standardUserDefaults] setObject:passwordTxt.text
                                                          forKey:login_password];
            } else {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:remeberme];
                
                [[NSUserDefaults standardUserDefaults] setObject:nil
                                                          forKey:login_password];
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
            if (![TouchIDEnabled_Key
                  isEqualToString:[[NSUserDefaults standardUserDefaults]
                                   objectForKey:TouchIDEnabled]]) {
                      [self enableTouchID];
                  } else {
                      [self performSelector:@selector(gotoDashBoard)
                                 withObject:nil
                                 afterDelay:0.3];
                  }
            
        }
        
        else {
            
            [self showAlertWithTitle:@"Efielddata Login"
                             message:[[response valueForKeyPath:@"Data.Message"]
                                      description]];
        }
    }
}
- (void)canEvaluatePolicy {
    
    NSError *error;
    BOOL success;
    
    // test if we can evaluate the policy, this test will tell us if Touch ID is available and enrolled
    success = [context canEvaluatePolicy: LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error];
    if (success) {
        [[NSUserDefaults standardUserDefaults]setObject:passwordTxt.text forKey:login_password];
        [[NSUserDefaults standardUserDefaults]setObject:TouchIDEnabled_Key forKey:TouchIDEnabled];
        [self performSelector:@selector(gotoDashBoard) withObject:nil afterDelay:0.3];
    }
    else {
        [self performSelector:@selector(gotoDashBoard) withObject:nil afterDelay:0.3];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self showAlertWithTitle:TouchID_NOT_AVAILABLE_TITLE message:Biometrics_NOT_AVAILABLE_MSG];
        });
        
    }
}

- (void)enableTouchID {
    NSError *error;
    
    success =
    [context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                         error:&error];
    UIAlertView *alert;
    if (success) {
      
        if(context.biometryType==LABiometryTypeFaceID){
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:FaceID_ALERT_TITLE
                                               message:FaceID_ALERT_MESSAGE
                                              delegate:self
                                     cancelButtonTitle:TouchID_ALERT_CANCELBTN
                                     otherButtonTitles:TouchID_ALERT_OKBTN, nil];
                                  alert.tag = 1001;
                                  alert.alertViewStyle = UIAlertViewStyleDefault;
                                  [alert show];
                              //}];
        } else if (context.biometryType == LABiometryTypeTouchID) {
            
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TouchID_ALERT_TITLE
                                               message:TouchID_ALERT_MESSAGE
                                              delegate:self
                                     cancelButtonTitle:TouchID_ALERT_CANCELBTN
                                     otherButtonTitles:TouchID_ALERT_OKBTN, nil];
             alert.tag = 1001;
            alert.alertViewStyle = UIAlertViewStyleDefault;
            [alert show];
            
        }
        
      
    } else {
        
        [self performSelector:@selector(gotoDashBoard)
                   withObject:nil
                   afterDelay:0.3];
 
    }
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex

{
    if (alertView.tag == 1006 && buttonIndex == 1) {
        [self logoutUser];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:remeberme];

        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:SERVERURL];
        [self performSegueWithIdentifier:@"back_toaccesscode" sender:self];
        
    } else if (alertView.tag == 999) {
        [self performSelector:@selector(gotoDashBoard)
                   withObject:nil
                   afterDelay:0.3];
    }
    
    else
        
        if (alertView.tag == 1001 && buttonIndex == 1) {
       //[self canEvaluatePolicy];
            [[NSUserDefaults standardUserDefaults] setObject:passwordTxt.text
                                                      forKey:login_password];
            [[NSUserDefaults standardUserDefaults] setObject:TouchIDEnabled_Key
                                                      forKey:TouchIDEnabled];
            [self performSelector:@selector(gotoDashBoard)
                       withObject:nil
                       afterDelay:0.3];
            
        } else if (alertView.tag == 1001 && buttonIndex == 0) {
            [self performSelector:@selector(gotoDashBoard)
                       withObject:nil
                       afterDelay:0.3];
        }
}

- (void)gotoDashBoard {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    UITabBarController *rootViewController =
    [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];
    rootViewController.selectedIndex = 0;
    [[UIApplication sharedApplication].keyWindow
     setRootViewController:rootViewController];
}
- (IBAction)rememberMeAction:(id)sender {
    if (rememberBtn.selected) {
        [rememberBtn setSelected:NO];
    } else {
        [rememberBtn setSelected:YES];
    }
}

- (IBAction)reset_accesscode:(id)sender {
    
    UIAlertView *callAlert =
    [[UIAlertView alloc] initWithTitle:nil
                               message:@"Are you sure to reset Access Code?"
                              delegate:self
                     cancelButtonTitle:@"NO"
                     otherButtonTitles:@"YES", nil];
    callAlert.tag = 1006;
    callAlert.alertViewStyle = UIAlertViewStyleDefault;
    
    [callAlert show];
    
 
}
@end
