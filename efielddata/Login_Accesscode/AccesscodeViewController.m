//
//  proceedViewController.m
//  Efield
//
//  Created by iPhone on 23/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import "AccesscodeViewController.h"
#import "Constants.h"
#import "AppDelegate.h"
#define    GETSERVERURL
@import LocalAuthentication;

@interface AccesscodeViewController ()<MFMailComposeViewControllerDelegate>
 @end

@implementation AccesscodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        proceedView.backgroundColor = [UIColor whiteColor];
    
    CGFloat borderWidth = 1.0f;
    
    
   proceedView.layer.borderColor = [UIColor colorWithRed:0.0/255.0 green:156.0/255.0 blue:21.0/255.0 alpha:1].CGColor;
 proceedView.layer.borderWidth = borderWidth;
    
    proceedView.layer.cornerRadius = 5.0f;
    logoImageView.layer.cornerRadius = 5.0f;
    proceed.layer.cornerRadius = 5.0f;
   
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state != UIApplicationStateBackground)
    {
        if ([TouchIDEnabled_Key isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:TouchIDEnabled]])
        {
//            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//            [appDelegate authenticateUser];
        }
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = [[NSString stringWithFormat:@"0"] integerValue];
    // Do any additional setup after loading the view.
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self setConstrainForiPad];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self setConstrainForiPad];
    [[NSUserDefaults standardUserDefaults]setBool:false forKey:isLoggedin];

}
- (void)setConstrainForiPad
{

    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        [mainScrollView removeConstraint:leftSpacingContrain];
        topSpacingContrain.constant = ([UIScreen mainScreen].bounds.size.height- 446)/2;
        widthConstrain.constant = 350.0;
    }
    else if (UIInterfaceOrientationIsLandscape(orientation) && [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        [mainScrollView removeConstraint:leftSpacingContrain];
    }
    else
    {
        [proceedView removeConstraint:widthConstrain];
    }
    
}
#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -100;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
    
    NSURL *url = [NSURL URLWithString:@"https://vconnexservices.com/"];
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:NULL];
    }else{
        // Fallback on earlier versions
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
    //Code to handle the gesture
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@""
                                                     message:@"Enter User name & Email"
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"Ok", nil];
    
    alert.tag = 897;
    alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    UITextField * alertTextField1 = [alert textFieldAtIndex:0];
    alertTextField1.keyboardType = UIKeyboardTypeDefault;
    alertTextField1.placeholder = @"User name";
    
    UITextField * alertTextField2 = [alert textFieldAtIndex:1];
    alertTextField2.keyboardType = UIKeyboardTypeEmailAddress;
    alertTextField2.placeholder = @"Email";
    alertTextField2.secureTextEntry=NO;
    
    [alert show];
}
- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    if (alertView.tag != 1001) {
        NSString *inputText = [[alertView textFieldAtIndex:0] text];
        NSString *inputText1 = [[alertView textFieldAtIndex:1] text];
        if([inputText length] >= 1 && [self isValidEmail:inputText1])
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    return YES;
}

-(BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
       // [self setConstrainForiPad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)proceedAction:(id)sender
{
   
    [[self view]endEditing:YES];
    if (![self isNetworkAvailable]) {
        [self showAlertWithTitle:@"Efield Alert" message:@"No network, please check your internet connection"];
        return;
    }else if ([self isNetworkAvailable]) {
       
         if ([accesscodeTxt hasText]    ) {
             
             proceed.enabled=NO;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        accesscodeTxt.text=
        [accesscodeTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        usernameStr=[accesscodeTxt.text stringByReplacingOccurrencesOfString:@" " withString:@""];
             NSDictionary *json = @{
                                    @"EfieldCode": usernameStr
                                   };
            NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
            [dictEntry setObject:json forKey:[NSString stringWithFormat:@"UserModel"]];
            NSLog(@"%@",dictEntry);
            DownloadManager *downloadObj = [[DownloadManager alloc]init];
            [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@GetEfieldClient",ACCESSCODEURL] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"GetEfieldClient"];
   
         }
    
    else if (![accesscodeTxt hasText])
    {
        
        [self showAlertWithTitle:@"Efield Login" message:@"Please enter Access Code"];
    }
  
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [mainScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsLandscape(orientation) && [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        [mainScrollView setContentOffset:CGPointMake(0,textField.center.y + 40) animated:YES];//you can set your  y cordinate as your req also
    }
}



#pragma mark - API Delegate

-(void)callBackWithFailureResponse:(NSString *)response andKey:(NSString *)key
{
       proceed.enabled=YES;
      [MBProgressHUD hideHUDForView:self.view animated:YES];
//    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Server may be busy. Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//
    
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:response delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [callAlert show];
    NSLog(@"CallBackFailure");
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    if([key isEqualToString:@"GetEfieldClient"])
    { 
         NSLog(@"SERVERURL %@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]);
          NSLog(@"response %@",response);
            proceed.enabled=YES;
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"]    )
        {
           
                [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@/api/WorkOrderApi/",[response valueForKeyPath:@"Data.EfieldUrl"]] forKey:SERVERURL];
            [[NSUserDefaults standardUserDefaults]setObject:[response valueForKeyPath:@"Data.EfieldUrl"] forKey:@"Domain_name"];
            
            NSString *comp_name=[NSString stringWithFormat:@"  %@  ",[response valueForKeyPath:@"Data.EfieldClientName"]];
            [[NSUserDefaults standardUserDefaults]setObject:comp_name forKey:companyname];
          [[NSUserDefaults standardUserDefaults]setObject:[response valueForKeyPath:@"Data.CssClientId"] forKey:@"CssClientId"];
          [[NSUserDefaults standardUserDefaults]setObject:[response valueForKeyPath:@"Data.EfieldCode"] forKey:@"EfieldCode"];
                NSLog(@"SERVERURL %@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]);
          
            [self performSegueWithIdentifier:@"loginsegue" sender:self];
            
        }
        
        else
        {
            
         
            [self showAlertWithTitle:@"Efielddata Login" message:[[response valueForKeyPath:@"Data.Message"] description]];
        }
    }
}



- (void)gotoDashBoard
{
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];
    rootViewController.selectedIndex = 0;
    [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
    
}

@end
