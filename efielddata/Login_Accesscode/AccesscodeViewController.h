//
//  LoginViewController.h
//  Efield
//
//  Created by iPhone on 23/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ViewController.h"  

@interface AccesscodeViewController : ViewController
{
    NSString *usernameStr, *emailStr;
    __weak IBOutlet UIImageView *logoImageView; 
    __weak IBOutlet UITextField *accesscodeTxt;
    __weak IBOutlet UIButton *proceed;
    __weak IBOutlet UIView *proceedView;
    __weak IBOutlet UIScrollView *mainScrollView;
    __weak IBOutlet NSLayoutConstraint *widthConstrain;
    __weak IBOutlet NSLayoutConstraint *leftSpacingContrain;
    __weak IBOutlet NSLayoutConstraint *topSpacingContrain;
}
- (IBAction)proceedAction:(id)sender;
- (IBAction)rememberMeAction:(id)sender;
@end
