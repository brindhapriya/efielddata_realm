                             //
//  AlertListVC.m
//  Efielddata
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import "edit_addTimesheet.h"
#import "Constants.h"  
#import "DynamicNoteFormVC.h"

#import "TestTypeselectVC.h"
#import "NoRecordsTableViewCell.h"
#import "workordertimesheetheadercell.h"
#import "workordertimesheetcell.h"
#import "timesheettableheadercell.h"
#import "timesheettablecell.h"


#import "MPBSignatureViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AlertListVC.h"
//#import "UITabBarItem+CustomBadge.h"
@interface edit_addTimesheet () <UITableViewDelegate, UITableViewDataSource,UITextViewDelegate,UITextFieldDelegate,UIPickerViewDataSource, UIPickerViewDelegate>
{
    BOOL isOnEditing;
    UIPickerView *picker;
    BOOL isMarkAll;
    __weak IBOutlet UILabel *titleLbl;
    __weak IBOutlet UITableView *alertSearchTable;
    NSMutableArray *searchResultArray;
    NSMutableArray *searchFullResultArray;
    NSInteger selectedRowIndex,deletetag,workorderindex;
    int selectedUtilityIndex;
    NSString *places,*editworkorderIDstr,*edittimesheet_IDstr;
    NSString *IsChange,*TestTypeId;
    CGFloat  lastScale;

    NSString *TaskName,*JobNumber,*ProjectName;
    NSString *datestr,*loadingmsg;
    UIImage *signatureimg;
    NSData *ttbresponse;
    NSString   *pdfurlstring;
   }
@end

@implementation edit_addTimesheet

UIButton *Timesheet_webview_completedbutton ;
UIWebView *Timesheet_webview_completed;
NSURL *Timesheet_targetURL ;

- (void)viewDidLoad {
      
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
  //  [self.view addGestureRecognizer:tap];
    _timesheet_absence.enabled = NO;

    loadingmsg=@"";
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
   // [self updateALERTcount];
//    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"searchText"];
//    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"searchText_alert"];
//    
//    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"patientlistflag"];
    _companynametxt.text=[[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];
    _popupview.hidden=true;

    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
     [currentDTFormatter1 setDateFormat:@"MM/dd/yyyy"];

    self.navigationController.navigationBar.hidden = YES;

     NSString *eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@", eventDateStr);
    self.date.text=eventDateStr;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 20)];
   _datefield.leftView = paddingView;
 _datefield.leftViewMode = UITextFieldViewModeAlways;
      NSString *todayDateStr = [currentDTFormatter1 stringFromDate:[NSDate date]];
    _datefield.text=todayDateStr;
    [_timesheet_date setDelegate:self];
    [_timesheet_hours setDelegate:self];
    [_timesheet_notes  setDelegate:self];
    _timesheet_hours.keyboardType = UIKeyboardTypeDecimalPad;

    [_datefield setDelegate:self];
    
    
    [_datefield addTarget:self action:@selector(updateDatacompleted) forControlEvents:UIControlEventEditingChanged];
    
    workorder_list = [[NSMutableArray alloc]init];
    _datefield.delegate=self;
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor whiteColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self
                       action:@selector(updateDatacompleted)
             forControlEvents:UIControlEventValueChanged];
    [_workorder_table addSubview:refreshControl];
      [_timesheettable addSubview:refreshControl];
    [self updateDatacompleted];
    _workorder_table.dataSource = self;
    _timesheettable.dataSource = self;
    _workorder_table.allowsSelection = NO;
    
    _timesheettable.allowsSelection = NO;
    

}

-(void)dismissKeyboard
{
      [self.view endEditing:YES];
  //  [aTextField resignFirstResponder];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
        return 35;
        
  
    
    
}




- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
        if (tableView == _workorder_table)
        {
            if([workorder_list count]==0)
            {
                return  2;
            }else{
                return [workorder_list count]+1;
            }
        }
        else
        {
            if([timesheet_list count]==0)
            {
                return  2;
            }else{
                return [timesheet_list count]+1;
            }
        }
   
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(tableView==_workorder_table)
    {
        if(indexPath.row==0)
        {
            workordertimesheetheadercell *cell = (workordertimesheetheadercell *)[tableView dequeueReusableCellWithIdentifier:@"workorderheadercell"];
             cell.backgroundColor = [UIColor  colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0];
            return cell;
            
        }
        else
        {
            if([workorder_list count]==0)
            {
                NoRecordsTableViewCell *cell = (NoRecordsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:noRecordsResuableIdentifier];
                cell.noRecordsLabel.text =loadingmsg;
                return cell;
                
            }
            else {
                workordertimesheetcell *cell = (workordertimesheetcell *)[tableView dequeueReusableCellWithIdentifier:@"workordertimesheetcell"];
                cell.date_val.text=[[workorder_list valueForKey:@"JobDateText"] objectAtIndex:indexPath.row-1];
                  cell.workorder_value.text=[[workorder_list valueForKey:@"Wono"] objectAtIndex:indexPath.row-1];
                  cell.hours_value.text=[[[workorder_list valueForKey:@"HoursSpent"] objectAtIndex:indexPath.row-1]description];
                
                cell.edit_Timesheet.tag = indexPath.row-1;

                [cell.edit_Timesheet addTarget:self action:@selector(editworkorder_clicked:) forControlEvents:UIControlEventTouchUpInside];

                if(indexPath.row % 2 == 0)
                    cell.backgroundColor = [UIColor  colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0];
                else
                    cell.backgroundColor = [UIColor  whiteColor];
                 return cell;
                
            }
            
        }
    }
    else
    {
        if(indexPath.row==0)
        {
            timesheettableheadercell *cell = (timesheettableheadercell *)[tableView dequeueReusableCellWithIdentifier:@"timeheadercell"];
               cell.backgroundColor = [UIColor  colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0];
            return cell;
            
            
        }
        else   {
            if([timesheet_list count]==0)
            {
                NoRecordsTableViewCell *cell = (NoRecordsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:noRecordsResuableIdentifier];
                cell.noRecordsLabel.text =loadingmsg;
                return cell;
                
            }
            else{
                timesheettablecell *cell = (timesheettablecell *)[tableView dequeueReusableCellWithIdentifier:@"timesheetcell"];
                cell.date_val.text=[[timesheet_list valueForKey:@"JobDateText"] objectAtIndex:indexPath.row-1];
                cell.absence_value.text=[[timesheet_list valueForKey:@"Type"] objectAtIndex:indexPath.row-1];
                NSLog(@"hrsspent%@",[[timesheet_list valueForKey:@"HrsSpent"] objectAtIndex:indexPath.row-1]);
                cell.hours_value.text=[[[timesheet_list valueForKey:@"HrsSpent"] objectAtIndex:indexPath.row-1]description];
                
                cell.edit_timesheet_details.tag = indexPath.row-1;
                
                [cell.edit_timesheet_details addTarget:self action:@selector(editworkorderdetail_clicked:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.delete_timesheet_details.tag = indexPath.row-1;
                
                [cell.delete_timesheet_details addTarget:self action:@selector(deleteworkorderdetail_clicked:) forControlEvents:UIControlEventTouchUpInside];

                if(indexPath.row % 2 == 0)
                    cell.backgroundColor = [UIColor  colorWithRed:211/255.0f green:211/255.0f blue:211/255.0f alpha:1.0];
                else
                         cell.backgroundColor = [UIColor  whiteColor];
                return cell;
                
                
            }
            
        }
    }
   
}

- (void)updateDatacompleted
{
    if([_datefield.text length]==0){
        self.closebtn.hidden=true;
     
    }else{
        self.closebtn.hidden=false;
      
        
    }
    
    
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message" message:@"No network, please check your internet connection"];
        return;
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
     
        NSDictionary *json = @{
                               
                               @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
                               
                               @"RoleName" :[[NSUserDefaults standardUserDefaults] objectForKey:userRole],
                             
                               @"FromJobDateTime" : _datefield.text
                               
                               
                               };
        
        
        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        
        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@IOSTimeSheetWorkOrderList",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"IOSTimeSheetWorkOrderList"];
        
        [refreshControl performSelector:@selector(endRefreshing) withObject:nil afterDelay:1.0f];
        
    }
}
- (IBAction)logoutAction:(id)sender
{
    [self logoutUser];
    
    
    //  }
}

- (IBAction)alertaction:(id)sender {
    [self performSegueWithIdentifier:@"alertsegue" sender:self];
}
- (void)viewWillAppear:(BOOL)animated
{
    
    
    [super viewWillAppear:animated];
    _popupview.hidden=true;

    NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];
 [currentDTFormatter1 setDateFormat:@"MM/dd/yyyy"];
    NSString *todayDateStr = [currentDTFormatter1 stringFromDate:[NSDate date]];

    [self updateDatacompleted];
    _datefield.text=todayDateStr;
    _alertcnt.text=[self updateALERTcount];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) showDatePickertextfield: (UIDatePickerMode) modeDatePicker currentLbl:(UITextField *)lbl
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    _closebtn.hidden=false;
    
    if([ _timesheet_date.text isEqualToString:@""]){
    }else{
        NSDate *mydate;
        NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
        [formatter1 setDateFormat:@"MM/dd/yyyy"];
        mydate = [formatter1 dateFromString: _timesheet_date.text];
        [picker setDate:mydate animated:NO];
    }
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                     if(modeDatePicker == UIDatePickerModeDate) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                                         
                                         NSString *datestring= [formatter stringFromDate:picker.date];
                                         
                                         lbl.text=datestring;
                                    
                                     }
                                     
                                     
                                 }];
    [alertController addAction:doneAction];
    
    
    UIAlertAction *clearAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                      
                                lbl.text=@"";
                                   self.closebtn.hidden=true;
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    [alertController addAction:cancelAction];
    
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController  animated:YES completion:nil]; }





-(void) showDatePicker: (UIDatePickerMode) modeDatePicker currentLbl:(UILabel *)lbl
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    _closebtn.hidden=false;

  
    if([ _datefield.text isEqualToString:@""]){
    }else{
        NSDate *mydate;
        NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
        [formatter1 setDateFormat:@"MM/dd/yyyy"];
        mydate = [formatter1 dateFromString: _datefield.text];
        [picker setDate:mydate animated:NO];
    }
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                     if(modeDatePicker == UIDatePickerModeDate) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                               
                                         NSString *datestring= [formatter stringFromDate:picker.date];
                                         
                                       _datefield.text=datestring;
                                         [self updateDatacompleted];
                                     }
                                     
                                     
                                 }];
    [alertController addAction:doneAction];
    
    
    UIAlertAction *clearAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                      
                                      _datefield.text=@"";
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    [alertController addAction:cancelAction];
    
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController  animated:YES completion:nil]; }





-(void)deleteworkorderdetail_clicked:(UIButton*)deleteworkorderID
{
    
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:@"Are you sure want to delete Timesheet"  delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES",nil];
    callAlert.tag = 56;
    [callAlert show];
    deletetag=deleteworkorderID.tag;
    
    
}
-(void)editworkorderdetail_clicked:(UIButton*)editworkorderID
{
    edittimesheet_IDstr=[[[timesheet_list valueForKey:@"TimeSheetId"] objectAtIndex:editworkorderID.tag]description];
     if(_popupview.hidden)
     {      _popupview.hidden=false;
     }else{
           _popupview.hidden=true;
     }
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_popupview];
    _timesheet_absence.text=[[[timesheet_list valueForKey:@"Type"] objectAtIndex:editworkorderID.tag]description];
  

     _timesheet_date.text=[[[timesheet_list valueForKey:@"JobDateText"] objectAtIndex:editworkorderID.tag]description];
     _timesheet_notes.text=[[[timesheet_list valueForKey:@"Notes"] objectAtIndex:editworkorderID.tag]description];
        _timesheet_hours.text=[[[timesheet_list valueForKey:@"HrsSpent"] objectAtIndex:editworkorderID.tag]description];

    //[self performSelector:@selector(editdetailstimesheet_popViewController) withObject:nil afterDelay:0.1];
    
    
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 5;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *item ;
    if(row==0)
    {item=@"Administration";}
    else    if(row==1)
    {item=@"Holiday";}
    else    if(row==2)
    {item=@"Sick";}
    else    if(row==3)
    {item=@"Training";}
    else
    {item=@"Vacation";}
    return item;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // perform some action
    
    if(row==0)
    {_timesheet_absence.text=@"Administration";}
    else    if(row==1)
    {_timesheet_absence.text=@"Holiday";}
    else    if(row==2)
    {_timesheet_absence.text=@"Sick";}
    else    if(row==3)
    {_timesheet_absence.text=@"Training";}
    else
    {_timesheet_absence.text=@"Vacation";}
    [picker removeFromSuperview];

}


- (void)createPicker1 {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-250, screenWidth, 44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:self
                                                                     action:@selector(doneClicked)];
    toolBar.items = @[flex, barButtonDone];
    barButtonDone.tintColor = [UIColor blackColor];
    
  picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, toolBar.frame.size.height, screenWidth, 200)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.showsSelectionIndicator = YES;
    
    
    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, toolBar.frame.size.height + picker.frame.size.height)];
    inputView.backgroundColor = [UIColor clearColor];
    [inputView addSubview:picker];
    [inputView addSubview:toolBar];
    
    _timesheet_absence.inputView = inputView;
    
    [self.view addSubview:inputView];
}
- (void)createPicker  {
    
//    UIView *pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 216)];
//    pickerView.backgroundColor = [UIColor colorWithRed:109.0/255.0 green:110.0/255.0 blue:120.0/255.0 alpha:1];
//
    
 picker = [[UIPickerView alloc] initWithFrame:CGRectMake(self.view.center.x/2,self.view.center.y+60,self.view.bounds.size.width/2, 250)];
    picker.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1];
 
    picker.showsSelectionIndicator = YES;
    picker.dataSource = self;
    picker.delegate = self;
 _timesheet_absence.inputView = picker;
 
    // add a toolbar with Cancel & Done button
    UIToolbar *toolBar  = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,320,44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;

    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTouched:)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTouched:)];

    // the middle button is to make the Done button align to right
    [toolBar setItems:[NSArray arrayWithObjects:cancelButton, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton, nil]];
_timesheet_absence.inputAccessoryView = toolBar;
    
    [self.view addSubview:picker];

 
}







-(void)editworkorder_clicked:(UIButton*)editworkorderID
{
    editworkorderIDstr=[[[workorder_list valueForKey:@"WorkOrderId"] objectAtIndex:editworkorderID.tag]description];
    workorderindex=editworkorderID.tag;
  [self performSelector:@selector(edittimesheet_popViewController) withObject:nil afterDelay:0.1];
    
    
}



- (void)edittimesheet_popViewController {
   

    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Time Sheet"
                                                                              message: @"Please update time spent on this work order"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder =@"Enter hours";
        textField.keyboardType = UIKeyboardTypeDecimalPad;
        //        textField.placeholder = @"Enter hours";
        //        textField.textColor = [UIColor blueColor];
        //        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        //        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
  
    [alertController addAction:[UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * hrsfield = textfields[0];
        NSLog(@"%@",hrsfield.text );
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *myNumber = [formatter numberFromString:hrsfield.text];
        
        if([hrsfield.text length]==0){
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please Enter hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
            
        }
        
//        else if ([hrsfield.text rangeOfCharacterFromSet:notDigits].location == NSNotFound)
//
//        {
 else     {
                
            float b = [hrsfield.text floatValue];
            if(b<24){
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            NSDictionary *json = @{
                                   
                                   @"WorkOrderId" : editworkorderIDstr,
                                   @"TimeSpent" : myNumber
                                   
                                   };
          
                
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:workorder_list[workorderindex]];
                //[dic setObject:hrsfield.text forKey:@"TextAnswer"];
                NSLog(@"dic : %@",dic);
              //  [dataList replaceObjectAtIndex:textField.tag withObject:dic];
            DownloadManager *downloadObj = [[DownloadManager alloc]init];
            //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            
            [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetWoForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetWoForApp"];
            }
     
            else
            {
               UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please enter valid hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [callAlert show];
            }
     
    }
//        else
//        {
//
//
//            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please enter valid hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            [callAlert show];
//        }
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

{
    
    
    if (0 == buttonIndex && alertView.tag == 1001)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
                NSString *ids=@"";
                for(int i=0;i<[workorder_list count];i++)
                {
                    if([ids length]>0)
                    {
                        NSString *idsval=[[workorder_list valueForKey:@"Wono"] objectAtIndex:0];
                        ids=[NSString stringWithFormat:@"%@,%@",ids,idsval];
                    }
                    else{
                        ids=[[workorder_list valueForKey:@"Wono"] objectAtIndex:0];
                    }
        
        
                }
                NSDictionary *json = @{
                                       @"List" :workorder_list,
                                       @"Details" :timesheet_list,
                                       @"WonoText": ids,
                                       @"JobDate": [[timesheet_list valueForKey:@"JobDate"] objectAtIndex:0],
                                       @"JobDateText": [[timesheet_list valueForKey:@"JobDateText"] objectAtIndex:0],
                                       @"Techname": [[timesheet_list valueForKey:@"Techname"] objectAtIndex:0]
        
                                       };
        
                DownloadManager *downloadObj = [[DownloadManager alloc]init];
                //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
                [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetForApp"];
    }else
    if (0 == buttonIndex && alertView.tag == 55)
    {
        [self updateDatacompleted];
    }
  else  if (1 == buttonIndex && alertView.tag == 56)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        
        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@DeleteTimeSheet?TimeSheetId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[[timesheet_list valueForKey:@"TimeSheetId"] objectAtIndex:deletetag]description]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"DeleteTimeSheet"];
    }
}
- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}




-(void)successmessage{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    //    NSInteger img_count = [[NSUserDefaults standardUserDefaults] integerForKey:@"img_count"];
    //
    //    for (int i=0; i<img_count;i++ )
    //    {
    //
    //        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:[NSString stringWithFormat:@"image%d", img_count]];
    //
    //    }
    //    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"img_count"];
    //
    NSError *error1;
    NSDictionary *response1 = [NSJSONSerialization JSONObjectWithData:ttbresponse
                                                              options:kNilOptions
                                                                error:&error1];
    
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:[[response1 valueForKeyPath:@"Response"] description] message:[[response1 valueForKeyPath:@"Message"] description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    callAlert.tag=1001;
    [callAlert show];
 
    
    // [self performSelector:@selector(popViewController) withObject:nil afterDelay:1.5];
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    
    if ([key isEqualToString:@"SubmitTimeSheetWoForApp"])
    {
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        //Open alert here
        
        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
        {
            
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            callAlert.tag = 55;
            [callAlert show];
            
        }
    }
    
    else if ([key isEqualToString:@"DeleteTimeSheet"])
    {
        _popupview.hidden=true;
        [picker removeFromSuperview];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        //Open alert here
        
        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
        {
            
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            callAlert.tag = 55;
            [callAlert show];
            
        }
    }
    else if([key isEqualToString:@"SubmitTimeSheetForApp"]){
        NSLog(@"SubmitTimeSheetForApp%@", response);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
        {
            
            [self updateDatacompleted];

            
            
        }
    }
    
    else if([key isEqualToString:@"CheckSubmitTimeSheetForApp"]){
        
     //   if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
       // {
        NSLog(@"CheckSubmitTimeSheetForAppLog%@", response);
        if([[response valueForKeyPath:@"Data.Response"]boolValue])
        {}else
                 
        {
            
                //        NSString *ids=@"";
                //        for(int i=0;i<[workorder_list count];i++)
                //        {
                //            if([ids length]>0)
                //            {
                //                        NSString *idsval=[[workorder_list valueForKey:@"Wono"] objectAtIndex:i];
                //                ids=[NSString stringWithFormat:@"%@,%@",ids,idsval];
                //            }
                //            else{
                //                 ids=[[workorder_list valueForKey:@"Wono"] objectAtIndex:i];
                //            }
                //
                //
                //        }
                //        NSDictionary *json = @{
                //                               @"List" :workorder_list,
                //                                   @"Details" :timesheet_list,
                //                               @"WonoText": ids,
                //                               @"JobDate": _datefield.text,
                //                               @"JobDateText": _datefield.text,
                //                               @"Techname": [[NSUserDefaults standardUserDefaults] objectForKey:username]
                //
                //                               };
                //
                //        DownloadManager *downloadObj = [[DownloadManager alloc]init];
                //        //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                //
                //        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetForApp"];
                
                //  }
                MPBSignatureViewControllerConfiguration *configuration = [[MPBSignatureViewControllerConfiguration alloc] initWithFormattedAmount:@""];
                
                static MPBSignatureViewControllerConfigurationScheme scheme = MPBSignatureViewControllerConfigurationSchemeGhLink;
                configuration.scheme = scheme;
                
                MPBSignatureViewController* signatureViewController = [[MPBSignatureViewController alloc] initWithConfiguration:configuration];
                
                signatureViewController.modalPresentationStyle = UIModalPresentationFullScreen;
                
                signatureViewController.continueBlock = ^(UIImage *signature) {
                    //   [self showImage: signature];
                    
                    signatureimg= signature;
                    
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    
                    NSString *boundary = [self generateBoundaryString];
                    
                    // configure the request
                    
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@UpdateTechnicianSign",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]]]];
                    [request setHTTPMethod:@"POST"];
                    
                    // set content type
                    
                    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
                    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
                    
                    // create body
                    NSMutableData *httpBody = [NSMutableData data];
                    
                    // add params (all params are strings)
                    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
                    [parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:username] forKey:@"UserName"];
                    
                    [parameters setValue:[NSString stringWithFormat:@"%d",1]forKey:@"EntryCount" ];
                    
                    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
                        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
                        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
                    }];
                    
                    //for (NSString *path in paths) {
                    
                    
                    
                    NSLog(@"request = %@", [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@%@\"\r\n", @"FilePath1", @"image1",@".jpeg"] dataUsingEncoding:NSUTF8StringEncoding]);
                    
                    
                    
                    [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@%@\"\r\n", @"FilePath1", @"image1",@".jpeg"] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    [httpBody appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    [httpBody appendData: UIImageJPEGRepresentation(signatureimg, 0.0)];
                    //[httpBody appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    // [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
                    //  [httpBody appendData:data];
                    [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                    NSURLSession *session = [NSURLSession sharedSession];  // use sharedSession or create your own
                    
                    NSURLSessionTask *task = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                        if (error) {
                            NSLog(@"error = %@", error);
                            return;
                        }
                        
                        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        NSLog(@"result = %@", result);
                        ttbresponse=data;
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self successmessage];
                        });
                    }];
                    [task resume];
                    
                    
                    
                    //
                    //        NSString *ids=@"";
                    //        for(int i=0;i<[workorder_list count];i++)
                    //        {
                    //            if([ids length]>0)
                    //            {
                    //                NSString *idsval=[[workorder_list valueForKey:@"Wono"] objectAtIndex:0];
                    //                ids=[NSString stringWithFormat:@"%@,%@",ids,idsval];
                    //            }
                    //            else{
                    //                ids=[[workorder_list valueForKey:@"Wono"] objectAtIndex:0];
                    //            }
                    //
                    //
                    //        }
                    //        NSDictionary *json = @{
                    //                               @"List" :workorder_list,
                    //                               @"Details" :timesheet_list,
                    //                               @"WonoText": ids,
                    //                               @"JobDate": [[timesheet_list valueForKey:@"JobDate"] objectAtIndex:0],
                    //                               @"JobDateText": [[timesheet_list valueForKey:@"JobDateText"] objectAtIndex:0],
                    //                               @"Techname": [[timesheet_list valueForKey:@"Techname"] objectAtIndex:0]
                    //
                    //                               };
                    //
                    //        DownloadManager *downloadObj = [[DownloadManager alloc]init];
                    //        //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                    //
                    //        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetForApp"];
                    
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                    
                    
                };
                
                
                signatureViewController.cancelBlock = ^ {
                    [self dismissViewControllerAnimated:YES completion:nil];
                };
                
                [self presentViewController:signatureViewController animated:YES completion:nil];
          
            
            
            
//
//        NSString *ids=@"";
//        for(int i=0;i<[workorder_list count];i++)
//        {
//            if([ids length]>0)
//            {
//                NSString *idsval=[[workorder_list valueForKey:@"Wono"] objectAtIndex:0];
//                ids=[NSString stringWithFormat:@"%@,%@",ids,idsval];
//            }
//            else{
//                ids=[[workorder_list valueForKey:@"Wono"] objectAtIndex:0];
//            }
//
//
//        }
//        NSDictionary *json = @{
//                               @"List" :workorder_list,
//                               @"Details" :timesheet_list,
//                               @"WonoText": ids,
//                               @"JobDate": [[timesheet_list valueForKey:@"JobDate"] objectAtIndex:0],
//                               @"JobDateText": [[timesheet_list valueForKey:@"JobDateText"] objectAtIndex:0],
//                               @"Techname": [[timesheet_list valueForKey:@"Techname"] objectAtIndex:0]
//
//                               };
//
//        DownloadManager *downloadObj = [[DownloadManager alloc]init];
//        //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
//
//        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetForApp"];
        }
    }
    else if ([key isEqualToString:@"GetTimeSheetPdfUrl"])
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        NSLog(@"responseLog%@", response);
    }
   else if ([key isEqualToString:@"SubmitTimeSheetDetailsWoForApp"])
    {
        _popupview.hidden=true;
        [picker removeFromSuperview];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        //Open alert here
        
        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"])
        {
            
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:nil message:[[response valueForKeyPath:@"Data.Message"]description] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            callAlert.tag = 55;
            [callAlert show];
            
        }
    }
    //SubmitTimeSheetDetailsWoForApp
    else
    if ([key isEqualToString:@"IOSTimeSheetWorkOrderList"])
    {
        
        //[[UIApplication sharedApplication] endIgnoringInteractionEvents];
        loadingmsg=@"No Data Found!";
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        //Open alert here
        
        workorder_list=[response valueForKeyPath:@"Data.List"] ;
        timesheet_list=[response valueForKeyPath:@"Data.DetailList"] ;
        _timefield.text=[response valueForKeyPath:@"Data.TimePeriod"];
        
        
        [_workorder_table reloadData];
        [_timesheettable reloadData];
        
        if([workorder_list count]>0)
        {
            
            if([[[workorder_list valueForKey:@"IsPdfShow"] objectAtIndex:0]boolValue])
            {
                _pdf.hidden=false;
                
                
            }
            else{
                _pdf.hidden=true;
            }
            
            if([[[workorder_list valueForKey:@"IsTimeSheet"] objectAtIndex:0]boolValue])
            {
                _submit.hidden=true;
                 _add_btn.hidden=true;
                
            }
            else{
                _submit.hidden=false;
                _add_btn.hidden=false;

            }
        }
       else if([timesheet_list count]>0)
       {
           if([[[timesheet_list valueForKey:@"IsPdfShow"] objectAtIndex:0]boolValue])
           {
               _pdf.hidden=false;
               
               
           }
           else{
               _pdf.hidden=true;
           }
           
           if([[[timesheet_list valueForKey:@"IsTimeSheet"] objectAtIndex:0]boolValue])
           {
               _submit.hidden=true;
               _add_btn.hidden=true;
               
           }
           else{
               _submit.hidden=false;
               _add_btn.hidden=false;
               
           }
           
       }
    }
}
 
- (IBAction)dateaction:(id)sender {
    [self showDatePicker:UIDatePickerModeDate currentLbl:self->jobdate];

}
- (IBAction)close:(id)sender {
//    self->jobdate.text=@"";
//    date=@"";
//    Wono=@"";
_datefield.text=@"";
    self.closebtn.hidden=true;
 //[self updateDatacompleted];
    //alertList = (NSMutableArray*)tempArr;
    // [_alert_table reloadData];
}
- (IBAction)absence_action:(id)sender {
     [self createPicker];
}
- (IBAction)timesheetdate_action:(id)sender {
        [picker removeFromSuperview];
    [self showDatePickertextfield:UIDatePickerModeDate currentLbl:_timesheet_date];


}
- (IBAction)timesheetcancel_action:(id)sender {
    _popupview.hidden=true;
        [picker removeFromSuperview];
}

-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Server may be busy. Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    //callAlert.tag = 56;
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
- (IBAction)timesheetsave_action:(id)sender {
    
    {
 
       
         float myNumber = [ _timesheet_hours.text  floatValue];
     
        if([_timesheet_date.text length]==0){
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please Enter Date" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
            
        }
        else
        if([_timesheet_absence.text length]==0){
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please Enter Absence" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
            
        }
        else
        if([_timesheet_hours.text length]==0){
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please Enter hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
            
        }
        
        //        else if ([hrsfield.text rangeOfCharacterFromSet:notDigits].location == NSNotFound)
        //
        //        {
        else   if (myNumber !=0.0) {
            
            if(myNumber < 24.0){
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                
          NSDictionary *json = @{
                    @"TimeSheetId" :edittimesheet_IDstr,
                    @"JobDate" : _timesheet_date.text,
                    @"TechName" : [[NSUserDefaults standardUserDefaults] objectForKey:username],
                    @"Type" : _timesheet_absence.text,
                    @"Notes" : _timesheet_notes.text,
                    @"HrsSpent" :_timesheet_hours.text
                    };
              
                DownloadManager *downloadObj = [[DownloadManager alloc]init];
                //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                
                [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetDetailsWoForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetDetailsWoForApp"];
                
            }
            else
            {
                
                
                UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please enter valid hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [callAlert show];
            }
        }
        else
        {
            
            
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please enter valid hours" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
        }
        
    }
}


- (IBAction)pdf_action:(id)sender {
    
    //GetTimeSheetPdfUrl
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc]init];
    NSString *str=
    
     [NSString stringWithFormat:@"%@GetTimeSheetPdfUrl?AuthorizationId=%@&UserName=%@&jobDate=%@&IsPrint=true",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:AuthorizationId],[[NSUserDefaults standardUserDefaults] objectForKey:username],  _datefield.text];
    NSLog(@"url%@",[NSString stringWithFormat:@"%@GetTimeSheetPdfUrl?AuthorizationId=%@&UserName=%@&jobDate=%@&IsPrint=true",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:AuthorizationId],[[NSUserDefaults standardUserDefaults] objectForKey:username],  _datefield.text]);
    [request setURL:[NSURL URLWithString:str]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
 pdfurlstring = [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding] ;
    
    NSLog(@"urlData%@",pdfurlstring);
    [self showpdfview];
//    DownloadManager *downloadObj = [[DownloadManager alloc]init];
//    //   [
//    [UIApplication sharedApplication] beginIgnoringInteractionEvents];
//    
//    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetTimeSheetPdfUrl?AuthorizationId=%@&UserName=%@&jobDate=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:AuthorizationId],[[NSUserDefaults standardUserDefaults] objectForKey:username],  _datefield.text] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetTimeSheetPdfUrl"];
}




-(void)showpdfview
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    Timesheet_targetURL = [NSURL URLWithString:pdfurlstring];

    [Timesheet_webview_completed removeFromSuperview];
    
    [Timesheet_webview_completedbutton removeFromSuperview];
    Timesheet_webview_completed=[[UIWebView alloc]initWithFrame:CGRectMake( 0, 0, self.view.frame.size.width-30, self.view.frame.size.height-100)];
    Timesheet_webview_completed.center = CGPointMake( self.view.frame.size.width/2, self.view.frame.size.height/2+30);
    
    
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:Timesheet_targetURL];
    //  webview_completed.opaque = NO;
    // webview_completed.delegate=self;
    //    webview_completed.backgroundColor = [UIColor clearColor];
    [Timesheet_webview_completed loadRequest:request];
    UIPinchGestureRecognizer *pgr = [[UIPinchGestureRecognizer alloc]
                                     initWithTarget:self action:@selector(handlePinchGesture:)];
    pgr.delegate = self;
    [ Timesheet_webview_completed addGestureRecognizer:pgr];
    [self.view addSubview:Timesheet_webview_completed];
    Timesheet_webview_completedbutton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [Timesheet_webview_completedbutton setFrame: CGRectMake(Timesheet_webview_completed.frame.origin.x+5,Timesheet_webview_completed.frame.origin.y+5,50, 30)];
    [Timesheet_webview_completedbutton setTitle:@"Close" forState:UIControlStateNormal];
    Timesheet_webview_completedbutton.userInteractionEnabled = YES;
    Timesheet_webview_completedbutton.titleLabel.lineBreakMode   = UILineBreakModeTailTruncation;
    Timesheet_webview_completedbutton.titleLabel.textColor =[UIColor blueColor];
    Timesheet_webview_completedbutton.showsTouchWhenHighlighted = YES;
    Timesheet_webview_completedbutton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleRightMargin|
    UIViewAutoresizingFlexibleTopMargin|
    UIViewAutoresizingFlexibleHeight|
    UIViewAutoresizingFlexibleBottomMargin;
    
    [Timesheet_webview_completedbutton addTarget:self action:@selector(closepdf1:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:Timesheet_webview_completedbutton];
}


- (void)handlePinchGesture:(UIPinchGestureRecognizer *)gestureRecognizer {
    
    if([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
        // Reset the last scale, necessary if there are multiple objects with different scales.
        lastScale = [gestureRecognizer scale];
    }
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan ||
        [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        
        CGFloat currentScale = [[[gestureRecognizer view].layer valueForKeyPath:@"transform.scale"] floatValue];
        
        // Constants to adjust the max/min values of zoom.
        const CGFloat kMaxScale = 2.0;
        const CGFloat kMinScale = 1.0;
        
        CGFloat newScale = 1 -  (lastScale - [gestureRecognizer scale]); // new scale is in the range (0-1)
        newScale = MIN(newScale, kMaxScale / currentScale);
        newScale = MAX(newScale, kMinScale / currentScale);
        CGAffineTransform transform = CGAffineTransformScale([[gestureRecognizer view] transform], newScale, newScale);
        [gestureRecognizer view].transform = transform;
        
        lastScale = [gestureRecognizer scale];  // Store the previous. scale factor for the next pinch gesture call
    }
}

- (IBAction)closepdf1:(id)sender {
    [Timesheet_webview_completed removeFromSuperview];
    [Timesheet_webview_completedbutton removeFromSuperview];
    
}
- (IBAction)add_action:(id)sender {
    
   self.timesheet_date.text=@"";
   self.timesheet_absence.text=@"";
    self.timesheet_hours.text=@"";
    self.timesheet_notes.text=@"";
    edittimesheet_IDstr=@"0";
    _popupview.hidden=false;
       [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_popupview];

}
- (IBAction)submit_action:(id)sender {
    
    if([workorder_list count]>0){
      //  CheckSubmitTimeSheetForApp
        
        
        NSDictionary *json = @{
                               @"TimeSheet" :workorder_list,
                       
                               };
        
        DownloadManager *downloadObj = [[DownloadManager alloc]init];
        //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@CheckSubmitTimeSheetForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"CheckSubmitTimeSheetForApp"];
    }
    else if([timesheet_list count]>0)
    {
//        NSString *ids=@"";
//        for(int i=0;i<[workorder_list count];i++)
//        {
//            if([ids length]>0)
//            {
//                        NSString *idsval=[[workorder_list valueForKey:@"Wono"] objectAtIndex:i];
//                ids=[NSString stringWithFormat:@"%@,%@",ids,idsval];
//            }
//            else{
//                 ids=[[workorder_list valueForKey:@"Wono"] objectAtIndex:i];
//            }
//
//
//        }
//        NSDictionary *json = @{
//                               @"List" :workorder_list,
//                                   @"Details" :timesheet_list,
//                               @"WonoText": ids,
//                               @"JobDate": _datefield.text,
//                               @"JobDateText": _datefield.text,
//                               @"Techname": [[NSUserDefaults standardUserDefaults] objectForKey:username]
//
//                               };
//
//        DownloadManager *downloadObj = [[DownloadManager alloc]init];
//        //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
//
//        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetForApp"];
        
  //  }
    MPBSignatureViewControllerConfiguration *configuration = [[MPBSignatureViewControllerConfiguration alloc] initWithFormattedAmount:@""];
    
    static MPBSignatureViewControllerConfigurationScheme scheme = MPBSignatureViewControllerConfigurationSchemeGhLink;
    configuration.scheme = scheme;
    
    MPBSignatureViewController* signatureViewController = [[MPBSignatureViewController alloc] initWithConfiguration:configuration];
    
    signatureViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    
    signatureViewController.continueBlock = ^(UIImage *signature) {
     //   [self showImage: signature];
        
    signatureimg= signature;
       
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            NSString *boundary = [self generateBoundaryString];
            
            // configure the request
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@UpdateTechnicianSign",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]]]];
            [request setHTTPMethod:@"POST"];
            
            // set content type
            
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
            [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            // create body
            NSMutableData *httpBody = [NSMutableData data];
            
            // add params (all params are strings)
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            [parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:username] forKey:@"UserName"];
            
            [parameters setValue:[NSString stringWithFormat:@"%d",1]forKey:@"EntryCount" ];
            
            [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
                [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
                [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
            }];
            
            //for (NSString *path in paths) {
            
            
            
            NSLog(@"request = %@", [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@%@\"\r\n", @"FilePath1", @"image1",@".jpeg"] dataUsingEncoding:NSUTF8StringEncoding]);
            
            
            
            [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@%@\"\r\n", @"FilePath1", @"image1",@".jpeg"] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [httpBody appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData: UIImageJPEGRepresentation(signatureimg, 0.0)];
            //[httpBody appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            // [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
            //  [httpBody appendData:data];
            [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            NSURLSession *session = [NSURLSession sharedSession];  // use sharedSession or create your own
            
            NSURLSessionTask *task = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if (error) {
                    NSLog(@"error = %@", error);
                    return;
                }
                
                NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"result = %@", result);
                ttbresponse=data;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self successmessage];
                });
            }];
            [task resume];
            
            
            
            //
            //        NSString *ids=@"";
            //        for(int i=0;i<[workorder_list count];i++)
            //        {
            //            if([ids length]>0)
            //            {
            //                NSString *idsval=[[workorder_list valueForKey:@"Wono"] objectAtIndex:0];
            //                ids=[NSString stringWithFormat:@"%@,%@",ids,idsval];
            //            }
            //            else{
            //                ids=[[workorder_list valueForKey:@"Wono"] objectAtIndex:0];
            //            }
            //
            //
            //        }
            //        NSDictionary *json = @{
            //                               @"List" :workorder_list,
            //                               @"Details" :timesheet_list,
            //                               @"WonoText": ids,
            //                               @"JobDate": [[timesheet_list valueForKey:@"JobDate"] objectAtIndex:0],
            //                               @"JobDateText": [[timesheet_list valueForKey:@"JobDateText"] objectAtIndex:0],
            //                               @"Techname": [[timesheet_list valueForKey:@"Techname"] objectAtIndex:0]
            //
            //                               };
            //
            //        DownloadManager *downloadObj = [[DownloadManager alloc]init];
            //        //   [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            //
            //        [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@SubmitTimeSheetForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"SubmitTimeSheetForApp"];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        
        
    };
    
    
    signatureViewController.cancelBlock = ^ {
        [self dismissViewControllerAnimated:YES completion:nil];
    };
    
    [self presentViewController:signatureViewController animated:YES completion:nil];
    }
    
    
}
@end
