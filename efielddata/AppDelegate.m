//
//  AppDelegate.m
//  efielddata
//
//  Created by MyMac1 on 11/20/17.
//  Copyright © 2017 vconnexservices. All rights reserved.
//

#import "AppDelegate.h" 
#import "Constants.h"

#import "TIMERUIApplication.h"
@import LocalAuthentication;

@interface AppDelegate ()

@end

@implementation AppDelegate
NSString * appStoreVersion,*currentVersion,*appID;
Boolean neeadsBool;



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{     // Override point for customization after application launch.
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidTimeout:) name:kApplicationDidTimeoutNotification object:nil];
    [(TIMERUIApplication *)[UIApplication sharedApplication] resetIdleTimer];
    if([[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL] == [NSNull null]||[[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]isEqualToString:@""]||[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL] ==nil   ){
        
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"Accesscode"];
        self.window.rootViewController = rootViewController;
        if([self needsUpdate]){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Update Available"
                                                                                     message:[NSString stringWithFormat: @"A new version of %@ is available. Please update to version %@ now.", @"EfieldData", appStoreVersion]
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *updateAlertAction = [UIAlertAction actionWithTitle:@"Update"
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction *action) {
                                                                          [self launchAppStore];
                                                                      }];
            
            [alertController addAction:updateAlertAction];
            [self.window makeKeyAndVisible];
            [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        }
    }
    else
      if ([TouchIDEnabled_Key isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:TouchIDEnabled]])
    {
        
        if([self needsUpdate]){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Update Available"
                                                                                     message:[NSString stringWithFormat: @"A new version of %@ is available. Please update to version %@ now.", @"EfieldData", appStoreVersion]
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *updateAlertAction = [UIAlertAction actionWithTitle:@"Update"
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction *action) {
                                                                          [self launchAppStore];
                                                                      }];
            
            [alertController addAction:updateAlertAction];
            [self.window makeKeyAndVisible];
            [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        }else{
        //            if([[NSUserDefaults standardUserDefaults] objectForKey:username] == [NSNull null]||[[[NSUserDefaults standardUserDefaults] objectForKey:username]isEqualToString:@""]||[[NSUserDefaults standardUserDefaults] objectForKey:username] ==nil){
        //
//
//        if([[NSUserDefaults standardUserDefaults] objectForKey:username] == [NSNull null]||[[[NSUserDefaults standardUserDefaults] objectForKey:username]isEqualToString:@""]||[[NSUserDefaults standardUserDefaults] objectForKey:username] ==nil||[[NSUserDefaults standardUserDefaults] objectForKey:password] == [NSNull null]||[[[NSUserDefaults standardUserDefaults] objectForKey:password]isEqualToString:@""]||[[NSUserDefaults standardUserDefaults] objectForKey:password] ==nil){
//
//
            //[[NSUserDefaults standardUserDefaults]removeObjectForKey:username];
            //[[NSUserDefaults standardUserDefaults]removeObjectForKey:password];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:remeberme];
            //[[NSUserDefaults standardUserDefaults]removeObjectForKey:userRole];
            //   [[NSUserDefaults standardUserDefaults]removeObjectForKey:userloginid];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:details];
            
            //Dashboard Counts
            
//            [[NSUserDefaults standardUserDefaults]removeObjectForKey:NS_HomeWithSelfCare];
//            [[NSUserDefaults standardUserDefaults]removeObjectForKey:NS_Hospital];
//            [[NSUserDefaults standardUserDefaults]removeObjectForKey:NS_InpatientSNF];
//            [[NSUserDefaults standardUserDefaults]removeObjectForKey:NS_OutpatientRehap];
//             [[NSUserDefaults standardUserDefaults]synchronize];
//
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"login"];
            self.window.rootViewController = rootViewController;
            [self.window makeKeyAndVisible];
            
        if([self needsUpdate]){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Update Available"
                                                                                     message:[NSString stringWithFormat: @"A new version of %@ is available. Please update to version %@ now.", @"EfieldData", appStoreVersion]
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *updateAlertAction = [UIAlertAction actionWithTitle:@"Update"
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction *action) {
                                                                          [self launchAppStore];
                                                                      }];
            
            [alertController addAction:updateAlertAction];
            [self.window makeKeyAndVisible];
            [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        }
//        }else{
//            if (![self isNetworkAvailable]) {
//                [self showAlertno_network:@"Efield Message" message:@"No network, please check your internet connection"];
//                return NO;
//            }else{
//
//                DownloadManager *downloadObj = [[DownloadManager alloc]init];
//                [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetLoginUser?UserName=%@&Password=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[NSUserDefaults standardUserDefaults] objectForKey:password]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetLoginUser"];
//
//            }
//
//        }
        }
      
    }
      else
          if ([[NSUserDefaults standardUserDefaults]boolForKey:remeberme])
              //&&!([[NSUserDefaults standardUserDefaults] objectForKey:password] ==nil)&&!([[NSUserDefaults standardUserDefaults] objectForKey:username] ==nil))
          {
              
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UITabBarController *rootViewController;
        
            rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];
            
       
        rootViewController.selectedIndex = 0;
        self.window.rootViewController = rootViewController;
              if([self needsUpdate]){
                  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Update Available"
                                                                                           message:[NSString stringWithFormat: @"A new version of %@ is available. Please update to version %@ now.", @"EfieldData", appStoreVersion]
                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                  UIAlertAction *updateAlertAction = [UIAlertAction actionWithTitle:@"Update"
                                                                              style:UIAlertActionStyleDefault
                                                                            handler:^(UIAlertAction *action) {
                                                                                [self launchAppStore];
                                                                            }];
                  
                  [alertController addAction:updateAlertAction];
                  [self.window makeKeyAndVisible];
                  [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
              }
        NSLog(@"launchOptions-->%@",launchOptions);
//        if ([launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey] || [launchOptions allKeys])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateDataAlert" object:self];
//        }
        UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
        UITabBar *tabBar = tabBarController.tabBar;
        UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
        UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
        UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
        UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
        
        tabBarItem1.title = @"Pending";
        tabBarItem2.title = @"Completed";
        tabBarItem3.title = @"Timesheet";
    tabBarItem4.title = @"Dashboard";

//        [tabBarItem1 setFinishedSelectedImage:[UIImage imageNamed:@"pending_higlighted"] withFinishedUnselectedImage:[UIImage imageNamed:@"pending"]];
//        [tabBarItem2 setFinishedSelectedImage:[UIImage imageNamed:@"completed_highlighted"] withFinishedUnselectedImage:[UIImage imageNamed:@"completed"]];
//        [tabBarItem3 setFinishedSelectedImage:[UIImage imageNamed:@"dashboard_highlighted"] withFinishedUnselectedImage:[UIImage imageNamed:@"dashboard"]];
        [[UITabBar appearance] setTintColor:[UIColor  colorWithRed:80/255.0f green:135/255.0f blue:20/255.0f alpha:1.0]];
       // [[UITabBar appearance] setBarTintColor:[UIColor  colorWithRed:0/255.0f green:201/255.0f blue:165/255.0f alpha:1.0]];
        
        
    }
   
  
    else
    {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:username];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:login_password];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:remeberme];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:userRole];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:details];
        //Dashboard Counts
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"login"];
        self.window.rootViewController = rootViewController;
        if([self needsUpdate]){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Update Available"
                                                                                     message:[NSString stringWithFormat: @"A new version of %@ is available. Please update to version %@ now.", @"EfieldData", appStoreVersion]
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *updateAlertAction = [UIAlertAction actionWithTitle:@"Update"
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction *action) {
                                                                          [self launchAppStore];
                                                                      }];
            
            [alertController addAction:updateAlertAction];
            [self.window makeKeyAndVisible];
            [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        }
    }
    
//    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
//    {
//        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
//
//    }
//    else
//    {
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
//    }
//
    
    return YES;
}
-(void)applicationDidTimeout:(NSNotification *) notif
{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:isLoggedin])
    {
        logoutTimer = [NSTimer scheduledTimerWithTimeInterval:300 target:self selector:@selector(autoLogout) userInfo:nil repeats:NO];
        alertSession = [[UIAlertView alloc]initWithTitle:@"Session Expiration Warning" message:@"Because you have been inactive, your session is about to expire." delegate:self cancelButtonTitle:@"Stay Logged In" otherButtonTitles:@"Log out now", nil];
        alertSession.tag = 56;
        [alertSession show];
    }
    NSLog (@"time exceeded!!");
}

- (void)showAlertno_network:(NSString*)title message:(NSString*)message
{
    
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    callAlert.tag = 58;
    [callAlert show];
    
}

- (void)launchAppStore {
    
    
    NSString *iTunesString = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@",  @"1315700444"];
    NSLog(@"%@",iTunesString);
    NSURL *iTunesURL = [NSURL URLWithString:iTunesString];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //        if (available(iOS 10.0, *)) {
        //            [[UIApplication sharedApplication] openURL:iTunesURL options:@{} completionHandler:nil];
        //    } else {
        [[UIApplication sharedApplication] openURL:iTunesURL];
        // }
//
//        if ([self respondsToSelector:@selector(harpyUserDidLaunchAppStore)]){
//            [self harpyUserDidLaunchAppStore];
//        }
    });
}
-(BOOL) needsUpdate{
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efield Message" message:@"No network, please check your internet connection"];
        return NO;
    }else{
        
        NSMutableURLRequest *request= [[NSMutableURLRequest alloc]init];
        NSString *str= [NSString stringWithFormat:@"%@GetAppVersion",ACCESSCODEURL];
        [request setURL:[NSURL URLWithString:str]];
        [request setHTTPMethod:@"GET"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        NSError *error;
        NSURLResponse *response;
      
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {

         //{
             if (error) {
                 
                 
                 UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Server Error - Please try again" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                 [callAlert show];
                // return NO;
                     neeadsBool=false;
             }
             else
             {
                 if(data != nil)
                 {
                     
                     NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                             NSLog(@"lookup%@",lookup);
                             //  if ([lookup[@"resultCount"] integerValue] == 1){
                             appStoreVersion = [lookup valueForKeyPath:@"Data.EfieldIOS"];//lookup[@"Data.PMIIOS"];
                             //     appID = lookup[@"results"][0][@"trackId"];
                      NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
                             currentVersion = infoDictionary[@"CFBundleShortVersionString"];
                     
                             float currentVersionfloat = [currentVersion floatValue];
                             float appStoreVersionfloat = [appStoreVersion floatValue];
                             NSLog(@"Need to update [%f  %f]", appStoreVersionfloat, currentVersionfloat);
                     
                             if (appStoreVersionfloat > currentVersionfloat){
                                 NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
                                // return YES;
                                 neeadsBool=true;
                             }
                             else{
                                 neeadsBool=false;
                                // return  NO;
                             }
                     
                 }
                 else
                 {
                    // return NO;
                     neeadsBool=false;
                 }
             }
         }];
        
        
     
        
//        NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
//        //    NSString* appbundleID = infoDictionary[@"CFBundleIdentifier"];
//        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@GetAppVersion",ACCESSCODEURL]];
//
//        NSData* data = [NSData dataWithContentsOfURL:url];
//        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//        NSLog(@"lookup%@",lookup);
//        //  if ([lookup[@"resultCount"] integerValue] == 1){
//        appStoreVersion = [lookup valueForKeyPath:@"Data.EfieldIOS"];//lookup[@"Data.PMIIOS"];
//        //     appID = lookup[@"results"][0][@"trackId"];
//
//        currentVersion = infoDictionary[@"CFBundleShortVersionString"];
//
//        float currentVersionfloat = [currentVersion floatValue];
//        float appStoreVersionfloat = [appStoreVersion floatValue];
//        NSLog(@"Need to update [%f  %f]", appStoreVersionfloat, currentVersionfloat);
//
//        if (appStoreVersionfloat > currentVersionfloat){
//            NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
//            return YES;
//        }
        return  neeadsBool;
  }
   // return NO;
    //}
}

- (BOOL)isNetworkAvailable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
//- (void)application:(UIApplication *)application   didRegisterUserNotificationSettings:   (UIUserNotificationSettings *)notificationSettings
//{
//    //register to receive notifications
//    [application registerForRemoteNotifications];
//}
//
//- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString   *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
//{
//    //handle the actions
//    if ([identifier isEqualToString:@"declineAction"]){
//    }
//    else if ([identifier isEqualToString:@"answerAction"]){
//    }
//}
//
//
//- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//    NSLog(@"Did Register for Remote Notifications with Device Token (%@)", deviceToken);
//
//    NSString* deviceTokenStr = [[[[deviceToken description]
//                                  stringByReplacingOccurrencesOfString: @"<" withString: @""]
//                                 stringByReplacingOccurrencesOfString: @">" withString: @""]
//                                stringByReplacingOccurrencesOfString: @" " withString: @""] ;
//    NSLog(@"Device_Token     -----> %@\n",deviceTokenStr);
//
//
//    [[NSUserDefaults standardUserDefaults]setObject:deviceTokenStr forKey:kdeviceToken];
//    [[NSUserDefaults standardUserDefaults]synchronize];
//}
//
//- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
//    NSLog(@"Did Fail to Register for Remote Notifications");
//
//    NSLog(@"%@, %@", error, error.localizedDescription);
//
//}
//
//
//- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
//{
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateDataAlert" object:self];
//    });
//}
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
//{
//
//    NSString *message = nil;
//    id alert = [userInfo objectForKey:@"aps"];
//    if ([alert isKindOfClass:[NSString class]])
//    {
//        message = alert;
//    } else if ([alert isKindOfClass:[NSDictionary class]])
//    {
//        message = [alert objectForKey:@"alert"];
//    }
//    //    if (alert) {
//    //        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Efield Alert Notification"
//    //                                                            message:message  delegate:self
//    //                                                  cancelButtonTitle:@"OK"
//    //                                                  otherButtonTitles:nil];
//    //        alertView.tag = 55;
//    //        [alertView show];
//    //    }
//    //
//
//    //Define notifView as UIView in the header file
//    [_notifView removeFromSuperview]; //If already existing
//
//    _notifView = [[UIView alloc] initWithFrame:CGRectMake(0, -70, self.window.frame.size.width, 80)];
//    [_notifView setBackgroundColor:[UIColor whiteColor]];
//
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10,15,30,30)];
//    imageView.image = [UIImage imageNamed:@"pushnotification_icon"];
//
//    UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 15, self.window.frame.size.width - 100 , 30)];
//    myLabel.font =    [UIFont systemFontOfSize:14.0];
//    myLabel.text = message;
//
//    [myLabel setTextColor:[UIColor blackColor]];
//    [myLabel setNumberOfLines:0];
//
//    [_notifView setAlpha:0.95];
//
//    //The Icon
//    [_notifView addSubview:imageView];
//
//    //The Text
//    [_notifView addSubview:myLabel];
//
//    //The View
//    [self.window addSubview:_notifView];
//
//    UITapGestureRecognizer *tapToDismissNotif = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                                        action:@selector(dismissNotifFromScreen)];
//    tapToDismissNotif.numberOfTapsRequired = 1;
//    tapToDismissNotif.numberOfTouchesRequired = 1;
//
//    [_notifView addGestureRecognizer:tapToDismissNotif];
//
//
//    [UIView animateWithDuration:1.0 delay:.1 usingSpringWithDamping:0.5 initialSpringVelocity:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
//
//        [_notifView setFrame:CGRectMake(0, 0, self.window.frame.size.width, 60)];
//
//    } completion:^(BOOL finished) {
//
//
//    }];
//
//
//    //Remove from top view after 5 seconds
//    [self performSelector:@selector(dismissNotifFromScreen) withObject:nil afterDelay:10.0];
//
//}
//- (void)dismissNotifFromScreen{
//
//    [UIView animateWithDuration:1.0 delay:.1 usingSpringWithDamping:0.5 initialSpringVelocity:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
//
//        [_notifView setFrame:CGRectMake(0, -70, self.window.frame.size.width, 60)];
//
//    } completion:^(BOOL finished) {
//
//
//    }];
//
//
//}
- (void)autoLogout
{
    [self alertView:alertSession clickedButtonAtIndex:1];
    [alertSession dismissWithClickedButtonIndex:1 animated:NO];
    [alertSession removeFromSuperview];
    [[NSUserDefaults standardUserDefaults]setBool:false forKey:isLoggedin];
    
     [[NSUserDefaults standardUserDefaults]removeObjectForKey:details];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
- (void)popViewController {
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:details];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (0 == buttonIndex && alertView.tag == 58)
    {
        UIApplication *app = [UIApplication sharedApplication];
        [app performSelector:@selector(suspend)];
        
        //wait 2 seconds while app is going background
        [NSThread sleepForTimeInterval:2.0];
        
        exit(0);
    }else
    if (alertView.tag == 55)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateDataAlert" object:self];
    }
    else if (alertView.tag == 56)
    {
        [logoutTimer invalidate];
        logoutTimer = nil;
        if (buttonIndex == 1)
        {
            [self performSelector:@selector(popViewController) withObject:nil afterDelay:0.5];

//            dispatch_async(dispatch_get_main_queue(), ^{
//             [[NSUserDefaults standardUserDefaults]removeObjectForKey:details];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"login"];
//            [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
//                     });
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [MBProgressHUD showHUDAddedTo:self.window animated:YES];
//                NSDictionary *json = @{
//
//                                       @"UserName" : [[NSUserDefaults standardUserDefaults] objectForKey:username]
//
//                                       };
//                NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
//                [dictEntry setObject:json forKey:[NSString stringWithFormat:@"UserModel"]];
//
//                DownloadManager *downloadObj = [[DownloadManager alloc]init];
//
//                [downloadObj callServerWithURLAuthentication:[NSString stringWithFormat:@"%@IOSResetUserAppId",SERVERURL] andParameter:dictEntry andMethod:@"POST" andDelegate:self andKey:@"logout"];
//
//            });
            
        }
    }
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:NULL];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    if([self needsUpdate]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Update Available"
                                                                                 message:[NSString stringWithFormat: @"A new version of %@ is available. Please update to version %@ now.", @"EfieldData", appStoreVersion]
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *updateAlertAction = [UIAlertAction actionWithTitle:@"Update"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction *action) {
                                                                      [self launchAppStore];
                                                                  }];
        
        [alertController addAction:updateAlertAction];
        [self.window makeKeyAndVisible];
        [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

 
#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.nammacompany.com.Efield" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"efielddata" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Efield.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - TouchID Authentication
- (void)authenticateUser
{
    LAContext *context = [[LAContext alloc] init];
    
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        if(context.biometryType==LABiometryTypeFaceID){
            [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                    localizedReason:FaceID_AUTHENTICATION_MSG
                              reply:^(BOOL success, NSError *error) {
                                  Boolean  IsNotAllowtoAdd;
                                  
                                  if (success) { 
                                      DownloadManager *downloadObj = [[DownloadManager alloc]init];
                                      [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetLoginUser?UserName=%@&Password=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[NSUserDefaults standardUserDefaults] objectForKey:login_password]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetLoginUsertouch"];
                                      
                                  } else {
                                      
                                      NSLog(@"error",error.localizedDescription);
                                      
                                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TouchID_AUTHENTICATION_ERROR_TITLE
                                                                                      message:TouchID_AUTHENTICATION_PROBLEM
                                                                                     delegate:nil
                                                                            cancelButtonTitle:TouchID_AUTHENTICATION_OKBTN
                                                                            otherButtonTitles:nil];
                                      if ([error.localizedDescription isEqualToString:@"Canceled by user."])
                                      {
                                          alert.title = @"Face ID Canceled";
                                          alert.message = @"You pressed cancel.";
                                          
                                          [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:login_password];
                                          [[NSUserDefaults standardUserDefaults]setObject:NO forKey:TouchIDEnabled];
                                      }
                                      else if ([error.localizedDescription isEqualToString:@"Biometry is locked out."])
                                      {
                                          alert.message = @"There was a problem verifying your identity.";
                                          
                                          [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:login_password];
                                          [[NSUserDefaults standardUserDefaults]setObject:NO forKey:TouchIDEnabled];
                                          
                                      }
                                      else if ([error.localizedDescription isEqualToString:@"Fallback authentication mechanism selected."])
                                      {
                                          alert.message = @"Please enter password.";
                                          [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:login_password];
                                          [[NSUserDefaults standardUserDefaults]setObject:NO forKey:TouchIDEnabled];
                                          
                                      }
                                      else if ([error.localizedDescription isEqualToString:@"Application retry limit exceeded."])
                                      {
                                          alert.message = @"TouchID retry limit exceeded.";
                                          [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:login_password];
                                          [[NSUserDefaults standardUserDefaults]setObject:NO forKey:TouchIDEnabled];
                                          
                                      }
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [alert show];
                                      });
                                  }
                              }];
        }
        else
            if(context.biometryType==LABiometryTypeTouchID){
                [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                        localizedReason:TouchID_AUTHENTICATION_MSG
                                  reply:^(BOOL success, NSError *error) {
                                      
                                      Boolean  IsNotAllowtoAdd;
                                      
                                      if (success) {
                                          
                                          DownloadManager *downloadObj = [[DownloadManager alloc]init];
                                          [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetLoginUser?UserName=%@&Password=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username],[[NSUserDefaults standardUserDefaults] objectForKey:login_password]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetLoginUsertouch"];
                                          
                                          
                                      } else {
                                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TouchID_AUTHENTICATION_ERROR_TITLE
                                                                                          message:TouchID_AUTHENTICATION_PROBLEM
                                                                                         delegate:nil
                                                                                cancelButtonTitle:TouchID_AUTHENTICATION_OKBTN
                                                                                otherButtonTitles:nil];
                                          if ([error.localizedDescription isEqualToString:@"Canceled by user."])
                                          {
                                              alert.title = @"Touch ID Canceled";
                                              alert.message = @"You pressed cancel.";
                                              
                                              [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:login_password];
                                              [[NSUserDefaults standardUserDefaults]setObject:NO forKey:TouchIDEnabled];
                                          }
                                          else if ([error.localizedDescription isEqualToString:@"Biometry is locked out."])
                                          {
                                              alert.message = @"There was a problem verifying your identity.";
                                              
                                              [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:login_password];
                                              [[NSUserDefaults standardUserDefaults]setObject:NO forKey:TouchIDEnabled];
                                              
                                          }
                                          else if ([error.localizedDescription isEqualToString:@"Fallback authentication mechanism selected."])
                                          {
                                              alert.message = @"Please enter password.";
                                              [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:login_password];
                                              [[NSUserDefaults standardUserDefaults]setObject:NO forKey:TouchIDEnabled];
                                              
                                          }
                                          else if ([error.localizedDescription isEqualToString:@"Application retry limit exceeded."])
                                          {
                                              alert.message = @"TouchID retry limit exceeded.";
                                              [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:login_password];
                                              [[NSUserDefaults standardUserDefaults]setObject:NO forKey:TouchIDEnabled];
                                              
                                          }
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [alert show];
                                          });
                                      }
                                  }];
            }
    }
    //  else {
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TouchID_AUTHENTICATION_ERROR_TITLE
    //                                                            message:Biometrics_NOT_AVAILABLE_MSG
    //                                                           delegate:nil
    //                                                  cancelButtonTitle:TouchID_AUTHENTICATION_OKBTN
    //                                                  otherButtonTitles:nil];
    //            [alert show];
    // });
    //  }
}

-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    if ([key isEqualToString:@"logout"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]isEqualToString:@"Success"])
        {
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:remeberme];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:details];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            LoginViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"login"];
            self.window.rootViewController = rootViewController;
            [self.window makeKeyAndVisible];
        }
    }
    else   if([key isEqualToString:@"GetLoginUser"])
    {
        
        
        if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"]    )
        {
            
            
         [[NSUserDefaults standardUserDefaults]setObject:[response valueForKeyPath:@"Data.AuthorizationId"] forKey:AuthorizationId];
            
            [[NSUserDefaults standardUserDefaults]setObject:[response valueForKeyPath:@"Data.RoleName"] forKey:userRole];
      
            [[NSUserDefaults standardUserDefaults]synchronize];
        
            [self performSelector:@selector(gotoDashBoard) withObject:nil afterDelay:0.65];
            
            
            
            
        }
        
    }
    
    else   if([key isEqualToString:@"GetLoginUsertouch"])
    {
        
       
            if ([[response valueForKeyPath:@"Data.Response"] isEqualToString:@"Success"]    )
            {
                
                
               [[NSUserDefaults standardUserDefaults]setObject:[response valueForKeyPath:@"Data.AuthorizationId"] forKey:AuthorizationId];
                
                [[NSUserDefaults standardUserDefaults]setObject:[response valueForKeyPath:@"Data.RoleName"] forKey:userRole];
              
                    [self performSelector:@selector(gotoDashBoard) withObject:nil afterDelay:0.3];
               
                
                
            }
 
       
     
        
    }
}
- (void)gotoDashBoard
{
    
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"mainTabbar"];
        rootViewController.selectedIndex = 0;
        [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
    if([self needsUpdate]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Update Available"
                                                                                 message:[NSString stringWithFormat: @"A new version of %@ is available. Please update to version %@ now.", @"EfieldData", appStoreVersion]
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *updateAlertAction = [UIAlertAction actionWithTitle:@"Update"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction *action) {
                                                                      [self launchAppStore];
                                                                  }];
        
        [alertController addAction:updateAlertAction];
        [self.window makeKeyAndVisible];
        [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
    }
    }

@end
