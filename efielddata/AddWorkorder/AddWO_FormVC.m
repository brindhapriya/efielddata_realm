
//
//  Created by brindha on 12/02/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "AddWO_FormVC.h"
#import "DLRadioButton.h"

#import "RadioTableViewCell.h"

#import "multiselectViewController.h"
#import "Checkbox_Cell.h"
#import "Testtype_category.h"

#import "Ready_Checkbox_Cell.h"
#import "AlertListVC.h"

@interface AddWO_FormVC ()<UITextViewDelegate,UITextFieldDelegate,AddWO_FormVCDelegate>{
    
    NSInteger rowOfTheCell;
    NSIndexPath *editingchildIndexPath;
    NSDateFormatter *globalformatter,*globaltimeformatter;
    Boolean isautoridirect;
    NSString *isready;
}
@end
NSMutableArray *newWOlist;
NSArray *ClientList,*ProjectList,*TestTypeList,*LabTechList,*FieldTechList,*SampleTypeList,*SiteContact;

@implementation AddWO_FormVC
  int selectedindex;
- (void)viewDidLoad {
     self.savebtn.enabled = NO;
    [super viewDidLoad];
   globalformatter = [[NSDateFormatter alloc] init];
    [globalformatter setDateFormat:@"MM/dd/yyyy"];
    
    globaltimeformatter = [[NSDateFormatter alloc] init];
    [globaltimeformatter setDateFormat:@"hh:mm a"];
    
     self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
    _alertcnt.text = [self updateALERTcount];
    _companynametxt.text =
    [[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
    NSString *eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
    
    _tbl_form.delegate = self;
    
    _tbl_form.dataSource = self;
  

    _date.text = eventDateStr;
    if([_Isedit isEqualToString:@"false"])
    {
        _header_lbl.text=@"Add New Work Order";
    newWOlist = [[NSMutableArray alloc] init];
    for (int i = 0; i < 21; i++) {
        NSString *display_Name, *display_Value, *Id, *category_Name, *category_Desc,*CssClientId,
        *datatype,*IsRequired;
        switch (i) {
            case 0:
                
                display_Name = @"Enter new work order details";
                display_Value = @"* indicates required fields";
                Id = @"";
                IsRequired = @"true";
                datatype = @"category";
                break;
            case 1:
                display_Name = @"Client";
                display_Value = @"";
                Id = @"";
                CssClientId = @"";
                datatype = @"dropdown";
                IsRequired = @"true";
                
                break; /* optional */
            case 2:
                display_Name = @"Project / Sub Project";
                display_Value = @"";
                Id = @"";
                datatype = @"dropdown";
                IsRequired = @"true";
                
                break; /* optional */
            case 3:
                display_Name = @"Test Type";
                display_Value = @"Select lab category for Sample Pickup.";
                Id = @"";
                datatype = @"category";
                IsRequired = @"false";
                break; /* optional */
            case 4:
                display_Name = @"";
                display_Value = @"";
                Id = @"FieldLab";
                datatype = @"radio";
                IsRequired = @"false";
                
                break; /* optional */
            case 5:
                display_Name = @"Test Type";
                display_Value = @"";
                Id = @"";
                datatype = @"dropdown";
                IsRequired = @"true";
                break; /* optional */
            case 6:
                display_Name = @"";
                display_Value = @"";
                Id = @"";
                datatype = @"checkbox";
                IsRequired = @"false";
                break; /* optional */
            case 7: /* Optional */
                display_Name = @"Type (Building / Site)";
                display_Value = @"";
                Id = @"";
                datatype = @"dropdown";
                IsRequired = @"false";
                break;
        
            case 8:
                display_Name = @"Sample Type";
                display_Value = @"";
                Id = @"";
                datatype = @"multiselect_dropdown";
                IsRequired = @"false";
                break; /* optional */
            case 9:
                display_Name = @"Assigned Lab Tech To";
                display_Value = @"";
                Id = @"";
                datatype = @"dropdown";
                IsRequired = @"false";
                break; /* optional */
//            case 9:
//                display_Name = @"";
//                display_Value = @"If Test Type is \"Soil Density\" or Concrete, choose "
//                @"specific work order type (building / site) below.";
//                Id = @"";
//                datatype = @"category";
//                IsRequired = @"false";
//                break; /* optional */
        
                
            case 10: /* Optional */
                display_Name = @"Assigned Field Tech To";
                display_Value = @"";
                Id = @"";
                datatype = @"dropdown";
                IsRequired = @"true";
                break;
            case 11: /* Optional */
                
              
                display_Name = @"Date Called-in";
                display_Value = [globalformatter stringFromDate:[NSDate date]];
                Id = @"";
                datatype = @"date";
                IsRequired = @"true";
                break;
            case 12: /* Optional */
                display_Name = @"Work Order Status";
                display_Value = @"Assigned";
                Id = @"";
                IsRequired = @"true";
                
                datatype = @"dropdown";
                
                break;
            case 13: /* Optional */
                display_Name = @"Job Date & Time";
                display_Value = [globalformatter stringFromDate:[NSDate date]];
                Id = [globaltimeformatter stringFromDate:[NSDate date]];
                datatype = @"dateandtime";
                IsRequired = @"true";
                break;
        
            case 14: /* Optional */
                display_Name = @"Contractor";
                display_Value = @"";
                Id = @"";
                datatype = @"TextField";
                IsRequired = @"false";
                break;
            case 15: /* Optional */
                display_Name = @"Site Contact";
                display_Value = @"";
                Id = @"";
                datatype = @"TextField";
                IsRequired = @"false";
                break;
            case 16: /* Optional */
                display_Name = @"Contact Phone";
                display_Value = @"";
                Id = @"";
                datatype = @"TextField";
                IsRequired = @"false";
                break;
            case 17: /* Optional */
                display_Name = @"Task Description";
                display_Value = @"";
                Id = @"";
                datatype = @"TextBox";
                IsRequired = @"false";
                break;
            case 18: /* Optional */
                display_Name = @"Job Site Address";
                display_Value = @"";
                Id = @"";
                datatype = @"TextField";
                IsRequired = @"false";
                break;
            case 19: /* Optional */
                display_Name = @"Driving Directions";
                display_Value = @"";
                Id = @"";
                datatype = @"TextBox";
                IsRequired = @"false";
                break;
            case 20:
                display_Name = @"Estimated Driving Time";
                display_Value = @"";
                Id = @"";
                datatype = @"TextField";
                IsRequired = @"false";
                break;
//            case 21:
//                display_Name = @"Ready & Notify Technician Now";
//                display_Value = @"";
//                Id = @"";
//                datatype = @"checkbox_ready";
//                IsRequired = @"false";
//                break; /* optional */
                
            default: /* Optional */
                display_Name = @"Estimated Driving Time";
                display_Value = @"";
                Id = @"";
                datatype = @"TextField";
                IsRequired = @"false";
                break;
        }
        if ([display_Value isKindOfClass:[NSNull class]] ||
            [display_Value isEqualToString:@"<null>"]) {
            
            display_Value = @"";
        }
        
        NSDictionary *AddWO_FormVC_dict ;
        if([display_Name isEqualToString:@"Client"])
        {
            AddWO_FormVC_dict = [NSDictionary
                                 dictionaryWithObjectsAndKeys:display_Name,
                                 @"display_Name",
                                 display_Value,
                                 @"display_Value", Id, @"Id",
                                 
                                 CssClientId,@"CssClientId",
                                 datatype,   @"DataType",
                                 IsRequired,  @"IsRequired"
                                 ,nil];
        }
        else{
            AddWO_FormVC_dict = [NSDictionary
                                 dictionaryWithObjectsAndKeys:display_Name,
                                 @"display_Name",
                                 display_Value,
                                 @"display_Value", Id, @"Id",
                                 datatype,   @"DataType",
                                 IsRequired,  @"IsRequired"
                                 ,nil];
        } [newWOlist addObject:AddWO_FormVC_dict];
    }
        _isreadystr = @"false";
        [_readyswitch setOn:false];
        
 
        
    }
    else{
        if ([_isreadystr isEqualToString:@"true"]) {
         
            [_readyswitch setOn:true];
            
            
            
        } else {
            
            [_readyswitch setOn:false];
            
          
        }
        _header_lbl.text=[NSString stringWithFormat:@"%@",_JobNumber];
        //[NSString stringWithFormat:@"Edit WO (%@)",_JobNumber];
   newWOlist  =_arraylist;
    }
 
    [self GetClientList];
 
    
    
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    // if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
    contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0);
    //    } else {
    //        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
    //    }
    
    _tbl_form.contentInset = contentInsets;
    _tbl_form.scrollIndicatorInsets = contentInsets;
    [_tbl_form scrollToRowAtIndexPath: editingchildIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    _tbl_form.contentInset = UIEdgeInsetsZero;
  _tbl_form.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (0 == buttonIndex && alertView.tag == 55) {
      
        [self.navigationController popViewControllerAnimated:YES];

        
    }
}
-(void)callBackWithSuccessResponse:(NSDictionary *)response andKey:(NSString *)key
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    
    if ([key isEqualToString:@"AddNewWorkOrderForApp"])
    {
        if ([[response valueForKeyPath:@"Data.Response"]
             isEqualToString:@"Success"]) {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"FileName"];
            UIAlertView *callAlert = [[UIAlertView alloc]
                                      initWithTitle:nil
                                      message:[[response valueForKeyPath:@"Data.Message"]
                                               description]
                                      delegate:self
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil];
            callAlert.tag = 55;
            [callAlert show];
            
        } else {
            
            UIAlertView *callAlert = [[UIAlertView alloc]
                                      initWithTitle:nil
                                      message:[[response valueForKeyPath:@"Data.Message"]
                                               description]
                                      delegate:self
                                      cancelButtonTitle:@"Ok"
                                      otherButtonTitles:nil];
            //  callAlert.tag = 55;
            [callAlert show];
        }

    }else
    if ([key isEqualToString:@"GetSampleType"])
    {
 SampleTypeList=[[NSMutableArray alloc]initWithArray: [response valueForKeyPath:@"Data"]  ];
        [self performSegueWithIdentifier:@"multiselect_segue" sender:self];

    }
    else  if ([key isEqualToString:@"GetClientList"])
    {
        ClientList=[[NSMutableArray alloc]initWithArray: [response valueForKeyPath:@"Data"]  ];
        
        [_tbl_form reloadData];
       
        

    }
    else   if ([key isEqualToString:@"GetProjectListForClient"])
    {
        ProjectList=[[NSMutableArray alloc]initWithArray: [response valueForKeyPath:@"Data"]  ];
//        if([ProjectList count]>0)
//        {
//            [self performSegueWithIdentifier:@"multiselect_segue" sender:self];
//        }
    }
    else   if ([key isEqualToString:@"GetTestTypeListForProject"])
    {
        NSLog(@"response%@",response);
        TestTypeList=[[NSMutableArray alloc]initWithArray: [response valueForKeyPath:@"Data.TaskList"]  ];
        NSMutableDictionary *dic =
        [[NSMutableDictionary alloc] initWithDictionary: newWOlist[15]];
        SiteContact=[[NSMutableArray alloc]initWithArray: [response valueForKeyPath:@"Data.SiteContact"]  ];
        NSString *SiteContactstring;
        SiteContactstring=@"";
        if([SiteContact count]>0)
        {
            SiteContactstring=[[SiteContact objectAtIndex:0]description];

        }
        
       [dic setObject:SiteContactstring forKey:@"display_Value"];
        [dic setObject:@"" forKey:@"Id"];
       [newWOlist replaceObjectAtIndex:15  withObject:dic];
        dic = [[NSMutableDictionary alloc] initWithDictionary: newWOlist[16]];
        [dic setObject:[response valueForKeyPath:@"Data.ContactPhone"] forKey:@"display_Value"];
        [dic setObject:@"" forKey:@"Id"];
        [newWOlist replaceObjectAtIndex:16 withObject:dic];
        dic =
        [[NSMutableDictionary alloc] initWithDictionary: newWOlist[17]];
        [dic setObject:[response valueForKeyPath:@"Data.ProjectInfo"] forKey:@"display_Value"];
        [dic setObject:@"" forKey:@"Id"];
        [newWOlist replaceObjectAtIndex:17  withObject:dic];
        dic =
        [[NSMutableDictionary alloc] initWithDictionary: newWOlist[18]];
        [dic setObject:[response valueForKeyPath:@"Data.JobSiteAddress"] forKey:@"display_Value"];
        [dic setObject:@"" forKey:@"Id"];
        [newWOlist replaceObjectAtIndex:18 withObject:dic];
        
        
        
        
        
        [_tbl_form reloadData];
        if([TestTypeList count]>0&&isautoridirect)
        {
            [self performSegueWithIdentifier:@"multiselect_segue" sender:self];
        }
//

    }
   else    if ([key isEqualToString:@"GetLabTechForApp"])
    {
        LabTechList=[[NSMutableArray alloc]initWithArray: [response valueForKeyPath:@"Data"]  ];
        if([LabTechList count]>0)
        {
            [self performSegueWithIdentifier:@"multiselect_segue" sender:self];
        }
    }
    else   if ([key isEqualToString:@"GetFieldTechForApp"])
    {
        FieldTechList=[[NSMutableArray alloc]initWithArray: [response valueForKeyPath:@"Data"]  ];
        if([FieldTechList count]>0)
        {
            [self performSegueWithIdentifier:@"multiselect_segue" sender:self];
        }
    }
    
}
-(void)GetSampleType
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetSampleType",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetSampleType"];
    ;
    
    
}
-(void)GetClientList
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetClientList?UserName=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults standardUserDefaults] objectForKey:username]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetClientList"];
     ;
 
    
}

-(void)GetProjectListForClient
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetProjectListForClient?ClientId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],newWOlist[1][@"Id"]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetProjectListForClient"];
    
    
}


-(void) textViewDidChange:(UITextView *)textView
{
    
    [_savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    [_savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
_savebtn.enabled = YES;

    if(textView.text.length == 0){
        textView.textColor = [UIColor lightGrayColor];
        textView.text = @"Type here ...";
        NSLog(@"textView : %ld",(long)textView.tag);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:newWOlist[textView.tag]];
        [dic setObject:@"" forKey:@"display_Value"];
        NSLog(@"dic : %@",dic);
        [newWOlist replaceObjectAtIndex:textView.tag withObject:dic];

        [textView resignFirstResponder];
    }

    else{
        NSLog(@"textView : %ld",(long)textView.tag);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:newWOlist[textView.tag]];
        [dic setObject:textView.text forKey:@"display_Value"];
        NSLog(@"dic : %@",dic);
        [newWOlist replaceObjectAtIndex:textView.tag withObject:dic];
    }
    
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
    
    if(textView.text.length != 0){
       // textView.text =   newWOlist[textView.tag][@"display_Value"];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:newWOlist[textView.tag]];
        [dic setObject: textView.text forKey:@"display_Value"];
        NSLog(@"dic : %@",dic);
        [newWOlist replaceObjectAtIndex:textView.tag withObject:dic];
        
    }else{
        textView.text =@"Type here ...";
     
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:newWOlist[textView.tag]];
    [dic setObject:@"" forKey:@"display_Value"];
    NSLog(@"dic : %@",dic);
    [newWOlist replaceObjectAtIndex:textView.tag withObject:dic];
    }  //  [_tblForm reloadData];
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    //    if([text isEqualToString:@"\n"]) {
    //        [textView resignFirstResponder];
    //        return NO;
    //    }
    
    return YES;
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    editingchildIndexPath= [NSIndexPath indexPathForRow:textView.tag inSection:0];
    
    // if([self substring:@"null" existsInString:_admitingdiagnosis]) {
    if([textView.text isEqualToString:@"Type here ..."]){
        textView.text = @"";
        textView.textColor = [UIColor lightGrayColor];}
    return YES;
}


- (void)textFieldDidChange:(UITextField *)textField {
    
    
    NSMutableDictionary *dic =
    [[NSMutableDictionary alloc] initWithDictionary: newWOlist[textField.tag]];
    if (![textField.text isEqualToString:@""]) {
        [dic setObject:textField.text forKey:@"display_Value"];
    } else {
        [dic setObject:@"" forKey:@"display_Value"];
    }
    //[dic setObject:textField.text forKey:@"TextAnswer"];
    NSLog(@"dic : %@", dic);
    [newWOlist replaceObjectAtIndex:textField.tag withObject:dic];
    
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    
    NSMutableDictionary *dic =
    [[NSMutableDictionary alloc] initWithDictionary: newWOlist[textField.tag]];
    if (![textField.text isEqualToString:@""]) {
        [dic setObject:textField.text forKey:@"display_Value"];
    } else {
        [dic setObject:@"" forKey:@"display_Value"];
    }
    //[dic setObject:textField.text forKey:@"TextAnswer"];
    NSLog(@"dic : %@", dic);
    [newWOlist replaceObjectAtIndex:textField.tag withObject:dic];
    
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
    
}


-(void)GetTestTypeListForProject
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    
    
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetTestTypeListForProject?ProjectId=%@&FilterTestType=%@&CssClientId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],newWOlist[2][@"Id"],newWOlist[4][@"Id"],newWOlist[1][@"CssClientId"]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetTestTypeListForProject"];
    
    
}


-(void)GetLabTechForApp
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetLabTechForApp?CssClientId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],newWOlist[1][@"CssClientId"]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetLabTechForApp"];
    
    
}

-(void)GetFieldTechForApp
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@GetFieldTechForApp?CssClientId=%@",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL],newWOlist[1][@"CssClientId"]] andParameter:nil andMethod:@"GET" andDelegate:self andKey:@"GetFieldTechForApp"];
    
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (CGFloat)calculateStringHeigth:(CGSize)size
                            text:(NSString *)text
                         andFont:(UIFont *)font {
    NSLog(@"text : %@", text);
    
    NSLog(@"Font : %@", font);
    NSLog(@"size : %@", NSStringFromCGSize(size));
    
    if ([text isEqual:[NSNull null]]) {
        text = @"";
    }
    
    if (![text length])
        return 0;
    
    CGSize labelSize = [text sizeWithFont:font
                        constrainedToSize:size
                            lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat labelHeight = labelSize.height;
    NSLog(@"labelHeight : %f", labelHeight);
    return labelHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = 60;
    
    if ([newWOlist[indexPath.row][@"DataType"]
         isEqualToString:@"category"]) {
        CGSize size = _tbl_form.frame.size;
        size.width = size.width - 16;
        height = [self calculateStringHeigth:size text:newWOlist[indexPath.row][@"display_Value"] andFont:[UIFont systemFontOfSize:13]];
        CGFloat height1;
        height1 = [self calculateStringHeigth:size text:newWOlist[indexPath.row][@"display_Name"] andFont:[UIFont systemFontOfSize:13]];
        if(![newWOlist[indexPath.row][@"display_Name"] length]&&![newWOlist[indexPath.row][@"display_Value"] length]){
            
            
            
            height = height + 37;
        }
        
        else
            if([newWOlist[indexPath.row][@"display_Name"] isKindOfClass:[NSNull class]]||[newWOlist[indexPath.row][@"display_Name"] isEqualToString:@""]){
                
                
                height = height + 12;
            }
            else {
                
                height = height +15+20;}
        
    }
    else
    if([newWOlist[indexPath.row][@"DataType"] isEqualToString:@"TextBox"])
    {
    height = 90;
    }
    
    else   if([newWOlist[indexPath.row][@"DataType"] isEqualToString:@"TextField"])
    {
        height =55;
    }
   else  if([newWOlist[indexPath.row][@"DataType"] isEqualToString:@"radio"])
   {
        height = 70;
   }else{
       height =55;
   }
    return height;
    
    
}
- (IBAction)logoutAction:(id)sender
{
    [self logoutUser];
    
}
- (IBAction)alertaction:(id)sender {
    [self performSegueWithIdentifier:@"alertsegue" sender:self];
    
}
-(void)callBackWithFailureResponse:(NSDictionary *)response andKey:(NSString *)key
{
    NSLog(@"CallBackFailure");
    UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Server may be busy. Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    // callAlert.tag = 56;
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}



#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    return [newWOlist count];

}

- (IBAction)logSelectedButton:(DLRadioButton *)radioButton {
    if (radioButton.isMultipleSelectionEnabled) {
        for (DLRadioButton *button in radioButton.selectedButtons) {
            NSLog(@"%@ is selected.\n", button.titleLabel.text);
        }
    } else {
        [_savebtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
        _savebtn.enabled = YES;
        NSLog(@"%@ is selected.\n", radioButton.selectedButton.titleLabel.text);
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]
                                    initWithDictionary:newWOlist[3]];
        if(radioButton.tag==101)
        {
        [dic setObject:@"FieldLab"
                forKey:@"Id"];
        }else
            if(radioButton.tag==102)
            {
                [dic setObject:@"SpecialtyAdmin"
                        forKey:@"Id"];
            }
        
        
        [self changevalue:4 ];
        
        [newWOlist replaceObjectAtIndex:3 withObject:dic];
        [_tbl_form reloadData];
        
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView

         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    
    if ([newWOlist[indexPath.row][@"DataType"] isEqualToString:@"checkbox_ready"]) {
        Ready_Checkbox_Cell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"ready_checkboxCell"];
        [cell.ready_btn setImage:[UIImage imageNamed:@"uncheckedCheckbox"] forState:UIControlStateNormal];
        [cell.ready_btn setImage:[UIImage imageNamed:@"checkedCheckbox"] forState:UIControlStateSelected];
        cell.ready_btn.backgroundColor = [UIColor clearColor];
          cell.ready_btn.tag=105;
  
        
        
        [cell.ready_btn setTitle:newWOlist[22][@"display_Name"] forState:UIControlStateNormal];
        
        
        if([newWOlist[22][@"display_Value"] isEqualToString: @"true"])
        {
            [cell.ready_btn setSelected:YES];
            
            
        }
        else{
            [cell.ready_btn setSelected:NO];
        }
    
        
        [cell.ready_btn addTarget:self action:@selector(ready_checkbutton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
 
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        return  cell;
        
    }else
    if ([newWOlist[indexPath.row][@"DataType"] isEqualToString:@"checkbox"]) {
        Checkbox_Cell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"checkboxCell"];
        [cell.non_billable setImage:[UIImage imageNamed:@"uncheckedCheckbox"] forState:UIControlStateNormal];
        [cell.non_billable setImage:[UIImage imageNamed:@"checkedCheckbox"] forState:UIControlStateSelected];
        cell.non_billable.backgroundColor = [UIColor clearColor];
        cell.special_Type.backgroundColor = [UIColor clearColor];

        [cell.special_Type setImage:[UIImage imageNamed:@"uncheckedCheckbox"] forState:UIControlStateNormal];
        [cell.special_Type setImage:[UIImage imageNamed:@"checkedCheckbox"] forState:UIControlStateSelected];
       cell.non_billable.tag=103;
        cell.special_Type.tag=104;
        [cell.special_Type setTitle:@"Inspection 'Specialty Type'" forState:UIControlStateNormal];

       
        [cell.non_billable setTitle:@"Non Billable" forState:UIControlStateNormal];
        
        
        if([_Isedit isEqualToString:@"true"])
            
            //5 /7/ 8
        {
            cell.userInteractionEnabled = NO;
            cell.backgroundColor=[UIColor  colorWithRed:222/255.0f green:222/255.0f blue:226/255.0f alpha:0.5];
            cell.accessoryType = UITableViewCellAccessoryNone;
            //
            //[cell.special_Type setEnabled:NO];
            cell.special_Type.userInteractionEnabled = NO;
           // [  cell.non_billable setEnabled:NO];
            cell.non_billable.userInteractionEnabled = NO;
        }else{
            cell.backgroundColor=[UIColor  whiteColor];
            
            cell.userInteractionEnabled = YES;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
          //  [cell.special_Type setEnabled:YES];
            cell.special_Type.userInteractionEnabled = YES;
           // [  cell.non_billable setEnabled:YES];
            cell.non_billable.userInteractionEnabled = YES;
        }
            
        
        
         if([newWOlist[6][@"display_Value"] isEqualToString: cell.non_billable.titleLabel.text])
        {
            [cell.non_billable setSelected:YES];

           
        }
        else{
            [cell.non_billable setSelected:NO];
        }
       if([newWOlist[6][@"display_Name"] isEqualToString:cell.special_Type.titleLabel.text])
        {
           [cell.special_Type setSelected:YES];
        }
       else{
            [cell.special_Type setSelected:NO];
       }
        
        [cell.non_billable addTarget:self action:@selector(checkbutton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.special_Type addTarget:self action:@selector(checkbutton_Clicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;

        return  cell;
        
    }
  else  if ([newWOlist[indexPath.row][@"DataType"] isEqualToString:@"category"]) {
        DescriptionTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
        
        cell.lblTitle.text = newWOlist[indexPath.row][@"display_Name"];
        
        //  if(newWOlist[indexPath.row][@"CategoryDescription"] != [NSNull null])
        [cell.lblTitle sizeToFit];
        cell.lblTitle.numberOfLines = 1;
        [cell.lblDiscribtion sizeToFit];
        cell.lblDiscribtion.numberOfLines = 0;
        
      
     
        if ([newWOlist[indexPath.row][@"display_Value"]
             isKindOfClass:[NSNull class]]) {
            cell.lblDiscribtion.hidden = YES;
            
        } else {
            cell.lblDiscribtion.hidden = NO;
            cell.lblDiscribtion.attributedText =[self AddcategoryRequiredField:newWOlist[indexPath.row][@"display_Value"] andIsRequired:[newWOlist[indexPath.row][@"IsRequired"] boolValue]];
            //newWOlist[indexPath.row][@"display_Value"];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
        else
            if([newWOlist[indexPath.row][@"DataType"] isEqualToString:@"date"]) {
                DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
                 cell.lblValue.tag = indexPath.row;
                if(newWOlist[indexPath.row][@"display_Value"] == [NSNull null]||[newWOlist[indexPath.row][@"display_Value"]isEqualToString:@""]){
                    cell.lblValue.text =@"";
                }else{
                    
                    
                    cell.lblValue.text =  newWOlist[indexPath.row][@"display_Value"];
                }
                cell.lblTitle.attributedText = [self AddRequiredField:newWOlist[indexPath.row][@"display_Name"] andIsRequired:[newWOlist[indexPath.row][@"IsRequired"] boolValue]];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
    
    }
    
            else
                if([newWOlist[indexPath.row][@"DataType"] isEqualToString:@"dateandtime"]) {
                    DateAndTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DateAndTimeCell"];
                     cell.lblValue.tag = indexPath.row;
                    if(newWOlist[indexPath.row][@"display_Value"] == [NSNull null]||[newWOlist[indexPath.row][@"display_Value"]isEqualToString:@""]){
                        cell.lblValue.text =@"";
                    }else{
                      //  cell.lblValue.text = newWOlist[indexPath.row][@"display_Value"];
                        
                        cell.lblValue.text = [NSString stringWithFormat:@"%@ %@",newWOlist[indexPath.row][@"display_Value"],newWOlist[indexPath.row][@"Id"]];
                    }
                    cell.lblTitle.attributedText = [self AddRequiredField:newWOlist[indexPath.row][@"display_Name"] andIsRequired:[newWOlist[indexPath.row][@"IsRequired"] boolValue]];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    return cell;
                    
                }
    
            else if ([newWOlist[indexPath.row][@"DataType"]
                isEqualToString:@"TextBox"]) {
        TextViewTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"TextViewCell"];
        if (cell == nil) {
            cell = [[TextViewTableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:@"TextViewCell"];
        }
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:newWOlist[indexPath.row][@"display_Name"]
                                        andIsRequired:[newWOlist[indexPath.row][@"IsRequired"] boolValue]];
        [cell.txtViewValue setKeyboardType:UIKeyboardTypeAlphabet];
        
        cell.txtViewValue.delegate = self;
        [cell.txtViewValue sizeToFit];
        
        cell.txtViewValue.tag = indexPath.row;
        if (newWOlist[indexPath.row][@"display_Value"] == [NSNull null] ||
            [newWOlist[indexPath.row][@"display_Value"] isEqualToString:@""]) {
            cell.txtViewValue.text = @"Type here ...";
            cell.txtViewValue.textColor = [UIColor lightGrayColor];
        } else if (newWOlist[indexPath.row][@"display_Value"] != [NSNull null]) {
            cell.txtViewValue.text = newWOlist[indexPath.row][@"display_Value"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if ([newWOlist[indexPath.row][@"DataType"]
                isEqualToString:@"TextField"]) {
        TextFieldTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"TextFieldCell"];
        if (cell == nil) {
            cell = [[TextFieldTableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:@"TextFieldCell"];
        }
        
        
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:newWOlist[indexPath.row][@"display_Name"]
                                        andIsRequired:[newWOlist[indexPath.row][@"IsRequired"] boolValue]];
        cell.txtValue.delegate = self;
        cell.txtValue.tag = indexPath.row;
        if(indexPath.row==18)
        {
            cell.txtValue.userInteractionEnabled = NO;
            cell.backgroundColor=[UIColor  colorWithRed:222/255.0f green:222/255.0f blue:226/255.0f alpha:0.5];
        }else{
            cell.backgroundColor=[UIColor  whiteColor];

            cell.txtValue.userInteractionEnabled = YES;

        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
       // [cell.txtValue addTarget:self
                 //         action:@selector(textFieldDidEndEditing:)
             //   forControlEvents:UIControlEventEditingChanged];
        if(indexPath.row==16)
        {
            [cell.txtValue setKeyboardType:UIKeyboardTypePhonePad];

        }
        else{
        [cell.txtValue setKeyboardType:UIKeyboardTypeAlphabet];
        }
        //
        [cell.txtValue addTarget:self
                          action:@selector(textFieldDidChange:)
                forControlEvents:UIControlEventEditingChanged];
        if (newWOlist[indexPath.row][@"display_Value"] == [NSNull null] ||
            [newWOlist[indexPath.row][@"display_Value"] isEqualToString:@""]) {
            cell.txtValue.placeholder = @"Type here ...";
            cell.txtValue.text = @"";
            
        } else
            //  if(newWOlist[indexPath.row][@"TextAnswer"] != [NSNull null])
        {
            cell.txtValue.text = newWOlist[indexPath.row][@"display_Value"];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
        //
    } else if ([newWOlist[indexPath.row][@"DataType"]
                isEqualToString:@"multiselect_dropdown"]) {
        MultySelectionTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
        
        cell.lblTitle.attributedText = [self
                                        AddRequiredField:newWOlist[indexPath.row][@"display_Name"]
                                        andIsRequired:[newWOlist[indexPath.row][@"IsRequired"] boolValue]];
        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tbl_form.frame));
        cell.lblValue.text = newWOlist[indexPath.row][@"display_Value"];
        [cell.lblTitle sizeToFit];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
       // cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if([_Isedit isEqualToString:@"true"])
            
            //5 /7/ 8
        {        if(indexPath.row==1||indexPath.row==2||indexPath.row==5||
                    indexPath.row==7|| indexPath.row==8)
        {
            cell.userInteractionEnabled = NO;
            cell.backgroundColor=[UIColor  colorWithRed:222/255.0f green:222/255.0f blue:226/255.0f alpha:0.5];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }else{
            cell.backgroundColor=[UIColor  whiteColor];
            
            cell.userInteractionEnabled = YES;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
        }
        }
        return cell;
  
    } else if ([newWOlist[indexPath.row][@"DataType"]
                isEqualToString:@"dropdown"]) {
        MultySelectionTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"MultySelectionCell"];
      cell.lblTitle.attributedText = [self
                                        AddRequiredField:newWOlist[indexPath.row][@"display_Name"]
                                        andIsRequired:[newWOlist[indexPath.row][@"IsRequired"] boolValue]];
        NSLog(@"Frame : %@", NSStringFromCGRect(cell.lblTitle.frame));
        NSLog(@"Frame 1: %@", NSStringFromCGRect(self.tbl_form.frame));
        cell.lblValue.text = newWOlist[indexPath.row][@"display_Value"];
        [cell.lblTitle sizeToFit];
        if([_Isedit isEqualToString:@"true"])
            
            //5 /7/ 8
        {        if(indexPath.row==1||indexPath.row==2||indexPath.row==5||
                    indexPath.row==7)
        {
            cell.userInteractionEnabled = NO;
            cell.backgroundColor=[UIColor  colorWithRed:222/255.0f green:222/255.0f blue:226/255.0f alpha:0.5];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }else{
            cell.backgroundColor=[UIColor  whiteColor];
            
            cell.userInteractionEnabled = YES;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

            
        }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
      static NSString *simpleTableIdentifier = @"testtpe_menu";
            
            Testtype_category *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil) {
                cell = [[Testtype_category alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
            }
            cell.testtype_segment.tag=101;
        
        
        
        if([_Isedit isEqualToString:@"true"])
            
            //5 /7/ 8
        {
            cell.userInteractionEnabled = NO;
            cell.backgroundColor=[UIColor  colorWithRed:222/255.0f green:222/255.0f blue:226/255.0f alpha:0.5];
            cell.accessoryType = UITableViewCellAccessoryNone;
              //
          //  [cell.testtype_segment disabl
             [cell.testtype_segment setEnabled:NO forSegmentAtIndex:0];

             [cell.testtype_segment setEnabled:NO forSegmentAtIndex:1];
            
            [cell.testtype_segment setTintColor:[UIColor  colorWithRed:145/255.0f green:145/255.0f blue:145/255.0f alpha:1.0]];

        }else{
            cell.backgroundColor=[UIColor  whiteColor];
            
            cell.userInteractionEnabled = YES;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
        }
        
        UIFont *font = [UIFont boldSystemFontOfSize:15.0f];
       // NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
          //                                                     forKey:NSFontAttributeName];
        
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont boldSystemFontOfSize:15], NSFontAttributeName,
                                    [UIColor blackColor], NSForegroundColorAttributeName,
                                    nil];
      //  [_segmentedControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
      //  NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
       // [ setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];

     
        
        
        
        [cell.testtype_segment setTitleTextAttributes:attributes
                                             forState:UIControlStateNormal];
            
        
            [ cell.testtype_segment addTarget:self action:@selector(segmentedControlValueChangedcharge_1:) forControlEvents:UIControlEventValueChanged];
            //    cell.chargetype1.selectedSegmentIndex = UISegmentedControlNoSegment;
            
            // self.savebtn.enabled = YES;
            
            //addTarget:self action:@selector(segmentedControlValueChanged2:) forControlEvents:UIControlEventValueChanged];
            //  cell.chargetype2.tag=102;
            //
            //   cell.chargetype1.selectedSegmentIndex=3;
            
            cell.accessoryType=UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.testtype_segment.apportionsSegmentWidthsByContent=YES;
//       cell.testtype_segment.setWidth(150, forSegmentAt: 0)
//           cell.testtype_segment.setWidth(150, forSegmentAt: 1)
            if([newWOlist[4][@"Id"] isEqualToString:@"FieldLab"])
            {
                cell.testtype_segment.selectedSegmentIndex=0;
                
            }else
            {
                cell.testtype_segment.selectedSegmentIndex=1;
                
            }
 
            
            return  cell;
        
        
    }
//        RadioTableViewCell *cell =
//        [tableView dequeueReusableCellWithIdentifier:@"radiocell"];
//
//        if (cell == nil) {
//            cell =
//            [[RadioTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                      reuseIdentifier:@"radiocell"];
//        }
//
//        if(indexPath.row==4)
//        {
//        [cell.button2 setTitle:@"Specialty / Admin Work"
//                      forState:UIControlStateNormal];
//        [cell.button1 setTitle:@"Sample Pickup, Field, Lab or Inspection Test"
//                      forState:UIControlStateNormal];
//
//
//                if([newWOlist[indexPath.row][@"Id"] isEqualToString:@"SpecialtyAdmin"])
//                {
//                    [cell.button2 setSelected:YES];
//                    [cell.button1 setSelected:NO];
//
//                }else{
//
//        [cell.button1 setSelected:YES];
//            [cell.button2 setSelected:NO];
//
//        }
//              cell.button1.tag = 101;
//              cell.button2.tag = 102;
//            [cell.button1 addTarget:self
//                             action:@selector(logSelectedButton:)
//                   forControlEvents:UIControlEventTouchUpInside];
//            [cell.button2 addTarget:self
//                             action:@selector(logSelectedButton:)
//                   forControlEvents:UIControlEventTouchUpInside];
//
//        }
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.accessoryType = UITableViewCellAccessoryNone;
//
//        return cell;
//    }
}
- (void)segmentedControlValueChangedcharge_1:(UISegmentedControl *)control
{
    
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
 NSIndexPath  *index_Path= [NSIndexPath indexPathForRow:4 inSection:0];
    //[NSIndexPath indexPathWithIndex:8];
    
    Testtype_category *cell = (Testtype_category *)[self.tbl_form  cellForRowAtIndexPath:index_Path];
   
    UISegmentedControl *chargetype3 = (UISegmentedControl*)[cell viewWithTag:101]; // you can
    
    NSString *title = [chargetype3 titleForSegmentAtIndex:chargetype3.selectedSegmentIndex];
    NSLog(@"Segment value changed1!,title%@",title);
    
    [[NSUserDefaults standardUserDefaults]setObject:title forKey:@"next_location"];
    NSLog(@"next_location%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"next_location"]);
    
    [chargetype3 setMomentary:NO];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]
                                initWithDictionary:newWOlist[4]];
    if(chargetype3.selectedSegmentIndex==0)
    {
        [dic setObject:@"FieldLab"
                forKey:@"Id"];
       
    }else
         {
            [dic setObject:@"SpecialtyAdmin"
                    forKey:@"Id"];
        
        }
    
    [self changevalue:5 ];
    
    [newWOlist replaceObjectAtIndex:4 withObject:dic];
    [_tbl_form reloadData];
    
    
    
    
    
    
}








- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath {

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // _lblAnimal.text = [animals objectAtIndex:indexPath.row];
 
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
    rowOfTheCell=indexPath.row;

    selectedindex=indexPath.row;
  if([newWOlist[indexPath.row][@"DataType"] isEqualToString:@"date"])
    {
        DateAndTimeTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        [self showDatePicker:@"date" currentLbl:cell.lblValue];
    }else
        if([newWOlist[indexPath.row][@"DataType"] isEqualToString:@"dateandtime"])
        {
            DateAndTimeTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            [self showDatePicker:@"dateandtime" currentLbl:cell.lblValue];
        }else
    if(selectedindex==2)
    {
        NSLog(@"newWOlist%@",newWOlist);
        if([[newWOlist[1][@"Id"]description] length]>0)
        {
            if([_Isedit isEqualToString:@"true"])
            {}else{
      //  [self GetProjectListForClient];
            [self performSegueWithIdentifier:@"multiselect_segue" sender:self];
            }
        }else{
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Select Client" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            // callAlert.tag = 56;
            [callAlert show];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
    }
    else  if(selectedindex==5)
    {
        if(![[newWOlist[1][@"CssClientId"] description] length]>0)
        {
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Select Client" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
        
        else if(![[newWOlist[2][@"Id"]description]  length]>0)
        {
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Select Project / Sub Project" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
        else{
           // [self performSegueWithIdentifier:@"multiselect_segue" sender:self];
            isautoridirect=true;
    [self GetTestTypeListForProject];
        }
    }
    else  if(selectedindex==8)
    {
        [self GetSampleType];
    }
    else  if(selectedindex==9)
    {
        if([[newWOlist[1][@"Id"]description] length]>0)
        {
  [self GetLabTechForApp];        }else{
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Select Client" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            // callAlert.tag = 56;
            [callAlert show];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }
      
    }
    else  if(selectedindex==10)
    {
        
        if([[newWOlist[1][@"Id"]description]  length]>0)
        {
            [self GetFieldTechForApp];      }else{
                UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Select Client" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                // callAlert.tag = 56;
                [callAlert show];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            }
      
    }
    
//    if(selectedindex==16)
//    {
//
//
//            if(![[newWOlist[1][@"CssClientId"] description] length]>0)
//            {
//                UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Select Client" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//                [callAlert show];
//                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//            }
//
//            else if(![[newWOlist[2][@"Id"]description]  length]>0)
//            {
//                UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efield Message" message:@"Select Project / Sub Project" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//                [callAlert show];
//                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//            }
//           else
//            {
//                //[self GetTestTypeListForProject];
//                [self performSegueWithIdentifier:@"multiselect_segue" sender:self];
//            }
//
//    }
    if(selectedindex==1
       ||selectedindex==7||selectedindex==12){
        if(selectedindex==1
          ){
            if([_Isedit isEqualToString:@"true"])
            {}else{
                [self performSegueWithIdentifier:@"multiselect_segue" sender:self];

            }
        }else{
            
        [self performSegueWithIdentifier:@"multiselect_segue" sender:self];
        }
    }
    
}
-(void) showDatePicker: (NSString *) dateformat currentLbl:(UILabel *)lbl
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:UIDatePickerModeDate];
    [alertController.view addSubview:picker];
 
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd/yyyy"];
        NSString *dateanswerstr=newWOlist[rowOfTheCell][@"display_Value"];
        if([dateanswerstr isEqualToString:@""]||[dateanswerstr isKindOfClass:[NSNull class]]){}else{
            NSData *date=[formatter dateFromString:dateanswerstr];
            [picker setDate:date];}
 
    
    UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
                                     self.savebtn.enabled = YES;
                                     
                                      NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                                         NSString *datestring= [formatter stringFromDate:picker.date];
                                         
                                       //  lbl.text=datestring;
                                         
                                         NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:newWOlist[lbl.tag]];
                                         [dic setObject:datestring forKey:@"display_Value"];
                                      
                                         NSLog(@"dic : %@",dic);
                                         [newWOlist replaceObjectAtIndex:lbl.tag withObject:dic];
                                  
                                if([dateformat isEqualToString:@"dateandtime"])
                                {
                                    
                                    [self show_timePicker:UIDatePickerModeTime currentLbl:lbl];
                                    
                                }
                                else{
                                    lbl.text=datestring;
                                }
                                     
                                 }];
    [alertController addAction:doneAction];
    
    
    UIAlertAction *clearAction = [UIAlertAction actionWithTitle:@"Clear" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                  {
                                     
                                      [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
                                      self.savebtn.enabled = YES;
                                          NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:newWOlist[lbl.tag]];
                                          [dic setObject:@"" forKey:@"display_Value"];
                                        
                                          [newWOlist replaceObjectAtIndex:lbl.tag withObject:dic];
                                          
                                     
                                      NSLog(@"Clear action");
                                      lbl.text=@"";
                                  }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    [alertController addAction:cancelAction];
    
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController = alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController  animated:YES completion:nil]; }


- (NSDate *)roundToNearestQuarterHour:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |
    NSCalendarUnitDay | NSCalendarUnitHour |
    NSCalendarUnitMinute | NSCalendarUnitWeekday |
    NSCalendarUnitWeekdayOrdinal | NSCalendarUnitWeekOfYear;
    NSDateComponents *components = [calendar components:unitFlags fromDate:date];
    NSInteger roundedToQuarterHour = round((components.minute / 15.0)) * 15;
    components.minute = roundedToQuarterHour;
    return [calendar dateFromComponents:components];
}
- (void)show_timePicker:(UIDatePickerMode)modeDatePicker
                  currentLbl:(UILabel *)lbl{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:UIDatePickerModeTime];
    [alertController.view addSubview:picker];
    picker.minuteInterval = 15;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
   // NSLog(@"starttime%@", _start_timelbl.text);
    NSString *dateanswerstr=newWOlist[rowOfTheCell][@"Id"];

    if ([dateanswerstr length] >0) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm a"];
 
        picker.date = [formatter dateFromString:dateanswerstr];
        
    } else {
        picker.date = [self roundToNearestQuarterHour:[NSDate date]];
    }
    
    UIAlertAction *doneAction = [UIAlertAction
                                 actionWithTitle:@"Done"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action) {
                                     [_savebtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                                                 green:110 / 255.0f
                                                                                  blue:255 / 255.0f
                                                                                 alpha:1.0]
                                                        forState:UIControlStateNormal];
                               _savebtn.enabled = YES;
                                     
                                     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                     [formatter setDateFormat:@"hh:mm a"];
                                     picker.minuteInterval = 15;
                                     NSString *datestring = [formatter stringFromDate:picker.date];
                                     lbl.text=[NSString stringWithFormat:@"%@ %@",newWOlist[rowOfTheCell][@"display_Value"],datestring];
                                     
                                     
                                     NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:newWOlist[lbl.tag]];
                                     [dic setObject:  datestring forKey:@"Id"];
                                     
                                     NSLog(@"dic : %@",dic);
                                     [newWOlist replaceObjectAtIndex:lbl.tag withObject:dic];
  
                                 }];
    [alertController addAction:doneAction];
    
    UIAlertAction *clearAction =
    [UIAlertAction actionWithTitle:@"Clear"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action) {
                               
                               [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
                               self.savebtn.enabled = YES;
                               lbl.text=@"";
                               
                               NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:newWOlist[lbl.tag]];
                               [dic setObject:@"" forKey:@"Id"];
                               
                               [newWOlist replaceObjectAtIndex:lbl.tag withObject:dic];
                               
                           }];
    
    UIAlertAction *cancelAction =
    [UIAlertAction actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction *action) {
                               NSLog(@"Cancel action");
                           }];
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController =
    alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect =
    CGRectMake(self.view.bounds.size.width / 2.0,
               self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController animated:YES completion:nil];
}
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
     if ([segue.identifier isEqualToString:@"alertsegue"]) {
         
         AlertListVC *vc = [segue destinationViewController];
         
     }else
    if ([segue.identifier isEqualToString:@"multiselect_segue"]) {

 multiselectViewController *vc= [segue destinationViewController];
    
    vc.delegate = self;
    vc.New_WOList=newWOlist;
    vc.selected_index=selectedindex;

     switch (selectedindex) {
        case 1:
            vc.arraylist= ClientList;
            vc.selected_index_multi=@"1";
             vc.header_txt=@"Client";
            break;
        case 2:
            vc.arraylist=ProjectList;
            vc.selected_index_multi=@"2";
             vc.header_txt=@"Project / Sub Project";

            break;
        case 5:
            vc.arraylist=TestTypeList;
            vc.selected_index_multi=@"5";
             vc.header_txt=@"Test Type";

            break;
         case 8:
            vc.arraylist=SampleTypeList;
            vc.selected_index_multi=@"8";
             vc.header_txt=@"Sample Type";
            break;
            
        case 9:
            vc.arraylist=LabTechList;
            vc.selected_index_multi=@"9";

             vc.header_txt=@"Assigned Lab Tech To";
            break;
        case 10:
            vc.arraylist=FieldTechList;
            vc.selected_index_multi=@"10";
            
             vc.header_txt=@"Assigned Field Tech To";
            break;
        case 7:
             vc.selected_index_multi=@"7";
             vc.header_txt=@"Type (Building / Site)";
            
            break;
        case 12:
             vc.selected_index_multi=@"12";
             vc.header_txt=@"Work Order Status";
            break;
        
             
        default:
            break;
    }
    }
  
}
- (NSMutableAttributedString *)AddRequiredField:(NSString *)title
                                  andIsRequired:(BOOL)required {
    
    if (required) {
        NSString *string = [NSString stringWithFormat:@"%@ *", title];
        NSMutableAttributedString *attString =
        [[NSMutableAttributedString alloc] initWithString:string];
        NSRange range = [string rangeOfString:@"*"];
        [attString addAttribute:NSForegroundColorAttributeName
                          value:[UIColor redColor]
                          range:range];
        return attString;
    } else {
        NSMutableAttributedString *attString =
        [[NSMutableAttributedString alloc] initWithString:title];
        return attString;
    }
}

- (NSMutableAttributedString *)AddcategoryRequiredField:(NSString *)title
                                  andIsRequired:(BOOL)required {
    
    if (required) {
        NSString *string = [NSString stringWithFormat:@"%@", title];
        NSMutableAttributedString *attString =
        [[NSMutableAttributedString alloc] initWithString:string];
        NSRange range = [string rangeOfString:@"*"];
        [attString addAttribute:NSForegroundColorAttributeName
                          value:[UIColor redColor]
                          range:range];
        return attString;
    } else {
        NSMutableAttributedString *attString =
        [[NSMutableAttributedString alloc] initWithString:title];
        return attString;
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)iRange replacementString:(NSString *)text

{
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
if(textField.tag==16){
    if (text.length) {
        if (textField.text.length<=11) {
            if (textField.text.length==3) {
                NSString *tempStr=[NSString stringWithFormat:@"%@-",textField.text];
                textField.text=tempStr;
            } else if (textField.text.length==7) {
                NSString *tempStr=[NSString stringWithFormat:@"%@-",textField.text];
                textField.text=tempStr;
            }
        } else {
            [textField resignFirstResponder];
            return NO;
        }
    }
}
       return YES;
}


-(void) ready_checkbutton_Clicked:(UIButton*)sender
{
    
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
    if (sender.selected)
    {
        [sender setSelected:NO];
    }
    else
    {
        [sender setSelected:YES];
    }
    UIButton *someButton = (UIButton*)sender;
    
    if(sender.tag==105)
    {
        if([newWOlist[22][@"display_Value"] isEqualToString:@"true"])
        {
            NSMutableDictionary *dic =
            [[NSMutableDictionary alloc] initWithDictionary: newWOlist[22]];
            
            [dic setObject:@"false" forKey:@"display_Value"];
            [dic setObject:@"" forKey:@"Id"];
            
         
            
            
            [newWOlist replaceObjectAtIndex:22  withObject:dic];
            
        }
        else{
            NSMutableDictionary *dic =
            [[NSMutableDictionary alloc] initWithDictionary: newWOlist[22]];
            
            [dic setObject:@"true" forKey:@"display_Value"];
            [dic setObject:@"" forKey:@"Id"];
            
 
            
            
            [newWOlist replaceObjectAtIndex:22  withObject:dic];
            
        }
    }
}



-(void) checkbutton_Clicked:(UIButton*)sender
{
    
    [self.savebtn setTitleColor:[UIColor  colorWithRed:20/255.0f green:110/255.0f blue:255/255.0f alpha:1.0] forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
    if (sender.selected)
    {
        [sender setSelected:NO];
    }
    else
    {
        [sender setSelected:YES];
    }
    UIButton *someButton = (UIButton*)sender;

    if(sender.tag==103)
    {
    if([newWOlist[6][@"display_Value"] isEqualToString:[someButton titleForState:UIControlStateNormal]])
    {
        NSMutableDictionary *dic =
        [[NSMutableDictionary alloc] initWithDictionary: newWOlist[6]];
        
        [dic setObject:@"" forKey:@"display_Value"];
        [dic setObject:@"" forKey:@"Id"];
        
     
        [newWOlist replaceObjectAtIndex:6  withObject:dic];
        
    }
    else{
        NSMutableDictionary *dic =
        [[NSMutableDictionary alloc] initWithDictionary: newWOlist[6]];
        
        [dic setObject:[someButton titleForState:UIControlStateNormal] forKey:@"display_Value"];
        [dic setObject:@"" forKey:@"Id"];
       
        
        [newWOlist replaceObjectAtIndex:6  withObject:dic];
        
    }
    }
    if(sender.tag==104)
    {
    if([newWOlist[6][@"display_Name"] isEqualToString:[someButton titleForState:UIControlStateNormal]])
    {
        NSMutableDictionary *dic =
        [[NSMutableDictionary alloc] initWithDictionary: newWOlist[6]];
        
        [dic setObject:@"" forKey:@"display_Name"];
        [dic setObject:@"" forKey:@"Id"];
       
        
        
        [newWOlist replaceObjectAtIndex:6  withObject:dic];
      
    }
else{
        NSMutableDictionary *dic =
        [[NSMutableDictionary alloc] initWithDictionary: newWOlist[6]];
        
        [dic setObject:[someButton titleForState:UIControlStateNormal] forKey:@"display_Name"];
        [dic setObject:@"" forKey:@"Id"];
        
    
        
        [newWOlist replaceObjectAtIndex:6  withObject:dic];
        
    }
    }
 }

- (IBAction)saveAction:(id)sender {

    [self save_operation];
}

- (IBAction)cancelAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)changevalue:(NSInteger)indexvalue{
    NSMutableDictionary *dic1 =
    [[NSMutableDictionary alloc] initWithDictionary: newWOlist[indexvalue]];
    
    [dic1 setObject:@"" forKey:@"display_Value"];
    [dic1 setObject:@"" forKey:@"Id"];
    [newWOlist replaceObjectAtIndex:indexvalue  withObject:dic1];
}

-(void)requrefield_changevalue:(NSInteger)indexvalue{
    NSMutableDictionary *dic1 =
    [[NSMutableDictionary alloc] initWithDictionary: newWOlist[indexvalue]];
    if([[[newWOlist valueForKey:@"display_Value"] objectAtIndex:5]isEqualToString:@"Sample Pick Up"])
    {
        
    [dic1 setObject:@"true" forKey:@"IsRequired"];
    }else{
        [dic1 setObject:@"false" forKey:@"IsRequired"];

    }
    [newWOlist replaceObjectAtIndex:indexvalue  withObject:dic1];
}

- (void)NewWOinfo_detailsinfo:(NSString *)display_Value
                          ID:(NSString *)Id
                  CSSClientId:(NSString *)CssClientId
                      index_number:(NSInteger)indexvalue {
    NSMutableDictionary *dic =
    [[NSMutableDictionary alloc] initWithDictionary: newWOlist[indexvalue]];
    
    [dic setObject:display_Value forKey:@"display_Value"];
    [dic setObject:Id forKey:@"Id"];
if(indexvalue == 1)
{
    [dic setObject:CssClientId forKey:@"CssClientId"];
    [newWOlist replaceObjectAtIndex:indexvalue  withObject:dic];

    if([[newWOlist[1][@"Id"]description] length]>0)
    {
         [self GetProjectListForClient];
    }
    
    [self changevalue:2];
    [self changevalue:5 ];
    [self changevalue:15 ];
    [self changevalue:10 ];
    [self changevalue:9 ];
 }
else{
    [newWOlist replaceObjectAtIndex:indexvalue  withObject:dic];

}
    if(indexvalue == 2)
    {
       [self changevalue:5 ];
        [self changevalue:15 ];
    if([[newWOlist[2][@"Id"]description]length]>0&&[[newWOlist[1][@"CssClientId"]description]length]>0){
        isautoridirect=false;
            [self GetTestTypeListForProject];
        }
        
    }
    if(indexvalue == 5)
    {
   //  if([[[newWOlist valueForKey:@"display_Value"] objectAtIndex:5]isEqualToString:@"Sample Pick Up"])
     //{
         [self changevalue:9 ];
         [self changevalue:8 ];
         [self requrefield_changevalue:9 ];
           [self requrefield_changevalue:8 ];
         
    // }
        
        }
    [_tbl_form reloadData];
}

-(void) save_operation
{
    NSString *contact_no =[[newWOlist valueForKey:@"display_Value"] objectAtIndex:16];
    contact_no = [contact_no stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSString *clientId=[[newWOlist valueForKey:@"Id"] objectAtIndex:1];
    NSString *ProjectId=[[newWOlist valueForKey:@"Id"] objectAtIndex:2];
    NSString *ProjectTestTypeId=[[newWOlist valueForKey:@"Id"] objectAtIndex:5];
    NSString *AssignName=[[newWOlist valueForKey:@"Id"] objectAtIndex:10];
    NSString *LabTechName=[[newWOlist valueForKey:@"Id"] objectAtIndex:9];
    NSString *ProjectInfo=[[newWOlist valueForKey:@"display_Value"] objectAtIndex:17];
    NSString *Contractor=[[newWOlist valueForKey:@"display_Value"] objectAtIndex:14];
 
    NSString *site_contact=[[newWOlist valueForKey:@"display_Value"] objectAtIndex:15];
    NSString *DateCalledIn=[[newWOlist valueForKey:@"display_Value"] objectAtIndex:11];
    NSString *DateCalledInText=[[newWOlist valueForKey:@"display_Value"] objectAtIndex:11];

    NSString *JobDateText=[[newWOlist valueForKey:@"display_Value"] objectAtIndex:13];
    NSString *JobDateTime=[NSString stringWithFormat:@"%@ %@",[[newWOlist valueForKey:@"display_Value"] objectAtIndex:13],[[newWOlist valueForKey:@"Id"] objectAtIndex:13]];
    NSString *JobDateTimeText=[NSString stringWithFormat:@"%@ %@",[[newWOlist valueForKey:@"display_Value"] objectAtIndex:13],[[newWOlist valueForKey:@"Id"] objectAtIndex:13]];
    NSString *SiteBuilding=@"";

    if([[[newWOlist valueForKey:@"display_Value"] objectAtIndex:7]isEqualToString:@"Site"])
    {
        SiteBuilding=@"2";
    }
    else  if([[[newWOlist valueForKey:@"display_Value"] objectAtIndex:7]isEqualToString:@"Building"])
    {
        SiteBuilding=@"1";
    }
    
    NSString * Status=@"";
    if([[[newWOlist valueForKey:@"display_Value"] objectAtIndex:12]isEqualToString:@"Assigned"])
    {
        Status=@"N";
    }
    else  if([[[newWOlist valueForKey:@"display_Value"] objectAtIndex:12]isEqualToString:@"Cancelled at office"])
    {
        Status=@"E";
    }
      NSString *IsSpecialityInspection=@"";
    NSString *IsNotBillable=@"";
   // newWOlist[5][@"display_Value"]
    if([[[newWOlist valueForKey:@"display_Value"] objectAtIndex:6]length]>0)
    {
        IsNotBillable=@"true";
    }
    else    {
        IsNotBillable=@"false";
    }
    if([[[newWOlist valueForKey:@"display_Name"] objectAtIndex:6]length]>0)
    {
        IsSpecialityInspection=@"true";
    }
    else    {
        IsSpecialityInspection=@"false";
    }
    NSString *wo_id,*job_no;
    if([_Isedit isEqualToString:@"true"])
    {
        wo_id=_WorkOrderId;
        job_no=_JobNumber;
        
    }
    else{
        wo_id=@"0";
        job_no=@"";
    }
    
    if([contact_no length]!=12&&[contact_no length]>=1){
        [self showAlertWithTitle:@"Efielddata Message" message:@"Invalid Contact phone number"];
    }
   else if([[[newWOlist valueForKey:@"display_Value"] objectAtIndex:5]isEqualToString:@"Sample Pick Up"]&&
           [[[[newWOlist valueForKey:@"Id"] objectAtIndex:8]description]length]==0){
        [self showAlertWithTitle:@"Efielddata Message" message:@"Please select type of sample test to be done and assign lab technician."];
    }
   else if([[[newWOlist valueForKey:@"display_Value"] objectAtIndex:5]isEqualToString:@"Sample Pick Up"]&&
           [LabTechName length]==0){
       [self showAlertWithTitle:@"Efielddata Message" message:@"Please select type of sample test to be done and assign lab technician."];
   }
    else
        if([clientId length]==0||[ProjectId length]==0||[ProjectTestTypeId length]==0
          ||[AssignName length]==0
          ||[Status length]==0
          ||[DateCalledIn length]==0
             ||[JobDateText length]==0
           )
        {
            UIAlertView *callAlert = [[UIAlertView alloc]initWithTitle:@"Efielddata Message" message:@"Please fill required fields" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [callAlert show];
 
        }
        else{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

            NSDictionary *json =   @{
                @"AssignName": AssignName,
                @"ClientId": clientId,
                @"Contact": site_contact,
                @"Contractor": Contractor,
                @"DateCalledIn": DateCalledIn,
                @"DateCalledInText":DateCalledInText,
                @"EstimatedDrivingTime":   [[newWOlist valueForKey:@"display_Value"] objectAtIndex:20] ,
                @"Instructions": [[newWOlist valueForKey:@"display_Value"] objectAtIndex:19],
                @"IsReady": _isreadystr,
                @"IsSpecialityInspection": IsSpecialityInspection,
                @"IsNotBillable":IsNotBillable,
                @"JobDate":[[newWOlist valueForKey:@"display_Value"] objectAtIndex:14],
                @"JobDateText": JobDateText,
                @"JobDateTime": JobDateTime,
                @"JobDateTimeText":JobDateTimeText,
                @"JobTime":[[newWOlist valueForKey:@"Id"] objectAtIndex:13],
                @"JobTimeText": [[newWOlist valueForKey:@"Id"] objectAtIndex:13],
                @"LabTechName": LabTechName,
                @"Phone":contact_no,
                @"ProjectId":ProjectId,
                @"ProjectInfo": ProjectInfo,
                @"ProjectTestTypeId":ProjectTestTypeId  ,
                @"SampleId": [[newWOlist valueForKey:@"Id"] objectAtIndex:8],
                @"SampleIds": [[newWOlist valueForKey:@"Id"] objectAtIndex:8],
                @"SiteBuilding": SiteBuilding,
                @"Status": Status,
                @"WorkOrderId": wo_id,
                @"JobNumber":job_no,
                @"UserName":[[NSUserDefaults standardUserDefaults] objectForKey:username]
                };
            NSLog(@"json%@",json);
    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    [downloadObj callServerWithURL:[NSString stringWithFormat:@"%@AddNewWorkOrderForApp",[[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL]] andParameter:json andMethod:@"POST" andDelegate:self andKey:@"AddNewWorkOrderForApp"];
        }
}
    
- (IBAction)isready:(id)sender {
        [self.savebtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                    green:110 / 255.0f
                                                     blue:255 / 255.0f
                                                    alpha:1.0]
                           forState:UIControlStateNormal];
        self.savebtn.enabled = YES;
        UIButton *btn = (UIButton *)sender;
        if ([sender isOn]) {
            _isreadystr = @"true";
            
        } else {
            _isreadystr = @"false";
            
        }
    }

@end
