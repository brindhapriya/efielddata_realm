//
//  DynamicNoteFormVC.h
//  Efield
//
//  Created by Praveen Kumar on 12/02/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextViewTableViewCell.h"
#import "TextFieldTableViewCell.h"
#import "SwitchTableViewCell.h"
#import "SingleSelectionTableViewCell.h"
#import "MultySelectionTableViewCell.h"
#import "SelectionViewController.h"
#import "DateAndTimeTableViewCell.h"
#import "DescriptionTableViewCell.h"
#import "SliderTableViewCell.h"
#import "ViewController.h"

@interface AddWO_FormVC : ViewController
{
   

}
@property(nonatomic, retain)NSMutableArray *arraylist;

@property (weak, nonatomic) IBOutlet UILabel *companynametxt;

@property (nonatomic, retain)NSString *Isedit,*WorkOrderId,*JobNumber,*isreadystr;

- (IBAction)logoutAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *alertcnt;
@property (weak, nonatomic) IBOutlet UILabel *date;
 @property (weak, nonatomic) IBOutlet UIButton *savebtn;
- (IBAction)saveAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tbl_form;
- (IBAction)logoutAction:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *header_lbl;
- (IBAction)alertaction:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *readyswitch;

- (IBAction)isready:(id)sender;
@end
