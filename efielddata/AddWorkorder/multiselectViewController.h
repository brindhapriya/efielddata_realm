//
//  multiselectViewController.h
//  Efield
//
//  Created by MyMac1 on 2/7/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "ViewController.h"


@protocol AddWO_FormVCDelegate <NSObject>

@required
- (void)NewWOinfo_detailsinfo:(NSString *)display_Value
                           ID:(NSString *)Id
                  CSSClientId:(NSString *)CssClientId
                        index_number:(NSInteger)indexvalue;
@end


@interface multiselectViewController : ViewController<UITableViewDelegate, UITableViewDataSource ,UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>

{
    
    NSMutableArray *arrayListTemp;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
 @property(nonatomic, retain)NSMutableArray *arraylist;
@property(nonatomic, retain)NSArray *New_WOList;

@property(nonatomic, retain)NSString *multiselect,*pre_selected_value;
@property (weak, nonatomic) IBOutlet UIButton *savebtn;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(nonatomic, retain)NSString *header_txt;
@property(nonatomic, retain)NSString *selected_index_multi;
@property(nonatomic,assign) NSInteger selected_index;

@property (weak, nonatomic) IBOutlet UILabel *headerlbl;
- (IBAction)save:(id)sender;
- (IBAction)cancel:(id)sender;

 @property (nonatomic, weak) id<AddWO_FormVCDelegate> delegate;

@end
