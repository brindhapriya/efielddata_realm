
//  Efield
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WO_Listcell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *job_number;
@property (weak, nonatomic) IBOutlet UILabel *WO_name;
@property (weak, nonatomic) IBOutlet UILabel *Task_desc;
@property (weak, nonatomic) IBOutlet UILabel *Job_Datetime;
@property (weak, nonatomic) IBOutlet UILabel *assigened_name;
@property (weak, nonatomic) IBOutlet UILabel *Project_name;
@property (weak, nonatomic) IBOutlet UIImageView *bell_image;


@end
