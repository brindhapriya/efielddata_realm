//
//  AlertListVC.m
//  Efielddata
//
//  Created by iPhone on 27/11/15.
//  Copyright (c) 2015 iPhone. All rights reserved.
//

#import "AlertListVC.h"
#import "Constants.h"
#import "DynamicNoteFormVC.h"
#import "NoRecordsTableViewCell.h"
#import "TestTypeselectVC.h"
#import "WO_Listcell.h"
#import "Work_orderListVC.h"

#import "AddWO_FormVC.h"
//#import "UITabBarItem+CustomBadge.h"
@interface Work_orderListVC () <UITextFieldDelegate> {
    NSString *isOnEditing;
    NSString *cancellworkorderIDstr;
    BOOL isMarkAll;
    NSIndexPath *COPYindexPath;
    __weak IBOutlet UILabel *titleLbl;
    __weak IBOutlet UITableView *alertSearchTable;
    NSMutableArray *searchResultArray;
    NSMutableArray *searchFullResultArray;
    NSMutableArray *newWO_list;
    NSInteger selectedRowIndex;
    int selectedUtilityIndex;
    NSString *places;
    NSString *IsChange, *TestTypeId,*isready;
    NSString *TaskName, *JobNumber, *ProjectName;
    NSString *loadingmsg, *wono;
    NSString *datestring;
}
@end

@implementation Work_orderListVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    wono = @"";
    datestring = @"";
    loadingmsg = @"Loading...";
    self.alertcnt.layer.masksToBounds = YES;
    self.alertcnt.layer.cornerRadius = 8.0;
    self.cloasebtn.hidden = true;
    // [self updateALERTcount];
    //    [[NSUserDefaults standardUserDefaults]setObject:@""
    //    forKey:@"searchText"];
    //    [[NSUserDefaults standardUserDefaults]setObject:@""
    //    forKey:@"searchText_alert"];
    //
    //    [[NSUserDefaults standardUserDefaults]setObject:@""
    //    forKey:@"patientlistflag"];
    _companynametxt.text =
    [[NSUserDefaults standardUserDefaults] objectForKey:companyname];
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];
    [currentDTFormatter setDateFormat:@"MMM dd YYYY"];
    
    NSString *eventDateStr = [currentDTFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@", eventDateStr);
    _date.text = eventDateStr;
    self.navigationController.navigationBar.hidden = YES;
    alertList = [[NSMutableArray alloc] init];
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor whiteColor];
    refreshControl.tintColor = [UIColor blackColor];
    
    [refreshControl addTarget:self
                       action:@selector(updateDatarefresh)
             forControlEvents:UIControlEventValueChanged];
    [WOTable addSubview:refreshControl];
    _alertcnt.text = [self updateALERTcount];
    // [self performSelectorInBackground:@selector(updateDeviceTokentoServer)
    // withObject:nil];
    self->jobdate.tag = 101;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 20)];
    self->jobdate.leftView = paddingView;
    self->jobdate.leftViewMode = UITextFieldViewModeAlways;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateData)
                                                 name:@"updateDataAlert"
                                               object:nil];
    [self->jobdate setDelegate:self];
    
    [self->jobdate addTarget:self
                      action:@selector(updateData1)
            forControlEvents:UIControlEventEditingDidEnd];
    
    [self->jobdate addTarget:self
                      action:@selector(enable_closebtn)
            forControlEvents:UIControlEventEditingChanged];
    //   [self->jobdate addTarget:self action:@selector(textFieldShouldClear)
    //   forControlEvents:UIControlEventEditingDidEnd];
    //[self->workorder addTarget:self action:@selector(updateData1)
    //forControlEvents:UIControlEventEditingChanged];
    
    //    [[NSUserDefaults standardUserDefaults]setObject:  @""
    //    forKey:@"predicateString"]; if( [[[NSUserDefaults standardUserDefaults]
    //    objectForKey:@"IsShowAddPatient"] isEqualToString:@"1"]){
    //        _addpatient_button.hidden=NO;
    //        _addpatient_button.userInteractionEnabled=YES;
    //    }else{
    //        _addpatient_button.hidden=YES;
    //        _addpatient_button.userInteractionEnabled=NO;
    //    }
    // Do any additional setup after loading the view.
    //}
    //       if([[[NSUserDefaults standardUserDefaults] objectForKey:userRole]
    //       isEqualToString:@"Admin"]){
    //
    //        [self->searchbar setImage:[UIImage imageNamed:@"patientlist"]
    //        forSearchBarIcon:UISearchBarIconBookmark
    //        state:UIControlStateNormal]; [self->searchbar setImage:[UIImage
    //        imageNamed:@"patientlist"] forSearchBarIcon:UISearchBarIconBookmark
    //        state:UIControlStateSelected];
    //        self->searchbar.showsBookmarkButton=YES;
    //    }
    //    else{
    //    self->searchbar.showsBookmarkButton=NO;}
    //
}
//
//- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar
//{
//
//    [self performSegueWithIdentifier:@"showlogfile" sender:nil];
//}

- (void)progress {
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
}
- (BOOL)textFieldShouldClear:(UITextField *)textField {
    // if we only try and resignFirstResponder on textField or searchBar,
    // the keyboard will not dissapear (at least not on iPad)!
    
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message"
                          message:
         @"No network, please check your internet connection"];
        
    } else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        
        NSDictionary *json = @{
                               
                               @"UserName" :
                                   [[NSUserDefaults standardUserDefaults] objectForKey:username],
                               
                               @"RoleName" :
                                   [[NSUserDefaults standardUserDefaults] objectForKey:userRole],
                               @"ReportStatus" : @"NW",
                               @"Status" : @"P",
                               @"Wono" : @"",
                               @"FromJobDateTime" : @""
                               
                               };
        NSMutableDictionary *dictEntry = [[NSMutableDictionary alloc] init];
        [dictEntry setObject:json
                      forKey:[NSString stringWithFormat:@"FilterWorkOrder"]];
        
        DownloadManager *downloadObj = [[DownloadManager alloc] init];
        //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        [downloadObj
         callServerWithURL:[NSString
                            stringWithFormat:@"%@GetDashboard",
                            [[NSUserDefaults
                              standardUserDefaults]
                             objectForKey:SERVERURL]]
         andParameter:dictEntry
         andMethod:@"POST"
         andDelegate:self
         andKey:@"GetDashboard"];
    }
    
    return YES;
}
//- (void)updateDeviceTokentoServer
//{
//
//    if (![self isNetworkAvailable]) {
//        [self showAlertno_network:@"Efielddata Message" message:@"No network,
//        please check your internet connection"]; return;
//    }else{
//        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]
//        objectForKey:kdeviceToken]);
//    DownloadManager *downloadObj = [[DownloadManager alloc]init];
//        NSDictionary *json ;
//        NSString *deviceoken=[[NSUserDefaults standardUserDefaults]
//        objectForKey:kdeviceToken]; if ([deviceoken isKindOfClass:[NSNull
//        class]]||[deviceoken isEqualToString:@"null"]||deviceoken==nil)
//        {
//            json = @{
//
//                     @"UserName" : [[NSUserDefaults standardUserDefaults]
//                     objectForKey:username],
//                     @"AppId" :@""
//
//                     };
//        }else
//        {
//            json = @{
//                     @"UserName" : [[NSUserDefaults standardUserDefaults]
//                     objectForKey:username],
//                     @"AppId" : deviceoken
//                     };
//               }
//
//        NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
//        [dictEntry setObject:json forKey:[NSString
//        stringWithFormat:@"UserModel"]];
//
//       // [downloadObj callServerWithURLAuthentication:[NSString
//       stringWithFormat:@"%@UpdateUserAppId?UserName=%@&AppId=%@",[[NSUserDefaults
//       standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults
//       standardUserDefaults] objectForKey:username],[[NSUserDefaults
//       standardUserDefaults] objectForKey:kdeviceToken]] andParameter:nil
//       andMethod:@"POST" andDelegate:self andKey:@"UpdateUserAppId"];
//     [downloadObj callServerWithURLAuthentication:[NSString
//     stringWithFormat:@"%@IOSUpdateUserAppId",[[NSUserDefaults
//     standardUserDefaults] objectForKey:SERVERURL]] andParameter:dictEntry
//     andMethod:@"POST" andDelegate:self andKey:@"UpdateUserAppId"];
//    }
//}
- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    //    if([[[NSUserDefaults standardUserDefaults]
    //    objectForKey:@"gotopatientlist"] isEqualToString:@"true"]){
    //
    //        [[NSUserDefaults standardUserDefaults]setObject:@"false"
    //        forKey:@"gotopatientlist"]; UINavigationController *nav
    //        =[[self.tabBarController childViewControllers] objectAtIndex:3];
    //
    //        PatientListVC *myController = (PatientListVC
    //        *)nav.topViewController; self.tabBarController.selectedIndex = 3;
    //    }
    //    else{
    //  _alertPatientID = nil;
    //}
}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[NSUserDefaults standardUserDefaults]
     setObject:@""
     forKey:@"Display_canceledJobDateText"];
    [[NSUserDefaults standardUserDefaults] setObject:@""
                                              forKey:@"DisplayJobDateText"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"datereplace"];
    
    _alertcnt.text = [self updateALERTcount];
    
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:isLoggedin];
    [[NSUserDefaults standardUserDefaults] setBool:true
                                            forKey:isPatientUpdatedNeed];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"pendingnext"]
         isEqualToString:@"false"]) {
        datestring = @"";
        wono = @"";
        
        self->jobdate.text = @"";
        
    } else if ([[[NSUserDefaults standardUserDefaults]
                 objectForKey:@"pendingnext"] isEqualToString:@"true"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"false"
                                                  forKey:@"pendingnext"];
    }
    [self updateData];
}
- (void)loadAlertData {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:details];
    NSDictionary *json = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    alertList =
    [[NSMutableArray alloc] initWithArray:[json valueForKeyPath:@"Data"]];
    tempArr = [[NSArray alloc] initWithArray:[json valueForKeyPath:@"Data"]];
    
    [WOTable reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//
//- (CGFloat)tableView:(UITableView *)tableView
//heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if ([alertList count] == 0) {
//        return 40;
//
//    } else {
//        //     if(![[[alertList valueForKey:@"StatusText"]
//        //     objectAtIndex:indexPath.row] isEqualToString:@"New"]){
//        //               return 205;
//        //     }else{
//
//        return 150;
//        //}
//    }
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   if ([alertList count] == 0) {
                return 40;
        
            } else {
           
                return 150;
              
            }
}

- (void)handlelocationtapFrom:(UITapGestureRecognizer *)recognizer {
    
    UIView *parentCell = recognizer.view.superview;
    
    while (![parentCell
             isKindOfClass:[UITableViewCell class]]) { // iOS 7 onwards the table cell
        // hierachy has changed.
        parentCell = parentCell.superview;
    }
    
    UIView *parentView = parentCell.superview;
    
    while (![parentView isKindOfClass:[UITableView class]]) {
        // iOS 7 onwards the table cell hierachy has changed.
        parentView = parentView.superview;
    }
    
    UITableView *tableView = (UITableView *)parentView;
    NSIndexPath *indexPath =
    [tableView indexPathForCell:(UITableViewCell *)parentCell];
    
    NSString *ProjectAddress = [[[alertList valueForKey:@"ProjectAddress"]
                                 objectAtIndex:indexPath.row] description];
    //
    
    ProjectAddress = [ProjectAddress stringByReplacingOccurrencesOfString:@" "
                                                               withString:@"+"];
    NSString *url = [NSString
                     stringWithFormat:@"https://www.google.com/maps/dir/"
                     @"Tierra+South+Florida,+Inc,+2765+Vista+Pkwy+%2310,+"
                     @"West+Palm+Beach,+FL+33411,+United+States/%@",
                     ProjectAddress];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)handleNameLblTapFrom:(UITapGestureRecognizer *)recognizer {
    
    UIView *parentCell = recognizer.view.superview;
    
    while (![parentCell
             isKindOfClass:[UITableViewCell class]]) { // iOS 7 onwards the table cell
        // hierachy has changed.
        parentCell = parentCell.superview;
    }
    
    UIView *parentView = parentCell.superview;
    
    while (![parentView isKindOfClass:[UITableView class]]) {
        // iOS 7 onwards the table cell hierachy has changed.
        parentView = parentView.superview;
    }
    
    UITableView *tableView = (UITableView *)parentView;
    NSIndexPath *indexPath =
    [tableView indexPathForCell:(UITableViewCell *)parentCell];
    
    NSLog(@"indexPath = %@", indexPath);
    
    NSString *phoneStr = [[[alertList valueForKey:@"Phone"]
                           objectAtIndex:indexPath.row] description];
    phoneStr = [phoneStr stringByReplacingOccurrencesOfString:@"-"
                                                   withString:@""];
    NSURL *phoneUrl =
    [NSURL URLWithString:[@"telprompt://" stringByAppendingString:phoneStr]];
    NSURL *phoneFallbackUrl =
    [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneStr]];
    
    if (![phoneStr isEqualToString:@""]) {
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:@"Call from Efielddata"
                                    message:@"Please click OK to call"
                                    preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction *action) {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 if ([UIApplication.sharedApplication
                                      canOpenURL:phoneFallbackUrl]) {
                                     [UIApplication.sharedApplication openURL:phoneFallbackUrl];
                                 } else if ([UIApplication.sharedApplication
                                             canOpenURL:phoneUrl]) {
                                     [UIApplication.sharedApplication openURL:phoneUrl];
                                 } else {
                                     [self showAlertWithTitle:@"Efielddata Message"
                                                      message:@"Your device not supported for "
                                      @"phone calls"];
                                 }
                             }];
        UIAlertAction *cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action) {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([alertList count] == 0) {
        NoRecordsTableViewCell *cell = (NoRecordsTableViewCell *)[tableView
                                                                  dequeueReusableCellWithIdentifier:noRecordsResuableIdentifier];
        cell.noRecordsLabel.text = loadingmsg;
        return cell;
        
    } else {
        NSString *cellIdentifier = @"cell";
        WO_Listcell *cell = (WO_Listcell *)[tableView
                                            dequeueReusableCellWithIdentifier:cellIdentifier];
        
        cell.job_number.text =
        [[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row];
        cell.Task_desc.text =
        [[alertList valueForKey:@"ProjectInfo"] objectAtIndex:indexPath.row];
        cell.Project_name.text = [[alertList valueForKey:@"ProjectFullName"]
                                  objectAtIndex:indexPath.row];
        cell.WO_name.text = [[alertList valueForKey:@"TaskDisplayName"]
                             objectAtIndex:indexPath.row];
        cell.Job_Datetime.text = [[alertList
                                   valueForKey:@"JobDateTimeDisplayUTCText"] objectAtIndex:indexPath.row];
        cell.assigened_name.text =
        [[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == WOTable) {
        return 1;
    } else {
        
        return [searchResultArray count];
    }
}
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if (tableView == WOTable) {
        NSLog(@"[alertList count] %lu", (unsigned long)[alertList count]);
        if ([alertList count] == 0) {
            return 1;
        } else {
            return [alertList count];
        }
    } else {
        if ([[[searchResultArray objectAtIndex:section] objectForKey:@"childObject"]
             count] == 0) {
            return 1;
        } else {
            return [[[searchResultArray objectAtIndex:section]
                     objectForKey:@"childObject"] count];
        }
    }
}
- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([alertList count] == 0) {
    } else {
        newWO_list = [[NSMutableArray alloc] init];
        NSString *datandtime =
        [[[alertList valueForKey:@"JobDateTimeDisplayUTCText"]
          objectAtIndex:indexPath.row] description];
        NSString *datestr = [[[alertList valueForKey:@"JobDateDisplayUTCText"]
                              objectAtIndex:indexPath.row] description];
        
        for (int i = 0; i < 21; i++) {
            NSString *display_Name, *display_Value, *Id, *category_Name,
            *category_Desc, *CssClientId, *datatype, *IsRequired;
            switch (i) {
                case 0:
                    
                    display_Name = @"Edit work order details";
                    display_Value = @"* indicates required fields";
                    Id = @"";
                    IsRequired = @"true";
                    datatype = @"category";
                    break;
                case 1:
                    display_Name = @"Client";
                    display_Value =
                    [[alertList valueForKey:@"ClientName"] objectAtIndex:indexPath.row];
                    Id = [[[alertList valueForKey:@"ClientId"] objectAtIndex:indexPath.row]
                          description];
                    CssClientId = [[[alertList valueForKey:@"CssClientId"]
                                    objectAtIndex:indexPath.row] description];
                    datatype = @"dropdown";
                    IsRequired = @"true";
                    
                    break; /* optional */
                case 2:
                    display_Name = @"Project / Sub Project";
                    display_Value = [[alertList valueForKey:@"ProjectFullName"]
                                     objectAtIndex:indexPath.row];
                    Id = [[[alertList valueForKey:@"ProjectId"] objectAtIndex:indexPath.row]
                          description];
                    datatype = @"dropdown";
                    IsRequired = @"true";
                    
                    break; /* optional */
                case 3:
                    display_Name = @"Test Type";
                    display_Value = @"Select lab category for Sample Pickup.";
                    Id = @"";
                    datatype = @"category";
                    IsRequired = @"false";
                    break; /* optional */
                    
                case 4:
                  
                    display_Name = @"";
                    display_Value = @"";
                    if([[[alertList valueForKey:@"WorkOrderType"]
                         objectAtIndex:indexPath.row] isEqualToString:@"Field"]||
                       [[[alertList valueForKey:@"WorkOrderType"]
                         objectAtIndex:indexPath.row] isEqualToString:@"Lab"]||
                       [[[alertList valueForKey:@"WorkOrderType"]
                         objectAtIndex:indexPath.row] isEqualToString:@"Inspection"])
                    {
                    Id = @"FieldLab";
                    }else{
                        Id = @"SpecialtyAdmin";
                    }
                    datatype = @"radio";
                    IsRequired = @"false";
                    
                    break; /* optional */
                case 5:
                    display_Name = @"Test Type";
                    display_Value = [[alertList valueForKey:@"TaskName"]
                                     objectAtIndex:indexPath.row];
                    Id = [[[alertList valueForKey:@"ProjectTestTypeId"]
                           objectAtIndex:indexPath.row] description];
                    datatype = @"dropdown";
                    IsRequired = @"true";
                    break; /* optional */
                case 6:
                    if ([[[[alertList valueForKey:@"IsNotBillable"]
                           objectAtIndex:indexPath.row] description]
                         isEqualToString:@"1"]) {
                        display_Value = @"Non Billable";
                    } else {
                        display_Value = @"";
                    }
                    if ([[[[alertList valueForKey:@"IsSpecialityInspection"]
                           objectAtIndex:indexPath.row] description]
                         isEqualToString:@"1"]) {
                        display_Name = @"Inspection 'Specialty Type'";
                    } else {
                        display_Name = @"";
                    }
                    
                    Id = @"";
                    datatype = @"checkbox";
                    IsRequired = @"false";
                    break; /* optional */
                case 7:  /* Optional */
                    
                    if ([[[[alertList valueForKey:@"SiteBuilding"]
                           objectAtIndex:indexPath.row] description]
                         isEqualToString:@"2"]) {
                        display_Value = @"Site";
                    } else {
                        display_Value = @"Building";
                    }
                    display_Name = @"Type (Building / Site)";
                    
                    Id = @"";
                    datatype = @"dropdown";
                    IsRequired = @"false";
                    break;
                    //
                case 8:
                    display_Name = @"Sample Type";
                    display_Value = [[alertList valueForKey:@"SampleNames"]
                                     objectAtIndex:indexPath.row];
                    Id = [[[alertList valueForKey:@"SampleIds"] objectAtIndex:indexPath.row]
                          description];
                    datatype = @"multiselect_dropdown";
                    if([[[alertList valueForKey:@"TaskName"]
                         objectAtIndex:indexPath.row]isEqualToString:@"Sample Pick Up"])
                    {
                        IsRequired = @"true";

                    }else{
                    IsRequired = @"false";
                    }
                    break; /* optional */
                case 9:
                    display_Name = @"Assigned Lab Tech To";
                    display_Value = [[alertList valueForKey:@"LabTechFullName"]
                                     objectAtIndex:indexPath.row];
                    Id = [[alertList valueForKey:@"LabTechName"]
                          objectAtIndex:indexPath.row];
                    datatype = @"dropdown";
                    if([[[alertList valueForKey:@"TaskName"]
                         objectAtIndex:indexPath.row]isEqualToString:@"Sample Pick Up"])
                    {
                        IsRequired = @"true";
                        
                    }else{
                        IsRequired = @"false";
                    }
                    break; /* optional */
                    
                case 10: /* Optional */
                    display_Name = @"Assigned Field Tech To";
                    display_Value = [[alertList valueForKey:@"FieldTechFullName"]
                                     objectAtIndex:indexPath.row];
                    Id =
                    [[alertList valueForKey:@"AssignName"] objectAtIndex:indexPath.row];
                    datatype = @"dropdown";
                    IsRequired = @"true";
                    break;
                case 11: /* Optional */
                    
                    display_Name = @"Date Called-in";
                    display_Value = [[alertList valueForKey:@"DateCalledInText"]
                                     objectAtIndex:indexPath.row];
                    Id = @"";
                    datatype = @"date";
                    IsRequired = @"true";
                    break;
                    break;
                case 12: /* Optional */
                    display_Name = @"Work Order Status";
                    if ([[[alertList valueForKey:@"Status"] objectAtIndex:indexPath.row]
                         isEqualToString:@"N"]) {
                        display_Value = @"Assigned";
                    } else {
                        display_Value = @"Cancelled at office";
                    }
                    Id = @"";
                    IsRequired = @"true";
                    
                    datatype = @"dropdown";
                    
                    break;
                    
                    break;
                case 13: /* Optional */
                    display_Name = @"Job Date & Time";
                    //
                    display_Value = datestr;
                    Id = [datandtime stringByReplacingOccurrencesOfString:datestr
                                                               withString:@""];
                    
                    datatype = @"dateandtime";
                    IsRequired = @"true";
                    break;
                    
                    break;
                    
                case 14: /* Optional */
                    display_Name = @"Contractor";
                    display_Value =
                    [[alertList valueForKey:@"Contractor"] objectAtIndex:indexPath.row];
                    Id = @"";
                    datatype = @"TextField";
                    IsRequired = @"false";
                    break;
                    break;
                case 15: /* Optional */
                    display_Name = @"Site Contact";
                    display_Value =
                    [[alertList valueForKey:@"Contact"] objectAtIndex:indexPath.row];
                    Id = @"";
                    datatype = @"TextField";
                    IsRequired = @"false";
                    break;
                    break;
                case 16: /* Optional */
                    display_Name = @"Contact Phone";
                    display_Value =
                    [[alertList valueForKey:@"Phone"] objectAtIndex:indexPath.row];
                    Id = @"";
                    datatype = @"TextField";
                    IsRequired = @"false";
                    break;
                    break;
                case 17: /* Optional */
                    display_Name = @"Task Description";
                    display_Value = [[alertList valueForKey:@"ProjectInfo"]
                                     objectAtIndex:indexPath.row];
                    Id = @"";
                    datatype = @"TextBox";
                    IsRequired = @"false";
                    break;
                case 18: /* Optional */
                    display_Name = @"Job Site Address";
                    // ProjectAddress
                    display_Value = [[alertList valueForKey:@"ProjectAddress"]
                                     objectAtIndex:indexPath.row];
                    Id = @"";
                    datatype = @"TextField";
                    IsRequired = @"false";
                    break;
                case 19: /* Optional */
                    display_Name = @"Driving Directions";
                    display_Value = [[alertList valueForKey:@"Instructions"]
                                     objectAtIndex:indexPath.row];
                    Id = @"";
                    datatype = @"TextBox";
                    IsRequired = @"false";
                    break;
                case 20:
                    display_Name = @"Estimated Driving Time";
                    display_Value = [[alertList valueForKey:@"EstimatedDrivingTime"]
                                     objectAtIndex:indexPath.row];
                    Id = @"";
                    datatype = @"TextField";
                    IsRequired = @"false";
                    break;
                    break;
                    //            case 21:
                    //                display_Name = @"Ready & Notify Technician Now";
                    //                display_Value = @"";
                    //                Id = @"";
                    //                datatype = @"checkbox_ready";
                    //                IsRequired = @"false";
                    //                break; /* optional */
                    
                default: /* Optional */
                    display_Name = @"Estimated Driving Time";
                    display_Value = @"";
                    Id = @"";
                    datatype = @"TextField";
                    IsRequired = @"false";
                    break;
            }
            //            switch (i) {
            //                case 0:
            //                    display_Name = @"Enter new work order details";
            //                    display_Value = @"* indicates required fields";
            //                    Id = @"";
            //                    IsRequired = @"true";
            //                    datatype = @"category";
            //                    break;
            //
            //                case 1:
            //                    display_Name = @"Client";
            //                    display_Value = [[alertList
            //                    valueForKey:@"ClientName"]
            //                    objectAtIndex:indexPath.row]; Id = [[[alertList
            //                    valueForKey:@"ClientId"]
            //                    objectAtIndex:indexPath.row]description];
            //                    CssClientId = [[[alertList
            //                    valueForKey:@"CssClientId"]
            //                    objectAtIndex:indexPath.row]description]; datatype =
            //                    @"dropdown"; IsRequired = @"true";
            //
            //                    break; /* optional */
            //                case 2:
            //                    display_Name = @"Project / Sub Project";
            //                    display_Value =[[alertList
            //                    valueForKey:@"ProjectFullName"]
            //                    objectAtIndex:indexPath.row]; Id = [[[alertList
            //                    valueForKey:@"ProjectId"]
            //                    objectAtIndex:indexPath.row]description]; datatype =
            //                    @"dropdown"; IsRequired = @"true";
            //
            //                    break; /* optional */
            //
            //                case 3:
            //                    display_Name = @"";
            //                    display_Value = @"";
            //                    Id = @"FieldLab";
            //                    datatype = @"radio";
            //                    IsRequired = @"false";
            //
            //                    break; /* optional */
            //                case 4:
            //                    //ProjectTestTypeId
            //                    //TaskDisplayName
            //                    display_Name = @"Test Type";
            //                    display_Value = [[alertList
            //                    valueForKey:@"TaskDisplayName"]
            //                    objectAtIndex:indexPath.row]; Id = [[[alertList
            //                    valueForKey:@"ProjectTestTypeId"]
            //                    objectAtIndex:indexPath.row]description]; datatype =
            //                    @"dropdown"; IsRequired = @"true"; break; /*
            //                    optional */
            //                case 5:
            //
            //                    if([[[[alertList valueForKey:@"IsNotBillable"]
            //                    objectAtIndex:indexPath.row]description]
            //                    isEqualToString:@"1"]) {display_Value =@"Non
            //                    Billable";} else{
            //                         display_Value = @"";
            //                    }
            //                    if([[[[alertList valueForKey:@"IsInspection"]
            //                    objectAtIndex:indexPath.row]description]
            //                    isEqualToString:@"1"]) {display_Name =@"Inspection
            //                    'Specialty Type'";} else{
            //                        display_Name = @"";
            //                    }
            //
            //
            //                    Id = @"";
            //                    datatype = @"checkbox";
            //                    IsRequired = @"false";
            //                    break; /* optional */
            //                case 6:
            //                    display_Name = @"";
            //                    display_Value = @"If Test Type is \"Sample Pickup\",
            //                    choose specific sample "
            //                    @"type below to create corresponding Lab work
            //                    orders."; Id = @""; datatype = @"category";
            //                    IsRequired = @"false";
            //                    break; /* optional */
            //                case 7:
            //                    //SampleNames
            //                    //SampleIds
            //                    display_Name = @"Sample Type";
            //                    display_Value = [[alertList
            //                    valueForKey:@"SampleNames"]
            //                    objectAtIndex:indexPath.row]; Id = [[[alertList
            //                    valueForKey:@"SampleIds"]
            //                    objectAtIndex:indexPath.row]description]; datatype =
            //                    @"multiselect_dropdown"; IsRequired = @"false";
            //                    break; /* optional */
            //                case 8:
            //                    display_Name = @"Assigned LabTech To";
            //                    display_Value = [[alertList
            //                    valueForKey:@"LabTechFullName"]
            //                    objectAtIndex:indexPath.row]; Id = [[alertList
            //                    valueForKey:@"LabTechName"]
            //                    objectAtIndex:indexPath.row]; datatype =
            //                    @"dropdown"; IsRequired = @"false"; break; /*
            //                    optional */
            //                case 9:
            //                    display_Name = @"";
            //                    display_Value = @"If Test Type is \"Soil Density\"
            //                    or Concrete, choose "
            //                    @"specific work order type (building / site)
            //                    below."; Id = @""; datatype = @"category";
            //                    IsRequired = @"false";
            //                    break; /* optional */
            //                case 10: /* Optional */
            //                    if([[[[alertList valueForKey:@"SiteBuilding"]
            //                    objectAtIndex:indexPath.row]description]
            //                    isEqualToString:@"2"])
            //                    {
            //                        display_Value = @"Site";
            //                    }
            //                    else{
            //                       display_Value = @"Building";
            //                    }
            //                    display_Name = @"Type (Building / Site)";
            //
            //                    Id = @"";
            //                    datatype = @"dropdown";
            //                    IsRequired = @"false";
            //                    break;
            //
            //                case 11: /* Optional */
            //                    //AssignFullName
            //                   // AssignName
            //                    display_Name = @"Assigned FieldTech To";
            //                    display_Value = [[alertList
            //                    valueForKey:@"AssignFullName"]
            //                    objectAtIndex:indexPath.row]; Id = [[alertList
            //                    valueForKey:@"AssignName"]
            //                    objectAtIndex:indexPath.row]; datatype =
            //                    @"dropdown"; IsRequired = @"true"; break;
            //                case 12: /* Optional */
            //
            //                    //DateCalledInText
            //                    display_Name = @"Date Called-in";
            //                    display_Value = [[alertList
            //                    valueForKey:@"DateCalledInText"]
            //                    objectAtIndex:indexPath.row]; Id = @""; datatype =
            //                    @"date"; IsRequired = @"true"; break;
            //                case 13: /* Optional */
            //                    display_Name = @"Work Order Status";
            //                    if([[[alertList valueForKey:@"Status"]
            //                    objectAtIndex:indexPath.row] isEqualToString:@"N"])
            //                    {
            //                    display_Value = @"Assigned";
            //                    }
            //                    else{
            //                       display_Value = @"Cancelled at office";
            //                    }
            //                    Id = @"";
            //                    IsRequired = @"true";
            //
            //                    datatype = @"dropdown";
            //
            //                    break;
            //                case 14: /* Optional */
            //
            //
            //                    display_Name = @"Job Date & Time";
            //
            //                    display_Value =datestr;
            //                    Id =[datandtime
            //                    stringByReplacingOccurrencesOfString:datestr
            //                                                              withString:@""];
            //                    //[[[[alertList
            //                    valueForKey:@"JobDateTimeDisplayUTCText"]
            //                    objectAtIndex:indexPath.row]description]
            //                    substringWithRange:NSMakeRange(11, 17)]
            //                    ;
            //                    datatype = @"dateandtime";
            //                    IsRequired = @"true";
            //                    break;
            //
            //                case 15: /* Optional */
            //
            //                   // Contractor
            //                    display_Name = @"Contractor";
            //                    display_Value = [[alertList
            //                    valueForKey:@"Contractor"]
            //                    objectAtIndex:indexPath.row]; Id = @""; datatype =
            //                    @"TextField"; IsRequired = @"false"; break;
            //                case 16: /* Optional */
            //                    display_Name = @"Site Contact";
            //                    display_Value =[[alertList valueForKey:@"Contact"]
            //                    objectAtIndex:indexPath.row]; Id = @""; datatype =
            //                    @"TextField"; IsRequired = @"false"; break;
            //                case 17: /* Optional */
            //                    display_Name = @"Contact Phone";
            //                    display_Value = [[alertList valueForKey:@"Phone"]
            //                    objectAtIndex:indexPath.row]; Id = @""; datatype =
            //                    @"TextField"; IsRequired = @"false"; break;
            //                case 18: /* Optional */
            //                    //ProjectInfo
            //                    display_Name = @"Task Description";
            //                    display_Value = [[alertList
            //                    valueForKey:@"ProjectInfo"]
            //                    objectAtIndex:indexPath.row]; Id = @""; datatype =
            //                    @"TextBox"; IsRequired = @"false"; break;
            //                case 19: /* Optional */
            //                    display_Name = @"Job Site Address";
            //                    //ProjectAddress
            //                    display_Value = [[alertList
            //                    valueForKey:@"ProjectAddress"]
            //                    objectAtIndex:indexPath.row]; Id = @""; datatype =
            //                    @"TextField"; IsRequired = @"false"; break;
            //                case 20: /* Optional */
            //                    display_Name = @"Driving Directions";
            //                    display_Value = [[alertList
            //                    valueForKey:@"Instructions"]
            //                    objectAtIndex:indexPath.row]; Id = @""; datatype =
            //                    @"TextBox"; IsRequired = @"false"; break;
            //                case 21:
            //                    //EstimatedDrivingTime
            //                    display_Name = @"Estimated Driving Time";
            //                    display_Value = [[alertList
            //                    valueForKey:@"EstimatedDrivingTime"]
            //                    objectAtIndex:indexPath.row]; Id = @""; datatype =
            //                    @"TextField"; IsRequired = @"false"; break;
            //                case 22:
            //                    display_Name = @"Ready & Notify Technician Now";
            //                    if([[[[alertList valueForKey:@"Ready"]
            //                    objectAtIndex:indexPath.row]description]
            //                    isEqualToString:@"Yes"])
            //                    {
            //                    display_Value = @"true";
            //                    }
            //                    else{
            //                        display_Value = @"false";
            //
            //                    }
            //                    Id = @"";
            //                    datatype = @"checkbox_ready";
            //                    IsRequired = @"false";
            //                    break; /* optional */
            //
            //                default: /* Optional */
            //                    //EstimatedDrivingTime
            //                    display_Name = @"Estimated Driving Time";
            //                    display_Value = [[alertList
            //                    valueForKey:@"EstimatedDrivingTime"]
            //                    objectAtIndex:indexPath.row]; Id = @""; datatype =
            //                    @"TextField"; IsRequired = @"false"; break;
            //            }
            if ([display_Value isKindOfClass:[NSNull class]] ||
                [display_Value isEqualToString:@"<null>"]) {
                
                display_Value = @"";
            }
            NSDictionary *AddWO_FormVC_dict;
            if ([display_Name isEqualToString:@"Client"]) {
                AddWO_FormVC_dict = [NSDictionary
                                     dictionaryWithObjectsAndKeys:display_Name, @"display_Name",
                                     display_Value, @"display_Value", Id,
                                     @"Id",
                                     
                                     CssClientId, @"CssClientId", datatype,
                                     @"DataType", IsRequired, @"IsRequired",
                                     nil];
            } else {
                AddWO_FormVC_dict = [NSDictionary
                                     dictionaryWithObjectsAndKeys:display_Name, @"display_Name",
                                     display_Value, @"display_Value", Id,
                                     @"Id", datatype, @"DataType",
                                     IsRequired, @"IsRequired", nil];
            }
            NSLog(@"AddWO_FormVC_dict%@", AddWO_FormVC_dict);
            [newWO_list addObject:AddWO_FormVC_dict];
        }
        isOnEditing = @"true";
        _workorderID =
        [[alertList valueForKey:@"WorkOrderId"] objectAtIndex:indexPath.row];
        _JobNumber =
        [[alertList valueForKey:@"JobNumber"] objectAtIndex:indexPath.row];
        if([[[[alertList valueForKey:@"Ready"]
              objectAtIndex:indexPath.row]description]
            isEqualToString:@"Yes"])
        {
            isready = @"true";
        }
        else{
            isready = @"false";
            
        }
        [self performSegueWithIdentifier:@"addWO_segue" sender:nil];
    }
}

//
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 57) {
        self.tabBarController.selectedIndex = 0;
    }
    if (alertView.tag == 58) {
    }
}

#pragma mark - API Delegate

- (void)callBackWithFailureResponse:(NSDictionary *)response
                             andKey:(NSString *)key {
    NSLog(@"CallBackFailure");
    //[[UIApplication sharedApplication] endIgnoringInteractionEvents];
    UIAlertView *callAlert =
    [[UIAlertView alloc] initWithTitle:@"Efielddata Message"
                               message:response
                              delegate:self
                     cancelButtonTitle:@"Ok"
                     otherButtonTitles:nil];
    callAlert.tag = 57;
    [callAlert show];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)callBackWithSuccessResponse:(NSDictionary *)response
                             andKey:(NSString *)key {
    if ([key isEqualToString:@"GetDashboard"]) {
        loadingmsg = @"No Data Found!";
        
        //  [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        WOTable.tableFooterView.hidden = YES;
        detailsDic = [[NSMutableDictionary alloc] initWithDictionary:response];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:response];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:details];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"data%@", response);
        
        [self loadAlertData];
    }
    
    else if ([key isEqualToString:@"logout"]) {
        if ([[response valueForKeyPath:@"Data.Response"]
             isEqualToString:@"Success"]) {
            [self logoutUser];
        } else {
            [self showAlertWithTitle:@"Efielddata Message"
                             message:@"Please try again later"];
        }
    }
}

- (void)showDatePicker:(UIDatePickerMode)modeDatePicker
            currentLbl:(UILabel *)lbl {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:modeDatePicker];
    [alertController.view addSubview:picker];
    
    UIAlertAction *doneAction = [UIAlertAction
                                 actionWithTitle:@"Done"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action) {
                                     if (modeDatePicker == UIDatePickerModeDate) {
                                         NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                         [formatter setDateFormat:@"MM/dd/yyyy"];
                                         datestring = [formatter stringFromDate:picker.date];
                                         _cloasebtn.hidden = false;
                                         self->jobdate.text = datestring;
                                         wono = @"";
                                         
                                         [self updateData];
                                         //                                         NSArray *temp;
                                         //                                         temp = [tempArr
                                         //                                         filteredArrayUsingPredicate:[NSPredicate
                                         //                                         predicateWithFormat:@"JobDateTimeText
                                         //                                         CONTAINS[c]
                                         //                                         %@",datestring]];
                                         //                                     alertList =
                                         //                                     (NSMutableArray*)temp;
                                         //                                     [WOTable reloadData];
                                     }
                                 }];
    [alertController addAction:doneAction];
    
    UIAlertAction *clearAction =
    [UIAlertAction actionWithTitle:@"Clear"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction *action) {
                               self->jobdate.text = @"";
                           }];
    
    UIAlertAction *cancelAction =
    [UIAlertAction actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction *action) {
                               NSLog(@"Cancel action");
                           }];
    
    [alertController addAction:cancelAction];
    
    [alertController addAction:clearAction];
    
    UIPopoverPresentationController *popoverController =
    alertController.popoverPresentationController;
    
    [popoverController setPermittedArrowDirections:0];
    popoverController.sourceView = self.view;
    popoverController.sourceRect =
    CGRectMake(self.view.bounds.size.width / 2.0,
               self.view.bounds.size.height / 2.0, 1.0, 1.0);
    [self presentViewController:alertController animated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    self.cloasebtn.hidden = true;
    [textField resignFirstResponder];
    
    // wono=@"";
    // datestring=@"";
    [self updateData];
    return YES;
}

- (void)updateData1 {
    datestring = @"";
    if ([self->jobdate.text length] == 0) {
        self.cloasebtn.hidden = true;
        wono = @"";
        datestring = @"";
        [self updateData];
    } else {
        self.cloasebtn.hidden = false;
        wono = self->jobdate.text;
        datestring = self->jobdate.text;
        
        if ([wono isEqualToString:@"00"] || [wono isEqualToString:@"0"]) {
            
        } else {
            [self updateData];
        }
    }
}

- (IBAction)add_WOaction:(id)sender {
    isOnEditing = @"false";
    [self performSegueWithIdentifier:@"addWO_segue" sender:self];
    
    // addWO_segue
}

- (IBAction)alertaction:(id)sender {
    [self performSegueWithIdentifier:@"alertsegue" sender:self];
}

- (void)enable_closebtn {
    if ([jobdate.text length] == 0) {
        self.cloasebtn.hidden = true;
    } else {
        self.cloasebtn.hidden = false;
    }
}

- (IBAction)cancelAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)updateDatarefresh {
    _cloasebtn.hidden = false;
    
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message"
                          message:
         @"No network, please check your internet connection"];
        return;
    } else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        wono = @"";
        
        self->jobdate.text = @"";
        
        NSDictionary *json = @{
                               
                               @"UserName" :
                                   [[NSUserDefaults standardUserDefaults] objectForKey:username],
                               
                               @"RoleName" :
                                   [[NSUserDefaults standardUserDefaults] objectForKey:userRole],
                               @"ReportStatus" : @"NW",
                               @"Wono" : @"",
                               @"FromJobDateTime" : @"",
                               @"IsNotReadyWorkOrder" : @1,
                               @"ProjectId" : @"0"
                               
                               };
        NSMutableDictionary *dictEntry = [[NSMutableDictionary alloc] init];
        [dictEntry setObject:json
                      forKey:[NSString stringWithFormat:@"FilterWorkOrder"]];
        
        DownloadManager *downloadObj = [[DownloadManager alloc] init];
        //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        [downloadObj
         callServerWithURL:[NSString
                            stringWithFormat:@"%@GetDashboard",
                            [[NSUserDefaults
                              standardUserDefaults]
                             objectForKey:SERVERURL]]
         andParameter:dictEntry
         andMethod:@"POST"
         andDelegate:self
         andKey:@"GetDashboard"];
    }
    [refreshControl performSelector:@selector(endRefreshing)
                         withObject:nil
                         afterDelay:1.0f];
    [refreshControl endRefreshing];
}
- (void)updateData {
    
    if (![self isNetworkAvailable]) {
        [self showAlertno_network:@"Efielddata Message"
                          message:
         @"No network, please check your internet connection"];
        return;
    } else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
        
        NSDictionary *json = @{
                               
                               @"UserName" :
                                   [[NSUserDefaults standardUserDefaults] objectForKey:username],
                               
                               @"RoleName" :
                                   [[NSUserDefaults standardUserDefaults] objectForKey:userRole],
                               @"ReportStatus" : @"NW",
                               @"ProjectId" : @"0",
                               @"Wono" : wono,
                               @"FromJobDateTime" : datestring,
                               
                               @"IsNotReadyWorkOrder" : @1
                               
                               };
        NSMutableDictionary *dictEntry = [[NSMutableDictionary alloc] init];
        [dictEntry setObject:json
                      forKey:[NSString stringWithFormat:@"FilterWorkOrder"]];
        
        DownloadManager *downloadObj = [[DownloadManager alloc] init];
        //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        [downloadObj
         callServerWithURL:[NSString
                            stringWithFormat:@"%@GetDashboard",
                            [[NSUserDefaults
                              standardUserDefaults]
                             objectForKey:SERVERURL]]
         andParameter:dictEntry
         andMethod:@"POST"
         andDelegate:self
         andKey:@"GetDashboard"];
    }
}
- (IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {
}
- (IBAction)logoutAction:(id)sender {
    //    if (![self isNetworkAvailable]) {
    //        [self showAlertno_network:@"Efielddata Message" message:@"No
    //        network, please check your internet connection"]; return;
    //    }else{
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    });
    //    DownloadManager *downloadObj = [[DownloadManager alloc]init];
    //    //[downloadObj callServerWithURLAuthentication:[NSString
    //    stringWithFormat:@"%@IOSResetUserAppId?UserName=%@",[[NSUserDefaults
    //    standardUserDefaults] objectForKey:SERVERURL],[[NSUserDefaults
    //    standardUserDefaults] objectForKey:username]] andParameter:nil
    //    andMethod:@"POST" andDelegate:self andKey:@"logout"];
    //        NSDictionary *json = @{
    //
    //                               @"UserName" : [[NSUserDefaults
    //                               standardUserDefaults] objectForKey:username]
    //
    //                               };
    //        NSMutableDictionary *dictEntry =[[NSMutableDictionary alloc] init];
    //        [dictEntry setObject:json forKey:[NSString
    //        stringWithFormat:@"UserModel"]];
    //
    //        [downloadObj callServerWithURL:[NSString
    //        stringWithFormat:@"%@IOSResetUserAppId",[[NSUserDefaults
    //        standardUserDefaults] objectForKey:SERVERURL]]
    //        andParameter:dictEntry andMethod:@"POST" andDelegate:self
    //        andKey:@"logout"];
    
    [self logoutUser];
    
    // }
}

- (IBAction)changepasswordAction:(id)sender {
    [self changePassword];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSString *name;
    NSString *room;
    [[NSUserDefaults standardUserDefaults] setObject:@"true"
                                              forKey:@"pendingnext"];
    
    if ([segue.identifier isEqualToString:@"alertsegue"]) {
        
        AlertListVC *vc = [segue destinationViewController];
        
    } else
        
        if ([segue.identifier isEqualToString:@"addWO_segue"]) {
            // dynamicform
            AddWO_FormVC *vc = [segue destinationViewController];
            vc.Isedit = isOnEditing;
            vc.WorkOrderId = _workorderID;
            vc.JobNumber = _JobNumber;
            vc.isreadystr=isready;
      
            if ([isOnEditing isEqualToString:@"true"]) {
                vc.arraylist = newWO_list;
            }
        }
}

- (IBAction)jobdateaction:(id)sender {
    [self.view endEditing:YES];
    [self showDatePicker:UIDatePickerModeDate currentLbl:self->jobdate];
}

- (IBAction)close:(id)sender {
    self->jobdate.text = @"";
    wono = @"";
    datestring = @"";
    self.cloasebtn.hidden = true;
    [self updateData];
}
@end
