//
//  multiselectViewController.m
//  Efield
//
//  Created by MyMac1 on 2/7/17.
//  Copyright © 2017 iPhone. All rights reserved.
//

#import "multiselectViewController.h"

#import "multiselectTableViewCell.h"

#import "AddWO_FormVC.h"
@interface multiselectViewController () <AddWO_FormVCDelegate> {
    NSString *selectedvalue, *selectedvalueid;
}

@end

@implementation multiselectViewController {
    
    NSMutableArray *arSelectedRows;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([_selected_index_multi isEqualToString:@"7"]||[_selected_index_multi isEqualToString:@"12"])
    {
        CGRect searchFrame = self.searchBar.frame;
        
        searchFrame.size.height = 0;
        
        self.searchBar.frame = searchFrame;
        self.searchBar.hidden = YES;
        
    }
    else{
    self.searchBar.delegate = (id)self;
    CGRect searchFrame = self.searchBar.frame;
    searchFrame.size.height = 44;
    self.searchBar.frame = searchFrame;
    self.searchBar.hidden = NO;
    }
    selectedvalue = @"";
    arSelectedRows = [[NSMutableArray alloc] init];
    
    arrayListTemp = self.arraylist;
    _headerlbl.text=_header_txt;
    
    [self.savebtn setTitleColor:[UIColor colorWithRed:85 / 255.0f
                                                green:85 / 255.0f
                                                 blue:85 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    
    self.savebtn.enabled = NO;
    
    selectedvalue = @"";
    // Do any additional setup after loading the view.
    if ([_selected_index_multi isEqualToString:@"8"]) {
        self.tableView.allowsMultipleSelection = YES;
    }
    if ([_selected_index_multi isEqualToString:@"7"]) {
        
      _arraylist= [[NSMutableArray alloc] init];
        
       
        NSDictionary *AddWO_FormVC_dict = [NSDictionary
                                           dictionaryWithObjectsAndKeys:
                                           @"Building",
                                           @"display_Value"
                                           ,nil];
        
        NSLog(@"AddWO_FormVC_dict%@",AddWO_FormVC_dict);
        [_arraylist addObject:AddWO_FormVC_dict];
        AddWO_FormVC_dict = [NSDictionary
                             dictionaryWithObjectsAndKeys:
                             @"Site",
                             @"display_Value"
                             ,nil];
        
        NSLog(@"AddWO_FormVC_dict%@",AddWO_FormVC_dict);
        [_arraylist addObject:AddWO_FormVC_dict];

    }
    else     if ([_selected_index_multi isEqualToString:@"12"]) {
        
        _arraylist= [[NSMutableArray alloc] init];
        
        
        NSDictionary *AddWO_FormVC_dict = [NSDictionary
                                           dictionaryWithObjectsAndKeys:
                                           @"Assigned",
                                           @"display_Value"
                                           ,nil];
        
        NSLog(@"AddWO_FormVC_dict%@",AddWO_FormVC_dict);
        [_arraylist addObject:AddWO_FormVC_dict];
        AddWO_FormVC_dict = [NSDictionary
                             dictionaryWithObjectsAndKeys:
                             @"Cancelled at office",
                             @"display_Value"
                             ,nil];
        
        NSLog(@"AddWO_FormVC_dict%@",AddWO_FormVC_dict);
        [_arraylist addObject:AddWO_FormVC_dict];
    }
}

//- (void)viewWillAppear:(BOOL)animated {
//    if([_selected_index_multi isEqualToString:@"1"])
//    {
//        for(int i=0;i<[self.arraylist count];i++)
//        {
//
//            if([[ [self.New_WOList valueForKey:@"Id"] objectAtIndex:1]
//            isEqualToString:[ [self.arraylist valueForKey:@"ClientId"]
//            objectAtIndex:i]])
//            {
//                NSIndexPath* selectedCellIndexPath= [NSIndexPath
//                indexPathForRow:i inSection:0]; [self tableView:self.tableView
//                didSelectRowAtIndexPath:selectedCellIndexPath];
//                [self.tableView selectRowAtIndexPath:selectedCellIndexPath
//                animated:YES scrollPosition:UITableViewScrollPositionNone];
//            }
//
//
//        }
//    }
//    if([_selected_index_multi isEqualToString:@"2"])
//    {
//        for(int i=0;i<[self.arraylist count];i++)
//        {
//
//            if([[ [self.New_WOList valueForKey:@"display_Value"]
//            objectAtIndex:2] isEqualToString:[ [self.arraylist
//            valueForKey:@"ProjectId"] objectAtIndex:i]])
//            {
//                NSIndexPath* selectedCellIndexPath= [NSIndexPath
//                indexPathForRow:i inSection:0]; [self tableView:self.tableView
//                didSelectRowAtIndexPath:selectedCellIndexPath];
//                [self.tableView selectRowAtIndexPath:selectedCellIndexPath
//                animated:YES scrollPosition:UITableViewScrollPositionNone];
//            }
//
//
//        }
//    }
//    if([_selected_index_multi isEqualToString:@"4"])
//    {
//        for(int i=0;i<[self.arraylist count];i++)
//        {
//
//            if([[ [self.New_WOList valueForKey:@"Id"] objectAtIndex:4]
//            isEqualToString:[ [self.arraylist
//            valueForKey:@"ProjectTestTypeId"] objectAtIndex:i]])
//            {
//                NSIndexPath* selectedCellIndexPath= [NSIndexPath
//                indexPathForRow:i inSection:0]; [self tableView:self.tableView
//                didSelectRowAtIndexPath:selectedCellIndexPath];
//                [self.tableView selectRowAtIndexPath:selectedCellIndexPath
//                animated:YES scrollPosition:UITableViewScrollPositionNone];
//            }
//
//
//        }
//    }
//
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBar:(UISearchBar *)searchBr textDidChange:(NSString *)searchText {
    if (searchText.length) {
        [arSelectedRows removeAllObjects];
        NSArray *temp;
        if ([self.selected_index_multi isEqualToString:@"1"]) {
            
    
            [arSelectedRows removeAllObjects];
            NSArray *temp = [arrayListTemp
                             filteredArrayUsingPredicate:
                             [NSPredicate
                              predicateWithFormat:@"ClientName CONTAINS[c] %@", searchText]];
            self.arraylist = (NSMutableArray *)temp;
            //     arrayListTemp = (NSMutableArray*)temp;
            [self.tableView reloadData];
        }
       else if ([self.selected_index_multi isEqualToString:@"2"]) {
            
            
            [arSelectedRows removeAllObjects];
            NSArray *temp = [arrayListTemp
                             filteredArrayUsingPredicate:
                             [NSPredicate
                              predicateWithFormat:@"ProjectFullName CONTAINS[c] %@", searchText]];
            self.arraylist = (NSMutableArray *)temp;
            //     arrayListTemp = (NSMutableArray*)temp;
            [self.tableView reloadData];
        }
       else if ([self.selected_index_multi isEqualToString:@"5"]) {
           
           
           [arSelectedRows removeAllObjects];
           NSArray *temp = [arrayListTemp
                            filteredArrayUsingPredicate:
                            [NSPredicate
                             predicateWithFormat:@"TaskName CONTAINS[c] %@", searchText]];
           self.arraylist = (NSMutableArray *)temp;
           //     arrayListTemp = (NSMutableArray*)temp;
           [self.tableView reloadData];
       }
       else if ([self.selected_index_multi isEqualToString:@"8"]) {
           
           
           [arSelectedRows removeAllObjects];
           NSArray *temp = [arrayListTemp
                            filteredArrayUsingPredicate:
                            [NSPredicate
                             predicateWithFormat:@"SampleName CONTAINS[c] %@", searchText]];
           self.arraylist = (NSMutableArray *)temp;
           //     arrayListTemp = (NSMutableArray*)temp;
           [self.tableView reloadData];
       }
       else if ([self.selected_index_multi isEqualToString:@"7"]||
                [self.selected_index_multi isEqualToString:@"12"]
                ||[self.selected_index_multi isEqualToString:@"15"]) {
           
           
           [arSelectedRows removeAllObjects];
           NSArray *temp = [arrayListTemp
                            filteredArrayUsingPredicate:
                            [NSPredicate
                             predicateWithFormat:@"display_Value CONTAINS[c] %@", searchText]];
           self.arraylist = (NSMutableArray *)temp;
           //     arrayListTemp = (NSMutableArray*)temp;
           [self.tableView reloadData];
       }
       else if ([self.selected_index_multi isEqualToString:@"9"]) {
           
           
           [arSelectedRows removeAllObjects];
           NSArray *temp = [arrayListTemp
                            filteredArrayUsingPredicate:
                            [NSPredicate
                             predicateWithFormat:@"FullNameDroupDown CONTAINS[c] %@", searchText]];
           self.arraylist = (NSMutableArray *)temp;
           //     arrayListTemp = (NSMutableArray*)temp;
           [self.tableView reloadData];
       }
       else if ([self.selected_index_multi isEqualToString:@"10"]) {
           
           
           [arSelectedRows removeAllObjects];
           NSArray *temp = [arrayListTemp
                            filteredArrayUsingPredicate:
                            [NSPredicate
                             predicateWithFormat:@"FullNameDroupDown CONTAINS[c] %@", searchText]];
           self.arraylist = (NSMutableArray *)temp;
           //     arrayListTemp = (NSMutableArray*)temp;
           [self.tableView reloadData];
       }
    }
    
    
    else {
        [arSelectedRows removeAllObjects];
        self.arraylist = arrayListTemp;
        [self.tableView reloadData];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBr {
    [searchBr setShowsCancelButton:YES animated:YES];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBr {
    [arSelectedRows removeAllObjects];
    [searchBr setShowsCancelButton:NO animated:YES];
    searchBr.text = @"";
    self.arraylist = arrayListTemp;
    [self.tableView reloadData];
    [searchBr resignFirstResponder];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [self.arraylist count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:@"multislectcell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:@"multislectcell"];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //    if([self.tableView.indexPathsForSelectedRows containsObject:indexPath]){
    //        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    //        [arSelectedRows addObject:indexPath];
    //
    //    }else{
    //        cell.accessoryType = UITableViewCellAccessoryNone;
    //   }
    if (![_selected_index_multi isEqualToString:@"8"]) {
        
        if ([self.tableView.indexPathsForSelectedRows containsObject:indexPath]) {
            
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [arSelectedRows addObject:indexPath];
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
    }
    if ([_selected_index_multi isEqualToString:@"8"]) {
        if ([[[[self.New_WOList valueForKey:@"display_Value"] objectAtIndex:8] description]
             containsString:[[self.arraylist valueForKey:@"SampleName"]
                              objectAtIndex:indexPath.row]]) {
                 cell.accessoryType = UITableViewCellAccessoryCheckmark;
                 [arSelectedRows addObject:indexPath];
                 
             } else {
                 cell.accessoryType = UITableViewCellAccessoryNone;
             }
        cell.textLabel.text = [[self.arraylist valueForKey:@"SampleName"]
                               objectAtIndex:indexPath.row];
    }
    if ([_selected_index_multi isEqualToString:@"1"]) {
        if ([[[[self.New_WOList valueForKey:@"Id"] objectAtIndex:1] description]
             isEqualToString:[[[self.arraylist valueForKey:@"ClientId"]
                              objectAtIndex:indexPath.row]description]]) {
                 cell.accessoryType = UITableViewCellAccessoryCheckmark;
                 [arSelectedRows addObject:indexPath];
                 
             } else {
                 cell.accessoryType = UITableViewCellAccessoryNone;
             }
        cell.textLabel.text = [[self.arraylist valueForKey:@"ClientName"]
                               objectAtIndex:indexPath.row];
    }
    
    if ([_selected_index_multi isEqualToString:@"2"]) {
        if ([[[[self.New_WOList valueForKey:@"Id"] objectAtIndex:2] description]
             isEqualToString:[[[self.arraylist valueForKey:@"ProjectId"]
                              objectAtIndex:indexPath.row]description]]) {
                 cell.accessoryType = UITableViewCellAccessoryCheckmark;
                 [arSelectedRows addObject:indexPath];
                 
             } else {
                 cell.accessoryType = UITableViewCellAccessoryNone;
             }
        cell.textLabel.text = [[self.arraylist valueForKey:@"ProjectFullName"]
                               objectAtIndex:indexPath.row];
    }
    if ([_selected_index_multi isEqualToString:@"5"]) {
        if ([[[[self.New_WOList valueForKey:@"Id"] objectAtIndex:5] description]
             isEqualToString:[[[self.arraylist valueForKey:@"ProjectTestTypeId"]
                              objectAtIndex:indexPath.row]description]]) {
                 cell.accessoryType = UITableViewCellAccessoryCheckmark;
                 [arSelectedRows addObject:indexPath];
                 
             } else {
                 cell.accessoryType = UITableViewCellAccessoryNone;
             }
        cell.textLabel.text =
        [[self.arraylist valueForKey:@"TaskName"] objectAtIndex:indexPath.row];
    }
    if ([_selected_index_multi isEqualToString:@"9"]) {
        if ([[[[self.New_WOList valueForKey:@"display_Value"] objectAtIndex:9] description]
             isEqualToString:[[[self.arraylist valueForKey:@"FullNameDroupDown"]
                              objectAtIndex:indexPath.row]description]]) {
                 cell.accessoryType = UITableViewCellAccessoryCheckmark;
                 [arSelectedRows addObject:indexPath];
                 
             } else {
                 cell.accessoryType = UITableViewCellAccessoryNone;
             }
        cell.textLabel.text =
        [[self.arraylist valueForKey:@"FullNameDroupDown"] objectAtIndex:indexPath.row];
    }
    if ([_selected_index_multi isEqualToString:@"10"]) {
        if ([[[[self.New_WOList valueForKey:@"display_Value"] objectAtIndex:10] description]
             isEqualToString:[[[self.arraylist valueForKey:@"FullNameDroupDown"]
                              objectAtIndex:indexPath.row]description]]) {
                 cell.accessoryType = UITableViewCellAccessoryCheckmark;
                 [arSelectedRows addObject:indexPath];
                 
             } else {
                 cell.accessoryType = UITableViewCellAccessoryNone;
             }
        cell.textLabel.text =
        [[self.arraylist valueForKey:@"FullNameDroupDown"] objectAtIndex:indexPath.row];
    }
//    if ([_selected_index_multi isEqualToString:@"12"]) {
//        if ([[[[self.New_WOList valueForKey:@"display_Value"] objectAtIndex:12] description]
//             isEqualToString:[[[self.arraylist valueForKey:@"display_Value"]
//                              objectAtIndex:indexPath.row]description]]) {
//                 cell.accessoryType = UITableViewCellAccessoryCheckmark;
//                 [arSelectedRows addObject:indexPath];
//
//             } else {
//                 cell.accessoryType = UITableViewCellAccessoryNone;
//             }
//        cell.textLabel.text =
//        [[self.arraylist valueForKey:@"display_Value"] objectAtIndex:indexPath.row];
//    }
    if ([_selected_index_multi isEqualToString:@"7"]) {
        if ([[[[self.New_WOList valueForKey:@"display_Value"] objectAtIndex:7] description]
             isEqualToString:[[[self.arraylist valueForKey:@"display_Value"]
                               objectAtIndex:indexPath.row]description]]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [arSelectedRows addObject:indexPath];
            
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.textLabel.text =
        [[self.arraylist valueForKey:@"display_Value"] objectAtIndex:indexPath.row];
    }
    if ([_selected_index_multi isEqualToString:@"12"]) {
        if ([[[[self.New_WOList valueForKey:@"display_Value"] objectAtIndex:12] description]
             isEqualToString:[[[self.arraylist valueForKey:@"display_Value"]
                               objectAtIndex:indexPath.row]description]]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [arSelectedRows addObject:indexPath];
            
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.textLabel.text =
        [[self.arraylist valueForKey:@"display_Value"] objectAtIndex:indexPath.row];
    }
    //  cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //  cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.numberOfLines=0;
    
    cell.textLabel.font = [UIFont systemFontOfSize:14.0];
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.savebtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    
    self.savebtn.enabled = YES;
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    // if([_selected_index_multi isEqualToString:@"0"]||[_selected_index_multi
    // isEqualToString:@"1"]){
    if (![_selected_index_multi isEqualToString:@"8"]) {
        if (arSelectedRows.count) {
            
            //  for (int i=0; i< [arSelectedRows count]; i++) {
            
            UITableViewCell *cell =
            [tableView cellForRowAtIndexPath:arSelectedRows[0]];
            cell.accessoryType = UITableViewCellAccessoryNone;
            [arSelectedRows removeObject:arSelectedRows[0]];
            // [arSelectedRows removeAllObjects];
            
            // }
        }
    }
    
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        //  if(arSelectedRows.count) {
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        [arSelectedRows removeObject:indexPath];
        //  [arSelectedRows removeAllObjects];
        
    } else {
        [arSelectedRows addObject:indexPath];
        // [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
        //   [self.tableView selectRowAtIndexPath:indexPath animated:YES
        //   scrollPosition:UITableViewScrollPositionNone];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    NSArray *selectedRows = [tableView indexPathsForSelectedRows];
    for (NSIndexPath *i in selectedRows) {
        if (![i isEqual:indexPath]) {
            [tableView deselectRowAtIndexPath:i animated:NO];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([_selected_index_multi isEqualToString:@"0"] ||
        [_selected_index_multi isEqualToString:@"1"]) {
        if ([arSelectedRows count] > 0) {
            NSInteger theRow = arSelectedRows[0];
            NSIndexPath *theIndexPath = [NSIndexPath indexPathForRow:theRow
                                                           inSection:0];
            [self.tableView selectRowAtIndexPath:theIndexPath
                                        animated:NO
                                  scrollPosition:UITableViewScrollPositionNone];
        }
    }
}

- (void)tableView:(UITableView *)tableView
didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.savebtn setTitleColor:[UIColor colorWithRed:20 / 255.0f
                                                green:110 / 255.0f
                                                 blue:255 / 255.0f
                                                alpha:1.0]
                       forState:UIControlStateNormal];
    self.savebtn.enabled = YES;
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        // [arSelectedRows removeAllObjects];
        [arSelectedRows removeObject:indexPath];
    }
}

- (IBAction)cancel:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
    //    bundle:nil]; location_dischargeViewController *viewController =
    //    [storyboard instantiateViewControllerWithIdentifier:@"loc_discharge"];
    //
    //    viewController.patientId = self.patientId;
    //    viewController.providesPresentationContextTransitionStyle = YES;
    //    viewController.definesPresentationContext = YES;
    //
    //    [viewController
    //    setModalPresentationStyle:UIModalPresentationOverCurrentContext]; [self
    //    presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)save:(id)sender {
    
    NSMutableArray *selections = [[NSMutableArray alloc] init];
    if([arSelectedRows count]>0)
    {
    for (NSIndexPath *indexPath in arSelectedRows) {
      //  [selections addObject:[[self.arraylist valueForKey:@"CODEDISP"]
      //                         objectAtIndex:indexPath.row]];
        
        if ([_selected_index_multi isEqualToString:@"1"]) {
            selectedvalue = [[self.arraylist valueForKey:@"ClientName"]
                             objectAtIndex:indexPath.row];
            selectedvalueid = [[[self.arraylist valueForKey:@"ClientId"]
                                objectAtIndex:indexPath.row]description];
            NSString *CSS_ClientId = [[[self.arraylist valueForKey:@"CssClientId"]
                                       objectAtIndex:indexPath.row]description];
            
            //  [_delegate NewWOinfo_detailsinfo:selectedvalue  ID:selectedvalueid
            //  CssClientId:CSS_ClientId  index_number:_selected_index];
            [_delegate NewWOinfo_detailsinfo:selectedvalue
                                          ID:selectedvalueid
                                 CSSClientId:CSS_ClientId
                                index_number:_selected_index];
            
        }
        
        else if ([_selected_index_multi isEqualToString:@"2"]) {
            selectedvalue = [[self.arraylist valueForKey:@"ProjectFullName"]
                             objectAtIndex:indexPath.row];
            selectedvalueid = [[[self.arraylist valueForKey:@"ProjectId"]
                                objectAtIndex:indexPath.row]description];
            NSString *CSS_ClientId = @"";
            [_delegate NewWOinfo_detailsinfo:selectedvalue
                                          ID:selectedvalueid
                                 CSSClientId:CSS_ClientId
                            index_number:_selected_index];
            
        }
        
        else if ([_selected_index_multi isEqualToString:@"5"]) {
            selectedvalue = [[self.arraylist valueForKey:@"TaskName"]
                             objectAtIndex:indexPath.row];
            selectedvalueid = [[[self.arraylist valueForKey:@"ProjectTestTypeId"]
                                objectAtIndex:indexPath.row]description];
            NSString *CSS_ClientId = @"";
            [_delegate NewWOinfo_detailsinfo:selectedvalue
                                          ID:selectedvalueid
                                 CSSClientId:CSS_ClientId
                                index_number:_selected_index];
            
        }
        
        else if ([_selected_index_multi isEqualToString:@"9"]) {
            selectedvalue = [[self.arraylist valueForKey:@"FullNameDroupDown"]
                             objectAtIndex:indexPath.row];
            selectedvalueid = [[self.arraylist valueForKey:@"UserName"]
                               objectAtIndex:indexPath.row];
         
            NSString *CSS_ClientId = @"";
            [_delegate NewWOinfo_detailsinfo:selectedvalue
                                          ID:selectedvalueid
                                 CSSClientId:CSS_ClientId
                                index_number:_selected_index];
            
        }
        
        else if ([_selected_index_multi isEqualToString:@"10"]) {
            selectedvalue = [[self.arraylist valueForKey:@"FullNameDroupDown"]
                             objectAtIndex:indexPath.row];
            selectedvalueid = [[self.arraylist valueForKey:@"UserName"]
                               objectAtIndex:indexPath.row];
            NSString *CSS_ClientId = @"";
            [_delegate NewWOinfo_detailsinfo:selectedvalue
                                          ID:selectedvalueid
                                 CSSClientId:CSS_ClientId
                                index_number:_selected_index];
            
        }
        else if ([_selected_index_multi isEqualToString:@"7"]||[_selected_index_multi isEqualToString:@"12"] ) {
            
            selectedvalue = [[self.arraylist valueForKey:@"display_Value"]
                             objectAtIndex:indexPath.row];
            selectedvalueid = @"";
            NSString *CSS_ClientId = @"";
            [_delegate NewWOinfo_detailsinfo:selectedvalue
                                          ID:selectedvalueid
                                 CSSClientId:CSS_ClientId
                                index_number:_selected_index];
            
        }
        
//        else if ( [_selected_index_multi isEqualToString:@"16"]) {
//
//            selectedvalue = [self.arraylist objectAtIndex:indexPath.row];
//            selectedvalueid = @"";
//            NSString *CSS_ClientId = @"";
//            [_delegate NewWOinfo_detailsinfo:selectedvalue
//                                          ID:selectedvalueid
//                                 CSSClientId:CSS_ClientId
//                                index_number:_selected_index];
//
//        }
//
        
        else if ([_selected_index_multi isEqualToString:@"8"])
            
        {
            if ([selectedvalue isEqualToString:@""]) {
                
                selectedvalue = [[self.arraylist valueForKey:@"SampleName"]
                                 objectAtIndex:indexPath.row];
                selectedvalueid = [[[self.arraylist valueForKey:@"SampleId"]
                                    objectAtIndex:indexPath.row]description];
                
            } else {
                selectedvalue = [selectedvalue
                                 stringByAppendingFormat:@", %@",
                                 [[self.arraylist valueForKey:@"SampleName"]
                                  objectAtIndex:indexPath.row]];
                selectedvalueid =[selectedvalueid
                                  stringByAppendingFormat:@",%@",
                                  [[self.arraylist valueForKey:@"SampleId"]
                                   objectAtIndex:indexPath.row]];
              
            }
            [_delegate NewWOinfo_detailsinfo:selectedvalue
                                          ID:selectedvalueid
                                 CSSClientId:@""
                                index_number:_selected_index];
        }
    }
}
    else{
        selectedvalue = @"";
        selectedvalueid = @"";
        NSString *CSS_ClientId = @"";
        [_delegate NewWOinfo_detailsinfo:selectedvalue
                                      ID:selectedvalueid
                             CSSClientId:CSS_ClientId
                            index_number:_selected_index];
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
